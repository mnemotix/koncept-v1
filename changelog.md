v1.1.0
-------------

- Fixed bugs
- Add search and drafts view.

v1.0.2
-------------

- Fixed bugs
- Adding submition process.

v1.0.1
-------------

- Adding CORS


v1.0.0
-------------

- Koncept is fully compliant with DSE.

v1.0.0-beta.1
-------------

- Koncept is now fully SKOS compatible (big model refactoring)
- Changed global graph visualization from Vis.js to d3.js


v0.5.0
------

- Add cascading deletion.

- Fixed remaining bugs related to model refactoring and dependencies updates.
- Fixed [WVRS-57](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-57)

v0.4.0
------

- Big model refactoring
- Add colors to schemes
- Updated dependencies.
- Removed FEGL thesaurus loading at startup.

v0.3.2
------

- Fixed typos
- Updated dependencies and synaptix version.

v0.3.1
------

- Fixed [WVRS-63](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-63)
- Fixed websocket broken connection.

v0.3.0
------

- Updated dependencies.

v0.2
----

- Added rules on concept to match automatic patterns.

v0.1.1
------

- Stable release.

v0.1.1-beta.4
-------------

- Added default matcher on every new label, and update it on label update.

- Fixed [WVRS-49](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-49)

v0.1.1-beta.3
-------------

- Fixed [WVRS-44](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-44)
- Fixed [WVRS-48](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-48)

v0.1.1-beta.2
-------------

- Removed Alpha warnings. 

v0.1.1-beta.0
-------------

- Implemented keyboard navigation [WVRS-9](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-9) 

- Fixed [WVRS-39](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-39)
- Fixed [WVRS-42](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-42)

v0.1.1-alpha.3
--------------

- Detection for oauth disconnection.
- Implemented [VWRS-21](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-21)

- Fixed [VWRS-37](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-37)
- Fixed [VWRS-38](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-38)

v0.1.1-alpha.2
--------------

- Implemented [VWRS-5](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-5) : Show version in home footer.
- Add a changelog page.

- Fixed [VWRS-6](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-6)
- Fixed [VWRS-7](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-7)
- Fixed [VWRS-8](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-8)
- Fixed [VWRS-11](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-11)
- Fixed [WVRS-14](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-14)
- Fixed [WVRS-14](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-15)
- Fixed [VWRS-16](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-16)
- Fixed [VWRS-22](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-22)
- Fixed [VWRS-26](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-26)
- Fixed [VWRS-27](https://mnemotix.atlassian.net/projects/WVRS/queues/6/WVRS-27)

v0.1.1-alpha.1
--------------

- Deleted useless links

v0.1.1-alpha.0
--------------

First released alpha version.
