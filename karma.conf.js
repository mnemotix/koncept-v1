/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */
var webpack = require('webpack');

var webpackConfig = require('./webpack.test.config');

webpackConfig.devtool = 'inline-source-map';
webpackConfig.plugins = [new webpack.HotModuleReplacementPlugin()];

var preprocessors = {};

preprocessors[require.resolve( './karma.tests.client.js')] =  [ 'webpack', 'sourcemap' ];

module.exports = function (config) {
  config.set({
    browsers: process.env.NODE_ENV == "test" ? [ 'PhantomJS' ] : ['Chrome'], //run in Chrome in dev env or in PhantomJS in CI
    singleRun: false, //just run once by default
    frameworks: [ 'jasmine' ], //use the jasmine test framework
    files: [
      './node_modules/babel-polyfill/dist/polyfill.js',
      './karma.tests.client.js' //just load this file
    ].map(require.resolve),
    preprocessors: preprocessors,
    reporters:  process.env.NODE_ENV == 'test' ? ['bamboo'] : [ 'dots', 'kjhtml' ], //report results in this format
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true //please don't spam the console when running in karma!
    },
    plugins: [
      require('karma-jasmine-html-reporter'),
      require('karma-bamboo-reporter'),
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-phantomjs-launcher'),
      require('karma-webpack'),
      require('karma-sourcemap-loader')
    ],
    phantomjsLauncher: {
      // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
      exitOnResourceError: true
    },
    browserDisconnectTimeout: 10000,
    browserDisconnectTolerance: 3

  });
};
