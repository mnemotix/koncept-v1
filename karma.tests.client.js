/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

var context = require.context('./client', true, /__tests__\/.*\.js$/); //make sure you have your directory and regex test set correctly!
context.keys().forEach(context);