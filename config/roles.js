let roles = {
  ROLE_CURATOR: {
    id: 'ROLE_CURATOR',
    default: true,
    name: "Curator",
    description: "This is the basic role. Members can be invited to participate in a project or an event.",
    mask: Math.pow(2, 0)
  },
  ROLE_LIBRARIAN: {
    id: 'ROLE_LIBRARIAN',
    name: "Librarian",
    description: "Members of this group can manage the thesaurus.",
    mask: Math.pow(2, 1),
    parents: ['ROLE_CURATOR']
  },
  ROLE_PROJECT_MANAGER: {
    id: 'ROLE_PROJECT_MANAGER',
    name: "Project manager",
    description: "Members of this group can create and manage their projects and invite people to participate.",
    mask: Math.pow(2, 2),
    parents: ['ROLE_CURATOR']
  },
  ROLE_RH: {
    id: 'ROLE_RH',
    name: "Human resources manager",
    description: "Members of this group have access to reports and analytics of the projects and people activities.",
    mask: Math.pow(2, 3),
    parents: ['ROLE_CURATOR']
  },
  ROLE_PUBLICATION: {
    id: 'ROLE_PUBLICATION',
    name: "Publication director",
    description: "Members of this group can create and manage publications, select content to share with public site.",
    mask:  Math.pow(2, 4),
    parents: ['ROLE_PROJECT_MANAGER']
  },
  ROLE_ADMIN: {
    id: 'ROLE_ADMIN',
    name: "Administrator",
    description: "Members of this group can do everything !",
    isAdmin: true,
    parents: ['ROLE_CURATOR', 'ROLE_LIBRARIAN', 'ROLE_PROJECT_MANAGER', 'ROLE_RH', 'ROLE_PUBLICATION']
  }
};

export default roles;

