import variables from './variables';
import chalk from 'chalk';


let initVariables = (isProduction) => {
  console.log(chalk.underline.bold("Environnement variables:\n"));

  Object.keys(variables).map(environmentVar => {
    if(!process.env[environmentVar]){
      let {defaultValue, description, defaultValueInProduction} = variables[environmentVar];

      if (defaultValue && (defaultValueInProduction || !isProduction)) {
        process.env[environmentVar] = defaultValue;
      } else {
        throw `Environment variable ${environmentVar} is not set. ${description}`;
      }
    }

    console.log(` ${chalk.yellow(environmentVar)}: ${process.env[environmentVar]}`);
  });

  console.log("\n");

};

export {
  variables,
  initVariables
}