/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 20/01/2017
 */

export default {
  UUID: {
    description: 'This is the application UUID.',
    defaultValue: 'KonceptGraphQLEndPoint-bc248ae2-93cd-424a-88f5-2b46d86dcd74',
    defaultValueInProduction: true
  },
  GRAPH_ORIGIN: {
    description: 'This is the application graph origin name.',
    defaultValue: 'mnx:app:weever',
    defaultValueInProduction: true
  },
  OAUTH_CLIENT_SECRET: {
    description: 'This is the OAUTH shared secret between this client app and OAuth2 server.',
    defaultValue: 'bc248ae2-93cd-424a-88f5-2b46d86dcd74'
  },
  OAUTH_REDIRECT_URL: {
    description: 'This is the OAUTH server base URL (https://keycloak.is.here)',
    defaultValue: 'http://localhost:8181'
  },
  OAUTH_TOKEN_VALIDATION_URL: {
    description: 'This is the OAUTH server token validation URL',
    defaultValue: 'http://localhost:8181'
  },
  OAUTH_REALM: {
    description: 'This is the OAUTH Realm name ',
    defaultValue: 'synaptix'
  },
  OAUTH_REALM_CLIENT: {
    description: 'This is the OAUTH Realm client ',
    defaultValue: 'owncloud'
  },
  APP_PORT: {
    description: 'This is listening port of the application.',
    defaultValue: 3001
  },
  APP_URL: {
    description: 'This is the base url of the application.',
    defaultValue: 'http://localhost:3001'
  },
  RABBITMQ_HOST : {
    description: 'This is RabbitMQ host.',
    defaultValue: 'localhost'
  },
  RABBITMQ_LOGIN : {
    description: 'This is RabbitMQ login.',
    defaultValue: 'guest'
  },
  RABBITMQ_PASSWORD : {
    description: 'This is RabbitMQ password.',
    defaultValue: 'guest'
  },
  RABBITMQ_EXCHANGE_NAME : {
    description: 'This is RabbitMQ exchange name.',
    defaultValue: 'synaptix-local'
  },
  CORS_ALLOWED_ORIGINS: {
    description: 'This is the allowed domains for cors requests',
    defaultValue: [],
    defaultValueInProduction: true
  }
}