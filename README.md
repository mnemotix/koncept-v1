Koncept
-------

A smart thesaurus editor.


## Development

### Mnemotix team

Ensure the following containers are running on dev1.mnemotix.com :

  - ``keycloak``

Ensure the following containers are running on your local docker machine (see [Synaptix repo](https://bitbucket.org/mnemotix/synaptix)):

  - ``dev-synaptix-index``
  - ``dev-synaptix-urigen``
  - ``dev-synaptix-graphstore``
  - ... and the related others like ``dev-rabbitmq``, ``dev-orientdb``...

Ensure the following environment variables are set :

  - RABBITMQ_HOST : This is the hostname (or ip) of the RabbitMQ broker (Aka: [DOCKER_MACHINE_IP])
  - RABBITMQ_LOGIN : Username for the RabbitMQ broker session.
  - RABBITMQ_PASSWORD : Password for the RabbitMQ broker session.
  
Launch the application :

```
npm i && npm start
```

### Others

The dev environnement development needs some docker containers to run.

```
# Keycloak to enable OAuth2 authentication
docker run --name keycloak -p 9443:8080 --link postgres:postgres -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin123!  -d jboss/keycloak

# RabbitMQ to broadcast AMQP messages
docker run --name rabbitmq -p 5671:5671 -p 5672:5672 -d rabbitmq:3-management
```

Then set up the keycloak configuration to add a Realm :


  - Go to http://[DOCKER_MACHINE_IP]:9443/auth/admin/master/console
  - Login admin/admin123!
  - Add a realm by importing this config file ``./keycloak.koncept.realm.dev.json`` 

Ensure the following environment variables are set :

  - RABBITMQ_HOST : This is the hostname (or ip) of the RabbitMQ broker (Aka: [DOCKER_MACHINE_IP])

Launch the AMQP dev backend module :

```
npm run-script watch:amqp
```

Launch the application :

```
npm i && npm start
```


## Production

### Without Docker

Ensure the following environment variables are set :

 - OAUTH_CLIENT_SECRET : This is the OAUTH shared secret between this client app and OAuth2 server.
 - OAUTH_REDIRECT_URL : This is the OAUTH server public URL (Ex : https://keycloak.is.here);
 - OAUTH_TOKEN_VALIDATION_URL : This is the OAUTH server intra docker container URL (Ex : http://prod-keycloak);
 - OAUTH_REALM :  This is the OAUTH Realm. 
 - OAUTH_REALM_CLIENT : This is the OAUTH realm client name
 - DB_CONNECTOR : This is the backend connector ID : (default : synaptix)
 - APP_PORT : This is the port on which NodeJS is listening.
 - APP_URL : This is the base URL of the application (Not redundant with APP_PORT in case of standard ports).
 - RABBITMQ_HOST : This is the hostname (or ip) of the RabbitMQ broker.
 - RABBITMQ_LOGIN : Username for the RabbitMQ broker session.
 - RABBITMQ_PASSWORD : Password for the RabbitMQ broker session.
 - RABBITMQ_EXCHANGE_NAME : Exchange name to communicate with synaptix
     
Launch this command line at bundling time :

```
NODE_ENV=production npm i && npm run-script build
```

Then this command at launching time : 

```
NODE_ENV=production npm start
```

### Within Docker

```
docker build -t mnx/koncept:`grep version package.json | sed "s/[\", ]//g" | cut -d':' -f2` .
```

```
docker-compose up
```

### Build and publish

```
./scripts/publish.sh
```

## Test

Launch this command

```
npm test
```