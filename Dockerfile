FROM node:8.6.0
RUN mkdir -p /opt/app
WORKDIR /opt/app
ADD . /opt/app
ENV NODE_ENV production
ENV APP_PORT 3001
RUN npm install --production
RUN npm rebuild node-sass
RUN npm run-script build
#
CMD [ "npm", "start" ]
EXPOSE 3001
