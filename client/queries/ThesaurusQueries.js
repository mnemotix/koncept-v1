/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default {
  thesaurus: () => Relay.QL`
    query {
      thesaurus(thesoId: $thesoId)
    }
  `
};
