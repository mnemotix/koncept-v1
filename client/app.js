/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import routes from './routes';

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import {Router, useRouterHistory, applyRouterMiddleware} from 'react-router';
import createHistory from 'history/lib/createBrowserHistory';
import useRelay from 'react-router-relay';
import useNamedRoutes from 'use-named-routes';
import cookie from 'cookie';

import '../server/schema/schema.json';
import './app.scss';

import io from 'socket.io-client';
import Ajax from 'simple-ajax';

import Package from '../package.json';

if (Package.version !== localStorage.getItem('version')) {
  console.log('Version updated, clearing localStorage.');
  localStorage.clear();
  sessionStorage.clear();
  localStorage.setItem('version', Package.version);
}

// The order of history enhancers matters here.
const history = useNamedRoutes(useRouterHistory(createHistory))({ routes });

ReactDOM.render(
  <Router history={history}
          routes={routes}
          render={applyRouterMiddleware(useRelay)}
          environment={Relay.Store}
  />,
  document.getElementById('content')
);

Relay.injectNetworkLayer(
 new Relay.DefaultNetworkLayer('/graphql', {
   credentials: 'same-origin',
   headers : {
     'X-Requested-With': 'XMLHttpRequest',
     'Lang' : localStorage.getItem('lang') || 'fr'
   },
   fetchTimeout: 60000
 })
);

// Test connection.
setInterval(() => {
  const heartbeatAjax = new Ajax({
    url: '/heartbeat',
    cors: true,
    requestedWith : 'XMLHttpRequest'
  });

  heartbeatAjax.send();
}, 30000);

var socket = io.connect(`${location.protocol}//${location.host}`);

socket.on('disconnected',  ({uid}) => {
  let cookies = cookie.parse(document.cookie);

  if(cookies.UID === uid){
    location.reload();
  }
});

