/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import TestUtils from 'react-addons-test-utils';
import App from '../App';

describe('App', () => {
  it('renders', () => {
    var element = TestUtils.renderIntoDocument(<App />);
    expect(element).toBeTruthy();
  });
});