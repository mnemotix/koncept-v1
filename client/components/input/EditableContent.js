/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';
import keyboard from 'keyboardjs';

import style from './style.scss';

export default class extends React.Component {
  static propTypes = {
    placeholder: React.PropTypes.string.isRequired,
    requiredPattern: React.PropTypes.string,
    required: React.PropTypes.bool,
    inputType: React.PropTypes.string
  };

  static defaultProps = {
    required : false,
    inputType : 'text'
  };

  state = {
    editing: false
  };

  renderContent() {
    return (
      <div onClick={this._handleToggleEditing}>
        {this.props.children ? this.props.children : (
          <span className={style.placeholder}>{this.props.placeholder}</span>
        )}
      </div>
    )
  }

  renderInput() {
    return (
      <div>
        <div className={style.editor}>
          <Input ref="input"
                 type={this.props.inputType}
                 className={this.props.inputClassName}
                 value={this.state.content || this.props.children || ''}
                 onChange={this._handleChange}
                 pattern={this.props.requiredPattern}
                 placeholder={this.props.placeholder}
                 onKeyPress={this.props.multiline ? () => {} : this._handleKeyPress.bind(this)}
          />
          <Button label="Enregistrer" primary raised onClick={this._handleSaveEdition.bind(this)}/>
          <IconButton icon="clear" primary  onClick={this._handleToggleEditing.bind(this)}/>
        </div>
        <div className={style.overlay} onClick={this._handleToggleEditing}></div>
      </div>
    )
  }

  render() {
    return (
      <div className={this.props.className}>
        { this.state.editing ? this.renderInput() : this.renderContent() }
      </div>
    );
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      this._handleSaveEdition();
    }
  };

  _handleToggleEditing = () => {
    const editing = !this.state.editing,
      keyboardContext = keyboard.getContext();

    if(editing) {
      keyboard.setContext('editable-content');
    } else {
      keyboard.setContext(this.state.keyboardContext);
    }

    this.setState({
      editing,
      content: editing ? this.state.content : null,
      keyboardContext : editing  ? keyboardContext : null
    });
  };

  _handleChange = (value) => {
    this.setState({
      content: value
    });
  };

  _handleSaveEdition = () => {
    if (!this.state.content) {
      return this._handleToggleEditing();
    }

    this.props.onSave(this.state.content);

    this.setState({
      editing: false,
      content: null
    });
  };

  static propTypes = {
    onSave: React.PropTypes.func.isRequired,
    children: React.PropTypes.string,
    inputClassName: React.PropTypes.string,
    className: React.PropTypes.string
  };
}