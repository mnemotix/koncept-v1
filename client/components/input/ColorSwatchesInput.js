/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';
import classnames from 'classnames';

import {SwatchesPicker} from 'react-color';
import material from 'material-colors';

import style from './style.scss';

export default class extends React.Component {

  state = {
    swatchesActive: false
  };

  render(){
    let properties = this.props;

    return (
      <div className={classnames(style['color-input'], this.props.className)}>
        <div className={style.input}>
          <Input onChange={(value) => this.props.onChangeCallback(value)}
                 onFocus={this._handleDisplaySwathes.bind(this)}
                 disabled={this.props.disabled}
                 className={classnames(style['color'], this.props.className)}
                 error={this.props.error}
                 icon={this.props.icon}
                 hint={this.props.hint}
                 required={this.props.required}
                 style={{backgroundColor: this.props.value || "#000"}}
                 label={this.props.label}
                 value=" "
          />
        </div>

        {this.state.swatchesActive ? (
          <div className={style.swatches}>
            <SwatchesPicker
              color={properties.value || material.red['900']}
              onChange={this._handleColorPicked.bind(this)}
              {...properties}
            />
          </div>
        ) : null}
      </div>
    );
  }

  _handleDisplaySwathes = () => {
    this.setState({
      swatchesActive: true
    });
  };

  _handleColorPicked = (color) => {
    this.setState({
      swatchesActive: false
    }, () => {
      this.props.onChangeCallback(color.hex);
    });
  };
}