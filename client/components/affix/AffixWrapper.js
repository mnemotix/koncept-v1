/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 29/01/2016
 */

/**
 * Inspired from https://gist.github.com/julianocomg/296469e414db1202fc86
 * Thanks to Juliano Castilho <julianocomg@gmail.com>
 */

import React from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';

export default class extends React.Component {

  static propTypes = {
    offset: React.PropTypes.number,
    affixClassName: React.PropTypes.string
  };

  state = {
    affix: false
  };

  static defaultProps = {
    offset: 0
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, true);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll, true);
  }

  handleScroll = () => {
    var affix = this.state.affix;
    var scrollTop = ReactDOM.findDOMNode(this).getBoundingClientRect().top;
    var offset = this.props.offset;

    if (affix && scrollTop >= offset) {
      this.setState({
        affix: false
      });
    }

    if (!affix && scrollTop < offset) {
      this.setState({
        affix: true
      });
    }
  };

  render() {
    var {className, offset, ...props} = this.props;

    return (
      <div className={className + ' ' + classnames({[this.props.affixClassName] : this.state.affix})}>
        {this.props.children}
      </div>
    );
  }

}