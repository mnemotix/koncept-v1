/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import Relay from 'react-relay';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import MockNetworkLayer from 'mock-network-layer';

//import testTree from 'react-test-tree';
import TopBar from '../TopBar';

import {ListItem} from 'react-toolbox/lib/list';
import {MenuItem} from 'react-toolbox/lib/menu';
import Input from 'react-toolbox/lib/input';

const fixtures = {
  user: {
    id: "user432543564",
    user: {
      email: 'foo@bat.de',
      firstName: 'foo',
      lastName: 'bar'
    }
  }
};

describe('TopBar', () => {
  var topbar;

  //beforeEach(() => {
  //  topbar = testTree(<TopBar {...fixtures}/>);
  //});
  //
  //it('renders', () => {
  //  expect(topbar).toBeTruthy();
  //});
  //
  //it('renders user avatar', () => {
  //  expect(topbar.get('avatarTrigger')).toBeTruthy();
  //  expect(topbar.get('avatar')).toBeTruthy();
  //});
  //
  //it('shows avatar menu on click avatar trigger', () => {
  //  topbar.get('avatarTrigger').click();
  //  expect(topbar.state.accountMenuActive).toEqual(true);
  //});
});