/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';

import classnames from 'classnames';

import AppBar from 'react-toolbox/lib/app_bar';
import IconButton from 'react-toolbox/lib/button';
import Drawer from 'react-toolbox/lib/drawer';
import Navigation from '../navigation/Navigation';
import { Link } from 'react-router';
import Logo from '../logo/Logo';
import Avatar from 'react-toolbox/lib/avatar';
import {Menu, MenuItem, MenuDivider} from 'react-toolbox/lib/menu';
import FontIcon from 'react-toolbox/lib/font_icon';


import style from "./style";

class TopBar extends React.Component {
  state = {
    sideNavigationActive: false,
    accountMenuActive: false
  };

  render() {
    let {user} = this.props;

    return (
      <AppBar className={style.appbar} fixed flat>
        <IconButton className={style["menu-btn"]}  icon='menu'  onClick={this._handleToggleSideNavigation} />

        <Logo className={style.logo} />

        <Link className={style.title} to='/'>
          <h1>
            Koncept
          </h1>
        </Link>


        <div className={style.account}>
          <div className={style['avatar-trigger']}  onClick={this._handleShowAccountMenu}>
            <Menu active={this.state.accountMenuActive} ref='accountMenu' onHide={this._handleHideAccountMenu} className={classnames({[style['account-menu']] : true, [style['visible']] : this.state.accountMenuActive})} position='topRight' ripple>
              <MenuItem value='signout' icon='power_settings_new' caption='Déconnexion' onClick={this._handleSignout}/>
            </Menu>

            <Avatar  className={style.avatar} title={user.firstName} />

            <FontIcon className={style['icon-more']} value="arrow_drop_down" />
          </div>
        </div>

        <Drawer active={this.state.sideNavigationActive} onOverlayClick={this._handleToggleSideNavigation}>
          <div className={style['navigation-header']}>
            <Logo className={style.logo} /> Koncept
          </div>

          <Navigation className={style.navigation} onGoToLink={this._handleToggleSideNavigation}/>
        </Drawer>
      </AppBar>
    );
  }

  _handleShowAccountMenu = () => {
    this.setState({ accountMenuActive : true });
  };

  _handleHideAccountMenu = () => {
    this.setState({ accountMenuActive : false });
  };

  _handleToggleSideNavigation = () => {
    this.setState({sideNavigationActive: !this.state.sideNavigationActive});
  };

  _handleSignout = () => {
    window.location.assign('/auth/logout');
  };
}

export default Relay.createContainer(TopBar, {
  fragments: {
    user: () => {
      return Relay.QL`
        fragment on User {
          id,
          email,
          firstName,
          lastName
        }
      `
    }
  }
});