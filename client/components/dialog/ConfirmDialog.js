/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import classnames from 'classnames';

import style from './style';

export default class extends React.Component {
  static propTypes = {
    cancelLabel : React.PropTypes.string,
    validateLabel : React.PropTypes.string,
    title : React.PropTypes.string.isRequired,
    sideNavigationActive : React.PropTypes.bool,
    onCancel: React.PropTypes.func.isRequired,
    onValidate: React.PropTypes.func.isRequired,
    loading: React.PropTypes.bool
  };

  render(){
    let actions = [
      {
        label: this.props.cancelLabel,
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      },
      {
        label: this.props.loading ? "En cours..." : this.props.validateLabel,
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onValidate();
          }
        }
      }
    ];

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
      >
        <section>
          {this.props.message}
        </section>
      </Dialog>
    )
  }
}