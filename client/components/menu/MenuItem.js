/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import {MenuItem} from 'react-toolbox/lib/menu';

export default class extends MenuItem {

  constructor(props, context){
    super(props, context);

    this.handleClickBackup = this.handleClick;

    this.handleClick = (e) => {
      e.stopPropagation();
      e.preventDefault();

      return this.handleClickBackup(e);
    };
  }


}