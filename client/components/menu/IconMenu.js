/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import {IconMenu} from 'react-toolbox/lib/menu';

export default class extends IconMenu {

  constructor(props, context){
    super(props, context);

    this.handleButtonClickBackup = this.handleButtonClick;

    this.handleButtonClick = (e) => {
      e.stopPropagation();
      e.preventDefault();

      return this.handleButtonClickBackup(e);
    };
  }


}