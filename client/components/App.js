/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';

class App extends React.Component {
  static childContextTypes = {
    lang: React.PropTypes.string
  };

  getChildContext() {
    return {
      lang: 'fr'
    };
  }

  render() {
    var {children} = this.props;

    return (
      <div className="app">
        { children }
      </div>
    )
  }
}

export default Relay.createContainer(App, {
  fragments: {}
});