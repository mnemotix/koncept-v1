/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';

export default class extends React.Component {
  static contextTypes = {
    router: React.PropTypes.func.isRequired
  };

  actions = [
    { label: "Recharger la page", onClick: this.handleReconnect },
  ];

  render() {
    console.log(this);
    return (
      <Dialog actions={this.actions} active={true} title='Oups... Petit problème technique.'>
        Il semblerait que vous soyez déconnecté.
      </Dialog>
    )
  }

  handleReconnect = () => {
    location.reload();
  };
}