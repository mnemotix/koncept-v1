/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';

import style from './style.scss';

export var HandledErrorMixin = ComposedComponent => class extends React.Component {

  static displayName = 'handledErrorHoc';

  constructor(props) {
    super(props);

    this.state = {
      errorDialogActive: false,
      errorDialogMessage : ''
    };

    ComposedComponent.prototype.showError = this.showError.bind(this);
    ComposedComponent.prototype.showMutationError = this.showMutationError.bind(this);
  }

  showError(error){
    this.setState({
      errorDialogActive: true,
      errorDialogMessage: error
    });
  }

  showMutationError(transaction) {
    let error = transaction.getError();

    if (error.message) {
      this.setState({
        errorDialogActive: true,
        errorDialogMessage: error.message.replace(/(?:\r\n|\r|\n)/g, '<br />')
      });

    } else if (error instanceof Response) {
      error.json().then(message => this.setState({
        errorDialogActive: true,
        errorDialogMessage: message.errors[0].message
      }));
    }
  }

  hideError(){
    this.setState({
      errorDialogActive: false,
      errorDialogMessage: ''
    });
  }

  render() {
    let actions = [
      { label: "Fermer", onClick: this.hideError.bind(this) }
    ];

    return (
      <div className="composed">
        <ComposedComponent {...this.props} {...this.state} showError={this.showError.bind(this)} showMutationError={this.showMutationError.bind(this)} />

        <Dialog actions={actions}
                active={this.state.errorDialogActive}
                title='Oups... Petit problème technique.'
                onOverlayClick={this.hideError.bind(this)}
        >
          <h2 className={style['error-title']}>Détails de l'erreur : </h2>
          <div className={style['error-details']} dangerouslySetInnerHTML={{__html:this.state.errorDialogMessage}}/>
        </Dialog>
      </div>
    );
  }
};