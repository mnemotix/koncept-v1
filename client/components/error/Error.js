/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';

import style from './style.scss';

export default class extends React.Component {

  state = {
    active: this.props.active || false
  };

  hideError(){
    this.setState({
      active: false
    });
  }

  render() {
    let actions = [
      { label: "Recharger la page", onClick: () => {
        location.reload();
      }}
    ];

    return (
      <Dialog actions={actions}
              active={this.state.active}
              title='Oups... Petit problème technique.'
              onOverlayClick={this.hideError.bind(this)}
      >
        <h2 className={style['error-title']}>Détails de l'erreur : </h2>
        <div className={style['error-details']} dangerouslySetInnerHTML={{__html: this.props.message.replace(/(?:\r\n|\r|\n)/g, '<br />')}}/>
      </Dialog>
    );
  }
};