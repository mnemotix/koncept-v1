/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import {withRouter} from 'react-router';

@withRouter
export default class extends React.Component {
  static defaultProps = {
    renderAsMenu: false,
    className: ''
  };

  getLinks(){
    return [
      <a onClick={this._handleClick.bind(this, '/')}>
        Accueil
      </a>,
      <a onClick={this._handleClick.bind(this, {
        name: 'thesautheque'
      })}>
        Thesauthèque
      </a>
    ];
  }

  _handleClick = (route) => {
    const {router} = this.props;

    router.push(route);

    if (this.props.onGoToLink){
      this.props.onGoToLink();
    }
  };

  render() {
    return (
      <nav className={this.props.className}>
        <ul>
          {this.getLinks().map((link, index) =>(
            <li key={`navigation_link_${index}`}>
              {link}
            </li>
          ))}
        </ul>
      </nav>
    );
  }
}