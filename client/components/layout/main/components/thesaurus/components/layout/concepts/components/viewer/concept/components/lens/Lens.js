/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 14/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import vis from 'vis';
import screenfull from 'screenfull';

import {Menu, MenuItem, Button} from 'react-toolbox';
import Preloader from '../../../../../../../../../../../../preloader/Preloader';

import SwitchFromNarrowerToRelatedRelationMutation from './mutations/SwitchFromNarrowerToRelatedRelationMutation';
import SwitchFromRelatedToNarrowerRelationMutation from './mutations/SwitchFromRelatedToNarrowerRelationMutation';


import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../../../../common/preloader/preloaderHoc';
import {withRouter} from 'react-router';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
@withRouter
class Lens extends React.Component {
  nodesDataset = new vis.DataSet();
  edgesDataset = new vis.DataSet();

  state = {
    selectedEdge: null,
    selectedNode: null,
    menuPositionX: 0,
    menuPositionY: 0,
    fullscreen: false,
    menuActive: true
  };

  edgebroaderVisOptions = {
    label: 'skos:broader',
    color:'#80CBC4',
    font :{ color:'#80CBC4', size: 11},
    group: 'skos:broader'
  };

  edgeNarrowerVisOptions = {
    label: 'skos:narrower',
    color:'#90CAF9',
    font :{ color:'#90CAF9', size: 11},
    group: 'skos:narrower'
  };

  edgeRelatedVisOptions = {
    label: 'skos:related',
    color:'#EF9A9A',
    font :{ color:'#EF9A9A', size: 11},
    group: 'skos:related'
  };

  componentWillUpdate(nextProps){
    if (nextProps.concept.narrowers.edges.length !== this.props.concept.narrowers.edges.length ||
      nextProps.concept.related.edges.length !== this.props.concept.related.edges.length
    ) {
      this.refreshGraph = true;
    }
  }

  componentDidUpdate(){
    if(this.refreshGraph){
      this.refreshGraph = false;

      this.nodesDataset.clear();
      this.edgesDataset.clear();
      this.renderGraph();
    }
  }
  componentDidMount(){
    this.renderGraph();
  }

  renderGraph(){
    const {concept} = this.props;

    this.nodesDataset.add({
      id: concept.id,
      label: concept.prefLabel.value,
      color: {
        background: '#DDD',
        highlight: {
          background: '#DDD'
        },
      },
      level: concept.ancestors.length,
      ancestors : concept.ancestors
    });

    concept.narrowers.edges.map(edge => {
      let narrower = edge.node;

      this.edgesDataset.add({
        from: concept.id,
        to: narrower.id,
        ...this.edgeNarrowerVisOptions
      });

      if (!this.nodesDataset.get(narrower.id)) {
        this.nodesDataset.add({
          id: narrower.id,
          label: narrower.prefLabel.value,
          level: concept.ancestors.length + 1,
          ancestors : narrower.ancestors
        });
      }
    });

    concept.related.edges.map(edge => {
      let related = edge.node;

      this.edgesDataset.add({
        from: concept.id,
        to: related.id,
        ...this.edgeRelatedVisOptions
      });

      if (!this.nodesDataset.get(related.id)) {
        this.nodesDataset.add({
          id: related.id,
          label: related.prefLabel.value,
          level: concept.ancestors.length + 1,
          ancestors : related.ancestors
        });
      }
    });

    concept.ancestors.reverse();

    concept.ancestors.map((ancestor, index) => {
      let label = ancestor.__typename === 'Concept' ? ancestor.prefLabel.value : ancestor.title;

      this.edgesDataset.add({
        from: index === 0 ? concept.id : concept.ancestors[index - 1].id,
        to: ancestor.id,
        ...this.edgebroaderVisOptions,
        ...(ancestor.__typename === 'Scheme' ? {
          label: 'skos:in_scheme',
          color: '#CE93D8',
          font :{ color:'#CE93D8'},
        } : {})
      });

      if (!this.nodesDataset.get(ancestor.id)) {
        this.nodesDataset.add({
          id: ancestor.id,
          label,
          level: concept.ancestors.length - index - 1,
          ...(ancestor.__typename === 'Scheme' ? {
            color: {
              border: ancestor.color,
              background: ancestor.color,
              highlight: {
                border: ancestor.color,
                background: ancestor.color
              },
            },
            font: {
              color: "#fff"
            },
            shapeProperties:{
              borderRadius: 5
            }
          } : {
            ancestors: ancestor.ancestors
          })
        });
      }
    });

    if (!this.network) {
      // initialize your network!
      this.network = new vis.Network(ReactDOM.findDOMNode(this.refs.network), {
        nodes: this.nodesDataset,
        edges: this.edgesDataset
      }, {
        nodes: {
          borderWidthSelected: 0,
          shadow: {
            enabled: true,
            color: 'rgba(0,0,0,0.3)',
            size: 5,
            x: 1,
            y: 2
          },
          shape: 'box',
          shapeProperties: {
            borderRadius: 0
          },
          color: {
            border: 'transparent',
            background: '#FFF',
            highlight: {
              border: 'rgba(0,0,0,0.5)',
              background: '#FFF'
            },
          }
        },
        edges: {
          arrows: {to: {enabled: true, scaleFactor: 0.5}},
          smooth: {
            type: 'cubicBezier',
            roundness: 0.3
          }
        },
        layout: {
          hierarchical: {
            sortMethod: "directed",
            direction: 'LR',
            nodeSpacing: 10,
            levelSeparation: 250
          }
        },
        physics: {
          barnesHut: {
            gravitationalConstant: -2000,
            centralGravity: 0,
            springLength: 100,
            springConstant: 0.04,
            damping: 0.09,
            avoidOverlap: 0
          }
        }
      });

      this.network.on('click', this._handleClick.bind(this));
    }
  }

  _handleClick = (event) => {
    this.setState({
      menuActive: false
    }, () => {
      setTimeout(() => {
        // Edge clicked
        if(event.nodes.length === 1) {
          this.setState({
            selectedNode: this.nodesDataset.get(event.nodes[0]),
            selectedEdge: null,
            menuPositionX: event.event.srcEvent.screenX,
            menuPositionY: event.event.srcEvent.screenY,
            menuActive: true
          });

          // Edge clicked
        } else if(event.edges.length === 1) {
          this.setState({
            selectedNode: null,
            selectedEdge: this.edgesDataset.get(event.edges[0]),
            menuPositionX: event.event.srcEvent.screenX,
            menuPositionY: event.event.srcEvent.screenY,
            menuActive: true
          });
        }
      }, 200);

    });

  };

  render() {
    let menuItems = [];

    if (this.state.selectedEdge) {
      if (this.state.selectedEdge.group === 'skos:narrower') {
        menuItems.push(
          <MenuItem icon='link'
                    key="crfntr"
                    caption='Transformer en une relation associative (skos:related)'
                    onClick={this._handleChangeRelationFromNarrowerToRelated.bind(this)}
          />
        );
      } else {
        menuItems.push(
          <MenuItem icon='link'
                    key="crfrtn"
                    caption='Transformer en une relation hiérarchique (skos:narrower)'
                    onClick={this._handleChangeRelationFromRelatedToNarrower.bind(this)}
          />
        );
      }
    }

    if (this.state.selectedNode) {
      menuItems = menuItems.concat([
        <MenuItem className='menu-item-edit-concept'
                  key="e"
                  icon='edit'
                  caption='Éditer le concept'
                  onClick={this._handleEditConcept}
        />,
        <MenuItem icon='remove_red_eye'
                  key="vh"
                  caption='Voir dans la hiérarchie'
                  onClick={this._handleViewConceptInHierarchy}
        />
      ]);
    }

    return (
      <div className={style.layout} ref="layout">
        <div className={style['menu-container']} style={{top: this.state.menuPositionY, left: this.state.menuPositionX}}>
          <Menu ref="menu" active={this.state.menuActive} position="auto">
            {menuItems.map(menuItem => menuItem)}
          </Menu>
        </div>

        <div ref="network" className={style.network}/>

        <Button className={style.fullScreenButton} icon={this.state.fullscreen ? "fullscreen_exit" : "fullscreen"} label="Plein écran" onClick={this._handleToggleFullscreen}/>

        <Preloader ref="preloader"/>
      </div>
    );
  }

  _handleToggleFullscreen = () => {
    let layout = ReactDOM.findDOMNode(this.refs.layout);

    this.setState({
      fullscreen: !this.state.fullscreen
    }, () => {
      screenfull.toggle(layout);
    });


  };

  _handleChangeRelationFromNarrowerToRelated = () => {
    const {concept} = this.props;

    const targetConcept = concept.narrowers.edges.find(edge => edge.node.id === this.state.selectedEdge.to).node;

    this.props.showPreloader();

    this.props.relay.commitUpdate(new SwitchFromNarrowerToRelatedRelationMutation({
      sourceConcept: concept,
      targetConcept: targetConcept
    }), {
      onSuccess: () => {
        this.props.showSnackbar('La relation a été modifiée');
        this.edgesDataset.update({
          ...this.state.selectedEdge,
          ...this.edgeRelatedVisOptions
        });
        this.props.hidePreloader();
      },
      onFailure: (transaction) => {
        this.props.showMutationError(transaction);
        this.props.hidePreloader();
      }
    });
  };


  _handleChangeRelationFromRelatedToNarrower = () => {
    const {concept} = this.props;

    const targetConcept = concept.related.edges.find(edge => edge.node.id === this.state.selectedEdge.to).node;

    this.props.showPreloader();

    this.props.relay.commitUpdate(new SwitchFromRelatedToNarrowerRelationMutation({
      sourceConcept: concept,
      targetConcept: targetConcept,
      targetParentConcept: targetConcept.broader
    }), {
      onSuccess: () => {
        this.props.showSnackbar('La relation a été modifiée');
        this.props.hidePreloader();

        this.edgesDataset.update({
          ...this.state.selectedEdge,
          ...this.edgeNarrowerVisOptions
        });
      },
      onFailure: (transaction) => {
        this.props.showMutationError(transaction);
        this.props.hidePreloader();
      }
    });
  };

  _handleEditConcept = () => {
    const {router} = this.props;

    router.push({
      name: 'concept',
      params: {
        thesoId: this.props.thesaurus.id,
        conceptId: this.state.selectedNode.id
      }
    });
  };

  _handleViewConceptInHierarchy = () => {
    const {router, thesaurus} = this.props;
    let concept = this.state.selectedNode;

    let ancestors = concept.ancestors;

    if (ancestors && ancestors.length > 0) {
      router.push({
        name: "thesaurus",
        params: {
          thesoId: thesaurus.id
        },
        query: {
          cids  : ancestors.map(ancestor => ancestor.id).join('/'),
          fcpt : concept.id,
          fcln: ancestors.length
        }
      });
    }
  };
}

export default Relay.createContainer(Lens, {
  fragments: {
    concept: () => Relay.QL`
      fragment on Concept {
        id,
        prefLabel: prefLabelForLang(lang: "fr"){
          value
        }
        ancestors{
          id
          __typename
          ...on Concept{
            prefLabel: prefLabelForLang(lang: "fr"){
              value
            }
            ancestors{
              id
            }
          }
          ...on Scheme{
            title
            color
          }
        }
        narrowers(first: 1000){
          edges{
            node{
              id,
              prefLabel: prefLabelForLang(lang: "fr"){
                value
              }
              ancestors{
                id
              }
              ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('targetConcept')},
              ${SwitchFromNarrowerToRelatedRelationMutation.getFragment('targetConcept')}
            }
          }
        },
        related(first: 1000){
          edges{
            node{
              id,
              prefLabel: prefLabelForLang(lang: "fr"){
                value
              }
              broader{
                id,
                ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('targetParentConcept')}
              }
              ancestors{
                id
              }
              ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('targetConcept')},
              ${SwitchFromNarrowerToRelatedRelationMutation.getFragment('targetConcept')}
            }
          }
        },
        ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('sourceConcept')}
        ${SwitchFromNarrowerToRelatedRelationMutation.getFragment('sourceConcept')}
      }
    `,
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id,
      }
    `
  }
});