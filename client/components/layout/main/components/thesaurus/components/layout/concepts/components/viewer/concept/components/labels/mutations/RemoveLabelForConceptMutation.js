/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeLabelForConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveLabelForConceptPayload {
        deletedId
        concept{
          prefLabels
          altLabels
          hiddenLabels
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_DELETE',
        parentName: 'concept',
        parentID: this.props.concept.id,
        connectionName: this.props.labelType,
        deletedIDFieldName: 'deletedId',
        pathToConnection: ['concept', this.props.labelType]
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      labelId: this.props.label.id,
      labelType: this.props.labelType,
      lang: this.props.lang,
      value: this.props.value
    };
  }
}
