/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 28/04/2016
 */

import React from 'react';
import Relay from 'react-relay';

import vis from 'vis';

import Concept from './Concept';

class ConceptList extends React.Component {
  static propTypes = {
    edgesDataset:React.PropTypes.instanceOf(vis.DataSet)
  };

  render() {
    let {edgesDataset, nodesDataset, concept, level, selectedNodes, color} = this.props;

    return (
      <div>
        {concept.narrowers.edges.map((edge) => {
          let id = `${concept.id}-${edge.node.id}-narrower`;

          if(!edgesDataset.get(id)) {
            edgesDataset.add({
              id ,
              from: concept.id,
              to: edge.node.id,
              color: color,
              font :{ color:'#03a9f4', size: 11},
              label: ''
            });
          }

          return <Concept key={edge.node.id}
                          nodesDataset={nodesDataset}
                          edgesDataset={edgesDataset}
                          concept={edge.node}
                          level={level}
                          selectedNodes={selectedNodes}
                          color={color}
          />
        })}

        {concept.related.edges.map((edge) => {
          let id = `${concept.id}-${edge.node.id}-related`;

          return <Concept key={edge.node.id}
                          nodesDataset={nodesDataset}
                          edgesDataset={edgesDataset}
                          concept={edge.node}
                          level={level+1}
                          selectedNodes={selectedNodes}
                          color={color}
          />
        })}
      </div>
    )
  }
}

export default Relay.createContainer(ConceptList, {
  fragments: {
    concept: () => Relay.QL`
      fragment on Concept {
        id,
        narrowers(first: 1000){
          edges{
            node {
              id,
              ${Concept.getFragment('concept')}
            }
          }
        },
        related(first: 1000){
          edges{
            node {
              id,
              ${Concept.getFragment('concept')}
            }
          }
        }
      }
    `
  }
});