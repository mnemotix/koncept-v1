/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    matcher: () => Relay.QL`
      fragment on Matcher {
        id
      }
    `,
    label: () => Relay.QL`
      fragment on LocalizedLabel {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{updateMatcherForLabel}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateMatcherForLabelPayload {
        matcher
        label{
          matchers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          label: this.props.label.id
        }
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          matcher: this.props.matcher.id
        }
      }
    ];
  }

  getVariables() {
    return {
      labelId: this.props.label.id,
      matcherId: this.props.matcher.id,
      expression: this.props.expression
    };
  }
}
