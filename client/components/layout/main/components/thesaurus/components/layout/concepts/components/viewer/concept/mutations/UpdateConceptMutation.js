/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class UpdateConceptMutation extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{updateConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateConceptPayload {
        concept
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      }
    ];
  }

  getVariables() {
    return {
      id: this.props.concept.id,
      field: this.props.field,
      value: this.props.value
    };
  }
}
