/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 03/02/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{createTopScheme}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on CreateTopSchemePayload {
        thesaurus{
          topSchemes
        }
        newSchemeEdge
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'REQUIRED_CHILDREN',
        children: [Relay.QL`
          fragment on CreateTopSchemePayload {
            newSchemeEdge
          }
      `]
      },
      {
        type: 'RANGE_ADD',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'topSchemes',
        edgeName: 'newSchemeEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      thesaurusId: this.props.thesaurus.id,
      schemeTitle: this.props.schemeTitle,
      schemeDescription: this.props.schemeDescription,
      schemeColor: this.props.schemeColor,
      topConceptLabel:  this.props.topConceptLabel
    };
  }
}