/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    collection: () => Relay.QL`
      fragment on Collection {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeConceptFromCollection}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveConceptFromCollectionPayload {
        collectionId,
        conceptId,
        concept{
          memberOf
        },
        collection{
          members
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_DELETE',
        parentName: 'concept',
        parentID: this.props.concept.id,
        connectionName: 'memberOf',
        deletedIDFieldName: 'collectionId',
        pathToConnection: ['concept', 'memberOf']
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'collection',
        parentID: this.props.collection.id,
        connectionName: 'members',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['collection', 'members']
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      collectionId: this.props.collection.id
    };
  }
}
