/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';
import classnames from 'classnames';
import Autocomplete from 'react-toolbox/lib/autocomplete';

import style from './style.scss';

import SetNarrowerRelation from '../../../../../../tree/mutations/SetNarrowerRelationMutation';
import SetRelatedRelation from '../../../../../../tree/mutations/SetRelatedRelationMutation';
import RemoveConceptMutation from '../../../../../../tree/mutations/RemoveConceptMutation';

import {HandledErrorMixin} from '../../../../../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from '../../../../../../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from '../../../../../../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class SearchConceptDialog extends React.Component {
  static propTypes = {
    onCancel: React.PropTypes.func.isRequired,
    active: React.PropTypes.bool,
    title: React.PropTypes.string,
    relationType: React.PropTypes.oneOf(['narrowers', 'related'])
  };

  state = {
    query: '',
    selectedConcept: null,
    loading: false
  };

  actions = [
    {
      label: "Annuler",
      onClick: (e) => {
        e.stopPropagation();
        e.preventDefault();
        return this.props.onCancel();
      }
    },
    {
      label: "Créer la relation",
      onClick: (e) => {
        e.stopPropagation();
        e.preventDefault();
        return this.createRelation();
      }
    }
  ];

  componentWillReceiveProps(nextProps) {
    if (nextProps.active === true && this.props.active === false) {
      this.setFocus = true;
    }
  }

  componentDidUpdate() {
    if (this.setFocus) {
      this.refs['queryInput'].getWrappedInstance().focus();
      this.setFocus = false;
    }
  }

  render() {
    const {thesaurus, concept} = this.props;

    return (
      <Dialog
        actions={this.actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
      >
        <section className={style.section}>

          <div className={style.explanation}/>

          <Input
            ref={'queryInput'}
            type='text'
            placeholder="Chercher un concept..."
            value={this.state.query}
            onChange={this._handleInputChange.bind(this)}
          />

          {this.state.loading ? (
              <div className={style.empty}>Recherche en cours...</div>
            )
            : thesaurus.searchConcepts.length === 0 && this.state.query !== '' ?
              (
                <div className={style.empty}>Aucun concept n'a été trouvé...</div>
              ) :
              (
                <div className={style.resultsLayout}>
                  <div className={style.results}>
                    {this.renderList()}
                  </div>
                </div>
              )}
        </section>
      </Dialog>
    )
  }

  renderList(){
    const {thesaurus, concept} = this.props;

    return thesaurus.searchConcepts
      .filter(searchConcept => searchConcept.id !== concept.id)
      .map(searchConcept => this.renderConcept(searchConcept));
  }

  renderConcept(concept, displayHierarchy = true, index){
    return (
      <div key={index || concept.id} className={classnames(style.conceptWrapper, {[style.withHierarchy]: displayHierarchy})}>
        <div className={classnames(style.concept, {
               [style.selected]: this.state.selectedConcept && this.state.selectedConcept.id === concept.id
             })}
             onClick={() => displayHierarchy ? this._handleSelect(concept) : null}
             onMouseOver={() => displayHierarchy ? this.setState({hoveredConcept: concept}) : null}
             onMouseOut={() => displayHierarchy ? this.setState({hoveredConcept: null}) : null}

        >

          <div className={style.inner}>
            {concept.prefLabel.value}
          </div>
        </div>

        {displayHierarchy ? (
          <div className={classnames(style.hierarchy, {[style.show]: this.state.hoveredConcept && this.state.hoveredConcept.id === concept.id })}>
            {this.renderConceptHierachy(concept, false)}
          </div>
        ) : null}

      </div>
    );
  }

  renderScheme(scheme, index) {
    return (
      <div key={index || scheme.id} className={style.conceptWrapper}>
        <div className={classnames(style.scheme, style.concept)}
             onClick={this._handleSelect.bind(this, scheme)}
        >

          <div className={style.inner}
               style={{backgroundColor: scheme.color || '#ababab'}}
          >
            {scheme.title}
          </div>
        </div>
      </div>
    );
  }

  renderConceptHierachy(concept){
    let {ancestors} = concept;

    ancestors = [].concat(ancestors).reverse();

    return (
      <div>
        <div className={style.link}>Broaders</div>
        <div className={style.list}>
          {ancestors.map((ancestor, index) => ancestor.__typename === "Concept" ? this.renderConcept(ancestor, false, index) : this.renderScheme(ancestor, index))}
        </div>
      </div>
    );
  }

  clearQuery() {
    this.setState({query: '', selectedConcept: null});
    this.props.relay.setVariables({query: ''});
  }

  _handleInputChange = (query) => {
    this.setState({query, loading: true});

    if (this.keyTimeout) {
      clearTimeout(this.keyTimeout);
    }

    this.keyTimeout = setTimeout(() => {
      this.props.relay.setVariables({query: this.state.query}, ({done}) =>{
        if(done){
          this.setState({loading: false})
        }
      });
    }, 500);

  };

  _handleSelect = (concept) => {
    if (this.state.selectedConcept === concept) {
      this.setState({selectedConcept: null});
    } else {
      this.setState({selectedConcept: concept});
    }
  };

  createRelation() {
    const {relationType, concept} = this.props,
      selectedConcept = this.state.selectedConcept;

    this.clearQuery();

    this.props.showPreloader();

    if (relationType === 'narrowers') {
      this.props.relay.commitUpdate(new SetNarrowerRelation({
        parentConcept: concept,
        narrowerId: selectedConcept.id
      }), {
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
          this.props.hidePreloader();
        }
      });

      this.props.relay.commitUpdate(
        new RemoveConceptMutation({
          parentId: selectedConcept.broader.id,
          concept: selectedConcept,
          isMoving: true // Tell server not to destroy the concept node
        }),
        {
          onSuccess: () => {
            this.props.showSnackbar('La relation a été modifiée');
            this.props.hidePreloader();
            this.onCreateSuccess();
          },
          onFailure: (transaction) => {
            this.props.showMutationError(transaction);
            this.props.hidePreloader();
          }
        }
      );
    } else {
      this.props.relay.commitUpdate(new SetRelatedRelation({
        concept,
        relatedConcept: selectedConcept
      }), {
        onSuccess: () => {
          this.props.showSnackbar('La relation a été modifiée');
          this.props.hidePreloader();
          this.onCreateSuccess();
        },
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
          this.props.hidePreloader();
        }
      });
    }
  }

  onCreateSuccess() {
    return this.props.onSelect();
  }
}

export {SearchConceptDialog};

export default Relay.createContainer(SearchConceptDialog, {
  initialVariables: {
    query: '',
    first: 10
  },
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus{
        id
        searchConcepts(query: $query, first: $first){
          id
          prefLabel: prefLabelForLang(lang: "fr"){
            value
          }
          broader{
            id
          }
          ancestors{
            id
            __typename
            ...on Concept{
              prefLabel: prefLabelForLang(lang: "fr"){
                value
              }
            }
            
            ...on Scheme {
              title
              color
            }
          }
          ${SetRelatedRelation.getFragment('relatedConcept')},
          ${RemoveConceptMutation.getFragment('concept')}
        }
      }
    `,
    concept: () => Relay.QL`
      fragment on Concept{
        id
        ${SetRelatedRelation.getFragment('concept')},
        ${SetNarrowerRelation.getFragment('parentConcept')},
      }
    `
  }
});
