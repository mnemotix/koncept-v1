/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    collection: () => Relay.QL`
      fragment on Collection {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeConceptFromCollection}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveConceptFromCollectionPayload {
        conceptId,
        collection{
          members
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'NODE_DELETE',
        parentName: 'collection',
        parentID: this.props.collection.id,
        connectionName: 'members',
        deletedIDFieldName: 'conceptId'
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.conceptId,
      collectionId: this.props.collection.id
    };
  }
}
