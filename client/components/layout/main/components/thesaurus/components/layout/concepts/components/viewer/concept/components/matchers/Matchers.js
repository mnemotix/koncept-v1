/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 23/03/2016
 */

import React from 'react';
import Relay from 'react-relay';

import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';

import UpdateMatcherForLabelMutation from './mutations/UpdateMatcherForLabelMutation';
import AddMatcherToLabelMutation from './mutations/AddMatcherToLabelMutation';
import RemoveMatcherForLabelMutation from './mutations/RemoveMatcherForLabelMutation';

import deepEqual from 'deep-equal';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class Matchers extends React.Component {

  state = {
    matchers:  this.props.label.matchers.edges.map(({node: matcher}) => matcher) || []
  };

  componentWillReceiveProps(nextProps){
    if (!deepEqual(nextProps.label.matchers, this.props.label.matchers)) {
      this.setState({
        matchers: nextProps.label.matchers.edges.map(({node: matcher}) => matcher)
      });
    }
  };

  componentDidUpdate(){
    if (this.focusInput) {
      this.setFocus();
    }
  }

  setFocus(){
    let input = this.focusInput;

    if (input) {
      input.getWrappedInstance().focus();
    }
  }

  render() {
    const {labelType} = this.props;

    return (
      <div>
        { this.state.matchers.length == 0 ? (
          <div className={style.none}>Aucune expression de recherche pour ce label</div>
        ) : ''}

        {this.state.matchers.map((matcher, index) => (
          <div key={matcher.id || index} className={style['input-row']}>
            <Input className={style.input}
                   value={matcher.expression || ""}
                   ref={`input_${index}`}
                   ref={(input) => { if(matcher.pending){this.focusInput = input; }}}
                   onChange={(value) => this._handleUpdateInputChange(value, index)}
                   onBlur={this._handleCommitUpdateMatcher.bind(this, matcher, index)}
                   onKeyPress={this._handleKeyPress.bind(this)}
            />

            <IconButton className={style['remove-button']} icon="close" onClick={this._handleDeleteMatcher.bind(this, matcher)} />
          </div>
        ))}

        <Button className={style['add-button']}
                onClick={this._handleAddMatcher}
                icon='add' label='Ajouter une expression de recherche' flat />
      </div>
    )
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      e.target.blur();
    }
  };

  _handleUpdateInputChange(expression, index) {
    var {matchers} = this.state,
      matcher = matchers[index];

    matchers[index] = {
      ...matcher,
      expression
    };

    this.setState({matchers});
  }

  _handleAddMatcher = () => {
    var matchers = this.state.matchers,
        pendingMatcher = {
          expression: '',
          pending: true
        };

    matchers.push(pendingMatcher);

    this.setState({
      matchers
    });
  };

  _handleDeleteMatcher = (matcher) => {
    let {label} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new RemoveMatcherForLabelMutation({
        label,
        matcher
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('L\'expression de recherche a été supprimée');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
          this.props.hidePreloader();
        }
      }
    );
  };

  _handleCommitUpdateMatcher = (matcher, index) => {
    const {label} = this.props,
      {expression, pending} = matcher;

    // Case of newly created matcher pending save.
    if (pending) {
      // Nothing in here, just remove it from the view.
      if (expression == '') {
        return this.setState({
          pendingMatcher: null
        });
      } else {
        this.props.showPreloader();

        this.props.relay.commitUpdate(
          new AddMatcherToLabelMutation({
            label,
            expression
          }), {
            onSuccess: () => {
              this.props.showSnackbar('L\'expression de recherche a été ajoutée');
              this.props.hidePreloader();
            },
            onFailure: (transaction) => {
              this.props.showMutationError(transaction);
              this.props.hidePreloader();
            }
          }
        );
      }
      // Matcher exists yet.
    } else if (expression == '') {
      return this._handleDeleteMatcher(matcher);
    } else {
      this.props.showPreloader();

      this.props.relay.commitUpdate(
        new UpdateMatcherForLabelMutation({
          matcher,
          label,
          expression
        }),
        {
          onSuccess: () => {
            this.props.showSnackbar('L\'expression de recherche a été modifiée');
            this.props.hidePreloader();
          },
          onFailure: (transaction) => {
            this.props.showMutationError(transaction);
            this.props.hidePreloader();
          }
        }
      );
    }
  };
}

export default Relay.createContainer(Matchers, {
  fragments: {
    label: () => Relay.QL`
      fragment on LocalizedLabel {
        id
        matchers(first: 50){
          edges{
            node{
              id
              expression
              ${UpdateMatcherForLabelMutation.getFragment('matcher')}
              ${RemoveMatcherForLabelMutation.getFragment('matcher')}
            }
          }
        }
        ${AddMatcherToLabelMutation.getFragment('label')}
        ${RemoveMatcherForLabelMutation.getFragment('label')}
        ${UpdateMatcherForLabelMutation.getFragment('label')}
      }
    `
  }
});