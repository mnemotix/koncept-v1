/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class MoveConceptMutation extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    parent: () => Relay.QL`
      fragment on SkosElementInterface {
        id
      }
    `,
    target: () => Relay.QL`
      fragment on SkosElementInterface {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{moveConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on MoveConceptPayload {
        conceptId
        conceptEdge
        concept{
          id
          ancestors{
            id
          }
        }
        parent{
          id
          ...on Concept{
            childrenCount
            narrowers
          }
          ...on Scheme{
            topConcepts
            topConceptsCount
          }
        }
        target{
          id
          ...on Concept{
            childrenCount
            narrowers
          }
          ...on Scheme{
            topConcepts
            topConceptsCount
          }
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'target',
        parentID: this.props.target.id,
        connectionName: 'narrowers',
        edgeName: 'conceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      },
      {
        type: 'RANGE_ADD',
        parentName: 'target',
        parentID: this.props.target.id,
        connectionName: 'topConcepts',
        edgeName: 'conceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'parent',
        parentID: this.props.parent.id,
        connectionName: 'narrowers',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['parent', 'narrowers']
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'parent',
        parentID: this.props.parent.id,
        connectionName: 'topConcepts',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['parent', 'topConcepts']
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      parentId: this.props.parent.id,
      targetId: this.props.target.id
    };
  }
}
