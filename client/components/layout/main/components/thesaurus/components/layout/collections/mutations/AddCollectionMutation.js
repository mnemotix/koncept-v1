/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addCollection}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddCollectionPayload {
        newCollectionEdge,
        thesaurus{
          collections
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'collections',
        edgeName: 'newCollectionEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      thesaurusId: this.props.thesaurus.id,
      title: this.props.title,
      description: this.props.description
    };
  }
}
