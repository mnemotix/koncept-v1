/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import classnames from 'classnames';

import {List, ListItem, ListDivider} from 'react-toolbox/lib/list';
import Button from 'react-toolbox/lib/button';
import FontIcon from 'react-toolbox/lib/font_icon';

import Collection from './../viewer/Collection';

import style from './style.scss';

class CollectionList extends React.Component {
  renderEmptyMessage(){
    return (
      <div>
        Aucune collection.
      </div>
    )
  }

  renderList(){
    const {thesaurus, selectedId} = this.props;

    return (
      <List className={style.list} selectable ripple>
        {thesaurus.collections.edges.map(edge => (
          <ListItem key={edge.node.id}
                    caption={edge.node.title}
                    className={classnames({[style.item]: true, [style.selected] : selectedId == edge.node.id })}
                    onClick={event => this.selectCollection(edge.node.id, event)}
                    rightActions={[
                      <FontIcon key={'delete'} className={style.remove} value="delete" onClick={event => this.removeCollection(edge.node, event)}/>
                    ]}
          />
        ))}
      </List>
    );
  }

  componentDidMount(){
    const {thesaurus} = this.props;
    if (!this.props.selectedId && thesaurus.collections.edges.length > 0){
      this.selectCollection(thesaurus.collections.edges[0].node.id);
    }
  }

  componentDidUpdate(){
    const {thesaurus} = this.props;
    if (!this.props.selectedId && thesaurus.collections.edges.length > 0){
      this.selectCollection(thesaurus.collections.edges[0].node.id);
    }
  }



  render(){
    const {thesaurus} = this.props;

    return (
      <div className={style["list-wrapper"]}>
        {
          thesaurus.collections.edges.length > 0 ? this.renderList() : this.renderEmptyMessage()
        }
      </div>
    );
  }

  selectCollection(collectionId, event){
    if (this.props.onSelected) {
      this.props.onSelected(collectionId, event)
    }
  }

  removeCollection(collection, event){
    event.stopPropagation();

    if (this.props.onRemove) {
      this.props.onRemove(collection, event)
    }
  }
}

export default Relay.createContainer(CollectionList, {
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        collections(first: 10){
          edges{
            node{
              id
              title
              description
              ${Collection.getFragment('collection')}
            }
          }
        }
      }
    `
  }
});