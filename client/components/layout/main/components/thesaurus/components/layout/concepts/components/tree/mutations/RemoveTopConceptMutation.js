/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    scheme: () => Relay.QL`
      fragment on Scheme {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeTopConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveTopConceptPayload {
        deletedId
        scheme{
          topConceptsCount
          topConcepts
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'NODE_DELETE',
        parentName: 'scheme',
        parentID: this.props.scheme.id,
        connectionName: 'topConcepts',
        deletedIDFieldName: 'deletedId'
      }
    ];
  }

  getVariables() {
    return {
      id: this.props.concept.id,
      schemeId: this.props.scheme.id
    };
  }

  getOptimisticResponse() {
    return {
      deletedId: this.props.id
    };
  }
}
