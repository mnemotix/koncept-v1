/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';

import classnames from "classnames";
import style from './style.scss';

export default class extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool
  };

  render(){
    let actions = [
      {
        label: "Annuler",
        onClick: (e) => {
          if(!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      },
      {
        label: this.props.loading ? "En cours..." : "Supprimer",
        onClick: (e) => {
          if(!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onRemove();
          }
        }
      }
    ];

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
      >
        <section>
          {this.props.message}
        </section>
      </Dialog>
    )
  }
}