/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/07/2016
 */

import React from 'react';
import Relay from 'react-relay';
import classnames from 'classnames';

import {FontIcon, Button, IconButton, Input, Dropdown} from 'react-toolbox/lib';

import UpdateRulesMutation from './mutations/UpdateRulesMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../../../../error/HandledErrorMixin';
import {preloaderHoc} from '../../../../../../../../../../../../common/preloader/preloaderHoc';
import {snackbarHoc} from '../../../../../../../../../../../../common/snackbar/snackbarHoc';

@HandledErrorMixin
@preloaderHoc
@snackbarHoc
class ViewerTabRules extends React.Component {
  state = {
    rules: this.props.concept.rules || [],
    pendingRuleIndex: null,
    pendingFocusedIndex: null
  };

  componentDidUpdate(){
    if (this.state.pendingFocusedIndex) {
      this.setFocus();
    }
  }

  setFocus(){
    let input = this.refs[`input_${this.state.pendingFocusedIndex}`];

    if (input) {
      input.getWrappedInstance().focus();
    }

    this.setState({ pendingFocusedIndex : null});
  }

  render() {
    const {concept} = this.props;

    return (
      <section className={style['rules-layout']}>

        { this.state.rules.length == 0 ? (
          <div className={style.none}>Aucune règle pour ce concept</div>
        ) : ''}

        {this.state.rules.map((rule, index) => (
          <div key={`rule-${index}`} className={style['rule']}>
            <div  className={style['rule-condition']}>
              <div className={style.enlightment}>If</div>
              <div className={style.title}>Conditions de la règle</div>

              <div>
                <Input className={style.input}
                       label="Type de noeud à tester"
                       value={this.state.rules[index].statement.nodeType || ""}
                       onChange={(value) => {
                              return this._handleUpdateStatementInputChange(index, 'nodeType', value);
                            }}
                       onBlur={this._handleInputUpdated.bind(this, index)}
                       onKeyPress={this._handleKeyPress.bind(this)}
                />

                <Input className={style.input}
                       label="Nom de la propriété du noeud à tester"
                       value={this.state.rules[index].statement.field || ""}
                       onChange={(value) => {
                              return this._handleUpdateStatementInputChange(index, 'field', value);
                            }}
                       onBlur={this._handleInputUpdated.bind(this, index)}
                       onKeyPress={this._handleKeyPress.bind(this)}
                />

                <Input className={style.input}
                       label="Expression régulière de test."
                       value={this.state.rules[index].statement.criteria || ""}
                       onChange={(value) => {
                              return this._handleUpdateStatementInputChange(index, 'criteria', value);
                            }}
                       onBlur={this._handleInputUpdated.bind(this, index)}
                       onKeyPress={this._handleKeyPress.bind(this)}
                />
              </div>
            </div>


            <div  className={style['rule-consequence']}>
              <div className={style.enlightment}>Then</div>
              <div className={style.title}>Conséquence de la règle</div>

              <div>
                <div className={style['node-thumb']}>
                  <div className={style.inner}>
                    <div className={style['node-title']}>
                      { this.state.rules[index].statement.nodeType }
                    </div>

                    <div className={style['node-properties']}>
                      { this.state.rules[index].statement.field } <span className={style.match}>MATCHES</span> "{ this.state.rules[index].statement.criteria }"
                    </div>
                  </div>
                </div>

                <div className={style['node-relation']}>
                  TAGGING_OBJECT
                </div>

                <div className={style['node-thumb']}>
                  <div className={style.inner}>
                    <div className={style['node-title']}>
                      Tagging
                    </div>
                    <div className={style['node-properties']}>
                      autoValidation : true
                    </div>
                  </div>
                </div>

                <div className={style['node-relation']}>
                  TAGGING_SUBJECT
                </div>

                <div className={style['node-thumb']}>
                  <div className={style.inner}>
                    <div className={style['node-title']}>
                      Concept
                    </div>

                    <div className={style['node-value']}>
                      { concept.prefLabels.edges[0].node.value}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            { index == this.state.pendingRuleIndex ? (
              <IconButton className={style['save-button']} icon="save" onClick={this._handleCommitUpdateRule.bind(this, index)} />
            ) : null}

            <IconButton className={style['remove-button']} icon="close" onClick={this._handleDeleteRule.bind(this, index)} />
          </div>
        ))}

        <Button className={style['add-button']}
                onClick={this._handleAddRule}
                icon='add' label='Ajouter une règle' flat />
      </section>
    )
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      e.target.blur();
    }
  };

  _handleUpdateStatementInputChange(ruleIndex, ruleProperty, value) {
    var {rules} = this.state;

    rules[ruleIndex] = {
      ...rules[ruleIndex],
      statement: {
        ...rules[ruleIndex].statement,
        [ruleProperty]: value
      }
    };

    this.setState({rules});
  }

  _handleUpdateInputChange(ruleIndex, ruleProperty, value) {
    var {rules} = this.state;

    rules[ruleIndex] = {
      ...rules[ruleIndex],
      [ruleProperty]: value
    };

    this.setState({rules});
  }

  _handleAddRule = () => {
    var rules = this.state.rules,
      pendingRule = {
        statement : {
          nodeType : '',
          field : '',
          fieldType : 'STRING',
          criteria : ''
        }
      };

    rules.push(pendingRule);

    this.setState({
      rules,
      pendingRuleIndex : rules.length - 1,
      pendingFocusedIndex : rules.length - 1
    });
  };

  _handleDeleteRule = (index) => {
    let rules = this.state.rules;

    rules.splice(index, 1);

    console.log(rules);

    this.setState({
      rules
    }, () => {
      console.log(this.state.rules);
      this.updateRules();
    });
  };

  _handleCommitUpdateRule = (index) => {
    const rules = this.state.rules;

    // Case of newly created rule pending save.
    if (index == this.state.pendingRuleIndex) {
      this.setState({
        pendingRuleIndex: null
      });
    }

    this.updateRules();

  };

  _handleInputUpdated = (index) => {
    if (index != this.state.pendingRuleIndex) {
      this.updateRules();
    }
  };

  updateRules(){
    let {concept} = this.props;
    let {rules} = this.state;

    this.props.showPreloader();

    this.props.relay.commitUpdate(new UpdateRulesMutation({
        concept,
        rules: rules.map(({statement}) => {
          let {nodeType, field, fieldType, criteria} = statement;

          return {
            statement : {nodeType, field, fieldType, criteria}
          };
        })
      }),
      {
        onSuccess: () => {
          this.props.hidePreloader();
          this.props.showSnackbar("Les règles ont bien été modifiées");
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      });
  }
}

export default Relay.createContainer(ViewerTabRules, {
  fragments: {
    concept: () => Relay.QL`
      fragment on Concept {
        id
        prefLabels(first: 50){
          edges{
            node{
              value
              lang
            }
          }
        }
        rules{
          statement{
            nodeType
            field
            fieldType
            criteria
          }
        }
        ${UpdateRulesMutation.getFragment('concept')}
      }
    `
  }
});