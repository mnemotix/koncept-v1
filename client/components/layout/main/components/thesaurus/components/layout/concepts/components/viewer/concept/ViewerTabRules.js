/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Relay from 'react-relay';
import classnames from 'classnames';

import {FontIcon, Button, IconButton} from 'react-toolbox/lib'

import Rules from './components/rules/Rules';

import style from './style.scss';

class ViewerTabRules extends React.Component {
  render() {
    return (
      <section className={classnames(style.section, style['rules-section'])}>
        <h4 className={style['section-title']}>
          <FontIcon className={style['section-icon']} value="location_searching"/> Règles

          <div className={style['section-subtitle']}>
            <p>
              Les règles sont liées au concept et utilisées par le backend pour créer des relations automatiques entre le concept
              et les objets composants l'écosystème des applications liées.
            </p>
          </div>
        </h4>

        <div className={style['section-separator']}></div>

        <Rules concept={this.props.concept}/>
      </section>
    );
  }
}

export default Relay.createContainer(ViewerTabRules, {
  fragments: {
    concept: () => Relay.QL`
      fragment on Concept {
        ${Rules.getFragment('concept')}
      }
    `
  }
});