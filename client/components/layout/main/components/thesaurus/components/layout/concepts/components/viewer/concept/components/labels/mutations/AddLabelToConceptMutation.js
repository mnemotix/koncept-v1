/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addLabelToConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddLabelToConceptPayload {
        newLabelEdge
        concept{
          prefLabels
          altLabels
          hiddenLabels
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'concept',
        parentID: this.props.concept.id,
        connectionName: this.props.labelType,
        edgeName: 'newLabelEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      labelType: this.props.labelType,
      lang: this.props.lang,
      value: this.props.value
    };
  }
}
