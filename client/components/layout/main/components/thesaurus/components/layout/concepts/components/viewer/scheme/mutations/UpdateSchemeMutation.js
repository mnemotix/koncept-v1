/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    scheme: () => Relay.QL`
      fragment on Scheme {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{updateScheme}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateSchemePayload {
        scheme{
          id
          ...on Scheme{
            title
            description
            color
          }
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          scheme: this.props.scheme.id
        }
      }
    ];
  }

  getVariables() {
    return {
      title: this.props.title,
      description: this.props.description,
      color: this.props.color,
      schemeId: this.props.scheme.id
    };
  }
}
