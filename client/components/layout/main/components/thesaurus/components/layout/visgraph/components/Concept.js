/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 28/04/2016
 */

import React from 'react';
import Relay from 'react-relay';

import vis from 'vis';

import ConceptList from './ConceptList';

class Concept extends React.Component {

  static contextTypes = {
    network: React.PropTypes.any
  };

  static propTypes = {
    nodesDataset: React.PropTypes.instanceOf(vis.DataSet),
    edgesDataset: React.PropTypes.instanceOf(vis.DataSet),
    selectedNodes: React.PropTypes.array
  };

  static defaultProps = {
    selectedNodes: []
  };

  state = {
    expand : false,
    loaded : false
  };

  componentDidUpdate(){
    if (!this.state.loaded && this.isConceptSelected() && !this.state.expand) {
      this.setState({
        expand: true
      }, () => {
        this.props.relay.setVariables({loaded: true}, ({done}) => {
          if (done) {
            this.setState({
              loaded: true
            });
          }
        })
      });
    }
  }

  isConceptSelected() {
    const {selectedNodes, concept} = this.props;
    return selectedNodes.indexOf(concept.id) > -1;
  }

  render() {
    let {concept, nodesDataset, edgesDataset, level, selectedNodes, color} = this.props;

    if(!nodesDataset.get(concept.id)) {
      var label = concept.prefLabels.edges.find(({node: label}) => label.lang == 'fr');

      nodesDataset.add({
        id: concept.id,
        label: label ? label.node.value : "ROOT",
        level: level,
        color: {
          background: color,
          border: concept.childrenCount > 0 ? "#FFF" : "",
          highlight: {
            background: color
          },
          hover: {
            background: color
          }
        },
        labelHighlightBold: false,
        font:{
          color: "#FFF",
          size: 12
        }
      });
    }

    {concept.related.edges.map((edge) => {
      let id = `${concept.id}-${edge.node.id}-related`;

      if(!edgesDataset.get(id)) {
        edgesDataset.add({
          id ,
          from: concept.id,
          to: edge.node.id,
          color:'#ef9a9a',
          font :{ color:'#ef9a9a', size: 11},
          label: 'related'
        });
      }
    })}

    if(this.props.relay.variables.loaded) {
      return <ConceptList concept={concept}
                          nodesDataset={nodesDataset}
                          edgesDataset={edgesDataset}
                          level={level+1}
                          selectedNodes={selectedNodes}
                          color={color}/>
    } else {
      return null;
    }
  }
}

export default Relay.createContainer(Concept, {
  initialVariables:{
    loaded: false
  },
  fragments: {
    concept: ({loaded}) => {
      return Relay.QL`
      fragment on Concept {
        id
        prefLabels(first: 50){
          edges{
            node{
              id
              lang
              value
            }
          }
        }
        childrenCount
        ${ConceptList.getFragment('concept').if(loaded)},
        related(first: 1000){
          edges{
            node {
              id
            }
          }
        }
      }
    `
    }
  }
});