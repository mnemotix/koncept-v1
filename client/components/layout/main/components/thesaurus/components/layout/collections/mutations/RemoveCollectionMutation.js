/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeCollection}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveCollectionPayload {
        deletedId,
        thesaurus{
          collections
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'NODE_DELETE',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'collections',
        deletedIDFieldName: 'deletedId'
      }
    ];
  }

  getVariables() {
    return {
      collectionId: this.props.collectionId,
      thesaurusId: this.props.thesaurus.id
    };
  }
}
