/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import ConfirmDialog from '../../../../../../../../../dialog/ConfirmDialog';

export default class extends ConfirmDialog {
  static defaultProps = {
    validateLabel: "Changer",
    cancelLabel: "Annuler"
  };
}