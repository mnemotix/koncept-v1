/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import { browserHistory } from 'react-router'

import {
  MenuItem, MenuDivider, ListItem, FontIcon,
  Button, Input, Tooltip, Link
} from 'react-toolbox';

import classnames from 'classnames';
import keyboard from 'keyboardjs';

import IconMenu from '../../../../../../../../../menu/IconMenu';
import CreateDialog from '../dialogs/CreateDialog';
import RemoveDialog from '../dialogs/RemoveDialog';

import AddSchemeTopConceptMutation from './mutations/AddSchemeTopConceptMutation';
import RemoveSchemeMutation from './mutations/RemoveSchemeMutation';
import RemoveTopSchemeMutation from './mutations/RemoveTopSchemeMutation';

import style from './style.scss';

const TooltipButton = Tooltip(Button);
const TooltipLink = Tooltip(Link);

import {HandledErrorMixin} from '../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class Concept extends React.Component {
  static propTypes = {
    onNodeExpanded: React.PropTypes.func.isRequired,
    onNodeRetracted: React.PropTypes.func.isRequired,
    onNodeEntered: React.PropTypes.func.isRequired,
    onCenterViewOnConcept: React.PropTypes.func.isRequired,
    isExpanded: React.PropTypes.bool,
    isFocused: React.PropTypes.bool
  };

  static defaultProps = {
    isFocused: false,
    isExpanded: false
  };

  state = {
    dialogCreateActive: false,
    dialogRemoveActive: false,
    creatingConcept: {}
  };

  clearKeyboadListeners(){
    keyboard.unbind('right', this._handleExpand);
  }

  initKeyboardListeners(){
    this.clearKeyboadListeners();

    keyboard.withContext('concept-tree', () => {
      if (this.props.isFocused) {
        keyboard.bind('enter', this._handleToggleNodeEntered);
        keyboard.bind('ctrl + q', this._handleToggleConceptCreateDialog);
        keyboard.bind(['del', 'command + backspace'], this._handleToggleSchemeRemoveDialog);

        if(this.props.scheme.topConceptsCount > 0) {
          keyboard.bind('right', this._handleExpand);
        }
      }
    });
  }

  componentDidMount(){
    this.initKeyboardListeners();

    if (this.props.isFocused) {
      this.props.onCenterViewOnConcept(ReactDOM.findDOMNode(this));
    }
  }

  componentDidUpdate() {
    this.initKeyboardListeners();
  }

  componentWillUpdate(nextProps){
    if (nextProps.isFocused && nextProps.isFocused !== this.props.isFocused) {
      this.props.onCenterViewOnConcept(ReactDOM.findDOMNode(this));
    }

    if (nextProps.scheme.topConceptsCount === 0) {
      this._handleRetract();
    }
  }

  renderDialogs(){
    return (
      <div>
        <CreateDialog
          active={this.state.dialogCreateActive}
          title='Nouveau concept'
          inputs={this.state.creatingConcept}
          relationTypes={this.relationTypes}
          onInputChange={this._handleCreateInputChange.bind(this)}
          onCreate={this.addSchemeTopConcept.bind(this)}
          onCancel={this._handleToggleConceptCreateDialog.bind(this)}
          loading={this.state.loading}
        />

        <RemoveDialog
          active={this.state.dialogRemoveActive}
          title={`Supprimer le schéma ${this.props.title}`}
          message={`Êtes-vous sûr de vouloir supprimer le schéma ?`}
          onOverlayClick={ this._handleToggleSchemeRemoveDialog }
          onValidate={this.removeScheme.bind(this)}
          onCancel={this._handleToggleSchemeRemoveDialog.bind(this)}
          loading={this.state.loading}
        />
      </div>
    );
  }

  renderMenu() {
    const {concept, isCut, thesaurus, router} = this.props;

    return (
      <IconMenu
        className={style['icon-menu']} icon='more_vert' menuRipple position="topLeft"
        onShow={this._handleToggleShowMenu}
        onHide={this._handleToggleShowMenu}
      >
        <MenuItem className='menu-item-edit-concept'
                  value='zoom_in' icon='remove_red_eye' caption='Éditer le schéma'
                  onClick={this._handleToggleNodeEntered}
        >
          <span className={style.shortcut}>(Double Clic ou Enter)</span>
        </MenuItem>

        <MenuItem className='menu-item-create-concept'
                  value='add' icon='add_circle' caption='Ajouter un concept'
                  onClick={this._handleToggleConceptCreateDialog}
        >
          <span className={style.shortcut}>(Ctrl+Q)</span>
        </MenuItem>

        <MenuDivider />

        <MenuItem className='menu-item-remove-concept'
                  value='delete' icon='delete' caption='Supprimer'
                  onClick={this._handleToggleSchemeRemoveDialog}
        >
          <span className={style.shortcut}>(Del)</span>
        </MenuItem>
      </IconMenu>
    );
  }

  render() {
    const {scheme, isExpanded, isFocused, isCut} = this.props;

    let className = classnames(style.scheme, {
      [style.focused]: isFocused,
      [style.expanded] : isExpanded,
      [style.cut] : isCut
    });

    return (
      <div className={className}>
        <div ref="inner" className={style.inner} style={{backgroundColor: scheme.color}} onDoubleClick={ this._handleToggleNodeEntered}>
          <div className={style.headline}>
            <div className={style.label}>{scheme.title || scheme.uri}</div>

            {this.renderMenu()}

            { isExpanded ? (
              <TooltipButton className={style['icon-close-hierarchy']}
                             tooltip="Cacher la hiérarchie"
                             icon='chevron_right'
                             onClick={this._handleRetract}
              />
            ) : ''}
          </div>
        </div>

        { scheme.topConceptsCount > 0 ? (
          <TooltipLink className={style['children-preview']} tooltip={scheme.topConceptsCount > 1 ? `${scheme.topConceptsCount} concepts supérieurs` : `${scheme.topConceptsCount} concept supérieur`}>
            {(new Array(Math.min(scheme.topConceptsCount, 10))).fill().map((_, index) => (
              <div key={index} className={style['child-preview']} onClick={this._handleExpand}></div>
            ))}
          </TooltipLink>
        ) : ''}

        {this.renderDialogs()}
      </div>
    );
  }

  _handleExpand = () => {
    if (!this.props.isExpanded) {
      this.props.onNodeExpanded(this.props.scheme.id);
    }
  };

  _handleRetract = () => {
    if (this.props.isExpanded) {
      this.props.onNodeRetracted(this.props.scheme.id);
    }
  };

  _handleToggleNodeEntered = (e) => {
    if (e) {
      e.preventDefault();
    }

    if (!this.menuShowing) {
      this.props.onNodeEntered(this.props.scheme.id, ReactDOM.findDOMNode(this.refs.inner));
    }
  };

  _handleToggleDialog = (e, stateKey) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    this.toggleDialog(stateKey)
  };


  toggleDialog(stateKey){
    keyboard.setContext(!this.state[stateKey] ? 'dialog' : 'concept-tree');

    this.setState({
      [stateKey] : !this.state[stateKey]
    });
  }

  _handleToggleConceptCreateDialog = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.setState({ creatingConcept : {}});
    this._handleToggleDialog(e, 'dialogCreateActive');
  };

  _handleCreateInputChange(value, e) {
    let fields = this.state.creatingConcept || {};

    this.setState({
      creatingConcept: {
        ...fields,
        [e.target.name]: value
      }
    });
  }

  _handleToggleSchemeRemoveDialog = (e) => {
    if (e) {
      e.preventDefault();
    }

    this._handleToggleDialog(e, 'dialogRemoveActive');
  };
  
  /**
   * Add a top concept
   */
  addSchemeTopConcept(){
    this.setLoadingState();

    this.props.relay.commitUpdate(
      new AddSchemeTopConceptMutation({
        scheme: this.props.scheme,
        thesaurus: this.props.thesaurus,
        topConceptLabel: this.state.creatingConcept.label
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('Le concept a été ajouté');
          this.clearLoadingState();
          this._handleToggleConceptCreateDialog();
        },
        onFailure: (transaction) => {
          this.clearLoadingState();
          this._handleToggleConceptCreateDialog();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  /**
   * Create a scheme
   */
  removeScheme = () => {
    this.setLoadingState();

    this._handleRetract();

    this.props.relay.commitUpdate(
      new RemoveTopSchemeMutation({
        scheme: this.props.scheme,
        thesaurus: this.props.thesaurus
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('Le schéma a été supprimé');
          this.clearLoadingState();
          this._handleToggleSchemeRemoveDialog();
        },
        onFailure: (transaction) => {
          this.clearLoadingState();
          this._handleToggleSchemeRemoveDialog();
          this.props.showMutationError(transaction);
        }
      }
    );
  };

  setLoadingState(){
    this.props.showPreloader();
  }

  clearLoadingState(){
    this.props.hidePreloader();
  }
}


export default Relay.createContainer(Concept, {
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        ${AddSchemeTopConceptMutation.getFragment('thesaurus')}
        ${RemoveTopSchemeMutation.getFragment('thesaurus')}
      }
    `,
    scheme: () => Relay.QL`
      fragment on Scheme {
        id
        title
        uri
        topConceptsCount
        color
        ${AddSchemeTopConceptMutation.getFragment('scheme')}
        ${RemoveTopSchemeMutation.getFragment('scheme')}
      }
    `
  }
});

