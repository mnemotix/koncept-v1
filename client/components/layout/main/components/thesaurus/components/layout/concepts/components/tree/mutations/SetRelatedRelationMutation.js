/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id,
        relatedCount
      }
    `,
    relatedConcept: () => Relay.QL`
      fragment on Concept {
        id,
        relatedCount
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{setRelatedRelation}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on SetRelatedRelationPayload {
        concept{
          relatedCount,
          related
        },
        relatedConcept{
          relatedCount,
          related
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          relatedConcept: this.props.relatedConcept.id
        }
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      relatedConceptId: this.props.relatedConcept.id
    };
  }
}
