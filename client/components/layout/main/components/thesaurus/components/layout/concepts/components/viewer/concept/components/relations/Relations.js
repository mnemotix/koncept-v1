/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 23/03/2016
 */

import React from 'react';
import Relay from 'react-relay';

import Input from 'react-toolbox/lib/input';
import {Button, IconButton} from 'react-toolbox/lib/button';

import SearchConceptsDialog from './components/search/SearchConceptsDialog';

import SwitchFromNarrowerToRelatedRelationMutation from '../lens/mutations/SwitchFromNarrowerToRelatedRelationMutation';
import SwitchFromRelatedToNarrowerRelationMutation from '../lens/mutations/SwitchFromRelatedToNarrowerRelationMutation';

import classnames from 'classnames'

import style from './style.scss';

class Relations extends React.Component {

  static propTypes = {
    relationType: React.PropTypes.oneOf(['narrowers', 'related']),
    relationName: React.PropTypes.string
  };

  state = {
    addDialogActive: false
  };

  getPrefLabel(concept){
    if (!concept.prefLabels) return;

    let prefLabel = concept.prefLabels.edges.find(({node: prefLabel}) => prefLabel.lang == 'fr') || concept.prefLabels.edges[0];
    return prefLabel.node.value;
  }

  render(){
    const {concept, relationType, thesaurus} = this.props;

    return (
      <div>
        <div className={classnames({[style.layout]: true, [style[relationType]]: true})}>
          <div>
            <div className={style.concept}>
              <div className={style.inner}>
                { this.getPrefLabel(concept)  }
              </div>
              <span className={style['relation-line']} />
            </div>
          </div>
          <div className={style.relations}>
            {this.renderRelations()}
          </div>
        </div>

        <div className={style.actions}>
          <Button className={style['add-button']}
                  onClick={() => this.setState({addDialogActive : true})}
                  icon='add' label='Ajouter une relation avec un concept existant' raised />

          <SearchConceptsDialog
            concept={concept}
            thesaurus={thesaurus}
            relationType={relationType}
            title="Ajouter une relation avec un concept existant"
            active={this.state.addDialogActive}
            onCancel={() => this.setState({addDialogActive : false})}
            onSelect={() => this.setState({addDialogActive : false})}
          />
        </div>
      </div>
    );
  }

  renderRelations(){
    const {concept, relationType, relationName} = this.props;

    if(concept[relationType].edges.length == 0) {
      return (
        <div className={style['no-relation']}> Aucune relation pour le moment </div>
      );
    }

    return concept[relationType].edges.map(edge => (
      <div className={style.relation} key={'narrower-' + edge.node.id}>
        <div className={style.wrapper}>
          <span className={style['relation-line']} />
          <div className={style['relation-details']} >
            {relationName}
          </div>
          <span className={style['relation-line']} />
          <div className={style.concept}>
            <div className={style.inner}>
              { this.getPrefLabel(edge.node)  }
            </div>
          </div>
        </div>
      </div>
    ));
  }
}

export default Relay.createContainer(Relations, {
  fragments: {
    thesaurus: () => {
      return Relay.QL`
        fragment on Thesaurus {
          ${SearchConceptsDialog.getFragment('thesaurus')}
        }`
    },
    concept: () => Relay.QL`
      fragment on Concept {
        id,
        prefLabels(first: 50){
          edges{
            node{
              value
              lang
            }
          }
        }
        narrowers(first: 1000){
          edges{
            node{
              id,
              prefLabels(first: 50){
                edges{
                  node{
                    value
                    lang
                  }
                }
              }
              ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('targetConcept')},
              ${SwitchFromNarrowerToRelatedRelationMutation.getFragment('targetConcept')}
            }
          }
        },
        related(first: 1000){
          edges{
            node{
              id,
              prefLabels(first: 50){
                edges{
                  node{
                    value
                    lang
                  }
                }
              }
              broader{
                id,
                ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('targetParentConcept')}
              },
              ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('targetConcept')},
              ${SwitchFromNarrowerToRelatedRelationMutation.getFragment('targetConcept')}
            }
          }
        },
        ${SwitchFromRelatedToNarrowerRelationMutation.getFragment('sourceConcept')},
        ${SwitchFromNarrowerToRelatedRelationMutation.getFragment('sourceConcept')},
        ${SearchConceptsDialog.getFragment('concept')}
      }
    `
  }
});