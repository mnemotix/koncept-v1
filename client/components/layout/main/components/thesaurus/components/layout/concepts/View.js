/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import classnames from 'classnames';
import Button from 'react-toolbox/lib/button';
import keyboard from 'keyboardjs';

import AffixWrapper from '../../../../../../../affix/AffixWrapper';
import Branches from './components/tree/Branches';

import style from './style.scss';
import {withRouter} from 'react-router';

@withRouter
class View extends React.Component {

  state = {
    savedLocation: null
  };

  componentDidUpdate(){
    this.switchKeyboardContext();
  }

  componentDidMount(){
    this.switchKeyboardContext();
  }

  switchKeyboardContext(){
    keyboard.setContext(!!this.props.children ? 'concept-viewer' : 'concept-tree');
  }

  render(){
    const {thesaurus, children} = this.props;

    return (

      <section className={style.concepts}>
        <div ref="tree" className={classnames({[style['hierarchy-wrap']] : true, [style.hidden] : !!children})}>
          <Branches
            thesaurus={thesaurus}
            onLocationChange={this._handleLocationChange}
            prefixKey="conceptsView"
            location={this.props.location}
          />
        </div>

        {!!children ? (
          <AffixWrapper affixClassName={style.affix}>
            <Button icon="close" floating primary
                    className={classnames({[style['close-viewer-button']] : true, [style.hidden] : !children})}
                    onClick={this._handleBackToHierarchy}
            />
          </AffixWrapper>
        ) : null}

        <div ref="animator"  className={classnames({[style.animator]: true, [style.fit]: !!children})}/>

        { children }
      </section>
    );
  }

  _handleLocationChange = (location) => {
    const {router} = this.props;

    location.state = {
      ...location.state,
      referer: this.props.location
    };

    return router.push(location);
  };

  _handleBackToHierarchy = () => {
    const {router, location} = this.props;

    if (location.state && location.state.referer){
      return router.push(location.state.referer);
    } else {
      return router.push({
        name: 'thesaurus',
        params: {
          thesoId: this.props.thesaurus.id,
        }
      });
    }

  };
}

export default Relay.createContainer(View, {
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id,
        ${Branches.getFragment('thesaurus')}
      }
    `
  }
});