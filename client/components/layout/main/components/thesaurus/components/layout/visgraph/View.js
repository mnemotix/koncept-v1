/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';

import FauxDOM from 'react-faux-dom';

import style from './style.scss';

import {Menu, MenuItem} from 'react-toolbox';

import * as d3 from 'd3';

import {d3Hoc} from        '../../../../../../../common/d3/d3Hoc';
import {preloaderHoc} from '../../../../../../../common/preloader/preloaderHoc';

import deepEqual from 'deep-equal';

import {withRouter} from 'react-router';

@d3Hoc
@preloaderHoc
@withRouter
class View extends React.Component {
  state = {
    updatingNode : null,
    selectedNode : null,
    selectedNodes: [],
    tree: <span/>,
    menuActive: false,
    menuPosition: {top: 0, left: 0}
  };

  svgMargin = {top: 20, right: 90, bottom: 30, left: 90};
  flatData = [];
  treeData;
  svg;
  hook;
  root;

  stratifyData() {
    const {thesaurus} = this.props,
      {topSchemes, columns} = thesaurus;

    this.flatData = [];

    this._addToFlatData(thesaurus.id, {
      id: thesaurus.id,
      label: thesaurus.title
    });

    topSchemes.edges.map(({node: topScheme}) => {
      this._addToFlatData(topScheme.id, {
        id: topScheme.id,
        label: topScheme.title,
        color: topScheme.color,
        hasChildren: topScheme.topConceptsCount > 0,
        parent: thesaurus.id
      });
    });

    columns.map(column => {
      let childrenProp;

      switch (column.__typename) {
        case "Concept":
          childrenProp = 'narrowers';
          break;

        case "Scheme":
          childrenProp = 'topConcepts';
          break;
      }

      column[childrenProp].edges.map(({node: concept}) => {
        this._addToFlatData(concept.id, {
          id: concept.id,
          label: concept.prefLabelForLang.value,
          parent: column.id,
          hasChildren: concept.childrenCount > 0
        });
      });
    });

    this.treeData = d3.stratify()
      .id(function (d) {
        return d.id;
      })
      .parentId(function (d) {
        return d.parent;
      })
      (this.flatData);

    // Assigns parent, children, height, depth
    this.root = d3.hierarchy(this.treeData, function (d) {
      return d.children;
    });
  }

  _addToFlatData(id, data) {
    let existingIndex = this.flatData.findIndex(node => node.id == id);

    if (existingIndex < 0) {
      this.flatData.push(data);
    } else {
      this.flatData[existingIndex] = Object.assign(this.flatData[existingIndex], data);
    }
  }

  componentDidMount() {
    this.stratifyData();
    this.generateTree();

    window.addEventListener('click', this._handleCloseMenu);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this._handleCloseMenu);
    d3.select(window).on('resize.updatesvg', null);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return !nextState.updatingNode || (!!nextState.updatingNode && !deepEqual(this.props.thesaurus, nextProps.thesaurus));
  }

  componentWillUpdate() {
    let updatingNode = this.state.updatingNode;

    if(this.state.updatingNode) {
      this.setState({updatingNode: null}, () => {
        this.stratifyData();
        this.updateTree(updatingNode);
      });
    }
  }

  updateTree(source){
    // Assigns the x and y position for the nodes
    let treeData = this.treemap(this.root),
        duration = 375;

    // Compute the new tree layout.
    let nodes = treeData.descendants(),
      links = treeData.descendants().slice(1);

    let maxDepth,
      childrenCountByDepth = [];

    nodes.forEach( (d) => {
      if(!maxDepth || maxDepth < d.depth){
        maxDepth = d.depth;
      }
      childrenCountByDepth[d.depth] = (childrenCountByDepth[d.depth] || 0) + 1;
    });

    let maxChildrenCount = Math.max(...childrenCountByDepth);

    if(maxChildrenCount * 25 > this.canvas.attr("height") ) {
      this.svgHeight = maxChildrenCount * 25;
      return this.updateCanvas(source);
    }

    // Normalize for fixed-depth.
    nodes.forEach( (d) => {
      d.hasChildren = d.data.data.hasChildren;

      if (maxDepth * 280 > this.canvas.attr("width") - (this.svgMargin.left + this.svgMargin.right)) {
        d.y = d.depth * (this.canvas.attr("width") - (this.svgMargin.left + this.svgMargin.right)) / maxDepth;
      } else {
        d.y = d.depth * 280;
      }

      let ancestors = d.ancestors();

      for(let ancestor of ancestors) {
        if (ancestor.data.data.color) {
          d.color = ancestor.data.data.color;
          break;
        }
      }
    });

    // Update the nodes...
    let node = this.svg.selectAll('g.node')
      .data(nodes, function (d) {
        return d.id || (d.id = d.data.id);
      });

    // Enter any new modes at the parent's previous position.
    let nodeEnter = node.enter()
      .append('g')
      .attr('class', 'node')
      .attr("transform", ()  => {
        return "translate(" + source.y0 + "," + source.x0 + ")";
      }).on('click', (d) => {
        d3.event.stopPropagation();
        if(d.hasChildren) {
          this._handleNodeClick(d);
        }
      })
      .on('contextmenu', function(d) {
        d3.event.stopPropagation();
        d3.event.preventDefault();

        showMenu(d, ReactDOM.findDOMNode(this).getBoundingClientRect());
      });

    let showMenu = this._handleShowMenu.bind(this);

    // Add Circle for the nodes
    nodeEnter.append('circle')
      .attr('class', 'node')
      .attr('r', 1e-6)
      .style("stroke", function (d) {
        return d.color ?  d3.color(d.color).darker(1.5) : "#03A9F4";
      })
      .style("fill", function (d) {
        return d.hasChildren ? d.color ? d.color : "#B3E5FC" : "#fff";
      });

    // Add labels for the nodes
    ["stroke", ""].map((className) => {
      let text = nodeEnter.append('text')
        .attr("dy", ".35em")
        .attr("x", 13)
        .attr("text-anchor", "start")
        .attr("class", className)
        .text(function (d) {
          return d.data.data.label;
        });
    });


    // UPDATE
    let nodeUpdate = nodeEnter.merge(node);

    // Transition to the proper position for the node
    nodeUpdate
      .transition()
      .duration(duration)
      .delay(duration)
      .attr("transform", function (d) {
        return "translate(" + d.y + "," + d.x + ")";
      })
      .call(this.hook);

    // Update the node attributes and style
    nodeUpdate
      .select('circle.node')
      .attr('r', 5)
      .attr('class', function (d) {
        return d.hasChildren ? 'has-children' : ''
      });


    // Remove any exiting nodes
    let nodeExit = node.exit().transition()
      .duration(duration)
      .attr("transform", function () {
        return "translate(" + source.y + "," + source.x + ")";
      })
      .remove()
      .call(this.hook);

    // On exit reduce the node circles size to 0
    nodeExit.select('circle')
      .attr('r', 1e-6);

    // On exit reduce the opacity of text labels
    nodeExit.select('text')
      .style('fill-opacity', 1e-6);

    // ****************** links section ***************************

    // Update the links...
    let link = this.svg.selectAll('path.link')
      .data(links, function (d) {
        return d.id;
      });

    // Enter any new links at the parent's previous position.
    let linkEnter = link.enter().insert('path', "g")
      .attr("class", "link")
      .style("stroke", function (d) {
        if (d.color) {
          let color = d3.color(d.color);
          color.opacity = 0.5;
          return color;
        } else {
          return "#CCC";
        }
      })
      .attr('d', function () {
        let o = {x: source.x0, y: source.y0};
        return diagonal(o, o)
      });

    // UPDATE
    let linkUpdate = linkEnter.merge(link);

    // Transition back to the parent element position
    linkUpdate.transition()
      .duration(duration)
      .delay(duration)
      .attr('d', function (d) {
        return diagonal(d, d.parent)
      })
      .call(this.hook)
    ;

    // Remove any exiting links
    link.exit().transition()
      .duration(duration)
      .attr('d', function () {
        let o = {x: source.x, y: source.y};
        return diagonal(o, o)
      })
      .remove()
      .call(this.hook);

    // Store the old positions for transition.
    nodes.forEach(function (d) {
      d.x0 = d.x;
      d.y0 = d.y;
    });

    // Creates a curved (diagonal) path from parent to the child nodes
    function diagonal(s, d) {
      return `M ${s.y} ${s.x}
          C ${(s.y + d.y) / 2} ${s.x},
            ${(s.y + d.y) / 2} ${d.x},
            ${d.y} ${d.x}`;
    }
  }

  generateTree() {
    let faux = new FauxDOM.Element('div');

    this.hook = this.props.createD3Hook(this, faux, "tree");
    this.canvas = d3.select(faux).append("svg");
    this.svg = this.canvas.append("g");
    this.treemap = d3.tree();

    this.root.y0 = 0;

    d3.select(window).on('resize.updatesvg', this.updateCanvas.bind(this));

    this.updateCanvas(this.root);
  }

  updateCanvas(source) {
    let wrapper = ReactDOM.findDOMNode(this.refs.wrapper),
      margin = this.svgMargin,
      width = wrapper.getBoundingClientRect().width - margin.left - margin.right,
      height = this.svgHeight ? this.svgHeight : wrapper.getBoundingClientRect().height - margin.top - margin.bottom;

    this.root.x0 = height / 2;

    this.treemap.size([height, width]);

    this.canvas
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom);

    this.svg.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    this.updateTree(source);
  }

  render() {
    return (
      <div ref="wrapper" className={style.layout}>
        <div className={style.menu} style={this.state.menuPosition}>
          <Menu active={this.state.menuActive} position='auto'>
            <MenuItem  icon='expand_more' caption='Déplier' onClick={(e) => {
              e.stopPropagation();
              this._handleNodeClick(this.state.selectedNode);
            }}/>
            <MenuItem  icon='view_list'   caption='Voir dans la hiérarchie' onClick={(e) => {
              e.stopPropagation();
              this._handleShowInHierarchy(this.state.selectedNode);
            }}/>
          </Menu>
        </div>

        {this.state.tree}
      </div>
    );
  }

  _handleNodeClick = (d) => {
    let clickedId = d.data.id,
      {columnIds} = this.props.relay.variables;

    if (columnIds.includes(clickedId)) {
      // Get all ancestors
      d.descendants().map(node => {
        if (columnIds.includes(node.id)) {
          columnIds.splice(columnIds.indexOf(node.id), 1);
        }
      });
    } else {
      columnIds.push(clickedId);
    }

    this.setState({
      updatingNode: d,
      menuActive: false
    }, () => {
      this.props.showPreloader();

      this.props.relay.setVariables({columnIds}, ({done}) => {
        if(done) {
          this.props.hidePreloader();
          this.saveSelectedId(columnIds, false);
        }
      });
    });
  };

  _handleShowInHierarchy = (d) => {
    const {router, thesaurus} = this.props;

    let ancestors = d.ancestors(),
      ancestorIds = ancestors.slice(1, ancestors.length - 1).reverse().map(ancestor => ancestor.id);

    if (ancestors.length > 0) {
      router.push({
        name: "thesaurus",
        params: {
          thesoId: thesaurus.id
        },
        query: {
          cids  : ancestorIds.join('/'),
          fcpt : d.id,
          fcln: ancestorIds.length
        }
      });
    }
  };

  _handleCloseMenu = () => {
    this.setState({
      menuActive: false,
      selectedNode: null
    });
  };

  _handleShowMenu = (d, bbox) => {
    this.setState({
      selectedNode: d,
      menuActive: true,
      menuPosition: {
        top: bbox.top,
        left: bbox.left + bbox.width
      }
    });
  };

  /**
   * Save the hierarchy path in either URL or sessionHistory
   *
   * @param columnIds
   * @param forceFetch
   */
  saveSelectedId(columnIds, forceFetch = true){
    const savedSelectedConceptIds = columnIds.join('/'),
      {router} = this.props,
      location = router.createLocation(window.location);

    if (forceFetch) {
      this.props.showPreloader();

      this.props.relay.setVariables({columnIds}, (state) => {
        if (state.done) {
          this.props.hidePreloader();
        }
      });
    }

    sessionStorage.setItem(`${this.props.thesaurus.id}_selectedConceptIdFor_graphvis`, savedSelectedConceptIds);

    if (location.query.sids !=  savedSelectedConceptIds) {
      router.push(`${location.pathname}?sids=${savedSelectedConceptIds}`);
    }
  }

  /**
   * Init the hierarchy path to restore the layout
   */
  initSavedSelectedId(){
    const {router} = this.props;

    let location = router.createLocation(window.location),
      columnIds = location.query.sids;

    if(!columnIds){
      columnIds = sessionStorage.getItem(`${this.props.thesaurus.id}_selectedConceptIdFor_graphvis`);
    }

    if (columnIds) {
      this.saveSelectedId(columnIds.split('/'));
    }
  }
}

export default Relay.createContainer(View, {
  initialVariables: {
    first: 1000,
    columnIds: []
  },
  prepareVariables: (prevVariables) => {
    return {
      ...prevVariables,
      columnIds: [].concat(prevVariables.columnIds || [])
    }
  },
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
        title
        topSchemes(first: $first) {
          edges{
            node{
              id
              title
              topConceptsCount
              color
            }
          }
        }
        columns(ids: $columnIds) {
          id
          __typename

          ...on Concept{
            narrowers(first: $first){
              edges{
                node{
                  id
                  prefLabelForLang(lang: "fr") {
                    value
                  }
                  childrenCount
                }
              }
            }
          }

          ...on Scheme{
            topConcepts(first: $first){
              edges{
                node{
                  id
                  prefLabelForLang(lang: "fr") {
                    value
                  }
                  childrenCount
                }
              }
            }
          }
        }
      }
    `
  }
});