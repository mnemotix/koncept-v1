/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';

import {
  MenuItem, MenuDivider, ListItem, FontIcon,
  Button, Input, Tooltip, Link, IconButton
} from 'react-toolbox';

import classnames from 'classnames';
import keyboard from 'keyboardjs';

import IconMenu from '../../../../../../../../../menu/IconMenu';
import CreateDialog from '../dialogs/CreateDialog';
import RemoveDialog from '../dialogs/RemoveDialog';
import ChangeRelationDialog from '../dialogs/ChangeRelationDialog';
import Preloader from '../../../../../../../../../preloader/Preloader';

import CreateConceptAndSetNarrowerRelationMutation from './mutations/CreateConceptAndSetNarrowerRelationMutation';
import CreateConceptAndSetRelatedRelationMutation from './mutations/CreateConceptAndSetRelatedRelationMutation';
import SetNarrowerRelationMutation from './mutations/SetNarrowerRelationMutation';
import SetRelatedRelationMutation from './mutations/SetRelatedRelationMutation';
import RemoveConceptMutation from './mutations/RemoveConceptMutation';
import RemoveTopConceptMutation from './mutations/RemoveTopConceptMutation';
import MoveConceptMutation from './mutations/MoveConceptMutation';
import ValidateConceptMutation from '../viewer/concept/mutations/ValidateConceptMutation';
import ReplaceConceptMutation from '../viewer/concept/mutations/ReplaceConceptMutation';


import SearchConceptDialog from '../searchDialog/SearchConceptsDialog';

import style from './style.scss';

const TooltipLink = Tooltip(Link);
const TooltipButton = Tooltip(Button);
const TooltipIconMenu = Tooltip(IconMenu);

import {HandledErrorMixin} from '../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../common/preloader/preloaderHoc';
import {confirmDialogHoc} from       '../../../../../../../../../common/dialog/dialogsHoc';
import {withRouter} from 'react-router';

@withRouter
@HandledErrorMixin
@snackbarHoc
@preloaderHoc
@confirmDialogHoc
class Concept extends React.Component {
  static propTypes = {
    onNodeExpanded: React.PropTypes.func.isRequired,
    onNodeRetracted: React.PropTypes.func.isRequired,
    onNodeEntered: React.PropTypes.func.isRequired,
    onNodeCut: React.PropTypes.func.isRequired,
    onEmptyClipboard: React.PropTypes.func.isRequired,
    onCenterViewOnConcept: React.PropTypes.func.isRequired,
    onConceptClicked: React.PropTypes.func.isRequired,
    isFocused: React.PropTypes.bool,
    isLastExpanded: React.PropTypes.bool,
    inLastColumn: React.PropTypes.bool,
    onSelectConcept: React.PropTypes.func
  };

  state = {
    dialogCreateActive: false,
    dialogRemoveActive: false,
    dialogChangeRelationActive: false,
    creatingConcept : {},
    loading: false,
    selected: false,
    showMenu: false,
    selectCallback: null,
    selectDialogTitle: ""
  };

  relationTypes = [{
    label: 'Relation hiérarchique • skos:narrower',
    value: 'narrower'
  },{
    label: 'Association • skos:related',
    value: 'related'
  }];

  getPrefLabelValueForCurrentLang(){
    return this.props.concept.label.value;
  }


  clearKeyboadListeners(){
    keyboard.unbind('ctrl + v', this._handlePasteConceptRequested);
    keyboard.unbind('right', this._handleExpand);
    keyboard.unbind('enter', this._handleToggleNodeEntered);
    keyboard.unbind('ctrl + q', this._handleToggleConceptCreateDialog);
    keyboard.unbind(['del', 'command + backspace'], this._handleToggleConceptRemoveDialog);
    keyboard.unbind('ctrl + x', this._handleCut);
  }

  initKeyboardListeners(){
    this.clearKeyboadListeners();

    keyboard.withContext('concept-tree', () => {
      if (this.props.isFocused || this.props.isCut) {
        keyboard.bind('ctrl + v', this._handlePasteConceptRequested);
      }

      if (this.props.isFocused) {
        keyboard.bind('enter', this._handleToggleNodeEntered);
        keyboard.bind('ctrl + q', this._handleToggleConceptCreateDialog);
        keyboard.bind(['del', 'command + backspace'], this._handleToggleConceptRemoveDialog);
        keyboard.bind('ctrl + x', this._handleCut);

        if(this.props.concept.childrenCount > 0) {
          keyboard.bind('right', this._handleExpand);
        }
      }
    });
  }

  componentDidMount(){
    this.initKeyboardListeners();

    if (this.props.isFocused) {
      this.props.onCenterViewOnConcept(ReactDOM.findDOMNode(this));
    }
  }

  componentDidUpdate(){
    this.initKeyboardListeners();

    // Check if there is still children while expanded, otherwiser request retraction.
    // Usefull after removal mutation.
    if (this.props.isExpanded && this.props.concept.childrenCount === 0) {
      this._handleRetract();
    }
  }

  componentWillUnmount(){
    this.clearKeyboadListeners();
  }

  componentWillUpdate(nextProps){
    if (nextProps.isFocused && nextProps.isFocused !== this.props.isFocused) {
      this.props.onCenterViewOnConcept(ReactDOM.findDOMNode(this));
    }

    if (nextProps.concept.childrenCount === 0) {
      this._handleRetract();
    }
  }

  render() {
    const {concept, thesaurus, relationTypeLabel, isSelected, isExpanded, isFocused, isCut, router} = this.props;

    let classNames = classnames({
        [style.concept]: true,
        [style.selected] : isFocused || isSelected || this.state.showMenu,
        [style.focused]: isFocused,
        [style.expanded] : isExpanded,
        [style.cut] : isCut,
        [style[relationTypeLabel]]: true,
        [style.isDraft]: concept.isDraft
      }),
      children, related;

    let innerConceptStyle = {
      borderColor: concept.topConceptOf ? concept.topConceptOf.color : null
    };

    if(concept.childrenCount > 0) {
      children = Array.from((new Array(Math.min(concept.childrenCount, 10))).keys());
    }

    if (concept.relatedCount > 0) {
      related  = Array.from((new Array(Math.min(concept.relatedCount, 10))).keys());
    }


    return (
      <div className={classNames}>
        <div className={style['relation-type']}>
          {relationTypeLabel}
        </div>

        <div ref="inner" style={innerConceptStyle} className={style.inner}  onClick={() => this.props.onConceptClicked(concept.id)} onDoubleClick={ this._handleToggleNodeEntered}>

          <div className={style.headline}>
            {concept.isDraft ? this.renderIsDraftMenu() : null}

            <div className={style.label}>{this.getPrefLabelValueForCurrentLang()}</div>

            {this.renderMenu()}

            { isExpanded ? (
              <TooltipButton className={style['icon-close-hierarchy']}
                             tooltip="Cacher la hiérarchie"
                             icon='chevron_right'
                             onClick={this._handleRetract}
              />
            ) : (
              <div className={style.relations}>

              </div>
            )}
          </div>

          {this.renderDialogs()}

        </div>

        { concept.childrenCount > 0 ? (
          <TooltipLink className={style['children-preview']} tooltip={concept.childrenCount > 1 ? `${concept.childrenCount} concepts descendants` : `${concept.childrenCount} concept descendant`}>
            {children.map((index) => (
              <div key={`${concept.id}_child_${index}`} className={style['child-preview']} onClick={this._handleExpand}></div>
            ))}
          </TooltipLink>
        ) : ''}

        <SearchConceptDialog active={!!this.state.selectConceptCallback}
                             thesaurus={thesaurus}
                             title={this.state.selectDialogTitle}
                             onCancel={() => this.setState({selectConceptCallback: null})}
                             onSelect={this.state.selectConceptCallback || (() => {})}
        />
      </div>
    );
  }

  renderDialogs(){
    return (
      <div>
        <CreateDialog
          active={this.state.dialogCreateActive}
          title='Nouveau concept'
          inputs={this.state.creatingConcept}
          relationTypes={this.relationTypes}
          onInputChange={this._handleCreateInputChange.bind(this)}
          onCreate={this.createConceptAndSetRelationship.bind(this)}
          onCancel={this._handleToggleConceptCreateDialog.bind(this)}
          loading={this.state.loading}
        />

        <RemoveDialog
          active={this.state.dialogRemoveActive}
          title={`Supprimer le concept ${this.getPrefLabelValueForCurrentLang()}`}
          message={`Êtes-vous sûr de vouloir supprimer le concept ${this.getPrefLabelValueForCurrentLang()} ?`}
          onOverlayClick={ this._handleToggleConceptRemoveDialog }
          onValidate={this.removeConcept.bind(this)}
          onCancel={this._handleToggleConceptRemoveDialog.bind(this)}
          loading={this.state.loading}
        />

        <ChangeRelationDialog
          active={this.state.dialogChangeRelationActive}
          title={`Changer le type de relation skos:narrower => skos:related`}
          message={(
            <div>
              <p>
                Êtes vous sûr de vouloir transformer la relation hiérarchie en une relation associative ?
              </p>
              <br/>
              <p>Cette action déplacera le concept dans la branche supérieure du thesaurus</p>
            </div>
          )}
          onOverlayClick={ this._handleToggleConceptChangeRelationDialog }
          onValidate={this.changeRelationType.bind(this)}
          onCancel={this._handleToggleConceptChangeRelationDialog.bind(this)}
          loading={this.state.loading}
        />
      </div>
    );
  }

  renderIsDraftMenu(){
    return (
      <TooltipIconMenu
        className={style.isDraftButton}
        icon="verified_user"
        menuRipple position={this.props.inLastColumn ? "topRight" : "topLeft"}
        tooltip={"Ce concept est une proposition et nécéssite d'être validé"}
        tooltipPosition="top"
      >
        <MenuItem className='menu-item-edit-concept'
                  icon='thumb_up'
                  caption='Valider le concept'
                  onClick={() => this.validateConcept()}
        />
        <MenuItem className='menu-item-edit-concept'
                  icon='edit'
                  caption='Editer le concept'
                  onClick={this._handleToggleNodeEntered}
        />
        <MenuItem icon='call_made'
                  caption='Déplacer le concept'
                  onClick={() => {
                    this.setState({
                      selectDialogTitle: "Sélectionner le concept parent sous lequel déplacer ce concept.",
                      selectConceptCallback: (concept) => {
                        this.setState({
                          selectConceptCallback: null,
                          selectDialogTitle: ""
                        });

                        this.moveConceptUnder(concept);
                      }
                    })
                  }}
        />
        <MenuItem icon='swap_horiz'
                  caption='Substituer par un autre concept'
                  onClick={this._handleSubstituteConcept}
        />
        <MenuItem className='menu-item-edit-concept'
                  icon='thumb_down'
                  caption='Invalider le concept'
                  onClick={() => this.invalidateConcept()}
        />
      </TooltipIconMenu>
    );
  }


  renderMenu() {
    const {concept, isCut, thesaurus, router} = this.props;

    return (
      <IconMenu
                className={style['icon-menu']} icon='more_vert' menuRipple position={this.props.inLastColumn ? "topRight" : "topLeft"}
                onShow={this._handleToggleShowMenu}
                onHide={this._handleToggleShowMenu}
      >
        <MenuItem className='menu-item-edit-concept'
                  value='zoom_in' icon='edit' caption='Éditer le concept'
                  onClick={this._handleToggleNodeEntered}
        >
          <span className={style.shortcut}>(Double Clic ou Enter)</span>
        </MenuItem>

        <MenuItem className='menu-item-create-concept'
                  value='add' icon='add_circle' caption='Ajouter un concept'
                  onClick={this._handleToggleConceptCreateDialog}
        >
          <span className={style.shortcut}>(Ctrl+Q)</span>
        </MenuItem>

        <MenuDivider />

        <MenuItem icon='call_made'
                  caption='Déplacer le concept'
                  onClick={() => {
                    this.setState({
                      selectDialogTitle: "Sélectionner le concept parent sous lequel déplacer ce concept.",
                      selectConceptCallback: (concept) => {
                        this.setState({
                          selectConceptCallback: null,
                          selectDialogTitle: ""
                        });

                        this.moveConceptUnder(concept);
                      }
                    })
                  }}
        />

        <MenuDivider />

        <MenuItem className='menu-item-relationships'
                  value='change_relations' icon='device_hub' caption='Modifier les relations associées'
                  onClick={() => {router.push({
                    name: 'concept-relations',
                    params: {
                      thesoId: thesaurus.id,
                      conceptId: concept.id
                    }
                  })}}
        />

        {/*
        <MenuItem className='menu-item-from-narrower-to-related'
                  value='change_relation' icon='swap_calls' caption='Transformer la relation hiérarchique en associative'
                  onClick={this._handleToggleConceptChangeRelationDialog}
        />
        */}
        <MenuDivider />

        <MenuItem className='menu-item-remove-concept'
                  value='delete' icon='delete' caption='Supprimer'
                  onClick={this._handleToggleConceptRemoveDialog}
        >
          <span className={style.shortcut}>(Del)</span>
        </MenuItem>
      </IconMenu>
    );
  }

  _handleToggleDialog = (e, stateKey) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    this.toggleDialog(stateKey)
  };


  toggleDialog(stateKey){
    keyboard.setContext(!this.state[stateKey] ? 'dialog' : 'concept-tree');

    this.setState({
      [stateKey] : !this.state[stateKey]
    });
  }

  _handleToggleConceptCreateDialog = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.setState({ creatingConcept : {}});
    this._handleToggleDialog(e, 'dialogCreateActive');
  };

  _handleToggleConceptRemoveDialog = (e) => {
    if (e) {
      e.preventDefault();
    }

    this._handleToggleDialog(e, 'dialogRemoveActive');
  };

  _handleToggleConceptChangeRelationDialog = (e) => {
    this._handleToggleDialog(e, 'dialogChangeRelationActive');
  };

  _handleCreateInputChange(value, e) {
    let fields = this.state.creatingConcept || {};

    this.setState({
      creatingConcept: {
        ...fields,
        [e.target.name]: value
      }
    });
  }

  _handleToggleNodeEntered = (e) => {
    if (e) {
      e.preventDefault();
    }

    if (!this.menuShowing) {
      this.props.onNodeEntered(this.props.concept.id, ReactDOM.findDOMNode(this.refs.inner));
    }
  };

  _handleExpand = () => {
    if (!this.props.isExpanded) {
      this.props.onNodeExpanded(this.props.concept.id);
    }
  };

  _handleRetract = () => {
    if (this.props.isExpanded) {
      this.props.onNodeRetracted(this.props.concept.id);
    }
  };

  _handleToggleShowMenu = (e) => {
    this.menuShowing = !this.menuShowing;
    this.setState({showMenu : this.menuShowing});
  };

  _handleCut = (e) => {
    if (e) {
      e.preventDefault();
    }

    this.props.onNodeCut(this.props.concept.id);
  };

  _handlePaste = (e) => {
    this.setConceptsInClipboardAsNarrowers();
  };

  _handlePasteConceptRequested = (e) => {
    if(e.metaKey || e.ctrlKey) {
      // That concept narrowers will receive the cut concept
      if (this.props.isSelected) {
        if (this.props.isCut) {
          return;
        }

        this.setNarrowerRelation(this.props.cutConceptId)
      }

      // That concept will be removed from the narrowers to be added
      // as a narrower of the selected concept
      if (this.props.isCut){
        this.removeConcept(true);
      }
    }
  };

  /**
   * Create a new concept and set a relation with current one.
   */
  createConceptAndSetRelationship() {
    this.props.showPreloader();

    let mutation,
      {relationType} = this.state.creatingConcept,
      {isExpanded, thesaurus} = this.props;

    if (relationType == 'related') {
      mutation = new CreateConceptAndSetRelatedRelationMutation({
        text : this.state.creatingConcept.label,
        concept: this.props.concept,
        relatedParentConcept: this.props.topConcept,
        thesaurus
      });
    } else {
      mutation = new CreateConceptAndSetNarrowerRelationMutation({
        text : this.state.creatingConcept.label,
        parentConcept: this.props.concept,
        thesaurus
      });
    }

    this.props.relay.commitUpdate(mutation, {
      onSuccess: () => {
        this._handleToggleConceptCreateDialog();

        if (relationType != 'related' && !isExpanded) {
          this._handleExpand();
        }

        this.props.hidePreloader();

        this.props.showSnackbar('Le concept a été ajouté');
      },
      onFailure: (transaction) => {
        this._handleToggleConceptCreateDialog();
        this.props.hidePreloader();
        this.props.showMutationError(transaction);
      }
    });
  };

  /**
   * Set a skos:narrower relation with an existing concept
   *
   * @param narrowerId
   */
  setNarrowerRelation(narrowerId) {
    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new SetNarrowerRelationMutation({
        parent: this.props.concept,
        narrowerId: narrowerId
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('La relation a été ajoutée');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  /**
   * Remove this concept.
   *
   * @param isMoving Inform the server if this is a moving action (cut/paste)
   * or a deletion.
   */
  removeConcept(isMoving = false){
    this._handleRetract();

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new RemoveConceptMutation({
        parent: this.props.parent,
        concept: this.props.concept,
        thesaurus: this.props.thesaurus,
        isMoving: isMoving
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('Le concept a été supprimé');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this._handleToggleConceptRemoveDialog();
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  };

  /**
   * Change relation type from skos:narrower to skos:related.
   *
   */
  changeRelationType(){
    // In Relay logic it turns out to :
    //  - Add this concept to its current parent related edges
    //  - Add this concept to the top concept narrowers edges
    //  - Remove this concept from its current parent narrowers edges
    //
    // So :

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new SetRelatedRelationMutation({
        concept: this.props.concept,
        relatedConcept: this.props.parent
      }),
      {
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
        }
      }
    );

    this.props.relay.commitUpdate(
      new SetNarrowerRelationMutation({
        parent: this.props.topConcept,
        narrowerId: this.props.concept.id
      }),
      {
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
        }
      }
    );

    this.props.relay.commitUpdate(
      new ({
        parentId: this.props.parent.id,
        concept: this.props.concept,
        isMoving: true // Tell server not to destroy the concept node
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('La relation a été modifiée');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  /**
   * Validate concept
   */
  validateConcept(){
    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new ValidateConceptMutation({
        concept: this.props.concept,
        thesaurus:  this.props.thesaurus
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('Le concept a été validé');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  /**
   * Invalidate concept
   */
  invalidateConcept(){
    const {taggingCount} = this.props.concept;
    if( taggingCount > 0) {
      const {concept, parent, showConfirmDialog, hideConfirmDialog} = this.props;

      showConfirmDialog(
        "Invalider le concept",
        (
          <div className={style.invalidateMessage}>
            <div>
              <p>
                <span className={style.bold}>Attention</span>, ce concept est lié à {taggingCount > 1 ? `${taggingCount} taggings`: `1 tagging`}. Invalider le concept <span className={style.bold}>supprimera tous les taggings lié</span>.
              </p>
              <p>
                Si vous désirez garder les taggings, il est préférable de <a onClick={() => {hideConfirmDialog(); this._handleSubstituteConcept();}}>ce concept à un autre</a>.
              </p>
            </div>
          </div>
        ),
        () => {
          this.removeConcept();
        },
        "Invalider",
        "Annuler"
      );
    } else {
      this._handleToggleConceptRemoveDialog();
    }
  }

  /**
   * Move concepts saved in clipboard as narrowers of this concept
   */
  setConceptsInClipboardAsNarrowers() {
    this.props.inClipboardConcepts.map((inClipboardConcept) => {
      this.props.relay.commitUpdate(new MoveConceptMutation({
        concept: inClipboardConcept,
        parent: inClipboardConcept.broader,
        target: this.props.concept
      }), {
        onSuccess: () => {
          this.props.showSnackbar('Les concepts ont été déplacés');
          this.props.onEmptyClipboard();
        },
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
        }
      });
    });
  }

  _handleSubstituteConcept = () => {
    this.setState({
      selectDialogTitle: "Sélectionner le concept de substitution",
      selectConceptCallback: (concept) => {
        this.setState({
          selectConceptCallback: null,
          selectDialogTitle: ""
        });

        this.substituteConceptWith(concept);
      }
    })
  };

  substituteConceptWith(subsituteConcept){
    const {concept, thesaurus, parent, showConfirmDialog, hideConfirmDialog} = this.props;

    showConfirmDialog(
      "Substituer un concept",
      (
        <div>
          <div>
            Confirmez-vous la substitution du concept
            <span className={style.inlineConcept}>{concept.label.value}</span> par le concept
            <span className={style.inlineConcept}>{subsituteConcept.prefLabel.value}</span> ?
          </div>
        </div>
      ),
      () => {
        this.props.showPreloader();

        this.props.relay.commitUpdate(new ReplaceConceptMutation({
          concept,
          targetConceptId: subsituteConcept.id,
          parent,
          thesaurus
        }), {
          onSuccess: () => {
            hideConfirmDialog();
            this.props.showSnackbar('Le concept a été substitué');
            this.props.hidePreloader();
          },
          onFailure: (transaction) => {
            hideConfirmDialog();
            this.props.showMutationError(transaction);
            this.props.hidePreloader();
          }
        });
      },
      "Substituer",
      "Annuler"
    );
  }

  moveConceptUnder(parentConcept){
    const {concept, parent, showConfirmDialog, hideConfirmDialog} = this.props;

    showConfirmDialog(
      "Déplacer le concept",
      (
        <div>
          <div>
            Confirmez-vous le déplacement du concept
            <span className={style.inlineConcept}>{concept.label.value}</span> sous le concept
            <span className={style.inlineConcept}>{parentConcept.prefLabel.value}</span> ?
          </div>
        </div>
      ),
      () => {
        this.props.showPreloader();

        this.props.relay.commitUpdate(new MoveConceptMutation({
          concept: concept,
          parent: parent,
          target: parentConcept
        }), {
          onSuccess: (response) => {
            hideConfirmDialog();

            setTimeout(() => {
              this.props.showSnackbar('Le concept a été déplacé');
              this.props.hidePreloader();

              this.props.onSelectConcept({
                conceptId: concept.id,
                ancestors: response.moveConcept.concept.ancestors.map(({id}) => id)
              });
            }, 2000);

          },
          onFailure: (transaction) => {
            hideConfirmDialog();
            this.props.showMutationError(transaction);
            this.props.hidePreloader();
          }
        });
      },
      "Déplacer",
      "Annuler"
    );
  }
}


export default Relay.createContainer(Concept, {
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus{
        id
        ${ValidateConceptMutation.getFragment('thesaurus')}
        ${SearchConceptDialog.getFragment('thesaurus')}
        ${ReplaceConceptMutation.getFragment('thesaurus')}
        ${RemoveConceptMutation.getFragment('thesaurus')}
      }
    `,
    concept: () => Relay.QL`
      fragment on Concept {
        id
        childrenCount
        label: prefLabelForLang(lang: "fr"){
          value
        }
        relatedCount,
        topConceptOf{
          id
          title
          color
        }
        taggingCount
        isDraft
        ${CreateConceptAndSetNarrowerRelationMutation.getFragment('parentConcept')}
        ${RemoveConceptMutation.getFragment('concept')}
        ${RemoveTopConceptMutation.getFragment('concept')}
        ${CreateConceptAndSetRelatedRelationMutation.getFragment('concept')}
        ${SetRelatedRelationMutation.getFragment('concept')}
        ${MoveConceptMutation.getFragment('concept')}
        ${ValidateConceptMutation.getFragment('concept')}
        ${ReplaceConceptMutation.getFragment('concept')}
      }
    `,
    parent: () => Relay.QL`
      fragment on SkosElementInterface {
        id
        __typename
        
        ...on Concept {
          ${SetRelatedRelationMutation.getFragment('relatedConcept')}
        }
        
        ...on Scheme {
          ${RemoveTopConceptMutation.getFragment('scheme')}
        }

        ${MoveConceptMutation.getFragment('parent')}
        ${ReplaceConceptMutation.getFragment('parent')}
        ${RemoveConceptMutation.getFragment('parent')}
      }
    `,
    inClipboardConcepts: () => Relay.QL`
      fragment on Concept @relay(plural: true){
        id,
        ${MoveConceptMutation.getFragment('concept')}
        broader{
          id
        }
      }
    `
  }
});

