/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `,
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    parent: () => Relay.QL`
      fragment on SkosElementInterface {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveConceptPayload {
        deletedId
        parent{
          id
          ...on Concept{
            childrenCount,
            narrowers
          }
          
          ...on Scheme{
            topConceptsCount
            topConcepts
          }
        }
        thesaurus{
          id
          draftConceptsCount
          draftConcepts
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'NODE_DELETE',
        parentName: 'parent',
        parentID: this.props.parent.id,
        connectionName: 'narrowers',
        deletedIDFieldName: 'deletedId'
      },
      {
        type: 'NODE_DELETE',
        parentName: 'parent',
        parentID: this.props.parent.id,
        connectionName: 'topConcepts',
        deletedIDFieldName: 'deletedId'
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          thesaurus: this.props.thesaurus.id
        }
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          parent: this.props.parent.id
        }
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      parentId: this.props.parent.id,
      thesoId: this.props.thesaurus.id,
      isMoving: this.props.isMoving
    };
  }
}
