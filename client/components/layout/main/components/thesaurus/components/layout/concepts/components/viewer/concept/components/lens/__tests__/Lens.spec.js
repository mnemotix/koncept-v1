/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import Relay from 'react-relay';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import MockNetworkLayer from 'mock-network-layer';
import {MenuItem} from 'react-toolbox/lib/menu';
import Lens from '../Lens';

var fixtures = {
  "concept": {
    "id": "Q29uY2VwdDoxS0hyVjY=",
    "prefLabels": [
      {
        "id": "TGFiZWw6VW1Zd0ll",
        "lang": "fr",
        "value": "Foo"
      }
    ],
    "searchExpression": null,
    "scopeNote": null,
    "definition": null,
    "example": null,
    "historyNote": null,
    "editorialNote": null,
    "changeNote": null,
    "altLabels": [],
    "hiddenLabels": [],
    "narrowers": {
      "edges": [
        {
          "node": {
            "id": "Q29uY2VwdDpNYXcya0Q=",
            "prefLabels": [
              {
                "id": "TGFiZWw6QjI5aHJx",
                "lang": "fr",
                "value": "Bar"
              }
            ]
          }
        },
        {
          "node": {
            "id": "Q29uY2VwdDpOVUJBNWc=",
            "prefLabels": [
              {
                "id": "TGFiZWw6OHdPTGFl",
                "lang": "fr",
                "value": "Baz"
              }
            ]
          }
        }
      ]
    },
    "related": {
      "edges": [
        {
          "node": {
            "id": "Q29uY2VwdDpmUnQ1eGI=",
            "prefLabels": [
              {
                "id": "TGFiZWw6R2dsMGUw",
                "lang": "fr",
                "value": "Biz"
              }
            ]
          }
        },
        {
          "node": {
            "id": "Q29uY2VwdDoyeEo3VXQ=",
            "prefLabels": [
              {
                "id": "TGFiZWw6aFFRbzRC",
                "lang": "fr",
                "value": "Boz"
              }
            ]
          }
        }
      ]
    }
  }
};

describe('Lens graph of concept', () => {
  var lens;
  //
  //beforeEach(() => {
  //  lens = testTree(<Lens {...fixtures} />);
  //});
  //
  //it('renders', () => {
  //  expect(lens).toBeTruthy();
  //});
  //
  //it('displays nodes', () => {
  //  expect(lens.get('nodes').length).toEqual(5);
  //});
  //
  //it('displays links', () => {
  //  expect(lens.get('links').length).toEqual(4);
  //});
  //
  //it('displays narrower edge menu when narrower edge is clicked', () => {
  //  lens.element.showEdgeMenu(lens.state.links[1]);
  //  expect(lens.state.selectedEdge).toEqual(lens.state.links[1]);
  //
  //  expect(TestUtils.scryRenderedComponentsWithType(lens.element, MenuItem)
  //    .find((menuItem) => menuItem.props.value == 'narrower_to_related')).toBeDefined();
  //
  //  expect(TestUtils.scryRenderedComponentsWithType(lens.element, MenuItem)
  //    .find((menuItem) => menuItem.props.value == 'related_to_narrower')).not.toBeDefined();
  //
  //});
  //
  //it('displays related edge menu when related edge is clicked', () => {
  //  lens.element.showEdgeMenu(lens.state.links[3]);
  //  expect(lens.state.selectedEdge).toEqual(lens.state.links[3]);
  //
  //  expect(TestUtils.scryRenderedComponentsWithType(lens.element, MenuItem)
  //    .find((menuItem) => menuItem.props.value == 'narrower_to_related')).not.toBeDefined();
  //
  //  expect(TestUtils.scryRenderedComponentsWithType(lens.element, MenuItem)
  //    .find((menuItem) => menuItem.props.value == 'related_to_narrower')).toBeDefined();
  //});
});
