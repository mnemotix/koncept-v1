/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 03/02/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `,
    scheme: () => Relay.QL`
      fragment on Scheme {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addTopConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddTopConceptPayload {
        scheme{
          topConcepts
          topConceptsCount
        }
        newConceptEdge
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'REQUIRED_CHILDREN',
        children: [Relay.QL`
          fragment on AddTopConceptPayload {
            newConceptEdge
          }
      `]
      },
      {
        type: 'RANGE_ADD',
        parentName: 'scheme',
        parentID: this.props.scheme.id,
        connectionName: 'topConcepts',
        edgeName: 'newConceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      thesoId: this.props.thesaurus.id,
      schemeId: this.props.scheme.id,
      topConceptLabel:  this.props.topConceptLabel
    };
  }
}