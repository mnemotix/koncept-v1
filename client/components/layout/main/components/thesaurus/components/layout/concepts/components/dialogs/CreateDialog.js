/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';
import classnames from 'classnames';
import Dropdown from 'react-toolbox/lib/dropdown';

import style from './style.scss';

export default class extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool,
    displayRelationTypesSelection: React.PropTypes.bool,
  };

  static defaultProps = {
    displayRelationTypesSelection: true
  };

  state = {
    selectedRelationType : this.props.relationTypes ? this.props.relationTypes[0].value : null
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.active == true && this.props.active == false) {
      this.setFocus = true ;
    }
  }

  componentDidUpdate(){
    if (this.setFocus) {
      this.refs['labelInput'].getWrappedInstance().focus();
      this.setFocus = false;
    }
  }

  renderRelationTypes() {
    if (this.props.relationTypes) {
      return (
        <section className={style.section}>
          <Dropdown
                  auto={true}
                  label="Type de relation"
                  source={this.props.relationTypes}
                  value={this.state.selectedRelationType}
                  onChange={(value) => this.onDropdownChange(value, 'relationType')}
          />
        </section>
      );
    }
  }

  render(){
    let actions = [
      {
        label: "Annuler",
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      },
      {
        label: this.props.loading ? "En cours..." : "Créer",
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCreate();
          }
        }
      }
    ];

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
      >
        <section className={style.section}>
          <Input ref={'labelInput'}
                 required={true}
                 onKeyPress={this._handleKeyPress.bind(this)}
                 type='text'
                 label='Label principal • skos:prefLabel (@fr)'
                 name='label'
                 value={this.props.inputs.label || ""}
                 onChange={this.props.onInputChange}
          />
        </section>

        { this.props.displayRelationTypesSelection ? this.renderRelationTypes() : null}
      </Dialog>
    )
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      this.props.onCreate();
    }
  };

  onDropdownChange(value, name){
    this.setState({
      selectedRelationType: value
    });

    this.props.onInputChange(value, {target: {name}});
  };
}