/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 03/02/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeScheme}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveSchemePayload {
        concept{
          topConceptOf
        },
        deletedId,
        thesaurus{
          schemes
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      },
      {
        type: 'NODE_DELETE',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'schemes',
        deletedIDFieldName: 'deletedId'
      }
    ];
  }

  getVariables() {
    return {
      schemeId: this.props.schemeId,
      thesaurusId: this.props.thesaurus.id,
      topConceptId: this.props.concept.id
    };
  }
}