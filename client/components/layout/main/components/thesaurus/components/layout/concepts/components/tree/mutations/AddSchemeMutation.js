/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 03/02/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    topConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addScheme}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddSchemePayload {
        topConcept{
          topConceptOf
        },
        thesaurus{
          schemes
        },
        newSchemeEdge
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          topConcept: this.props.topConcept.id
        }
      },
      {
        type: 'REQUIRED_CHILDREN',
        children: [Relay.QL`
          fragment on AddSchemePayload {
            newSchemeEdge
          }
      `]
      },
      {
        type: 'RANGE_ADD',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'schemes',
        edgeName: 'newSchemeEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      label: this.props.label,
      description: this.props.description,
      topConceptId: this.props.topConcept.id,
      thesaurusId: this.props.thesaurus.id
    };
  }
}