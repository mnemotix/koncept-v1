/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    label: () => Relay.QL`
      fragment on LocalizedLabel {
        id
      }
    `,
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{updateLabelForConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateLabelForConceptPayload {
        label
        concept{
          prefLabels
          altLabels
          hiddenLabels
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          label: this.props.label.id
        }
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      labelId: this.props.label.id,
      labelType: this.props.labelType,
      lang: this.props.lang,
      value: this.props.value
    };
  }
}
