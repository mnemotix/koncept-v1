/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id,
        relatedCount
      }
    `,
    relatedParentConcept: () => Relay.QL`
      fragment on Concept {
        id,
        narrowers(first: 1000){
          edges{
            node {
              id
            }
          }
        }
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{createConceptAndSetRelatedRelation}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on CreateConceptAndSetRelatedRelationPayload {
        concept{
          relatedCount,
          related
        },
        newRelatedConceptEdge,
        relatedParentConcept{
          narrowers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      },
      {
        type: 'REQUIRED_CHILDREN',
        children: [Relay.QL`
          fragment on CreateConceptAndSetRelatedRelationPayload {
            newRelatedConceptEdge
          }
      `],
      },
      {
        type: 'RANGE_ADD',
        parentName: 'relatedParentConcept',
        parentID: this.props.relatedParentConcept.id,
        connectionName: 'narrowers',
        edgeName: 'newRelatedConceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      text: this.props.text,
      conceptId: this.props.concept.id,
      relatedParentConceptId: this.props.relatedParentConcept.id,
      thesoId: this.props.thesaurus.id
    };
  }
}
