/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Relay from 'react-relay';

import FontIcon from 'react-toolbox/lib/font_icon';
import Input from 'react-toolbox/lib/input';
import Autocomplete from 'react-toolbox/lib/autocomplete';
import {List, ListItem} from 'react-toolbox/lib/list';
import {Button, IconButton} from 'react-toolbox/lib/button';

import Labels from './components/labels/Labels';

import UpdateConceptMutation from './mutations/UpdateConceptMutation';
import AddConceptToCollectionMutation from './mutations/AddConceptToCollectionMutation';
import RemoveConceptFromCollectionMutation from './mutations/RemoveConceptFromCollectionMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class ViewerTabProperties extends React.Component {
  availableCollections = {};

  state = {
    scopeNote: this.props.concept.scopeNote,
    definition: this.props.concept.definition,
    example: this.props.concept.example,
    historyNote: this.props.concept.historyNote,
    editorialNote: this.props.concept.editorialNote,
    changeNote: this.props.concept.changeNote,
    updatingFields: null
  };

  componentWillMount(){
    this.prepareCollections(this.props);
  }

  componentWillUpdate(nextProps) {
    this.prepareCollections(nextProps);
  }

  prepareCollections(props){
    const {thesaurus, concept} = props;

    this.availableCollections = {};

    thesaurus.collections.edges.map(availableCollectionEdge => {
      let exists = concept.memberOf.edges.find(collectionEdge => collectionEdge.node.id == availableCollectionEdge.node.id)

      if (!exists) {
        this.availableCollections[availableCollectionEdge.node.id] = availableCollectionEdge.node.title;
      }
    });
  }

  render() {
    const {concept} = this.props;

    var collections = [];

    concept.memberOf.edges.map(edge => {
      collections.push(edge.node.id);
    });

    return (
      <div>
        <section className={style.section}>
          <h4 className={style['section-title']}>
            <FontIcon className={style['section-icon']} value="label"/> Label principal • <small>skos:prefLabel</small>
          </h4>

          <Labels labelType='prefLabels' concept={this.props.concept} ensureOneLabelPerLang={true} />
        </section>

        <section className={style.section}>
          <h4 className={style['section-title']}>
            <FontIcon className={style['section-icon']} value="label_outline"/>  Label secondaire •  <small>skos:altLabel</small>
          </h4>

          <Labels labelType='altLabels' concept={this.props.concept} />
        </section>

        <section className={style.section}>
          <h4 className={style['section-title']}>
            <FontIcon className={style['section-icon']} value="label_outline"/>  Label caché •  <small>skos:hiddenLabel</small>
          </h4>

          <Labels labelType='hiddenLabels' concept={this.props.concept} />
        </section>

        <section className={style.section}>
          <h4 className={style['section-title']}>
            <FontIcon className={style['section-icon']} value="group_work"/>  Collections • <small>skos:memberOf</small>
          </h4>

          <List className={style["collections"]}>
            {concept.memberOf.edges.length > 0 ? concept.memberOf.edges.map(edge => (
              <div key={edge.node.id} className={style["collection-item"]}>
                <ListItem selectable caption={edge.node.title}/>
                <IconButton className={style["remove-collection-button"]} icon="delete" onClick={this._handleRemoveFromCollection.bind(this, edge.node.id)}/>
              </div>
            )) : <div className={style['collections-empty']}>N'appartient à aucune collection.</div>}
          </List>

          <div className={style['section-body']}>
            <Autocomplete className={style['dropdown']}
                          direction="down"
                          label="Associer à une collection"
                          onChange={this._handleAddToCollection}
                          source={this.availableCollections}
                          value={''}
                          multiple={false}
            />
          </div>
        </section>

        <section className={style.section}>
          <h4 className={style['section-title']}>
            <FontIcon className={style['section-icon']} value="book"/>  Notes documentaires
          </h4>

          <div className={style['section-body']}>
            {this.renderTextInput("Note d'usage • skos:scopeNote", 'scopeNote', true)}
            {this.renderTextInput('Définition • skos:definition', 'definition', true)}
            {this.renderTextInput("Exemple d'utilisation • skos:example", 'example', true)}
            {this.renderTextInput('Note historique • skos:historyNote', 'historyNote', true)}
            {this.renderTextInput('Note éditoriale • skos:editorialNote', 'editorialNote', true)}
            {this.renderTextInput('Note de changement • skos:changeNote', 'changeNote', true)}
          </div>

        </section>
      </div>
    );
  }

  renderTextInput(label, field, multiline = false){
    return (
      <Input className={style.input}
             value={this.state[field] || ""}
             label={label}
             multiline={multiline}
             onChange={(value) => this._handleUpdateSimpleInputChange(value, field)}
             onBlur={this._handleCommitUpdateField.bind(this, field)}
             onKeyPress={multiline ? () => {} : this._handleKeyPress.bind(this)}
      />
    )
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      e.target.blur();
    }
  };

  _handleUpdateSimpleInputChange(value, field) {
    this.setState({
      [field]: value
    })
  }

  _handleAddToCollection = (collectionId) => {
    const {concept, thesaurus} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(new AddConceptToCollectionMutation({
      collection: thesaurus.collections.edges.find(edge => edge.node.id == collectionId).node,
      concept
    }), {
      onSuccess: () => {
        this.props.showSnackbar('Le concept a été ajouté à la collection');
        this.props.hidePreloader();
      },
      onFailure: (transaction) => {
        this.props.showMutationError(transaction);
        this.props.hidePreloader();
      }
    });
  };

  _handleRemoveFromCollection = (collectionId) => {
    const {concept, thesaurus} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(new RemoveConceptFromCollectionMutation({
      collection: thesaurus.collections.edges.find(edge => edge.node.id == collectionId).node,
      concept
    }), {
      onSuccess: () => {
        this.props.showSnackbar('Le concept a été supprimé de la collection');
        this.props.hidePreloader();
      },
      onFailure: (transaction) => {
        this.props.showMutationError(transaction);
        this.props.hidePreloader();
      }
    });
  };

  _handleCommitUpdateField = (field) => {
    const {concept} = this.props;

    if (concept[field] != this.state[field]) {
      this.props.showPreloader();

      this.props.relay.commitUpdate(
        new UpdateConceptMutation({
          concept,
          field,
          value: this.state[field]
        }),
        {
          onSuccess: () => {
            this.props.showSnackbar('Le concept a été modifié');
            this.props.hidePreloader();
          },
          onFailure: (transaction) => {
            this.props.showMutationError(transaction);
            this.props.hidePreloader();
          }
        }
      );
    }
  };
}

export default Relay.createContainer(ViewerTabProperties, {
  fragments: {
    concept: () => {
      return Relay.QL`
        fragment on Concept {
          id,
          prefLabels(first: 50){
            edges{
              node{
                value
                lang
              }
            }
          }
          scopeNote
          definition
          example
          historyNote
          editorialNote
          changeNote
          memberOf(first: 20) {
            edges{
              node{
                id
                title
                description
              }
            }
          },
          ${Labels.getFragment('concept')},
          ${UpdateConceptMutation.getFragment('concept')},
          ${AddConceptToCollectionMutation.getFragment('concept')},
          ${RemoveConceptFromCollectionMutation.getFragment('concept')}
        }
      `
    },

    thesaurus: () => {
      return Relay.QL`
        fragment on Thesaurus {
          id,
          collections(first: 10) {
            edges{node{
              id
              title
              description
              ${AddConceptToCollectionMutation.getFragment('collection')},
              ${RemoveConceptFromCollectionMutation.getFragment('collection')}
            }}
          }
        }`
    }
  }
});