/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    label: () => Relay.QL`
      fragment on LocalizedLabel {
        id
      }
    `,
    matcher: () => Relay.QL`
      fragment on Matcher {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeMatcherForLabel}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveMatcherForLabelPayload {
        deletedId
        label{
          matchers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_DELETE',
        parentName: 'label',
        parentID: this.props.label.id,
        connectionName: 'matchers',
        deletedIDFieldName: 'deletedId',
        pathToConnection: ['label', 'matchers']
      }
    ];
  }

  getVariables() {
    return {
      labelId: this.props.label.id,
      matcherId: this.props.matcher.id
    };
  }
}
