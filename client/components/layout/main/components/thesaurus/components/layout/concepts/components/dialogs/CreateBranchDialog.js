/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';
import classnames from 'classnames';
import Dropdown from 'react-toolbox/lib/dropdown';

import ColorSwatchesInput from '../../../../../../../../../input/ColorSwatchesInput';


import style from './style.scss';

export default class extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.active == true && this.props.active == false) {
      this.setFocus = true ;
    }
  }

  componentDidUpdate(){
    if (this.setFocus) {
      this.refs['labelInput'].getWrappedInstance().focus();
      this.setFocus = false;
    }
  }

  render(){
    let actions = [
      {
        label: "Annuler",
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      },
      {
        label: this.props.loading ? "En cours..." : "Créer",
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCreate();
          }
        }
      }
    ];

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
      >
        <section className={style.section}>
          <Input ref={'labelInput'}
                 required={true}
                 onKeyPress={this._handleKeyPress.bind(this)}
                 type='text'
                 label='Nom de la branche'
                 value={this.props.inputs.schemeTitle || ""}
                 onChange={(value) => this.props.onInputChange(value, 'schemeTitle')}
          />

          <Input
                 required={true}
                 onKeyPress={this._handleKeyPress.bind(this)}
                 type='text'
                 label='Description de la branche'
                 multiline={true}
                 value={this.props.inputs.schemeDescription || ""}
                 onChange={(value) => this.props.onInputChange(value, 'schemeDescription')}
          />

          <ColorSwatchesInput className={style.color}
                              value={this.props.inputs.schemeColor || "#000"}
                              label={"Couleur du schéma"}
                              onChangeCallback={(color) => this.props.onInputChange(color, 'schemeColor')}
          />
        </section>
      </Dialog>
    )
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      this.props.onCreate();
    }
  };
}