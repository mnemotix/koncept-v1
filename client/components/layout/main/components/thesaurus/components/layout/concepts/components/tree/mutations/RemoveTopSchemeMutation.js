/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 03/02/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `,
    scheme: () => Relay.QL`
      fragment on Scheme {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeTopScheme}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveTopSchemePayload {
        deletedId
        thesaurus{
          schemes
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'NODE_DELETE',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'schemes',
        deletedIDFieldName: 'deletedId'
      }
    ];
  }

  getVariables() {
    return {
      schemeId: this.props.scheme.id,
      thesaurusId: this.props.thesaurus.id
    };
  }
}