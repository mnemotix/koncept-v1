/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 03/02/2016
 */

import React from 'react';
import Relay from 'react-relay';

import {Tab, Tabs} from 'react-toolbox';
import Input from 'react-toolbox/lib/input';
import AffixWrapper from '../../../../../../../../../../affix/AffixWrapper';

import Concept from '../../tree/Concept';
import ColorSwatchesInput from '../../../../../../../../../../input/ColorSwatchesInput';

import UpdateSchemeMutation from './mutations/UpdateSchemeMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class SchemeViewer extends React.Component {

  state = {
    title: this.props.scheme.title,
    description: this.props.scheme.description,
    color: this.props.scheme.color
  };

  render() {
    const {scheme, thesaurus} = this.props;

    return (
      <div className={this.props.className}>

        <AffixWrapper className={style["header-wrapper"]} affixClassName={style.affix}>
          <header className={style.header}>
            <div className={style["header-wrap"]}>
              <div className={style.preTitle}>Scheme</div>

              <h3 className={style.title}>
                {scheme.title}
              </h3>

              <Tabs className={style.tabs} index={0}>
                <Tab style={style.tab} label="Propriétés" />
              </Tabs>
            </div>
          </header>
        </AffixWrapper>

        <div>
          <section className={style.section}>
            <div className={style['section-body']}>
              <ColorSwatchesInput className={style.color}
                                  value={this.state.color}
                                  label={"Couleur du schéma"}
                                  onChangeCallback={(color) => {
                                    this.setState({color}, () => {
                                      this.updateScheme();
                                    });
                                  }}
              />

              {this.renderTextInput('Titre', 'title', false)}
              {this.renderTextInput('Description', 'description', true)}
            </div>
          </section>
        </div>
      </div>
    )
  }

  renderTextInput(label, field, multiline = false){
    return (
      <Input className={style.input}
             value={this.state[field] || ""}
             label={label}
             multiline={multiline}
             onChange={(value) => this._handleUpdateSimpleInputChange(value, field)}
             onBlur={this._handleCommitUpdateField.bind(this, field)}
             onKeyPress={multiline ? () => {} : this._handleKeyPress.bind(this)}
      />
    )
  }

  updateScheme(){
    const {scheme} = this.props;
    const {description, title, color} = this.state;

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new UpdateSchemeMutation({
        scheme,
        description, title, color
      }),
      {
        onSuccess: () => {
          this.props.hidePreloader();
          this.props.showSnackbar('Le schéma a été modifié')
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      e.target.blur();
    }
  };

  _handleUpdateSimpleInputChange(value, field) {
    this.setState({
      [field]: value
    })
  }

  _handleCommitUpdateField = (field) => {
    const {scheme} = this.props;

    if (scheme[field] != this.state[field]) {
      this.updateScheme();
    }
  };
}

export default Relay.createContainer(SchemeViewer, {
  fragments: {
    scheme: () => Relay.QL`
      fragment on Scheme {
        id
        title
        description
        color
        ${UpdateSchemeMutation.getFragment('scheme')}
      }
    `,
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
        ${Concept.getFragment('thesaurus')}
      }
    `
  }
});