/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import Relay from 'react-relay';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import {MenuItem} from 'react-toolbox/lib/menu';
import {shallow} from 'enzyme';

import Concept from '../Concept';
import style from '../style.scss';

var fixtures = {
  concept: {
    "id": "Q29uY2VwdDpNYXcya0Q=",
    "childrenCount": 2,
    "prefLabels": [
      {
        "id": "TGFiZWw6QjI5aHJx",
        "value": "Foo",
        "lang": "fr"
      },
      {
        "id": "TGFiZWw6MExBZlFu",
        "value": "Bar",
        "lang": "en"
      },
      {
        "id": "TGFiZWw6UkxVTjRO",
        "value": "Baz",
        "lang": "es"
      }
    ],
    "relatedCount": 3,
    "topConceptOf": null
  }
};

var stubs = {
  onNodeExpanded: () => {},
  onNodeRetracted: () => {},
  onNodeSelected: () => {},
  onSchemeSelected: () => {},
  onNodeCut: () => {},
  onEmptyClipboard: () => {}
};

describe('Concept in thesaurus navigation', () => {
  var wrapper;

  beforeEach(() => {
    spyOn(stubs, 'onNodeExpanded');
    spyOn(stubs, 'onNodeRetracted');
    spyOn(stubs, 'onNodeSelected');
    spyOn(stubs, 'onSchemeSelected');
    spyOn(stubs, 'onNodeCut');

    wrapper = shallow(<Concept {...fixtures} {...stubs}/>);
  });

  it('renders', () => {
    expect(wrapper).toBeTruthy();
  });

  it('displays main prefLabel', () => {
    expect(wrapper.find(`.${style.label}`).first().text()).toEqual('Foo');
  });

  it('displays other prefLabels', () => {
    expect(wrapper.find(`.${style['other-label']}`).length).toEqual(2);
  });

  it('displays related thumbs', () => {
    expect(wrapper.find(`.${style['related-item-preview']}`).length).toEqual(3);
  });

  it('displays narrowers thumbs', () => {
    expect(wrapper.find(`.${style['child-preview']}`).length).toEqual(2);
  });

  it(`opens the remove dialog on remove button click`, () => {
    var menuItem = wrapper.find('.menu-item-remove-concept').first();
    expect(menuItem).toBeDefined();

    menuItem.simulate('click');

    expect(wrapper.state('dialogRemoveActive')).toBe(true);

    wrapper.simulate('click');
  });

  it(`opens the create dialog on create button click`, () => {
    var menuItem = wrapper.find('.menu-item-create-concept').first();
    expect(menuItem).toBeDefined();

    menuItem.simulate('click');

    expect(wrapper.state('dialogCreateActive')).toBe(true);

    wrapper.simulate('click');
  });

  it(`opens the change relation dialog on change relation button click`, () => {
    var menuItem = wrapper.find('.menu-item-from-narrower-to-related').first();
    expect(menuItem).toBeDefined();

    menuItem.simulate('click');

    expect(wrapper.state('dialogChangeRelationActive')).toBe(true);
  });

  it(`opens the scheme creation dialog on scheme creation button click`, () => {
    var menuItem = wrapper.find('.menu-item-create-schema').first();
    expect(menuItem).toBeDefined();

    menuItem.simulate('click');

    expect(wrapper.state('dialogSchemeCreateActive')).toBe(true);
  });

  it(`doesn't display scheme removal button`, () => {
    expect(wrapper.find('.menu-item-remove-schema-leave-concepts').length).toEqual(0);
  });

  it(`displays cut concept button`, () => {
    expect(wrapper.find('.menu-item-cut-concept').length).toEqual(1);
  });

  it('Calls the cut function on cut clicked', () => {
    wrapper.find('.menu-item-cut-concept').first().simulate('click');

    expect(stubs.onNodeCut).toHaveBeenCalled();
  });

  it(`doesn't display paste concept button`, () => {
    expect(wrapper.find('.menu-item-paste-concept').length).toEqual(0);
  });
});

const fixtures2 = {
  concept: {
    "id": "Q29uY2VwdDpNYXcya0Q=",
    "childrenCount": 43,
    "prefLabels": [
      {
        "id": "TGFiZWw6QjI5aHJx",
        "value": "Foo",
        "lang": "fr"
      }
    ],
    "relatedCount": 45,
    "topConceptOf": null
  }
};

describe('A big concept in thesaurus navigation', () => {
  var wrapper;

  beforeEach(() => {
    wrapper = shallow(<Concept {...fixtures2} {...stubs}/>);
  });

  it('displays only 10 related thumbs', () => {
    expect(wrapper.find(`.${style['related-item-preview']}`).length).toEqual(10);
  });

  it('displays only 10 narrowers thumbs', () => {
    expect(wrapper.find(`.${style['child-preview']}`).length).toEqual(10);
  });
});

const fixtures3 = {
  concept: {
    "id": "Q29uY2VwdDpNYXcya0Q=",
    "childrenCount": 0,
    "prefLabels": [
      {
        "id": "TGFiZWw6QjI5aHJx",
        "value": "Foo",
        "lang": "fr"
      }
    ],
    "relatedCount": 0,
    "topConceptOf": null
  }
};

describe('A leaf concept in thesaurus navigation', () => {
  var wrapper;

  beforeEach(() => {
    wrapper = shallow(<Concept {...fixtures3} {...stubs}/>);
  });

  it('displays no related thumbs', () => {
    expect(wrapper.find(`.${style['related-item-preview']}`).length).toEqual(0);
  });

  it('displays no narrowers thumbs', () => {
    expect(wrapper.find(`.${style['child-preview']}`).length).toEqual(0);
  });
});

let fixture4 = {
  ...fixtures3,
  inClipboardConcepts : [
    {
      "id": "foo_moving"
    }
  ]
};

describe('A concept with others concepts in clipboard', () => {
  var wrapper;

  beforeEach(() => {
    spyOn(stubs, 'onEmptyClipboard');

    wrapper = shallow(<Concept {...fixture4} {...stubs}/>);
  });


  it(`displays paste concept button`, () => {
    expect(wrapper.find('.menu-item-paste-concept').length).toEqual(1);
  });

  it(`calls the paste function on paste clicked and empty the clipboard`, (done) => {
    spyOn(wrapper.instance(), 'setConceptsInClipboardAsNarrowers').and.callThrough();

    Relay.Store.succeedWith({}, () => {
      // Update wrapper since Relay is mocked
      wrapper.update();

      expect(stubs.onEmptyClipboard).toHaveBeenCalled();
      done();
    });

    wrapper.find('.menu-item-paste-concept').first().simulate('click');

    expect(wrapper.instance().setConceptsInClipboardAsNarrowers).toHaveBeenCalled();
  });
});

let fixture5 = {
  concept: {
    "id": "foo_moving",
    "childrenCount": 0,
    "prefLabels": [
      {
        "id": "TGFiZWw6QjI5aHJx",
        "value": "Foo",
        "lang": "fr"
      }
    ],
    "relatedCount": 0,
    "topConceptOf": null
  },
  inClipboardConcepts : [
    {
      "id": "foo_moving"
    }
  ],
  isCut: true
};

describe('A concept with himself in clipboard', () => {
  var wrapper;

  beforeEach(() => {
    wrapper = shallow(<Concept {...fixture5} {...stubs}/>);
  });

  it(`displays paste concept button as disabled`, () => {
    expect(wrapper.find('.menu-item-paste-concept').length).toEqual(1);
    expect(wrapper.find('.menu-item-paste-concept').first().prop('disabled')).toEqual(true);
  });

  it(`doesn't display cut concept button`, () => {
    expect(wrapper.find('.menu-item-cut-concept').length).toEqual(0);
  });
});