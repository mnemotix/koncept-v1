/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';
import classnames from 'classnames';

import {MenuItem, MenuDivider, Snackbar, FontIcon, Button, Input, IconButton} from 'react-toolbox';

import SweetScroll from "../../../../../../../../../common/scrollable/sweetScroll";

import keyboard from 'keyboardjs';

import IconMenu from '../../../../../../../../../menu/IconMenu';

import CreateDialog from '../../components/dialogs/CreateBranchDialog';
import DraftConceptsList from './components/draftConcepts/DraftConceptsList';
import SearchConceptsList from './components/searchConcepts/SearchConceptsList';

import Scheme from './Scheme';
import Column from './Column';

import CreateTopSchemeMutation from './mutations/CreateTopSchemeMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from '../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from '../../../../../../../../../common/preloader/preloaderHoc';
import {withRouter} from 'react-router';

import deepEqual from 'deep-equal';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
@withRouter
class Component extends React.Component {
  static propTypes = {
    // This type is important to uniquely retrieve this concept across views
    prefixKey: React.PropTypes.string.isRequired,
    onLocationChange: React.PropTypes.func.isRequired
  };

  state = {
    dialogCreateActive: false,
    creatingBranch: {},
    loading: false,
    cutConceptId: null,
    snackbarMessage: '',
    snackbarActive: false,
    waitForNewColumns: false,
    isTopConceptFocused: null,
    focusOnColumnIndex: 0,
    focusOnConceptId: null,
    focusedTopSchemeIndex: 0,
    query: "",
    showSearchAside: false,
    showDraftConcepts: false,
    columnIds: []
  };

  stateSaveConfigs = [{
    key: 'focusOnColumnIndex',
    queryParam: 'fcln',
    parseFn: parseInt
  }, {
    key: 'focusOnConceptId',
    queryParam: 'fcpt',
  }, {
    key: 'focusedTopSchemeIndex',
    queryParam: 'fts',
    parseFn: parseInt
  }, {
    key: 'query',
    queryParam: 'qs'
  }, {
    key: 'showSearchAside',
    queryParam: 'shsa',
    stringifyFn: (v) => v ? 1 : 0,
    parseFn: (v) => !!parseInt(v)
  }, {
    key: 'showDraftConcepts',
    queryParam: 'shdc',
    stringifyFn: (v) => v ? 1 : 0,
    parseFn: (v) => !!parseInt(v)
  }, {
    key: 'columnIds',
    queryParam: 'cids',
    stringifyFn: (v) => v.join("/"),
    parseFn: (v) => v.split("/")
  }];

  componentWillMount() {
    this.initState();
  }

  componentWillReceiveProps(nextProps){
    if(!deepEqual(nextProps.location.query, this.props.location.query)) {
      this.initState(nextProps.location);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    let shouldIt = !(deepEqual(nextProps.thesaurus, this.props.thesaurus) && deepEqual(nextState, this.state));

    return shouldIt;
  }

  componentWillUpdate(nextProps, nextState) {
    if (!deepEqual(nextState, this.state)) {
      this.saveState(nextState);
    }
  }

  componentDidMount() {
    this.initKeyboardListeners();
    window.addEventListener('scroll', this._handleWindowScroll);

    this.sweetVScroll = new SweetScroll({
      offset: -300,
      verticalScroll: true,
      horizontalScroll: false,
      outputLog: true
    }, "html");

    this.sweetHScroll = new SweetScroll({
      offset: -300,
      verticalScroll: false,
      horizontalScroll: true,
      outputLog: true
    }, ReactDOM.findDOMNode(this.refs.treeViewport));
  }

  componentDidUpdate() {
    this.initKeyboardListeners();
  }

  componentWillUnmount() {
    this.clearKeyboardListeners();
    window.removeEventListener('scroll', this._handleWindowScroll);
  }

  clearKeyboardListeners() {
    keyboard.unbind('ctrl + v', this._handlePasteConceptRequested);
    keyboard.unbind('right', this._handleFocusOnNextColumn);
    keyboard.unbind('left', this._handleFocusOnPreviousColumn);
    keyboard.unbind('ctrl + q', this._handleToggleBranchCreateDialog);
    keyboard.unbind('down', this._handleFocusNextTopScheme);
    keyboard.unbind('up', this._handleFocusPreviousTopScheme);
  }

  initKeyboardListeners() {
    this.clearKeyboardListeners();

    keyboard.withContext('concept-tree', () => {
      keyboard.bind('right', this._handleFocusOnNextColumn);
      keyboard.bind('left', this._handleFocusOnPreviousColumn);
      keyboard.bind('down', this._handleFocusNextTopScheme);
      keyboard.bind('up', this._handleFocusPreviousTopScheme);

      if (this.state.isTopConceptFocused && !this.state.dialogCreateActive) {
        keyboard.bind('ctrl + q', this._handleToggleBranchCreateDialog);
        keyboard.bind('ctrl + v', this._handlePasteConceptRequested);
      }
    });
  }

  render() {
    const {thesaurus, isSelected} = this.props;

    return (
      <div className={classnames(style.layout)}>
        <div className={classnames(style.asideLeft, {[style.show]: this.state.showSearchAside})}>
          <div className={style.title}>
            Recherche
          </div>

          <SearchConceptsList
            thesaurus={thesaurus}
            focusOnConceptId={this.state.focusOnConceptId}
            onSelectedConcept={this._handleSelectConcept}
            onQueryChange={(query) => this.setState({query})}
            query={this.state.query}
          />
        </div>

        <div className={classnames(style.middle)} ref="treeViewport">
          <div className={style.hierarchy}>
            <div className={style.header}>
              <div className={classnames({
                [style.root]: true,
                [style.selected]: isSelected,
                [style.focused]: this.state.isTopConceptFocused
              })}>

                <div ref="thesaurusNode" className={style.inner}>
                  <IconButton className={style.icon} icon="search"
                              onClick={() => this.setState({showSearchAside: !this.state.showSearchAside})}/>

                  <span className={style["root-label"]}>Thésaurus</span>

                  <IconMenu ref="rootMenu" className={style['icon-menu']} icon='more_vert' position='topLeft'
                            menuRipple>
                    <MenuItem className='menu-item-create-concept'
                              icon='add_circle'
                              caption='Ajouter une branche' onClick={this._handleToggleBranchCreateDialog}
                    >
                      <span className={style.shortcut}>(Ctrl+Q)</span>
                    </MenuItem>

                    <MenuDivider/>

                    {thesaurus.inClipboardConcepts && thesaurus.inClipboardConcepts.length > 0 ? (
                      <div>
                        <MenuItem className='menu-item-paste-concept'
                                  value='paste' icon='content_paste'
                                  caption='Coller le concept sauvegardé à la racine du thesaurus'
                                  onClick={this._handlePaste.bind(this)}
                        >
                          <span className={style.shortcut}>(Ctrl+V)</span>
                        </MenuItem>

                        <MenuDivider/>
                      </div>
                    ) : (<div/>)}


                    <MenuItem icon='settings_backup_restore' caption='Replier le thesaurus'
                              onClick={this._handleRetractAll}/>

                  </IconMenu>
                </div>
              </div>

              {thesaurus.draftConceptsCount > 0 ? (
                <Button raised={this.state.showDraftConcepts} className={style.showDraftConceptsButton}
                        onClick={this._handleShowDraftConcepts}>
                  <span>
                    <span className={style.badge}>{thesaurus.draftConceptsCount}</span> concepts à valider
                  </span>
                </Button>
              ) : null}
            </div>


            <div className={style['columns']}>
              {
                this.props.thesaurus.topSchemes.edges.length > 0 ?
                  this.renderColumns() :
                  this.renderThesaurusEmptyHelp()
              }
            </div>

            {this.renderDialogs()}

            <Snackbar active={this.state.snackbarActive}
                      icon='question_answer'
                      label={this.state.snackbarMessage}
                      timeout={2000}
                      onTimeout={() => this.setState({snackbarActive: false})}
            />

          </div>
        </div>

        <div
          className={classnames(style.asideRight, style.draftConcepts, {[style.show]: this.state.showDraftConcepts})}>
          <div className={style.title}>
            Concepts à valider
          </div>

          <DraftConceptsList
            thesaurus={thesaurus}
            focusOnConceptId={this.state.focusOnConceptId}
            onSelectedConcept={this._handleSelectConcept}
          />
        </div>

      </div>
    )
  }

  _handleShowDraftConcepts = () => {
    this.setState({showDraftConcepts: !this.state.showDraftConcepts});
  };

  _handleSelectConcept = ({conceptId, ancestors}) => {
    this.setColumnIds(ancestors, {
      focusOnConceptId: conceptId,
      focusOnColumnIndex: ancestors.length
    });
  };

  renderColumns() {
    const {columns, topSchemes, inClipboardConcepts} = this.props.thesaurus,
      topSchemeSelectedIndex = columns.length > 0 ?
        topSchemes.edges.findIndex(({node: scheme}) => columns[0].id === scheme.id) : -1;

    return (
      <div className={style.columns}>
        <div className={style.column}>
          <div className={classnames(style['concept-list'], {[style['contains-expanded']]: columns.length > 0})}>
            {topSchemes.edges.map(({node: scheme}, index) => {

              let isExpanded = topSchemeSelectedIndex === index,
                isSiblingSelected = topSchemeSelectedIndex >= index,
                className = classnames(style['concept-item'], {
                  [style['selected-sibling']]: isSiblingSelected,
                  [style['expanded']]: isExpanded
                });

              return <div className={className} key={scheme.id}>
                <Scheme scheme={scheme}
                        thesaurus={this.props.thesaurus}
                        onNodeExpanded={this._handleTopNodeExpanded}
                        onNodeRetracted={this._handleTopNodeRetracted}
                        isExpanded={isExpanded}
                        isSiblingSelected={isSiblingSelected}
                        isFocused={this.state.focusOnColumnIndex === 0 && this.state.focusedTopSchemeIndex === index}
                        onCenterViewOnConcept={this._handleCenterViewOnElement}
                        onNodeEntered={this._handleSchemeEntered}
                />
              </div>
            })}

            {this.state.loading ? (
              <div className={style.loading}>Chargement des suivants...</div>
            ) : null}
          </div>
        </div>

        {columns.map((column, index) => {
          return (
            <div key={index} className={style.column}>
              {this.renderColumn(column, index + 1, columns.length)}
            </div>
          );
        })}
      </div>
    );
  }

  renderColumn(column, columnIndex, columnsCount) {
    const {thesaurus} = this.props,
      {columnIds} = this.props.relay.variables,
      expandedConceptId = columnIds[columnIndex],
      {inClipboardConcepts} = thesaurus,
      isColumnFocused = this.state.focusOnColumnIndex === columnIndex;

    return <Column column={column}
                   columnIndex={columnIndex}
                   thesaurus={thesaurus}
                   prefixKey={this.props.prefixKey}
                   isLastColumn={columnIndex === columnsCount}
                   isBeforeLastColumn={columnIndex === columnsCount - 1}
                   isColumnFocused={isColumnFocused}
                   focusedConceptId={isColumnFocused ? this.state.focusOnConceptId : null}
                   selectedConceptId={this.state.selectedConceptId}
                   expandedConceptId={expandedConceptId}
                   cutConceptId={this.state.cutConceptId}
                   onNodeEntered={this._handleConceptEntered}
                   onNodeExpanded={this._handleNodeExpanded}
                   onNodeRetracted={this._handleNodeRetracted}
                   onNodeCut={this._handleNodeCut}
                   onEmptyClipboard={this._handleEmptyClipboard}
                   onSchemeSelected={this._handleSchemeSelected}
                   onCenterViewOnConcept={this._handleCenterViewOnElement}
                   onFocusColumn={this._handleFocusColumn.bind(this, columnIndex)}
                   onFocusConcept={this._handleFocusConcept.bind(this)}
                   inClipboardConcepts={inClipboardConcepts || []}
                   onSelectConcept={this._handleSelectConcept}
    />;
  }

  renderThesaurusEmptyHelp() {
    return (
      <div className={style.help}>
        <p>Le thésaurus est vide.</p>
        <p className={style.light}>
          Pour ajouter des nouveaux concepts, cliquer sur l'icone
          <FontIcon className={style['icon-menu']} value="more_vert"/>
          pour dérouler le menu contextuel au nœud racine et ajouter des concepts.
        </p>
      </div>
    );
  }

  renderDialogs() {
    return (
      <div>
        <CreateDialog
          active={this.state.dialogCreateActive}
          title='Ajouter une branche'
          inputs={this.state.creatingBranch}
          onInputChange={this._handleCreateInputChange.bind(this)}
          onCreate={this.createBranch.bind(this)}
          onCancel={this._handleToggleBranchCreateDialog.bind(this)}
          loading={this.state.loading}
        />
      </div>
    );
  }

  _handleTopNodeExpanded = (conceptId) => {
    this.setColumnIds([conceptId]);
  };

  _handleTopNodeRetracted = () => {
    this.setColumnIds([]);
  };

  _handleNodeExpanded = (conceptId, unexpandedConceptId) => {
    let columnIds = this.props.relay.variables.columnIds.slice(),
      unexpandedConceptIndex = columnIds.indexOf(unexpandedConceptId);

    // Remove visible hierarchy
    if (unexpandedConceptId && unexpandedConceptIndex !== -1) {
      columnIds = columnIds.slice(0, unexpandedConceptIndex);
    }

    // This is a unexpand request.
    if (conceptId !== unexpandedConceptId) {
      columnIds.push(conceptId);
    }

    this.setColumnIds(columnIds);
  };

  _handleNodeRetracted = (conceptId) => {
    this._handleNodeExpanded(conceptId, conceptId);
  };

  _handleRetractAll = () => {
    this.setColumnIds([]);
  };

  _handleSchemeSelected = (schemeId) => {
    this.props.router.push(`/thesaurus/${this.props.thesaurus.id}/schemes/${schemeId}`);
  };

  _handleNodeCut = (nodeIds) => {
    this.setConceptsInClipboard(Array.isArray(nodeIds) ? nodeIds : [nodeIds]);
  };

  _handleEmptyClipboard = () => {
    this.emptyClipboard();
  };

  _handlePaste = () => {
    // this.props.thesaurus.inClipboardConcepts.map((inClipboardConcept) => {
    //   this.props.relay.commitUpdate(new MoveConceptMutation({
    //     movingConcept: inClipboardConcept,
    //     sourceParentConcept: inClipboardConcept.broader,
    //     targetParentConcept: this.getTopConcept()
    //   }), {
    //     onSuccess: () => {
    //       this.emptyClipboard();
    //     },
    //     onFailure: (transaction) => {
    //       this.props.showMutationError(transaction);
    //     }
    //   });
    // });
  };

  _handlePasteConceptRequested = (e) => {
    if (e.metaKey || e.ctrlKey) {
      if (this.state.isTopConceptFocused) {
        this._handlePaste();
      }
    }
  };

  _handleToggleBranchCreateDialog = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    this.toggleBranchCreateDialog();
  };

  _handleFocusOnNextColumn = () => {
    let nextIndex = this.state.focusOnColumnIndex + 1;

    if (nextIndex <= this.props.thesaurus.columns.length) {
      this._handleFocusColumn(nextIndex);
    }
  };

  _handleFocusOnPreviousColumn = () => {
    let nextIndex = this.state.focusOnColumnIndex - 1;

    if (nextIndex >= -1) {
      this._handleFocusColumn(nextIndex);
    }
  };

  _handleFocusColumn = (columnIndex) => {
    this.setState({
      isTopConceptFocused: columnIndex == -1,
      focusOnColumnIndex: columnIndex
    }, () => {
      if (this.state.isTopConceptFocused) {
        this._handleCenterViewOnElement(this.refs.thesaurusNode);
      }
    });
  };

  _handleFocusConcept = (conceptId) => {
    this.setState({
      focusOnConceptId: conceptId
    });
  };

  toggleBranchCreateDialog() {
    this.setState({
      dialogCreateActive: !this.state.dialogCreateActive,
      creatingBranch: {}
    });
  }

  _handleCreateInputChange(value, property) {
    let fields = this.state.creatingBranch || {};

    this.setState({
      creatingBranch: {
        ...fields,
        [property]: value
      }
    });
  }

  setLoadingState() {
    this.setState({
      loading: true
    });

    this.props.showPreloader();
  }

  clearLoadingState() {
    this.setState({
      loading: false
    });

    this.props.hidePreloader();
  }

  createBranch() {
    this.setLoadingState();

    this.props.relay.commitUpdate(
      new CreateTopSchemeMutation({
        thesaurus: this.props.thesaurus,
        ...this.state.creatingBranch
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('La branche a été créée');
          this.clearLoadingState();
          this.toggleBranchCreateDialog();
        },
        onFailure: (transaction) => {
          this.clearLoadingState();
          this.toggleBranchCreateDialog();
          this.props.showMutationError(transaction);
        }
      }
    );
  };

  /**
   * Save the hierarchie path in either URL or sessionHistory
   *
   * @param columnIds
   * @param appendState
   */
  setColumnIds(columnIds, appendState = {}) {
    if (JSON.stringify(columnIds) !== JSON.stringify(this.state.columnIds)){
      this.setState({
        columnIds,
        ...appendState
      }, () => {
        this.refreshColumns();
      });
    }
  }

  refreshColumns(callback = () => {}) {
    if (!deepEqual(this.state.columnIds, this.props.relay.variables.columnIds)) {
      this.props.showPreloader();

      this.setState({
        loading: true
      }, () => {
        this.props.relay.setVariables({columnIds: this.state.columnIds}, ({done}) => {
          if (done) {

            this.setState({
              loading: false
            });

            this.props.hidePreloader();
            callback();
          }
        });
      });
    } else {
      callback();
    }
  }

  initState(location) {
    if (!location){
      location = this.props.location;
    }

    let state = JSON.parse(sessionStorage.getItem(`${this.props.thesaurus.id}_savedTreeState`)) || {};

    this.stateSaveConfigs.map(({queryParam, key, parseFn}) => {
      let value = location.query[queryParam];

      if (value) {
        state[key] = parseFn ? parseFn(value) : value;
      }
    });

    this.setState(state, () => {
      this.refreshColumns();
    });
  }

  saveState(state) {
    if (!state) {
      state = this.state;
    }

    const {router} = this.props;
    let location = router.createLocation(window.location);

    let storage = {};

    this.stateSaveConfigs.map(({queryParam, key, stringifyFn}) => {
      storage[key] = state[key];

      if (queryParam) {
        location.query[queryParam] = stringifyFn ? stringifyFn(state[key]) : state[key];
      }
    });

    sessionStorage.setItem(`${this.props.thesaurus.id}_savedTreeState`, JSON.stringify(storage));
    this.props.router.push(location);
  }

  setConceptsInClipboard(inClipboardConceptIds) {
    this.props.showPreloader();

    this.props.relay.setVariables({inClipboardConceptIds}, (state) => {
      if (state.done) {
        this.props.hidePreloader();

        this.setState({
          snackbarActive: true,
          snackbarMessage: 'Concept sauvegardé dans le presse papier'
        });
      } else if (state.error) {
        this.props.hidePreloader();
        this.showError(state.error);
      }
    });
  }

  emptyClipboard() {
    this.props.relay.setVariables({inClipboardConceptIds: []});
  }

  _handleCenterViewOnElement = (element) => {
    let viewport = ReactDOM.findDOMNode(this.refs.treeViewport);

    if (!element.getBoundingClientRect || !viewport|| !viewport.getBoundingClientRect) {
      return;
    }

    let conceptBbox = element.getBoundingClientRect(),
      viewportBbox = viewport.getBoundingClientRect(),
      needVScroll = (conceptBbox.bottom + 50 > window.innerHeight) || conceptBbox.top < 50,
      needHScroll = (conceptBbox.right + 50 > viewportBbox.width ) || conceptBbox.left < 50;

    if (needVScroll && this.sweetVScroll) {
      this.sweetVScroll.toElement(element);
    }

    if (needHScroll && this.sweetHScroll) {
      this.sweetHScroll.toElement(element);
    }
  };

  _handleFocusNextTopScheme = (e) => {
    if (this.state.focusOnColumnIndex === 0) {
      e.preventDefault();

      const {topSchemes} = this.props.thesaurus;

      if (this.state.focusedTopSchemeIndex < topSchemes.edges.length - 1) {
        this.saveFocusedTopScheme(this.state.focusedTopSchemeIndex + 1);
      }
    }
  };

  _handleFocusPreviousTopScheme = (e) => {
    if (this.state.focusOnColumnIndex === 0) {
      e.preventDefault();

      if (this.state.focusedTopSchemeIndex > 0) {
        this.saveFocusedTopScheme(this.state.focusedTopSchemeIndex - 1);
      }
    }
  };

  _handleConceptEntered = (conceptId) => {
    return this.props.onLocationChange({
      name: 'concept',
      params: {
        thesoId: this.props.thesaurus.id,
        conceptId
      }
    });
  };

  _handleSchemeEntered = (schemeId) => {
    return this.props.onLocationChange({
      name: 'scheme',
      params: {
        thesoId: this.props.thesaurus.id,
        schemeId
      }
    });
  };

  saveFocusedTopScheme(focusedTopSchemeIndex) {
    this.setState({
      focusedTopSchemeIndex
    });
  }

  _handleWindowScroll = () => {
    let bottom = ReactDOM.findDOMNode(this).getBoundingClientRect().bottom;

    if (!this.state.loading && (bottom - 300) < window.innerHeight) {
      this.loadMoreItems();
    }
  };

  loadMoreItems() {
    const {thesaurus} = this.props;

    if (thesaurus.topSchemes.pageInfo.hasNextPage) {
      this.props.showPreloader();
      this.setState({loading: true}, () => {
        this.props.relay.setVariables({
          first: this.props.relay.variables.first + 10
        }, (readyState) => {
          if (readyState.done) {
            this.setState({loading: false});
            this.props.hidePreloader();
          }
        });
      });
    }
  }
}

export default Relay.createContainer(Component, {
  initialVariables: {
    columnIds: [],
    inClipboardConceptIds: [],
    first: 20,
  },
  fragments: {
    thesaurus: ({inClipboardConceptIds}) => Relay.QL`
      fragment on Thesaurus {
        id
        title
        description
        draftConceptsCount
        topSchemes(first: $first) {
          edges{
            node{
              id
              ${Scheme.getFragment('scheme')}
            }
          }
          pageInfo{
            hasNextPage
          }
        }
        columns(ids: $columnIds) {
          id
          ${Column.getFragment('column')},
        },
        inClipboardConcepts(ids: $inClipboardConceptIds) {
          id,
          broader{
            id
          },
          ${Column.getFragment('inClipboardConcepts')},
        }
        ${Column.getFragment('thesaurus', {inClipboardConceptIds})}
        ${CreateTopSchemeMutation.getFragment('thesaurus')}
        ${Scheme.getFragment('thesaurus')}
        ${DraftConceptsList.getFragment('thesaurus')}
        ${SearchConceptsList.getFragment('thesaurus')}
      }
    `
  }
});