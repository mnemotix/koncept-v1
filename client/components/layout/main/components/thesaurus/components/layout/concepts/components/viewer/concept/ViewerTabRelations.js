/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Relay from 'react-relay';
import classnames from 'classnames';

import Lens from './components/lens/Lens';
import Relations from './components/relations/Relations';

import style from './style.scss';

class ViewerTabRelations extends React.Component {
  render() {
    const {thesaurus, concept} = this.props;

    return (
      <div>
        <section className={classnames(style.section, style.noPadding)}>
          <Lens thesaurus={thesaurus} concept={concept} />
        </section>

        {/*<section className={style.section}>*/}
          {/*<h2 className={style['section-title']}>Relations hiérarchiques • skos:narrower</h2>*/}

          {/*<Relations thesaurus={thesaurus} concept={concept} relationType="narrowers" relationName="skos:narrower"/>*/}
        {/*</section>*/}

        <section className={style.section}>
          <h2 className={style['section-title']}>Relations associatives • skos:related</h2>

          <Relations thesaurus={thesaurus} concept={concept} relationType="related" relationName="skos:related" />
        </section>
      </div>
    )
  }
}

export default Relay.createContainer(ViewerTabRelations, {
  fragments: {
    concept: () => Relay.QL`
      fragment on Concept {
        id
        ${Lens.getFragment('concept')}
        ${Relations.getFragment('concept')}
      }
    `,
    thesaurus: () => {
      return Relay.QL`
        fragment on Thesaurus {
          id
          ${Lens.getFragment('thesaurus')},
          ${Relations.getFragment('thesaurus')}
        }`
    }
  }
});