/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    sourceConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    targetConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    targetParentConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{switchFromRelatedToNarrowerRelation}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on SwitchFromRelatedToNarrowerRelationPayload {
        newNarrowerConceptEdge,
        deletedRelatedConceptId,
        sourceConcept{
          childrenCount,
          narrowers,
          related
        },
        targetParentConcept{
          childrenCount,
          narrowers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'REQUIRED_CHILDREN',
        children: [
          Relay.QL`
            fragment on SwitchFromRelatedToNarrowerRelationPayload {
              newNarrowerConceptEdge,
              sourceConcept
            }
          `
        ]
      },
      {
        type: 'RANGE_ADD',
        parentName: 'sourceConcept',
        parentID: this.props.sourceConcept.id,
        connectionName: 'narrowers',
        edgeName: 'newNarrowerConceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'targetParentConcept',
        parentID: this.props.targetParentConcept.id,
        connectionName: 'narrowers',
        deletedIDFieldName: 'deletedRelatedConceptId',
        pathToConnection: ['targetParentConcept', 'narrowers']
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'sourceConcept',
        parentID: this.props.sourceConcept.id,
        connectionName: 'related',
        deletedIDFieldName: 'deletedRelatedConceptId',
        pathToConnection: ['sourceConcept', 'related']
      }
    ];
  }

  getVariables() {
    return {
      sourceId: this.props.sourceConcept.id,
      targetId: this.props.targetConcept.id,
      targetParentId: this.props.targetParentConcept.id
    };
  }
}
