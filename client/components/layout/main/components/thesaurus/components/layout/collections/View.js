/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import React from 'react';
import Relay from 'react-relay';

import Button from 'react-toolbox/lib/button';

import CreateDialog from './components/dialogs/CreateDialog';
import RemoveDialog from './components/dialogs/RemoveDialog';
import Preloader from '../../../../../../../preloader/Preloader';

import CollectionList from './components/list/CollectionList';
import AddCollectionMutation from './mutations/AddCollectionMutation';
import RemoveCollectionMutation from './mutations/RemoveCollectionMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../common/preloader/preloaderHoc';
import {withRouter} from 'react-router';

@withRouter
@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class View extends React.Component {

  state = {
    dialogCreateActive: false,
    dialogRemoveActive: false,
    creatingCollection: {},
    removingCollection: {},
    loading: false
  };

  renderEmptyView(){
    return (
      <div className={style.empty}>

        <div className={style['empty-message']}>
          Le thésaurus ne contient aucune collection pour le moment.
        </div>

        <Button className={style['add-collection']} primary raised
                icon="create"
                label="Créer une collection"
                onClick={this._handleToggleCreateDialog}
        />

        {this.renderDialogs()}

        <Preloader ref="preloader" />
      </div>
    )
  }

  renderDialogs(){
    return (
      <div>
        <CreateDialog
          active={this.state.dialogCreateActive}
          title='Nouvelle collection'
          inputs={this.state.creatingCollection}
          onInputChange={this._handleCreateInputChange.bind(this)}
          onCreate={this.createCollection.bind(this)}
          onCancel={this._handleToggleCreateDialog.bind(this)}
          loading={this.state.loading}
        />

        <RemoveDialog
          active={this.state.dialogRemoveActive}
          title={`Supprimer la collection ${this.state.removingCollection.title}`}
          message={`Êtes-vous sûr de vouloir supprimer la collection ${this.state.removingCollection.title} ?`}
          onOverlayClick={ this._handleToggleRemoveDialog }
          onRemove={this.removeCollection.bind(this)}
          onCancel={this._handleToggleRemoveDialog.bind(this)}
          loading={this.state.loading}
        />
      </div>
    );
  }

  render(){
    const {thesaurus, children} = this.props;

    if (thesaurus.collections.edges.length == 0) {
      return this.renderEmptyView()
    }

    return (
      <div className={style.layout}>
        <aside className={style.aside}>
          <CollectionList thesaurus={thesaurus}
                          selectedId={this.props.params.collectionId}
                          onSelected={this.showCollection.bind(this)}
                          onRemove={this._handleRemoveCollection}
          />

          <Button className={style['add-collection']}
                  primary raised
                  icon="create" label="Ajouter une collection"
                  onClick={this._handleToggleCreateDialog} />
        </aside>

        <section className={style.viewer}>
          { children }
        </section>

        {this.renderDialogs()}
      </div>
    );
  }

  showCollection(collectionId){
    const {router} = this.props;

    router.push({
      name: 'collection',
      params: {
        thesoId: this.props.thesaurus.id,
        collectionId
      }
    });
  }

  createCollection(){
    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new AddCollectionMutation({
        thesaurus: this.props.thesaurus,
        title: this.state.creatingCollection.title,
        description: this.state.creatingCollection.description
      }),
      {
        onSuccess: () => {
          this.toggleCreateDialog();
          this.props.hidePreloader();
          this.props.showSnackbar('Le schéma a été modifié');
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
          this.toggleCreateDialog();
        }
      }
    );
  }

  _handleRemoveCollection = (collection) => {
    this.setState({
      dialogRemoveActive: true,
      removingCollection: collection
    });
  };

  removeCollection(){
    const {router} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new RemoveCollectionMutation({
        thesaurus: this.props.thesaurus,
        collectionId: this.state.removingCollection.id
      }),
      {
        onSuccess: () => {
          this.toggleRemoveDialog();
          this.props.hidePreloader();
          this.props.showSnackbar('Le schéma a été modifié');

          setTimeout(() => {
            router.push({
              name: 'thesaurus',
              params: {
                thesaurusId: this.props.thesaurus.id,
              }
            });
          }, 1000);
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
          this.toggleRemoveDialog();
        }
      }
    );
  }

  _handleToggleCreateDialog = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    this.toggleCreateDialog();
  };

  toggleCreateDialog(){
    this.setState({
      dialogCreateActive : !this.state.dialogCreateActive,
      creatingCollection: {}
    });
  }

  _handleToggleRemoveDialog = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    this.toggleRemoveDialog();
  };

  toggleRemoveDialog(){
    this.setState({
      dialogRemoveActive : !this.state.dialogRemoveActive
    });
  }

  _handleCreateInputChange(value, e) {
    let fields = this.state.creatingCollection || {};

    this.setState({
      creatingCollection: {
        ...fields,
        [e.target.name]: value
      }
    });
  }
}

export default Relay.createContainer(View, {
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id,
        collections(first: 10) {
          edges{
            node {
              id
              title
            }
          }
        }
        ${CollectionList.getFragment('thesaurus')},
        ${AddCollectionMutation.getFragment('thesaurus')},
        ${RemoveCollectionMutation.getFragment('thesaurus')}
      }
    `
  }
});