/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    collection: () => Relay.QL`
      fragment on Collection {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addConceptToCollection}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddConceptToCollectionPayload {
        collectionEdge,
        collection{
          members
        },
        conceptEdge,
        concept{
          memberOf
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'concept',
        parentID: this.props.concept.id,
        connectionName: 'memberOf',
        edgeName: 'collectionEdge',
        rangeBehaviors: {
          '': 'append'
        }
      },
      {
        type: 'RANGE_ADD',
        parentName: 'collection',
        parentID: this.props.collection.id,
        connectionName: 'members',
        edgeName: 'conceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      collectionId: this.props.collection.id
    };
  }
}
