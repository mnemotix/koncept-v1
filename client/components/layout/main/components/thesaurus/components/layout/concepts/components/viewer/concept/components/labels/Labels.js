/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 14/01/2016
 */

import React from 'react';
import Relay from 'react-relay';

import {Input, IconButton, Autocomplete} from 'react-toolbox';

import AddLabelToConceptMutation from './mutations/AddLabelToConceptMutation';
import RemoveLabelForConceptMutation from './mutations/RemoveLabelForConceptMutation';
import UpdateLabelForConceptMutation from './mutations/UpdateLabelForConceptMutation';

import style from './style.scss';

import deepEqual from 'deep-equal';

import {HandledErrorMixin} from '../../../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class Labels extends React.Component {
  static propTypes = {
    ensureOneLabelPerLang: React.PropTypes.bool,
    labelType: React.PropTypes.oneOf(['prefLabels', 'altLabels', 'hiddenLabels']).isRequired
  };

  static defaultProps = {
    ensureOneLabelPerLang: false
  };

  availableLangs = {
   fr : 'Français',
   en : 'English',
   es : 'Espagnol'
  };

  state = {
    labels: this.props.concept[this.props.labelType].edges.map(({node: label}) => label),
    pendingLabel: null
  };

  componentWillReceiveProps(nextProps){
    if(!deepEqual(nextProps.concept[this.props.labelType], this.props.concept[this.props.labelType])) {
      this.setState({
        labels: nextProps.concept[this.props.labelType].edges.map(({node: label}) => label)
      });
    }
  };

  componentDidUpdate(){
    this.setFocus();
  }

  setFocus(){
    if (this.focusInput) {
      this.focusInput.getWrappedInstance().focus();
      delete this.focusInput;
    }
  }

  getLabels(){
    let labels = this.state.labels;

    if (this.state.pendingLabel) {
      labels.push(this.state.pendingLabel)
    }

    return labels;
  }

  render(){
    const {labelType, ensureOneLabelPerLang} = this.props;

    return (
      <div className={style['section-body']}>
        {this.getLabels().map((label, index) => (
            <div key={label.id || index} className={style['input-row']}>
              <Input className={style.input}
                     ref={(input) => {if(label.pending){this.focusInput = input;}}}
                     name={label.lang}
                     value={label.value || ""}
                     label={this.availableLangs[label.lang]}
                     onChange={(value, e) => {
                                return this._handleUpdateInputLabelChange(value, e.target.name, index);
                              }}
                     onBlur={this._handleCommitUpdateLabel.bind(this, label, index)}
                     onKeyPress={this._handleKeyPress.bind(this)}
              />

              <IconButton className={style['remove-button']} icon="close" onClick={this._handleDeleteLabel.bind(this, label)} />
            </div>
          ))}

        { this.renderDropdown(this.getLabels(), this.addLabel, labelType, ensureOneLabelPerLang) }
      </div>
    )
  }

  renderDropdown(currentLabels, callback, labelType, ensureOneLabelPerLang = true) {
    var remainingLangs = Object.assign({}, this.availableLangs);

    Object.keys(remainingLangs).map((lang) => {
      if (ensureOneLabelPerLang && currentLabels.findIndex((label) => label.lang == lang) != -1) {
        delete remainingLangs[lang];
      }
    });

    if (Object.keys(remainingLangs).length > 0) {
      return (
        <Autocomplete className={style['dropdown']}
                      direction="down"
                      label="Ajouter un label en..."
                      onChange={(value) => callback(value, labelType)}
                      source={remainingLangs}
                      value={''}
                      multiple={false}
        />
      )
    }
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      e.target.blur();
    }
  };

  _handleUpdateInputLabelChange(value, lang, index) {
    var {labels} = this.state,
      label = labels[index];

    labels[index] = {
      ...label,
      value,
      lang
    };

    this.setState({labels});
  }

  addLabel = (lang) => {
    var labels = this.getLabels(),
      pendingLabel = {
        value: '',
        lang,
        pending: true
      };

    labels.push(pendingLabel);

    this.setState({
      labels
    });
  };

  _handleCommitUpdateLabel = (label, index) => {
    const {concept, labelType} = this.props,
      {value, lang} = label;

    // Case of newly created label pending save.
    if (label.pending) {
      let labels = this.getLabels();

      // Nothing in here, just remove it from the view.
      if (value == '') {
        labels.splice(index, 1);

        return this.setState({
          labels
        });
      } else {
        this.props.showPreloader();

        this.props.relay.commitUpdate(new AddLabelToConceptMutation({
          concept,
          labelType,
          value,
          lang
        }),{
          onSuccess: () => {
            this.props.showSnackbar('Le label a été ajouté');
            this.props.hidePreloader();
          },
          onFailure: (transaction) => {
            this.props.showMutationError(transaction);
            this.props.hidePreloader();
          }
        });
      }

    // Case of removing en existing label
    } else if (value == '') {
      this._handleDeleteLabel(label);
    } else {
      this.props.showPreloader();

      this.props.relay.commitUpdate(new UpdateLabelForConceptMutation({
        concept,
        label,
        labelType,
        value,
        lang
      }),{
        onSuccess: () => {
          this.props.showSnackbar('Le label a été modifié');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this.props.showMutationError(transaction);
          this.props.hidePreloader();
        }
      });
    }
  };

  _handleDeleteLabel = (label) => {
    const {concept, labelType} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(new RemoveLabelForConceptMutation({
      concept,
      label,
      labelType
    }),{
      onSuccess: () => {
        this.props.showSnackbar('Le label a été supprimé');
        this.props.hidePreloader();
      },
      onFailure: (transaction) => {
        this.props.showMutationError(transaction);
        this.props.hidePreloader();
      }
    });
  };
}


export default Relay.createContainer(Labels, {
  fragments: {
    concept: () => {
      return Relay.QL`
        fragment on Concept {
          id,
          prefLabels(first: 50){
            edges{
              node{
                id
                lang
                value
                ${UpdateLabelForConceptMutation.getFragment('label')}
              }
            }
          }
          altLabels(first: 50){
            edges{
              node{
                id
                lang
                value
                ${UpdateLabelForConceptMutation.getFragment('label')}
              }
            }
          }
          hiddenLabels(first: 50){
            edges{
              node{
                id
                lang
                value
                ${UpdateLabelForConceptMutation.getFragment('label')}
              }
            }
          }
          ${UpdateLabelForConceptMutation.getFragment('concept')}
          ${AddLabelToConceptMutation.getFragment('concept')}
          ${RemoveLabelForConceptMutation.getFragment('concept')}
        }
      `
    }
  }
});