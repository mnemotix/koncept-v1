/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Relay from 'react-relay';

import FontIcon from 'react-toolbox/lib/font_icon';

import style from './style.scss';

import Matchers from './components/matchers/Matchers';

class ViewerTabMatchers extends React.Component {
  render() {
    const {concept} = this.props;

    return (
      <section className={style.section}>
        <h4 className={style['section-title']}>
          <FontIcon className={style['section-icon']} value="location_searching"/> Expressions de recherche

          <div className={style['section-subtitle']}>
            <p>
              Les expressions de recherche sont liées aux labels et utilisées par le moteur de recherche pour
              percoler en temps réel des concepts liés au contenu des objets consultés.
            </p>
            <p>
              Le formalisme d'expression de recherche suit le standard des <a href="https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html#_simple_query_string_syntax" target="_blank"> recherches de texte simples ElasticSearch</a>
            </p>
          </div>
        </h4>

        {this.renderMatchersForLabels(concept.prefLabels, 'prefLabels')}
        {this.renderMatchersForLabels(concept.altLabels || [], 'altLabels')}
        {this.renderMatchersForLabels(concept.hiddenLabels || [], 'hiddenLabels')}
      </section>
    )
  }

  renderMatchersForLabels(labels, type) {
    return labels.edges.map(({node: label}, index) => (
      <div key={`${type}-${index}`} className={style['matchers-section']}>
        <div className={style.label}>
          <span className={style['label-header']}>{type}@{label.lang}</span>
          <span className={style['label-value']}>
            {label.value}
          </span>
        </div>

        <div className={style.matchers}>
          <Matchers label={label} labelType={type} />
        </div>
      </div>
    ));
  }
}

export default Relay.createContainer(ViewerTabMatchers, {
  fragments: {
    concept: () => Relay.QL`
      fragment on Concept {
        id,
        prefLabels(first: 50){
          edges{
            node{
              lang
              value
              ${Matchers.getFragment('label')}
            }
          }
        }
        altLabels(first: 50){
          edges{
            node{
              lang
              value
              ${Matchers.getFragment('label')}
            }
          }
        }
        hiddenLabels(first: 50){
          edges{
            node{
              lang
              value
              ${Matchers.getFragment('label')}
            }
          }
        }
      }
    `
  }
});