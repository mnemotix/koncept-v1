/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    parentConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{setNarrowerRelation}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on SetNarrowerRelationPayload {
        newConceptEdge,
        parentConcept{
          childrenCount,
          narrowers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'REQUIRED_CHILDREN',
        children: [Relay.QL`
          fragment on SetNarrowerRelationPayload {
            newConceptEdge,
            parentConcept{
              childrenCount
            }
          }
      `],
      },
      {
        type: 'RANGE_ADD',
        parentName: 'parentConcept',
        parentID: this.props.parentConcept.id,
        connectionName: 'narrowers',
        edgeName: 'newConceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      parentId: this.props.parentConcept.id,
      narrowerId: this.props.narrowerId
    };
  }
}
