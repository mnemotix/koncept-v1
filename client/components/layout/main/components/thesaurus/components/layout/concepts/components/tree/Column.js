/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';

import Concept from './Concept';
import Scheme from './Scheme';

import classnames from 'classnames';

import keyboard from 'keyboardjs';

import style from './style.scss';

import {preloaderHoc} from       '../../../../../../../../../common/preloader/preloaderHoc';

import {withRouter} from 'react-router';

import deepEqual from 'deep-equal';

@preloaderHoc
@withRouter
class Component extends React.Component {
  static propTypes = {
    // This type is important to uniquely retrieve this concept across views
    columnIndex: React.PropTypes.number.isRequired,
    prefixKey: React.PropTypes.string.isRequired,
    onNodeExpanded: React.PropTypes.func.isRequired,
    onNodeRetracted: React.PropTypes.func.isRequired,
    onNodeEntered: React.PropTypes.func.isRequired,
    onSchemeSelected: React.PropTypes.func.isRequired,
    onCenterViewOnConcept: React.PropTypes.func.isRequired,
    expandedConceptId: React.PropTypes.string,
    focusedConceptId: React.PropTypes.string,
    isTopColumn: React.PropTypes.bool,
    isLastColumn: React.PropTypes.bool,
    isBeforeLastColumn: React.PropTypes.bool,
    isColumnFocused: React.PropTypes.bool,
    onFocusColumn: React.PropTypes.func.isRequired,
    onFocusConcept: React.PropTypes.func.isRequired,
    onSelectConcept: React.PropTypes.func
  };

  state = {
    loading: false,
    focusedConceptId: null,
    savedFocusedId: null
  };

  componentWillReceiveProps(nextProps){
    if(nextProps.column.id !== this.props.column.id) {
      this.setState({
        focusedConceptId: null,
        savedFocusedId: null
      });
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
    return !(deepEqual(nextProps, this.props) && deepEqual(nextState, this.state));
  }

  componentDidMount(){
    this.initKeyboardListeners();
    window.addEventListener('scroll', this._handleWindowScroll);
    this.initState();
  }

  componentDidUpdate(){
    this.initKeyboardListeners();
    this.initState();
  }

  initState(){
    if(this.props.isColumnFocused) {
      this.setState({
        focusedConceptId: this.getFocusedConceptIndex() >= 0  ? this.props.focusedConceptId : this.state.savedFocusedId || this.getItems()[0].id
      });
    }
  }

  clearKeyboardListeners(){
    keyboard.unbind('down', this._handleFocusNextConcept);
    keyboard.unbind('up', this._handleFocusPreviousConcept);
  }

  initKeyboardListeners(){
    this.clearKeyboardListeners();

    keyboard.withContext('concept-tree', () => {
      if (this.props.isColumnFocused) {
        keyboard.bind('down', this._handleFocusNextConcept);
        keyboard.bind('up', this._handleFocusPreviousConcept);
      }
    });
  }

  componentWillUnmount(){
    this.clearKeyboardListeners();
    window.removeEventListener('scroll', this._handleWindowScroll);
  }

  getItems(){
    const {column} = this.props;

    if (column.__typename === "Concept") {
      return column.narrowers.edges.map(({node: item}) => item);
    } else {
      return column.topConcepts.edges.map(({node: item}) => item);
    }
  }

  render() {
    return this.renderList(this.getItems()) || null;
  }

  renderList(items) {
    let isSelectedPreviousSibling = !!this.props.expandedConceptId,
      listClassNames = classnames({
      [style['concept-list']]: true,
      [style['contains-expanded']] : isSelectedPreviousSibling
    });

    if (items.length > 0) {
      return (
        <div className={listClassNames}>
          {items.map(item => {
            let itemClassNames = classnames({
              [style['concept-item']] : true,
              [style['selected-sibling']] : isSelectedPreviousSibling,
              [style['expanded']] : this.props.expandedConceptId === item.id
            });

            if (this.props.expandedConceptId === item.id) {
              isSelectedPreviousSibling = false;
            }

            return (
              <div key={item.id} className={itemClassNames}>
                <Concept thesaurus={this.props.thesaurus}
                         concept={item}
                         parent={this.props.column}
                         isSelected={this.props.selectedConceptId === item.id}
                         isExpanded={this.props.expandedConceptId === item.id}
                         isLastExpanded={this.props.isBeforeLastColumn && this.props.expandedConceptId === item.id}
                         isFocused={this.props.isColumnFocused && this.state.focusedConceptId === item.id}
                         cutConceptId={this.props.cutConceptId}
                         isCut={!!this.props.inClipboardConcepts.find(ct => ct.id === item.id)}
                         selectedConceptId={this.props.selectedConceptId}
                         onNodeEntered={this._handleNodeEntered}
                         onNodeExpanded={this._handleNodeExpanded}
                         onNodeRetracted={this._handleNodeRetracted}
                         onSchemeSelected={this.props.onSchemeSelected}
                         onCenterViewOnConcept={this.props.onCenterViewOnConcept}
                         onNodeCut={this.props.onNodeCut}
                         onConceptClicked={this._handleConceptClicked}
                         onEmptyClipboard={this.props.onEmptyClipboard}
                         inClipboardConcepts={this.props.inClipboardConcepts}
                         inLastColumn={this.props.isLastColumn}
                         onSelectConcept={this.props.onSelectConcept}
                />
              </div>
            );
          })}

          {this.state.loading ? (
            <div className={style.loading}>Chargement des suivants...</div>
          ) : null}
        </div>
      );
    }
  }

  _handleNodeExpanded = (conceptId) => {
    this.props.onNodeExpanded(conceptId, this.props.expandedConceptId);
  };

  _handleNodeRetracted = (conceptId) => {
    this.props.onNodeRetracted(conceptId);
  };

  _handleNodeEntered = (conceptId) => {
    keyboard.unbind('down', this._handleFocusNextConcept);
    keyboard.unbind('up', this._handleFocusPreviousConcept);

    this.props.onNodeEntered(conceptId);
  };

  _handleConceptClicked = (itemId) => {
    this.props.onFocusColumn(this.props.columnIndex);
    this.focusOnConcept(itemId);
  };

  _handleFocusNextConcept = (e) => {
    e.preventDefault();

    let items = this.getItems();
    let selectedConceptIndex = Math.max(0, this.getFocusedConceptIndex());

    if (selectedConceptIndex < items.length - 1) {
      this.focusOnConcept(items[selectedConceptIndex + 1].id);
    }
  };

  _handleFocusPreviousConcept = (e) => {
    e.preventDefault();

    let items = this.getItems();
    let selectedConceptIndex = Math.max(0, this.getFocusedConceptIndex());

    if (selectedConceptIndex > 0) {
      this.focusOnConcept(items[selectedConceptIndex - 1].id);
    }
  };

  focusOnConcept(id){
    this.setState({
      savedFocusedId: id
    }, () => {
      this.props.onFocusConcept(id);
    });
  }

  getFocusedConceptIndex(){
    let items = this.getItems();

    if (this.props.focusedConceptId) {
      return items.findIndex(item => item.id == this.props.focusedConceptId);
    } else {
      return -1;
    }
  }

  _handleWindowScroll = () => {
    let bottom = ReactDOM.findDOMNode(this).getBoundingClientRect().bottom;

    if (!this.state.loading && (bottom - 300) < window.innerHeight) {
      this.loadMoreItems();
    }
  };

  loadMoreItems() {
    const {column} = this.props;
    let connection;

    if (column.__typename === "Concept") {
      connection = column.narrowers;
    } else {
      connection = column.topConcepts;
    }

    if (connection.pageInfo.hasNextPage) {
      this.props.showPreloader();

      this.setState({loading: true}, () => {
        this.props.relay.setVariables({
          first: this.props.relay.variables.first + 10
        }, (readyState) => {
          if (readyState.done) {
            this.setState({loading: false});
            this.props.hidePreloader();
          }
        });
      });
    }
  }
}

export default Relay.createContainer(Component, {
  initialVariables: {
    first: 10
  },
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus{
        id,
        ${Concept.getFragment('thesaurus')}
      }
    `,
    inClipboardConcepts: () => Relay.QL`
      fragment on Concept @relay(plural: true){
        id,
        ${Concept.getFragment('inClipboardConcepts')}
      }
    `,
    column: () => Relay.QL`
      fragment on SkosElementInterface{
        id
        __typename
        
        ...on Concept{
          narrowers(first: $first){
            edges{
              node {
                id
                ${Concept.getFragment('concept')}
              }
            }
            pageInfo{
              hasNextPage
            }
          }
        }
        
        ...on Scheme{
          topConcepts(first: $first){
            edges{
              node {
                id
                ${Concept.getFragment('concept')}
              }
            }
            pageInfo{
              hasNextPage
            }
          }
        }
        
        ${Concept.getFragment('parent')}
      }
    `
  }
});
