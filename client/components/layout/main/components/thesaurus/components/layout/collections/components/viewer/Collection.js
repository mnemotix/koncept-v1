/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import React from 'react';
import ReactDOM from 'react-dom';
import Relay from 'react-relay';

import {List, ListItem, ListSubHeader} from 'react-toolbox/lib/list';
import FontIcon from 'react-toolbox/lib/font_icon';

import Concept from '../../../concepts/components/tree/Concept';
import RemoveDialog from '../../components/dialogs/RemoveDialog';
import EditableContent from '../../../../../../../../../input/EditableContent';

import RemoveConceptFromCollectionMutation from './mutations/RemoveConceptFromCollectionMutation';
import UpdateCollectionMutation from './mutations/UpdateCollectionMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class Collection extends React.Component {

  state = {
    dialogRemoveActive: false,
    removingConcept: null
  };

  getPrefLabelValueForCurrentLang(concept) {
    var prefLabel = concept.prefLabels.edges.find(({node: prefLabel}) => prefLabel.lang == 'fr');

    return prefLabel ? prefLabel.node.value : '[LABEL NON RENSEIGNÉ]';
  }

  renderConceptsList(){
    const {collection} = this.props;

    return (
      <List ripple selectable className={style.concepts}>
        <ListSubHeader caption='Concepts de la collection' />
        {collection.members.edges.map(edge => (
          <ListItem className={style.concept} key={edge.node.id} caption={this.getPrefLabelValueForCurrentLang(edge.node)}>
            <FontIcon className={style.remove} value="delete" onClick={event => this._handleRemoveConceptFromCollection(edge.node, event)}/>
          </ListItem>
        ))}
      </List>
    );
  }

  renderEmptyMessage(){
    return (
      <div className={style.empty}>Aucun concept n'est associé à cette collection</div>
    );
  }

  renderDialog(){
    if (!this.state.removingConcept) {
      return;
    }

    const conceptLabel = this.getPrefLabelValueForCurrentLang(this.state.removingConcept);

    return (
      <RemoveDialog
        active={this.state.dialogRemoveActive}
        title={`Supprimer un concept`}
        message={`Êtes-vous sûr de vouloir supprimer le concept ${conceptLabel} de la collection ?`}
        onOverlayClick={ this._handleToggleRemoveDialog }
        onRemove={this.removeConceptFromCollection.bind(this)}
        onCancel={this._handleToggleRemoveDialog.bind(this)}
      >
      </RemoveDialog>
    );
  }

  render() {
    const {collection} = this.props;

    return (
      <div>
        <EditableContent className={style.title} inputClassName={style.input}
                         placeholder="Ajouter un titre..."
                         onSave={(title) => this.updateCollection({title})}>
          {collection.title}
        </EditableContent>

        <EditableContent className={style.description} inputClassName={style.input}
                         placeholder="Ajouter une description..."
                         onSave={(description) => this.updateCollection({description})}>
          {collection.description}
        </EditableContent>

        {collection.members.edges.length > 0 ? this.renderConceptsList() : this.renderEmptyMessage()}

        {this.renderDialog()}
      </div>
    )
  }

  _handleToggleRemoveDialog = (e) => {
    if (e) {
      e.stopPropagation();
      e.preventDefault();
    }

    this.toggleRemoveDialog();
  };

  toggleRemoveDialog(){
    this.setState({
      dialogRemoveActive : !this.state.dialogRemoveActive
    });
  }

  _handleRemoveConceptFromCollection = (concept, event) => {
    event.stopPropagation();
    this.setState({
      removingConcept: concept
    });
    this.toggleRemoveDialog();
  };

  updateCollection(props){
    const {collection} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new UpdateCollectionMutation({
        collection: collection,
        title: collection.title,
        description: collection.description,
        ...props
      }),
      {
        onSuccess: () => {
          this.props.hidePreloader();
          this.props.showSnackbar('La collection a été modifiée');
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  removeConceptFromCollection(){
    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new RemoveConceptFromCollectionMutation({
        collection: this.props.collection,
        conceptId: this.state.removingConcept.id
      }),
      {
        onSuccess: () => {
          this.props.hidePreloader();
          this.props.showSnackbar('La concept a été supprimé de la collection');
          this.toggleRemoveDialog();

        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
          this.toggleRemoveDialog();
        }
      }
    );
  }
}

export default Relay.createContainer(Collection, {
  fragments: {
    collection: () => Relay.QL`
      fragment on Collection {
        id
        title
        description
        members(first: 30){
          edges{
            node{
              id
              prefLabels(first: 50){
                edges{
                  node{
                    value
                    lang
                  }
                }
              }
            }
          }
        },
        ${RemoveConceptFromCollectionMutation.getFragment('collection')}
        ${UpdateCollectionMutation.getFragment('collection')}
      }
    `
  }
});