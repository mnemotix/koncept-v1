/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 14/01/2016
 */

import React from 'react';
import Relay from 'react-relay';

import {Tab, Tabs, Button} from 'react-toolbox';
import AffixWrapper from '../../../../../../../../../../affix/AffixWrapper';

import style from './style.scss';
import {withRouter} from 'react-router';

import ValidateConceptMutation from './mutations/ValidateConceptMutation';

const TABS = ['concept', 'concept-relations', 'concept-matchers', 'concept-rules'];

import {preloaderHoc} from '../../../../../../../../../../preloader/preloaderHoc';
import {HandledErrorMixin} from '../../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../../../../../../../common/snackbar/snackbarHoc';

@withRouter
@preloaderHoc
@HandledErrorMixin
@snackbarHoc
class ConceptViewer extends React.Component {
  hasRelations(){
    const {related, narrowers} = this.props.concept;

    return related.edges.length > 0 || narrowers.edges.length > 0;
  }


  render() {
    const {thesaurus, concept, router} = this.props;

    let tabIndex = TABS.indexOf(this.props.children.props.routerProps.route.name);

    return (
      <div className={this.props.className}>

        <AffixWrapper className={style["header-wrapper"]} affixClassName={style.affix}>
          <header className={style.header}>
            <div className={style["header-wrap"]}>
              <div className={style.preTitle}>Concept {NODE_ENV === "dev" ? `(${atob(concept.id)})` : null}</div>

              {concept.isDraft ? (
                <Button className={style.checkButton} icon="check" label="Valider ce concept" raised onClick={this._handleValidate}/>
              ) : null}


              <h3 className={style.title}>
                {this.props.concept.label.value}
              </h3>

              <Tabs className={style.tabs} index={tabIndex !== -1 ? tabIndex : 0}>
                <Tab style={style.tab} label="Propriétés" onClick={() => router.push({name: 'concept', params: {thesoId: thesaurus.id, conceptId: concept.id}})} />
                <Tab style={style.tab} label="Relations"  onClick={() => router.push({name: 'concept-relations', params: {thesoId: thesaurus.id, conceptId: concept.id}})}/>
                <Tab style={style.tab} label="Expressions de recherche"  onClick={() => router.push({name: 'concept-matchers', params: {thesoId: thesaurus.id, conceptId: concept.id}})}/>
                <Tab style={style.tab} label="Règles"  onClick={() => router.push({name: 'concept-rules', params: {thesoId: thesaurus.id, conceptId: concept.id}})}/>
              </Tabs>
            </div>
          </header>
        </AffixWrapper>

        {this.props.children}
      </div>
    )
  }

  _handleValidate = () => {
    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new ValidateConceptMutation({
        concept: this.props.concept,
        thesaurus:  this.props.thesaurus
      }),
      {
        onSuccess: () => {
          this.props.showSnackbar('Le concept a été validé');
          this.props.hidePreloader();
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  };
}


export default Relay.createContainer(ConceptViewer, {
  fragments: {
    concept: () => {
      return Relay.QL`
        fragment on Concept {
          id
          label: prefLabelForLang(lang: "fr"){
            value
          }
          isDraft
          narrowers(first: 1){
            edges{
              node{
                id
              }
            }
          }
          related(first: 1){
            edges{
              node{
                id
              }
            }
          }
          ${ValidateConceptMutation.getFragment('concept')}
        }
      `
    },

    thesaurus: () => {
      return Relay.QL`
        fragment on Thesaurus {
          id
          ${ValidateConceptMutation.getFragment('thesaurus')}
        }`
    }
  }
});