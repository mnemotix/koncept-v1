/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    label: () => Relay.QL`
      fragment on LocalizedLabel {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addMatcherToLabel}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddMatcherToLabelPayload {
        newMatcherEdge
        label{
          matchers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'label',
        parentID: this.props.label.id,
        connectionName: 'matchers',
        edgeName: 'newMatcherEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      labelId: this.props.label.id,
      expression: this.props.expression
    };
  }
}
