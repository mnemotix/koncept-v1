/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import Relay from 'react-relay';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import MockNetworkLayer from 'mock-network-layer';
import Matchers from '../components/matchers/Matchers';

import { shallow } from 'enzyme';

import ViewerTabExpressions from '../ViewerTabMatchers';

var fixtures = {
  concept: {
    "id": "Q29uY2VwdDpNYXcya0Q=",
    "prefLabels": [
      {
        "id": "TGFiZWw6QjI5aHJx",
        "value": "Foo Zi",
        "lang": "fr",
        "matchers": [
          {
            id: "TGFiZWw6QjI5aHaaa",
            expression: "foo zi"
          },
          {
            id: "TGFiZWw6QjI5aHbbb",
            expression: "foo_zi"
          }
        ]
      }
    ],
    "altLabels" : [
      {
        "id": "TGFiZWw6MExBZlFu",
        "value": "Bar",
        "lang": "en",
        "matchers": [
          {
            id: "TGFiZWw6QjI5aHccc",
            expression: "bar"
          }
        ]
      }
    ],
    "hiddenLabels" : [
      {
        "id": "TGFiZWw6UkxVTjRO",
        "value": "Baz",
        "lang": "es"
      }
    ]
  }
};

describe('Concept viewer over matchers tab', () => {
  var wrapper;

  beforeEach(() => {
    wrapper = shallow(<ViewerTabExpressions {...fixtures} />);
  });

  it('renders', () => {
    expect(wrapper).toBeTruthy();
  });

  it('displays 3 labels sections', () => {
    expect(wrapper.find(Matchers).length).toEqual(3);
  });
});
