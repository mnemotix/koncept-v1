/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class ValidateConceptMutation extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{validateConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on ValidateConceptPayload {
        conceptId
        concept{
          id
          isDraft
        }
        thesaurus{
          id
          draftConceptsCount
          draftConcepts
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          concept: this.props.concept.id
        }
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          thesaurus: this.props.thesaurus.id
        }
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'draftConcepts',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['thesaurus', 'draftConcepts']
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      thesoId: this.props.thesaurus.id
    };
  }
}
