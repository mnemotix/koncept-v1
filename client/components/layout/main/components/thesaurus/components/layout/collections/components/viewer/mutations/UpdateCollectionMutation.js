/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    collection: () => Relay.QL`
      fragment on Collection {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{updateCollection}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateCollectionPayload {
        collection{
          title
          description
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          collection: this.props.collection.id
        }
      }
    ];
  }

  getVariables() {
    return {
      title: this.props.title,
      description: this.props.description,
      collectionId: this.props.collection.id
    };
  }
}
