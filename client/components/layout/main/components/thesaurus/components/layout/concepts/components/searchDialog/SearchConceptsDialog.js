/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import {Dialog, Input, FontIcon} from 'react-toolbox';
import classnames from 'classnames';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../../../../../../error/HandledErrorMixin';
import {snackbarHoc} from '../../../../../../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from '../../../../../../../../../common/preloader/preloaderHoc';

@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class SearchConceptDialog extends React.Component {
  static propTypes = {
    onCancel: React.PropTypes.func.isRequired,
    onSelect: React.PropTypes.func.isRequired,
    active: React.PropTypes.bool,
    title: React.PropTypes.string
  };

  state = {
    query: '',
    selectedConcept: null,
    loading: false
  };

  getActions(){
    return [
      {
        label: "Annuler",
        onClick: (e) => {
          e.stopPropagation();
          e.preventDefault();
          return this.props.onCancel();
        }
      }
    ].concat(this.state.selectedConcept ? [
      {
        label: "Sélectionner",
        onClick: (e) => {
          e.stopPropagation();
          e.preventDefault();
          return this.props.onSelect(this.state.selectedConcept);
        }
      }
    ] : []);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active === true && this.props.active === false) {
      this.setFocus = true;
    }
  }

  componentDidUpdate() {
    if (this.setFocus) {
      this.refs['queryInput'].getWrappedInstance().focus();
      this.setFocus = false;
    }
  }

  render() {
    const {thesaurus} = this.props;

    return (
      <Dialog
        actions={this.getActions()}
        active={this.props.active}
        title={this.props.title || "Rechercher un concept"}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        type="small"
      >
        <section className={style.section}>
          <div className={style.searchInput}>
            <FontIcon className={style.icon} value="search"/>
            <Input
              ref={'queryInput'}
              type='text'
              className={style.input}
              placeholder="Chercher un concept..."
              value={this.state.query}
              onChange={this._handleInputChange.bind(this)}
            />
          </div>



          {this.state.loading ? (
              <div className={style.empty}>Recherche en cours...</div>
            )
            : this.props.relay.variables.isSearching ? thesaurus.searchConcepts.length === 0 && this.state.query !== '' ?
              (
                <div className={style.empty}>Aucun concept n'a été trouvé...</div>
              ) :
              (
                <div className={style.resultsLayout}>
                  <div className={style.results}>
                    {this.renderList()}
                  </div>
                </div>
              ) : null}
        </section>
      </Dialog>
    )
  }

  renderList(){
    const {thesaurus} = this.props;

    return thesaurus.searchConcepts.map(searchConcept => this.renderConcept(searchConcept));
  }

  renderConcept(concept, displayHierarchy = true){
    return (
      <div key={concept.id} className={classnames({[style.conceptWrapper]: displayHierarchy})}>
        <div className={classnames(style.concept, {
               [style.selected]: this.state.selectedConcept && this.state.selectedConcept.id === concept.id
             })}
             onClick={() => displayHierarchy ? this._handleSelect(concept) : null}

        >

          <div className={style.inner}>
            {concept.prefLabel.value}
          </div>
        </div>

        {displayHierarchy ? (
          <div className={classnames(style.hierarchy, {[style.show]: this.state.selectedConcept && this.state.selectedConcept.id === concept.id })}>
            {this.renderConceptHierachy(concept, false)}
          </div>
        ) : null}

      </div>
    );
  }

  renderScheme(scheme) {
    return (
      <div key={scheme.id}
           className={classnames(style.scheme, style.concept)}
           onClick={this._handleSelect.bind(this, scheme)}
      >

        <div className={style.inner}
             style={{backgroundColor: scheme.color}}
        >
          {scheme.title}
        </div>
      </div>
    );
  }

  renderConceptHierachy(concept){
    let {ancestors} = concept;

    ancestors = [].concat(ancestors).reverse();

    return (
      <div>
        {ancestors.map(ancestor => ancestor.__typename === "Concept" ? this.renderConcept(ancestor, false) : this.renderScheme(ancestor))}
      </div>
    );
  }

  _handleInputChange = (query) => {
    this.setState({query, loading: true});

    if (this.keyTimeout) {
      clearTimeout(this.keyTimeout);
    }

    this.keyTimeout = setTimeout(() => {
      this.props.relay.setVariables({
        query: this.state.query,
        isSearching: true
      }, ({done}) =>{
        if(done){
          this.setState({loading: false})
        }
      });
    }, 100);

  };

  _handleSelect = (concept) => {
    this.setState({selectedConcept: this.state.selectedConcept === concept ? null : concept});
  };
}

export {SearchConceptDialog};

export default Relay.createContainer(SearchConceptDialog, {
  initialVariables: {
    query: '',
    first: 10,
    isSearching: false
  },
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus{
        id
        searchConcepts(query: $query, first: $first) @include(if: $isSearching){
          id
          prefLabel: prefLabelForLang(lang: "fr"){
            value
          }
          broader{
            id
          }
          ancestors{
            id
            __typename
            ...on Concept{
              prefLabel: prefLabelForLang(lang: "fr"){
                value
              }
            }
            
            ...on Scheme {
              title
              color
            }
          }
        }
      }
    `
  }
});
