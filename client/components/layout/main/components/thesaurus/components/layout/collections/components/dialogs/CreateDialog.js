/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';
import classnames from 'classnames';
import Dropdown from 'react-toolbox/lib/dropdown';

import style from './style.scss';

export default class extends React.Component {
  static propTypes = {
    loading: React.PropTypes.bool
  };

  render(){
    let actions = [
      {
        label: "Annuler",
        onClick: (e) => {
          if(!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      },
      {
        label: this.props.loading ? "En cours..." : "Créer",
        onClick: (e) => {
          if(!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCreate();
          }
        }
      }
    ];

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
      >
        <section className={style.section}>
          <Input required={true}
                 label='Nom de la collection'
                 name='title'
                 value={this.props.inputs.title || ""}
                 onChange={this.props.onInputChange}
                 ref={'titleInput'}
                 onKeyPress={this._handleKeyPress.bind(this)}
          />

          <Input required={true}
                 label='Description'
                 name='description'
                 value={this.props.inputs.description || ""}
                 onChange={this.props.onInputChange}
                 onKeyPress={this._handleKeyPress.bind(this)}
          />
        </section>
      </Dialog>
    )
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.active == true && this.props.active == false) {
      this.setFocus = true ;
    }
  }

  componentDidUpdate(){
    if (this.setFocus) {
      this.refs['titleInput'].getWrappedInstance().focus();
      this.setFocus = false;
    }
  }

  _handleKeyPress = (e) => {
    let code = e.which || e.keyCode;

    if (code == 13) {
      this.props.onCreate();
    }
  };
}