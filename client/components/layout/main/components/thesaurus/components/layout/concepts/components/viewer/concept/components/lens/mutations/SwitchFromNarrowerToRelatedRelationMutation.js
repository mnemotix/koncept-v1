/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    sourceConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    targetConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    topConcept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{switchFromNarrowerToRelatedRelation}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on SwitchFromNarrowerToRelatedRelationPayload {
        newRelatedConceptEdge,
        newNarrowerConceptEdge,
        deletedNarrowerConceptId,
        sourceConcept{
          childrenCount,
          narrowers,
          related
        },
        topConcept{
          narrowers
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'REQUIRED_CHILDREN',
        children: [
          Relay.QL`
            fragment on SwitchFromNarrowerToRelatedRelationPayload {
              newRelatedConceptEdge,
              sourceConcept
            }
          `,
          Relay.QL`
            fragment on SwitchFromNarrowerToRelatedRelationPayload {
              newNarrowerConceptEdge,
              topConcept
            }
          `
        ]
      },
      {
        type: 'RANGE_ADD',
        parentName: 'sourceConcept',
        parentID: this.props.sourceConcept.id,
        connectionName: 'related',
        edgeName: 'newRelatedConceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      },
      {
        type: 'RANGE_ADD',
        parentName: 'topConcept',
        parentID: this.props.topConcept.id,
        connectionName: 'narrowers',
        edgeName: 'newNarrowerConceptEdge',
        rangeBehaviors: {
          '': 'append'
        }
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'sourceConcept',
        parentID: this.props.sourceConcept.id,
        connectionName: 'narrowers',
        deletedIDFieldName: 'deletedNarrowerConceptId',
        pathToConnection: ['sourceConcept', 'narrowers']
      }
    ];
  }

  getVariables() {
    return {
      sourceId: this.props.sourceConcept.id,
      targetId: this.props.targetConcept.id,
      topConceptId: this.props.topConcept.id
    };
  }
}
