/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import classnames from 'classnames';

import {Input, FontIcon, IconButton, Tooltip} from 'react-toolbox';

const TooltipIconButton = Tooltip(IconButton);

import style from './style.scss';

import {withRouter} from 'react-router';

@withRouter
class SearchConceptsList extends React.Component {

  static propTypes = {
    focusOnConceptId: React.PropTypes.string,
    onSelectedConcept: React.PropTypes.func.isRequired,
    onQueryChange: React.PropTypes.func.isRequired,
    query: React.PropTypes.string
  };

  state = {
    query: '',
    loading: false
  };

  componentWillMount(){
    if(this.props.query){
      this._handleQueryChange(this.props.query);
    }
  }
  render(){
    const {thesaurus, className} = this.props;

    return (
      <div>
        <div className={style.searchInput}>
          <FontIcon className={style.icon} value="search"/>
          <Input
            className={style.input}
            ref={'queryInput'}
            type='text'
            placeholder="Chercher un concept..."
            value={this.state.query}
            onChange={this._handleQueryChange}
          />
        </div>

        { this.state.loading ? (
          <div className={style.loading}>
            Recherche en cours...
          </div>
        ) : this.props.relay.variables.isSearching ? thesaurus.searchConcepts.map((concept) => (
          <div className={classnames(style.concept, {[style.selected] : this.props.focusOnConceptId === concept.id})}
               key={concept.id}
               onClick={() => this._handleClick(concept)}>
            <div className={style.inner}>
              { concept.prefLabel.value  }

              {concept.isDraft ? (
                <TooltipIconButton
                  className={style.isDraftButton}
                  icon="verified_user"
                  tooltip={"Ce concept est en mode brouillon et nécéssite d'être validé"}
                  tooltipPosition="top"
                />
              ) : null}
            </div>
          </div>
        )) : null}

        {!this.state.loading && thesaurus.searchConcepts && thesaurus.searchConcepts.length === 0 ? (
          <div className={style.loading}>
            Aucun résultat trouvé...
          </div>
        ) : null}
      </div>
    );
  }

  _handleQueryChange = (query) => {
    this.setState({query}, () => {
      if(this.queryTimeout) {
        clearTimeout(this.queryTimeout);
      }

      this.queryTimeout = setTimeout(() => {
        this.setState({
          loading: true
        });

        this.props.relay.setVariables({
          qs: query,
          isSearching: true
        }, (({done}) => {
          if (done) {
            this.setState({
              loading: false
            });
          }
        }));
      }, 200);
    });

    this.props.onQueryChange(query);
  };

  _handleClick = (concept) => {
    const {router, thesaurus} = this.props;
    this.props.onSelectedConcept({
      ancestors  : concept.ancestors.map(ancestor => ancestor.id),
      conceptId : concept.id
    });
  }
}

export default Relay.createContainer(SearchConceptsList, {
  initialVariables:{
    isSearching: false,
    qs: null
  },
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
        searchConcepts(first: 10, query: $qs) @include(if: $isSearching){
          id
          prefLabel: prefLabelForLang(lang: "fr"){
            value
          }
          isDraft
          ancestors{
            id
            ...on Concept{
              prefLabel: prefLabelForLang(lang: "fr"){
                value
              }
            }
            
            ...on Scheme{
              title
            }
          }
        }
      }
    `
  }
});