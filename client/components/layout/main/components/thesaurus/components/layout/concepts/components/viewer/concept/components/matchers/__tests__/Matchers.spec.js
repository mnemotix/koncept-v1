/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import Relay from 'react-relay';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import Input from 'react-toolbox/lib/input';

import Matchers from '../Matchers';
import style from '../style.scss';

var fixtures = {
  label: {
    "id": "TGFiZWw6QjI5aHJx",
    "value": "Foo Zi",
    "lang": "fr",
    "matchers": [
      {
        id: "TGFiZWw6QjI5aHaaa",
        expression: "foo zi"
      },
      {
        id: "TGFiZWw6QjI5aHbbb",
        expression: "foo_zi"
      }
    ]
  }
};

describe('Concept label matchers', () => {
  var wrapper;

  beforeEach(() => {
    wrapper = shallow(<Matchers {...fixtures} />);
  });

  it('renders', () => {
    expect(wrapper).toBeTruthy();
  });

  it('displays 2 matchers inputs', () => {
    expect(wrapper.find(Input).length).toEqual(2);
  });

  it('adds a matcher field on add button clicked', () => {
    let buttons = wrapper.find('.' + style['add-button']);

    spyOn(wrapper.instance(), 'setFocus').and.callThrough();

    expect(buttons.length).toEqual(1);

    buttons.first().simulate('click');

    let inputs = wrapper.find(Input);

    expect(inputs.length).toEqual(3);

    expect(wrapper.instance().setFocus).toHaveBeenCalled();
  });


});
