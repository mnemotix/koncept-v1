/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class ReplaceConceptMutation extends Relay.Mutation {
  static fragments = {
    concept: () => Relay.QL`
      fragment on Concept {
        id
      }
    `,
    parent: () => Relay.QL`
      fragment on SkosElementInterface {
        id
      }
    `,
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{replaceConcept}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on ReplaceConceptPayload {
        conceptId
        parent{
          ...on Concept{
            narrowers
          }
          
          ...on Scheme{
            topConcepts
          }
        }
        thesaurus{
          id
          draftConceptsCount
          draftConcepts
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_DELETE',
        parentName: 'parent',
        parentID: this.props.parent.id,
        connectionName: 'narrowers',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['parent', 'narrowers']
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'parent',
        parentID: this.props.parent.id,
        connectionName: 'topConcepts',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['parent', 'topConcepts']
      },
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          thesaurus: this.props.thesaurus.id
        }
      },
      {
        type: 'RANGE_DELETE',
        parentName: 'thesaurus',
        parentID: this.props.thesaurus.id,
        connectionName: 'draftConcepts',
        deletedIDFieldName: 'conceptId',
        pathToConnection: ['thesaurus', 'draftConcepts']
      }
    ];
  }

  getVariables() {
    return {
      conceptId: this.props.concept.id,
      targetConceptId: this.props.targetConceptId,
      thesoId: this.props.thesaurus.id
    };
  }
}
