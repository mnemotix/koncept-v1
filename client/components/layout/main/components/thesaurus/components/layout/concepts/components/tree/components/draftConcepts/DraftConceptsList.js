/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import classnames from 'classnames';

import {List, ListItem, FontIcon} from 'react-toolbox/lib/list';

import style from './style.scss';

import {withRouter} from 'react-router';

@withRouter
class DraftConceptsList extends React.Component {

  static propTypes = {
    focusOnConceptId: React.PropTypes.string,
    onSelectedConcept: React.PropTypes.func.isRequired
  };

  render(){
    const {thesaurus} = this.props;

    return (
      <div className={style.list}>
        {thesaurus.draftConcepts.edges.map(({node: concept}) => (
          <div className={classnames(style.concept, {[style.selected] : this.props.focusOnConceptId === concept.id})}
               key={concept.id}
               onClick={() => this._handleClick(concept)}>
            <div className={style.inner}>
              { concept.prefLabel.value  }
            </div>
          </div>
        ))}
      </div>
    );
  }

  _handleClick = (concept) => {
    const {router, thesaurus} = this.props;
    this.props.onSelectedConcept({
      ancestors  : concept.ancestors.map(ancestor => ancestor.id),
      conceptId : concept.id
    });
  }
}

export default Relay.createContainer(DraftConceptsList, {
  fragments: {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
        draftConcepts(first: 10){
          edges{
            node{
              id
              prefLabel: prefLabelForLang(lang: "fr"){
                value
              }
              ancestors{
                id
                ...on Concept{
                  prefLabel: prefLabelForLang(lang: "fr"){
                    value
                  }
                }
                
                ...on Scheme{
                  title
                }
              }
            }
          }
        }
      }
    `
  }
});