/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 15/01/2016
 */


import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesaurus: () => Relay.QL`
      fragment on Thesaurus {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{updateThesaurus}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on UpdateThesaurusPayload {
        thesaurus{
          title
          description
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'FIELDS_CHANGE',
        fieldIDs: {
          thesaurus: this.props.thesaurus.id
        }
      }
    ];
  }

  getVariables() {
    return {
      title: this.props.title,
      description: this.props.description,
      thesaurusId: this.props.thesaurus.id
    };
  }
}
