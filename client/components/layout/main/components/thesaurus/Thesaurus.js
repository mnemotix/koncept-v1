/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import {Link} from 'react-router';
import classnames from 'classnames';
import Button from 'react-toolbox/lib/button';
import {Tab, Tabs} from 'react-toolbox/lib/tabs';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import EditableContent from '../../../../input/EditableContent';

import UpdateThesaurusMutation from './mutations/UpdateThesaurusMutation';

import style from './style.scss';

import {HandledErrorMixin} from '../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../common/preloader/preloaderHoc';
import {withRouter} from 'react-router';

@withRouter
@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class Thesaurus extends React.Component {
  state = {
    selectedConceptId: null
  };

  tabs = [
    'index', 'collections', 'graph'
  ];

  render() {
    const {thesaurus, children, router} = this.props;

    let tabIndex = this.tabs.indexOf(this.props.children.props.routerProps.route.name);

    return (
      <div className={classnames({[style.thesaurus] : true, [style['show-editor']] : this.state.selectedConceptId})}>
        <header className={style.header}>
          <div className={style['back-to-thesautheque']}>
            <Button icon="arrow_back" label="Retour à la thésauthèque" onClick={this._handleNavigateToThesautheque}/>
          </div>

          <EditableContent className={style.title}
                           inputClassName={style.input}
                           onSave={(title) => this.updateThesaurus({title})}
                           placeholder="Ajouter un titre..."
          >
            {thesaurus.title}
          </EditableContent>

          <EditableContent className={style.subtitle}
                           inputClassName={style.input}
                           onSave={(description) => this.updateThesaurus({description})}
                           placeholder="Ajouter une description..."
          >
            {thesaurus.description}
          </EditableContent>
        </header>

        <Tabs className={style.tabs} index={tabIndex != -1 ? tabIndex : 0}>
          <Tab className={style.tab} label="Hiérarchie" onClick={() => {router.push({name: 'thesaurus', params: {thesoId: thesaurus.id}})}} />
          <Tab className={style.tab} label="Collections" onClick={() => {router.push({name: 'collections', params: {thesoId: thesaurus.id}})}} />
          <Tab className={style.tab} label="Graphe global" onClick={() => {router.push({name: 'graph', params: {thesoId: thesaurus.id}})}} />
        </Tabs>

        <section className={style['tab-content']}>
          {children}
        </section>

      </div>
    )
  }

  updateThesaurus(props){
    const {thesaurus} = this.props;

    this.props.showPreloader();

    this.props.relay.commitUpdate(
      new UpdateThesaurusMutation({
        thesaurus: thesaurus,
        title: thesaurus.title,
        description: thesaurus.description,
        ...props
      }),
      {
        onSuccess: () => {
          this.props.hidePreloader();
          this.props.showSnackbar('Thésaurus mis à jour');
        },
        onFailure: (transaction) => {
          this.props.hidePreloader();
          this.props.showMutationError(transaction);
        }
      }
    );
  }

  _handleNavigateToThesautheque  = () => {
    const {router} = this.props;

    router.push({name: 'thesautheque'});
  };
}


export default Relay.createContainer(DragDropContext(HTML5Backend)(Thesaurus), {
  fragments: {
    thesaurus: () => {
      return Relay.QL`
        fragment on Thesaurus {
          id
          title
          description
          ${UpdateThesaurusMutation.getFragment('thesaurus')}
        }
      `
    }
  }
});