/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesautheque: () => Relay.QL`
      fragment on Thesautheque {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{addThesaurus}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddThesaurusPayload {
        newThesaurusEdge,
        thesautheque{
          thesauri
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'RANGE_ADD',
        parentName: 'thesautheque',
        parentID: this.props.thesautheque.id,
        connectionName: 'thesauri',
        edgeName: 'newThesaurusEdge',
        rangeBehaviors: {
          '': 'append'
        }
      }
    ];
  }

  getVariables() {
    return {
      title: this.props.title,
      description: this.props.description
    };
  }
}
