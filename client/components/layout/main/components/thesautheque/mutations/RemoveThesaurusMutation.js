/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import Relay from 'react-relay';

export default class extends Relay.Mutation {
  static fragments = {
    thesautheque: () => Relay.QL`
      fragment on Thesautheque {
        id
      }
    `
  };

  getMutation() {
    return Relay.QL`mutation{removeThesaurus}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on RemoveThesaurusPayload {
        deletedId,
        thesautheque{
          thesauri
        }
      }
    `;
  }

  getConfigs() {
    return [
      {
        type: 'NODE_DELETE',
        parentName: 'thesautheque',
        parentID: this.props.thesautheque.id,
        connectionName: 'thesauri',
        deletedIDFieldName: 'deletedId'
      }
    ];
  }

  getVariables() {
    return {
      id: this.props.id
    };
  }
}
