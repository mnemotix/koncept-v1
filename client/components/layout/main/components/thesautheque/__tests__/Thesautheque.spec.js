/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

import React from 'react';
import Relay from 'react-relay';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import MockNetworkLayer from 'mock-network-layer';

import Thesautheque from '../Thesautheque';

import {ListItem} from 'react-toolbox/lib/list';
import {MenuItem} from 'react-toolbox/lib/menu';
import Input from 'react-toolbox/lib/input';

const fixtures = {
  thesautheque: {
    id: "thesoFoo",
    thesauri: {
      edges: [
        {
          node: {
            id: 'foo1',
            name: 'Foo 1',
            description: 'Bar 1'
          }
        },
        {
          node: {
            id: 'foo 2',
            name: 'Foo 2',
            description: 'Bar 2'
          }
        }
      ]
    }
  }
};

describe('Thesautheque', () => {
  var thesautheque;

  //beforeEach(() => {
  //  thesautheque = testTree(<Thesautheque {...fixtures}/>);
  //});
  //
  //it('renders', () => {
  //  expect(thesautheque).toBeTruthy();
  //});
  //
  //it('shows a thesauri list with 2 items', () => {
  //  var items = TestUtils.scryRenderedComponentsWithType(thesautheque.element, ListItem);
  //  expect(items.length).toEqual(2);
  //});
  //
  //it('adds a thesaurus, ', (done) => {
  //  thesautheque.get('buttonAddThesaurus').click();
  //  expect(thesautheque.state.dialogCreateActive).toBe(true);
  //
  //  Relay.Store.succeedWith({}, () => {
  //    expect(thesautheque.state.dialogCreateActive).toBe(false);
  //
  //    done();
  //  });
  //
  //  thesautheque.element._handleSaveThesaurus();
  //});
});