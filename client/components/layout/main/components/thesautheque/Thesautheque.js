/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import {List, ListItem} from 'react-toolbox/lib/list';
import {IconMenu, MenuItem} from 'react-toolbox/lib/menu';
import Button from 'react-toolbox/lib/button';
import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';

import Preloader from '../../../../preloader/Preloader';

import AddThesaurusMutation from './mutations/AddThesaurusMutation';
import RemoveThesaurusMutation from './mutations/RemoveThesaurusMutation';

import style from './style.scss';


import {HandledErrorMixin} from '../../../../error/HandledErrorMixin';
import {snackbarHoc} from        '../../../../common/snackbar/snackbarHoc';
import {preloaderHoc} from       '../../../../common/preloader/preloaderHoc';
import {withRouter} from 'react-router';

@withRouter
@HandledErrorMixin
@snackbarHoc
@preloaderHoc
class Thesautheque extends React.Component {
  state = {
    dialogCreateActive: false,
    dialogRemoveActive: false,
    creatingThesaurusFields: {},
    removingThesaurus: null,
    loading: false
  };

  render() {
    var {thesautheque} = this.props;

    let createThesaurusActions = [
      { label: "Annuler", onClick: this._handleToggleThesaurusCreateDialog },
      { label: this.state.loading ? "En cours..." : "Créer", onClick: this._handleSaveThesaurus }
    ];

    let removeThesaurusActions = [
      { label: "Annuler", onClick: this._handleCloseThesaurusRemoveDialog },
      { label: this.state.loading ? "En cours..." : "Supprimer", onClick: this._handleRemoveThesaurus }
    ];

    return (
      <div>
        <header className={style.header}>
          <h2 className={style.title}>Thesauthèque</h2>

          <p className={style.subtitle}>
            Liste de tous les thésauri du projet.
          </p>
        </header>

        <section className={style.section}>
          { thesautheque.thesauri.edges.length == 0  ? (
            <div className={style.empty}>La thésauthèque ne contient aucun thésaurus pour le moment.</div>
          ) : ''}
          <List  className={style.list} ripple>
            {thesautheque.thesauri.edges.map(tsEdge => (
                <div className={style.item} key={tsEdge.node.id}>
                  <ListItem selectable caption={tsEdge.node.title} legend={tsEdge.node.description} onClick={(e) => this.navigateToThesaurus(tsEdge.node)} />
                  <IconMenu className={style['theso-icon-menu']} icon='more_vert' position='topRight' menuRipple>
                    <MenuItem value='delete' icon='delete' caption='Supprimer' onClick={(e) => this._handleOpenThesaurusRemoveDialog(tsEdge.node)}/>
                  </IconMenu>
                </div>
            ))}
          </List>

          <Button  className={style['add-thesaurus']} primary raised icon="create" label="Ajouter un thésaurus" onClick={this._handleToggleThesaurusCreateDialog} />

          <Dialog

            actions={createThesaurusActions}
            active={this.state.dialogCreateActive}
            title='Créer un thésaurus'
            onOverlayClick={this._handleToggleThesaurusCreateDialog}
          >
            <section>
              <Input  type='text' label='Nom' name='title' value={this.state.creatingThesaurusFields.title|| ""} onChange={this._handleCreateInputChange.bind(this)}/>
              <Input  type='text' multiline={true} label='Description' name='description' value={this.state.creatingThesaurusFields.description|| ""} onChange={this._handleCreateInputChange.bind(this)}/>
            </section>
          </Dialog>

          <Dialog

            actions={removeThesaurusActions}
            active={this.state.dialogRemoveActive}
            title='Supprimer un thésaurus'
            onOverlayClick={ this._handleCloseThesaurusRemoveDialog }
          >
            <section>
              Êtes-vous sûr de vouloir supprimer le thésaurus {this.state.removingThesaurus ? this.state.removingThesaurus.title : ''}?
            </section>
          </Dialog>

          <Preloader ref="preloader" />
        </section>
      </div>
    )
  }

  navigateToThesaurus(thesaurus) {
    const {router} = this.props;

    router.push({
      name: 'thesaurus',
      params: {
        thesoId: thesaurus.id
      }
    });
  }

  _handleCreateInputChange(value, e) {
    let fields = this.state.creatingThesaurusFields || {};

    this.setState({
      creatingThesaurusFields: {
        ...fields,
        [e.target.name]: value
      }
    });
  }

  _handleToggleThesaurusCreateDialog = () => {
    this.setState({
      dialogCreateActive : !this.state.dialogCreateActive,
      creatingThesaurusFields: {}
    });
  };


  _handleOpenThesaurusRemoveDialog(thesaurus) {
    this.setState({
      removingThesaurus: thesaurus,
      dialogRemoveActive : !this.state.dialogRemoveActive
    });
  }

  _handleCloseThesaurusRemoveDialog = () => {
    this.setState({
      removingThesaurus: null,
      dialogRemoveActive : !this.state.dialogRemoveActive
    });
  };

  _handleSaveThesaurus = () => {
    if (!this.state.loading) {
      this.setLoadingState();

      this.props.relay.commitUpdate(
        new AddThesaurusMutation({
          thesautheque: this.props.thesautheque,
          ...this.state.creatingThesaurusFields
        }),
        {
          onSuccess: () => {
            this._handleToggleThesaurusCreateDialog();
            this.clearLoadingState();
            this.props.showSnackbar("Le thésaurus a été ajouté");
          },
          onFailure: (transaction) => {
            this._handleToggleThesaurusCreateDialog();
            this.clearLoadingState();
            this.props.showMutationError(transaction);
          }
        }
      );
    }
  };

  _handleRemoveThesaurus = () => {
    if (!this.state.loading) {
      this.setLoadingState();

      this.props.relay.commitUpdate(
        new RemoveThesaurusMutation({
          thesautheque: this.props.thesautheque,
          id: this.state.removingThesaurus.id
        }),
        {
          onSuccess: () => {
            this.props.showSnackbar('Le thésaurus a été supprimé');
            this.clearLoadingState();
            this._handleCloseThesaurusRemoveDialog()
          },
          onFailure: (transaction) => {
            this.clearLoadingState();
            this._handleCloseThesaurusRemoveDialog();
            this.props.showMutationError(transaction);
          }
        }
      );
    }
  };

  setLoadingState(){
    this.setState({
      loading: true
    });

    this.refs.preloader.show();
  }

  clearLoadingState(){
    this.setState({
      loading: false
    });

    this.refs.preloader.hide();
  }
}

export default Relay.createContainer(Thesautheque, {
  initialVariables: {
    lang: "fr"
  },
  fragments: {
    thesautheque: ({lang}) => {
      return Relay.QL`
        fragment on Thesautheque {
          thesauri(first: 10){
            edges {
              node {
                id
                title
                description
              }
            }
          },
          ${AddThesaurusMutation.getFragment('thesautheque')},
          ${RemoveThesaurusMutation.getFragment('thesautheque')}
        }
      `
    }
  }
});