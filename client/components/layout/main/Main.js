/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import FontIcon from 'react-toolbox/lib/font_icon';

import Thesautheque from './components/thesautheque/Thesautheque';
import TopBar from '../../topbar/TopBar';

import style from './style.scss';

class Main extends React.Component {
  render() {
    var {children, routes, params, user} = this.props;
    var variables = this.props.relay.variables;

    return (
      <div className={style["app-wrapper"]}>
        <TopBar user={user} />

        <div className={style.viewport}>

          <section className={style.content}>
            { children }
          </section>
        </div>
      </div>
    )
  }

  selectLang(lang) {
    this.props.relay.setVariables({
      lang: lang
    });
  }
}

export default Relay.createContainer(Main, {
  fragments: {
    thesautheque: () => {
      return Relay.QL`
        fragment on Thesautheque {
          ${Thesautheque.getFragment('thesautheque')}
        }
      `
    },
    user: () => {
      return Relay.QL`
        fragment on User {
          id,
          ${TopBar.getFragment('user')}
        }
      `
    }
  }
});