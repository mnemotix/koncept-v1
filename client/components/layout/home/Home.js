/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';
import {browserHistory} from 'react-router';

import Logo from '../../logo/Logo';
import Navigation from '../../navigation/Navigation';
import style from './style.scss';

import Package from '../../../../package.json';

import {withRouter} from 'react-router';

@withRouter
class Home extends React.Component {
  render() {
    const {router} = this.props;

    return (
      <article>
        <header className={style.header}>
          <Logo className={style.logo} />
          <a  onClick={() => router.push({name: 'home'})}>
            <h2 className={style.title}>Koncept</h2>
          </a>
          <h4 className={style.subtitle}>
            Un éditeur de thésaurus simple et efficace
          </h4>
          <Navigation className={style.navigation}/>
        </header>

        <section className={style.content}>
          {this.props.children ?  this.props.children : (
            <section>
              <div className={style['catch-phrase']}>
                <span>Un éditeur de thésaurus en ligne unique en son genre s'adaptant à la terminologie </span>
                <a href="https://www.w3.org/TR/2009/NOTE-skos-primer-20090818/" target="_blank">SKOS</a>.
              </div>
            </section>
          )}

        </section>

        <footer className={style.footer}>
          <div className={style.left}>
            <span>Koncept © 2016</span>
            <small>Développé par <a href="http://www.mnemotix.com" target="_blank">Mnemotix</a></small>
          </div>

          <div className={style.right}>
            <span>Version {Package.version}</span>
            <span>•</span>
            <a onClick={() => router.push({name: "changelog"})}>Changelog</a>
          </div>
        </footer>
      </article>
    )
  }

  selectLang(lang) {
    this.props.relay.setVariables({
      lang: lang
    });
  }
}

export default Relay.createContainer(Home, {
  fragments: {}
});