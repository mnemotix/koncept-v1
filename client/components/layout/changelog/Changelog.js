/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import Relay from 'react-relay';

import style from './style.scss';

import ChangeLogMd from '../../../../changelog.md';

class ChangeLog extends React.Component {
  render() {
    return (
      <article className={style.changelog}>
        <header className={style.header}>
          <h1 className={style.title}>Changelog</h1>
        </header>

        <section className={style.content} dangerouslySetInnerHTML={{__html: ChangeLogMd}} />
      </article>
    )
  }

  selectLang(lang) {
    this.props.relay.setVariables({
      lang: lang
    });
  }
}

export default Relay.createContainer(ChangeLog, {
  fragments: {}
});