/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';

import classnames from 'classnames';

import style from "./style";

export default class extends React.Component {
  state = {
    active: false
  };

  render() {
    let classNames = classnames({
      [style.show]: this.state.active,
      [style['default-loader']] : true
    });

    return (
      <span className={this.props.className || classNames}>
        <span className={style.inner} />
      </span>
    );
  }

  show() {
    this.setState({active: true});
  }

  hide() {
    this.setState({active: false});
  }
}