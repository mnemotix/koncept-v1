/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Preloader from './Preloader';

import style from './style.scss';

export var preloaderHoc = ComposedComponent => class extends React.Component {

  static displayName = 'preloaderHoc';

  state = {
    preloaderActive: false
  };

  showPreloader(){
    this.setState({
      preloaderActive: true
    });
  }

  hidePreloader(){
    this.setState({
      preloaderActive: false
    });
  }

  render() {
    return (
      <div className="preloader-composed">
        <ComposedComponent {...this.props} {...this.state} showPreloader={this.showPreloader.bind(this)} hidePreloader={this.hidePreloader.bind(this)}/>

        <Preloader active={this.state.preloaderActive} />
      </div>
    );
  }
};