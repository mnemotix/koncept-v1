/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import {Snackbar} from 'react-toolbox/lib';

import style from './style.scss';

export var snackbarHoc = ComposedComponent => class extends React.Component {

  static displayName = 'snackbarHoc';

  state = {
    snackbarActive: false,
    snackbarLabel : '',
    snackbarType : 'accept',
    snackbarIcon : 'info'
  };

  showSnackbar(snackbarLabel, snackbarType = 'accept', snackbarIcon = 'info'){
    this.setState({
      snackbarActive: true,
      snackbarLabel,
      snackbarType,
      snackbarIcon
    });
  }

  hideSnackbar(){
    this.setState({
      snackbarActive: false
    });
  }

  render() {
    return (
      <div className="snackbar-composed">
        <ComposedComponent {...this.props} {...this.state} showSnackbar={this.showSnackbar.bind(this)} hideSnackbar={this.hideSnackbar.bind(this)}/>

        <Snackbar ref='snackbar'
                  active={this.state.snackbarActive}
                  icon={this.state.snackbarIcon}
                  label={this.state.snackbarLabel}
                  onClick={this.hideSnackbar.bind(this)}
                  type={this.state.snackbarType}
                  timeout={2000}
                  onTimeout={this.hideSnackbar.bind(this)}
        >
        </Snackbar>
      </div>
    );
  }
};