/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';

import classnames from 'classnames';

import style from "./style";

export default class extends React.Component {
  state = {
    active: false,
    completion: 0
  };

  render() {
    let classNames = classnames({
      [style.show]: this.state.active || this.props.active ,
      [style['default-loader']] : true
    }), innerStyle = {};

    if (this.props.freezed) {
      innerStyle.height = (this.props.completion * 100) + '%';
    }

    return (
      <span className={classnames(this.props.className || classNames, {[style['freezed']] : this.props.freezed})}>
        <span className={style.inner} style={innerStyle}/>
      </span>
    );
  }

  show() {
    this.setState({active: true});
  }

  hide() {
    this.setState({active: false, completion: 0});
  }
}