/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';
import Preloader from './Preloader';

import style from './style.scss';

export var preloaderHoc = ComposedComponent => class extends React.Component {

  static displayName = 'preloaderHoc';

  state = {
    preloaderActive: false,
    freezed: false,
    completion: 0
  };

  showPreloader(freezed = false){
    this.setState({
      preloaderActive: true,
      freezed,
      completion: 0
    });
  }

  hidePreloader(){
    this.setState({
      preloaderActive: false
    });
  }

  updatePreloader(completion){
    this.setState({
      completion
    });
  }

  render() {
    return (
      <div className="preloader-composed">
        <ComposedComponent {...this.props} {...this.state}
          showPreloader={this.showPreloader.bind(this)}
          hidePreloader={this.hidePreloader.bind(this)}
          updatePreloader={this.updatePreloader.bind(this)}
        />

        <Preloader active={this.state.preloaderActive} freezed={this.state.freezed} completion={this.state.completion}/>
      </div>
    );
  }
};