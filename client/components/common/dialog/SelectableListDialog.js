/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import {Dialog, List} from 'react-toolbox/lib';
import classnames from 'classnames';

import style from './style';
import theme from './theme';

export default class extends React.Component {
  static propTypes = {
    cancelLabel : React.PropTypes.string,
    validateLabel : React.PropTypes.string,
    multiple : React.PropTypes.bool,
    title : React.PropTypes.string,
    active : React.PropTypes.bool,
    onCancel: React.PropTypes.func,
    onValidate: React.PropTypes.func,
    loading: React.PropTypes.bool,
    headerContent: React.PropTypes.any,
    listItems:  React.PropTypes.array,
    listComponent: React.PropTypes.any,
    footerContent: React.PropTypes.any,
    type: React.PropTypes.any
  };

  render(){
    const {t} = this.props;

    let actions = [
      {
        label: this.props.cancelLabel || "Annuler",
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      }
    ];

    if (this.props.validateLabel) {
      actions.push({
        label: this.props.validateLabel,
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onValidate();
          }
        }
      })
    }

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
        theme={{...theme, ...this.props.theme}}
        type={this.props.type}
        className={style.selectableListDialog}
      >
        <section className={style.innerSection} ref="innerSection">
          {this.props.headerContent ? (
              <div className={style.header}>
                {this.props.headerContent}
              </div>
            ) : null}


          <div className={style.list}>
            { this.props.listComponent ? this.props.listComponent : (
              <List selectable>
                {this.props.listItems}
              </List>
            )}
          </div>

          {this.props.footerContent ? (
              <div className={style.footer}>
                {this.props.footerContent}
              </div>
            ) : null}
        </section>
      </Dialog>
    )
  }
}