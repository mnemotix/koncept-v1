/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 18/01/2016
 */

import React from 'react';
import Dialog from 'react-toolbox/lib/dialog';
import classnames from 'classnames';

import style from './style.scss';


export default class extends React.Component {
  static propTypes = {
    cancelLabel : React.PropTypes.string,
    title : React.PropTypes.string,
    active : React.PropTypes.bool,
    onCancel: React.PropTypes.func,
    loading: React.PropTypes.bool,
    content: React.PropTypes.any
  };

  render(){
    const {t} = this.props;

    let actions = [
      {
        label: this.props.cancelLabel || "Annuler",
        onClick: (e) => {
          if (!this.props.loading) {
            e.stopPropagation();
            e.preventDefault();
            return this.props.onCancel();
          }
        }
      }
    ];

    return (
      <Dialog
        actions={actions}
        active={this.props.active}
        title={this.props.title}
        onOverlayClick={this.props.onCancel}
        onEscKeyDown={this.props.onCancel}
        style={classnames({[style.loading]: this.props.loading})}
        theme={this.props.theme || {}}
        type={this.props.type}
      >
        <section>
          {this.props.content}
        </section>
      </Dialog>
    )
  }
}