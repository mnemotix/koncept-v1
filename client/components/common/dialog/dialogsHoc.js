/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 21/03/2016
 */

import React from 'react';

import ConfirmDialog from './ConfirmDialog';
import SelectableListDialog from './SelectableListDialog';
import Dialog from './Dialog';

import style from './style.scss';

export var confirmDialogHoc = (ComposedComponent)=> class extends React.Component {

  static displayName = 'confirmDialogHoc';

  state = {
    dialog: {
      active: false,
      content: '',
      title: '',
      cancelLabel: null
    },
    confirm: {
      active: false,
      content: '',
      title: '',
      cancelLabel: null,
      validateLabel: null,
      onValidate: () => {}
    },
    selectable: {
      active: false,
      headerContent: '',
      footerContent: '',
      listItems: [],
      title: '',
      cancelLabel: null,
      onSelect: () => {}
    }
  };

  getWrappedInstance() {
    if(this.refs.composedComponent.getWrappedInstance) {
      return this.refs.composedComponent.getWrappedInstance();
    } else {
      return this.refs.composedComponent;
    }
  }


  showConfirmDialog(title, content, onValidate, validateLabel, cancelLabel){
    this.setState({
      confirm : {
        active: true,
        title, content, onValidate, validateLabel, cancelLabel
      }
    });
  }

  showSelectableListDialog(props){
    this.setState({
      selectable : {
        active: true,
        ...props
      }
    });
  }

  showDialog(props, type = 'dialog'){
    this.setState({
      [type] : {
        active: true,
        ...props
      }
    });
  }

  hideDialog(dialog){
    this.setState({
      [dialog] : {
        active: false,
        onValidate: () => {
        }
      }
    });
  }

  render() {
    return (
      <div className={style.dialogHoc}>
        <ComposedComponent {...this.props}
                           ref="composedComponent"
                           showConfirmDialog={this.showConfirmDialog.bind(this)}
                           hideConfirmDialog={this.hideDialog.bind(this, 'confirm')}
                           showSelectableListDialog={this.showSelectableListDialog.bind(this)}
                           hideSelectableListDialog={this.hideDialog.bind(this, 'selectable')}
                           showDialog={this.showDialog.bind(this)}
                           hideDialog={this.hideDialog.bind(this, 'dialog')}
                           getWrappedInstance={this.getWrappedInstance.bind(this)}
        />

        <ConfirmDialog {...this.state.confirm} onCancel={this.hideDialog.bind(this, 'confirm')} />
        <SelectableListDialog {...this.state.selectable} onCancel={this.hideDialog.bind(this, 'selectable')} />
        <Dialog {...this.state.dialog} onCancel={this.hideDialog.bind(this, 'dialog')} />
      </div>
    );
  }
};