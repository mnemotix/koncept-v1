/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import React from 'react';
import {IndexRoute, Route} from 'react-router';

import AppQueries from './queries/AppQueries';
import ThesaurusQueries from './queries/ThesaurusQueries';
import CollectionQueries from './queries/CollectionQueries';
import ConceptQueries from './queries/ConceptQueries';
import UserQueries from './queries/UserQueries';
import SchemeQueries from './queries/SchemeQueries';

import App from './components/App';
import Main from './components/layout/main/Main';

import Home from './components/layout/home/Home';
import ChangeLog from './components/layout/changelog/Changelog';
import Thesautheque from './components/layout/main/components/thesautheque/Thesautheque';
import Thesaurus from './components/layout/main/components/thesaurus/Thesaurus';
import ThesaurusIndexView from './components/layout/main/components/thesaurus/components/layout/concepts/View';

import ConceptViewerView from './components/layout/main/components/thesaurus/components/layout/concepts/components/viewer/concept/Viewer';
import ConceptViewerPropertiesTabView from './components/layout/main/components/thesaurus/components/layout/concepts/components/viewer/concept/ViewerTabProperties';
import ConceptViewerRelationsTabView from './components/layout/main/components/thesaurus/components/layout/concepts/components/viewer/concept/ViewerTabRelations';
import ConceptViewerExpressionsTabView from './components/layout/main/components/thesaurus/components/layout/concepts/components/viewer/concept/ViewerTabMatchers';
import ConceptViewerRulesTabView from './components/layout/main/components/thesaurus/components/layout/concepts/components/viewer/concept/ViewerTabRules';

import SchemeViewerView from './components/layout/main/components/thesaurus/components/layout/concepts/components/viewer/scheme/Viewer';


import ThesaurusCollectionsLayoutView from './components/layout/main/components/thesaurus/components/layout/collections/View';
import ThesaurusCollectionViewerView from './components/layout/main/components/thesaurus/components/layout/collections/components/viewer/Collection';
import ThesaurusGlobalGraphLayoutView from './components/layout/main/components/thesaurus/components/layout/visgraph/View';

import Preloader from './components/common/preloader/Preloader';
import Error from './components/error/Error';

function render(params){
  let {props, routerProps, error, element} = params;

  if (error) {
    return <Error active={true} message={error.message} />
  }

  if (!props) {
    return <Preloader {...routerProps} active={true}/>;
  }

  return React.cloneElement(element, props);
}

export default (
  <Route component={App}>
    <Route name="home" path="/" component={Home} >
      <Route name="changelog" path="/changelog" component={ChangeLog} render={render} />
    </Route>

    <Route component={Main} queries={Object.assign({}, AppQueries, UserQueries)} render={render}>
      <Route name="thesautheque" path="/thesautheque" component={Thesautheque} queries={AppQueries} render={render}/>
      <Route name="thesaurus" path="/thesaurus/:thesoId" component={Thesaurus} queries={ThesaurusQueries} render={render}>
        <IndexRoute name="index" component={ThesaurusIndexView} queries={ThesaurusQueries} render={render}/>

        <Route name="concepts" path="concepts" component={ThesaurusIndexView} queries={ThesaurusQueries} render={render}>
          <Route name="concept" path=":conceptId" component={ConceptViewerView} queries={Object.assign({}, ConceptQueries, ThesaurusQueries)} render={render}>
            <IndexRoute component={ConceptViewerPropertiesTabView} queries={Object.assign({}, ConceptQueries, ThesaurusQueries)} render={render}/>
            <Route name="concept-relations" path="relations" component={ConceptViewerRelationsTabView} queries={Object.assign({}, ConceptQueries, ThesaurusQueries)} render={render}/>
            <Route name="concept-matchers" path="matchers" component={ConceptViewerExpressionsTabView} queries={ConceptQueries} render={render}/>
            <Route name="concept-rules" path="rules" component={ConceptViewerRulesTabView} queries={ConceptQueries} render={render}/>
          </Route>
        </Route>

        <Route name="schemes" path="schemes" component={ThesaurusIndexView} queries={ThesaurusQueries} render={render}>
          <Route name="scheme" path=":schemeId" component={SchemeViewerView} queries={Object.assign({}, SchemeQueries, ThesaurusQueries)} render={render} />
        </Route>

        <Route name="collections" path="collections" component={ThesaurusCollectionsLayoutView} queries={ThesaurusQueries} render={render}>
          <Route name="collection" path=":collectionId" component={ThesaurusCollectionViewerView} queries={CollectionQueries} render={render}/>
        </Route>

        <Route name="graph" path="global-graph" component={ThesaurusGlobalGraphLayoutView} queries={ThesaurusQueries} prepareParams={(params, { location }) => {
          let { sids } = location.query;

          if(!sids){
            sids = sessionStorage.getItem(`${params.thesoId}_selectedConceptIdFor_graphvis`);
          }

          return {
            ...params,
            columnIds: sids ? sids.split('/') : null
          };
        }}/>
      </Route>
    </Route>
  </Route>
);