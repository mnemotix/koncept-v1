/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 23/02/2016
 */

import Relay from 'react-relay';

class MockNetworkLayer {
  mutationSuceedsWith(response){
    this.mutationResponse = response;
  }

  querySuceedsWith(response, callback){
    this.callback = callback;
    this.queryResponse = response;
  }

  sendMutation(request) {
    return request.resolve({response: this.mutationResponse});
  }

  sendQueries(queryRequest) {
    return queryRequest.resolve({response:  this.queryResponse});
  }

  supports(...options) {
    return false
  }
}

var mockNetworkLayer = new MockNetworkLayer();

Relay.injectNetworkLayer(mockNetworkLayer);

export default mockNetworkLayer;