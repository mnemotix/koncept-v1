/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * @url https://gist.github.com/mikberg/07b4006e22aacf31ffe6
 *
 * Date : 22/02/2016
 */

import Relay from 'real-react-relay';

export class Mutation extends Relay.Mutation {
  _resolveProps(props) {
    this.props = props;
  }
}

export class MockStore {
  reset() {
    this.successResponse = undefined;
    this.failureResponse = undefined;
    this.failureAfterCallback = undefined;
  }

  succeedWith(response, afterCallback) {
    this.reset();
    this.successResponse = response;
    this.successAfterCallback = afterCallback;
  }

  failWith(response, afterCallback) {
    this.reset();
    this.failureResponse = response;
    this.failureAfterCallback = afterCallback;
  }

  update(callbacks) {
    if (this.successResponse) {
      callbacks.onSuccess(this.successResponse);
      this.successAfterCallback();
    } else if (this.failureResponse) {
      callbacks.onFailure(this.failureResponse);
      this.failureAfterCallback();
    }

    this.reset();
  }

  commitUpdate(mutation, callbacks) {
    return this.update(callbacks);
  }

  applyUpdate(mutation, callbacks) {
    return this.update(callbacks);
  }
}

export const Store = new MockStore();
export const Route = Relay.Route;
export const PropTypes = Relay.PropTypes;

export default {
  ...Relay,
  Mutation,
  createContainer: (component, specs) => {
    /* eslint no-param-reassign:0 */
    component.getRelaySpecs = () => specs;
    return component;
  },
  Store
};