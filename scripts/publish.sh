#!/usr/bin/env bash

repository=mnx/koncept
registry=docker.mnemotix.com
version=`grep version package.json | sed "s/[\", ]//g" | cut -d':' -f2`

build(){
    docker build -t $repository:$version .
}

push(){
    docker tag $repository:$version $registry/$repository:$version
    docker push $registry/$repository:$version
}

echo "Compiling ${repository}:${version}..."
build
echo "Push tag on $registry/${repository}:${version}..."
push

exit 0