/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

const chalk   = require('chalk');
const Package = require('./package.json');

console.info(`
██╗  ██╗ ██████╗ ███╗   ██╗ ██████╗███████╗██████╗ ████████╗
██║ ██╔╝██╔═══██╗████╗  ██║██╔════╝██╔════╝██╔══██╗╚══██╔══╝
█████╔╝ ██║   ██║██╔██╗ ██║██║     █████╗  ██████╔╝   ██║
██╔═██╗ ██║   ██║██║╚██╗██║██║     ██╔══╝  ██╔═══╝    ██║
██║  ██╗╚██████╔╝██║ ╚████║╚██████╗███████╗██║        ██║
╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝╚══════╝╚═╝        ╚═╝

Version ${chalk.yellow(Package.version)}
Coded with ${chalk.red('❤')} by ${chalk.underline.yellow('Mnemotix')}

  `);

process.env.UUID = "KonceptGraphQLEndPoint-f86d12da-12ef-4ac9-a22b-0def5ce5ed6e";
process.env.GRAPH_ORIGIN = "mnx:app:koncept";

const express = require('express');
const graphQLHTTP = require('express-graphql');
const path = require('path');
const webpack = require('webpack');
const cookieParser = require('cookie-parser');
const ClientOAuth2 = require('client-oauth2');
const jwt = require('jsonwebtoken');
const uuid = require('uuid').v4;
const cors = require('cors');

const config = require('./webpack.config.js');

const Schema = require('./server/schema/schema').Schema;

const isDeveloping = process.env.NODE_ENV !== 'production';
const isTesting = process.env.NODE_ENV == 'test';

const Config = require('./config/config');

Config.initVariables(!(isTesting || isDeveloping));

const BASE_URL = process.env.APP_URL;

const oAuth2Client = new ClientOAuth2({
  clientId: 'koncept',
  clientSecret: process.env.OAUTH_CLIENT_SECRET,
  accessTokenUri:   process.env.OAUTH_TOKEN_VALIDATION_URL + '/auth/realms/' +  process.env.OAUTH_REALM + '/protocol/openid-connect/token',
  authorizationUri: process.env.OAUTH_REDIRECT_URL + '/auth/realms/' +  process.env.OAUTH_REALM +  '/protocol/openid-connect/auth',
  authorizationGrants: ['credentials'],
  redirectUri: BASE_URL + '/auth/login/callback',
  scopes: []
});

const setCookies = function(res, accessToken, refreshToken) {
  res.cookie('access_token', accessToken, {httpOnly: true, path : '/'});
  res.cookie('refresh_token', refreshToken, {httpOnly: true, path : '/'});
  res.cookie('UID', uuid(), {path : '/'});
};

const clearCookies = function(res){
  res.clearCookie('access_token', {path : '/'});
  res.clearCookie('refresh_token', {path : '/'});
};

const saveRedirectUri = function(req, res) {
  res.cookie('redirect_uri', BASE_URL + req.url, {httpOnly: true, path : '/'});
};

const setRedirectUri = function(req, res) {
  let redirectUri = req.cookies.redirect_uri || '/';
  res.clearCookie('redirect_uri', {path : '/'});
  res.redirect(redirectUri);
};

const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

app.use(cors({
  credentials: true,
  origin: JSON.parse(process.env.CORS_ALLOWED_ORIGINS)
}));

app.use(cookieParser());

app.get('/auth/login/callback', function(req, res){
  console.log('\x1b[36mInited connection from keycloak. Try to get the token from KC code ...\x1b[0m');

  oAuth2Client.code.getToken(BASE_URL + req.url)
    .then(function (user) {
      console.log('\x1b[32mOk !\x1b[0m');

      // Refresh the current users access token.
      user.refresh().then(function (updatedUser) {
        setCookies(res, updatedUser.accessToken, updatedUser.refreshToken);
        setRedirectUri(req, res);
      });
    })
    .catch(function(reason) {
      res.status(500).send('Something gone wrong with OAuth 2... <br/>' + JSON.stringify(reason));
    });
});

app.get('/auth/logout', function(req, res){
  let logoutCallbackUri = BASE_URL + '/auth/logout/callback';
  res.redirect(process.env.OAUTH_REDIRECT_URL + '/auth/realms/' +  process.env.OAUTH_REALM + '/protocol/openid-connect/logout?redirect_uri=' + encodeURIComponent(logoutCallbackUri));
});

app.get('/auth/logout/callback', function(req, res){
  io.sockets.emit('disconnected', {uid: req.cookies.UID});
  clearCookies(res);
  res.redirect('/');
});

if (!isTesting) {
  app.use(function (req, res, next) {
    let validateToken = function(accessToken, refreshToken){
      req.user = jwt.decode(accessToken);

      // Access token expired ? Refresh it !
      if (Date.now() > (req.user.exp * 1000)) {
        console.log('\x1b[36mRefreshing OAuth2 access token\x1b[0m');

        oAuth2Client.createToken(accessToken, refreshToken).refresh().then(function(updatedUser){
          setCookies(res, updatedUser.accessToken, updatedUser.refreshToken);
          next();
        }).catch(function(){
          clearCookies(res);

          if (req.xhr || !!parseInt(req.get('X-Direct-Grant'))) {
            io.sockets.emit('disconnected', {uid: req.cookies.UID});
            res.sendStatus(401);
          } else {
            res.redirect(req.url);
          }
        });
      } else {
        next();
      }
    };

    // Check if OAuth2 access token is present in in cookie params.
    if (req.cookies && req.cookies.access_token && req.cookies.refresh_token) {
      validateToken(req.cookies.access_token, req.cookies.refresh_token);
    } else if(req.xhr || !!parseInt(req.get('X-Direct-Grant'))){
      res.sendStatus(401);
    } else {
      saveRedirectUri(req, res);
      res.redirect(oAuth2Client.code.getUri());
    }
  });
}

app.get('/heartbeat', function(req, res){
  res.send('Alive !');
});

app.use('/graphql', graphQLHTTP(function(req){
  return {
    graphiql: true,
    pretty: true,
    schema: Schema,
    context: {
      user: req.user,
      io,
      lang: req.get('Lang') || 'fr'
    },
    formatError: error => {
      let {message, locations, stack} = error;

      try {
        let advancedMessage = JSON.parse(message);

        let {message, errorType, stackTrace} = advancedMessage.error;

        console.log(
          `${chalk.underline.red.bold("Synaptix side error")}: ${message} (${chalk.redBright(errorType)})
  ${chalk.bold("Trace")}: ${chalk.gray(stackTrace)}
  ${chalk.bold("Payload")}: ${chalk.gray(JSON.stringify(advancedMessage.requestPayload))}
`);
        return {
          message,
          locations,
          stack
        };

      } catch(e){
        console.log(
          `${chalk.underline.red.bold("NodeJS side error")}: ${message}
  ${chalk.bold("Trace")}: ${chalk.gray(stack)}
`);
        return {
          message,
          locations,
          stack
        };
      }
    }
  };
}));

if (isDeveloping) {
  const webpackMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');

  process.env.NODE_ENV = "development";

  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('*', function response(req, res) {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
    res.end();
  });
} else {
  app.use(express.static(__dirname + '/dist'));
  app.get('*', function response(req, res) {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });
}


server.listen(process.env.APP_PORT, '0.0.0.0', function onStart(err) {
  if (err) {
    console.log(err);
  }

  console.info(`NodeJS Express started and listening on port ${chalk.red.bold(process.env.APP_PORT)}...\n\n`);

  let database = require('./server/schema/database').default;

  database.init();
});
