/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  devtool: 'eval-source-map',
  entry: [
    'webpack-hot-middleware/client?reload=true',
    path.resolve(__dirname, 'client', 'app.js')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'client/index.tpl.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
  ],
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader:  'babel',
      query: {
        plugins: ['./server/utils/babelRelayPlugin'].map(require.resolve),
        cacheDirectory: 'node_modules/.cache/babel-loader'
      }
    }, {
      test: /\.(scss|css)$/,
      loader: 'style!css?sourceMap&modules&constLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass?sourceMap!toolbox'
    },{
      test: /\.(png|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=100000'
    },
    {
      test: /\.json$/,
      loader: 'json-loader'
    },
    {
      test: /\.md$/,
      loader: "html!markdown"
    }]
  },
  resolve: {
    extensions: [
      '', '.jsx', '.scss', '.js', '.json', '.ttf', '.woff', '.svg', '.eot'
    ]
  },
  postcss: [ autoprefixer({ browsers: ['last 2 versions'] })],
  toolbox: {
    theme: path.join(__dirname, 'client/theme.scss')
  },
  node : { fs: 'empty', child_process: 'empty' }
};