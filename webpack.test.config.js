/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

const config = require('./webpack.config');
const path = require('path');

if (!config.resolve.alias) {
  config.resolve.alias = {}
}

config.resolve.alias['react-relay'] = path.join(__dirname, '/client/utils/mockRelay.js');
config.resolve.alias['mock-network-layer'] = path.join(__dirname, '/client/utils/mockNetworkLayer.js');
config.resolve.alias['real-react-relay'] = path.join(__dirname, '/node_modules/react-relay/');

config.devtool = "inline-source-map";

config.externals = {
  'cheerio': 'window',
  'react/lib/ExecutionEnvironment': true,
  'react/lib/ReactContext': true
};

module.exports = config;