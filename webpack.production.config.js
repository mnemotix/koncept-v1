/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/02/2016
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StatsPlugin = require('stats-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: [
    path.resolve(__dirname, 'client', 'app.js')
  ],
  output: {
    path: path.join(__dirname, '/dist/'),
    filename: '[name]-[hash].min.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new HtmlWebpackPlugin({
      template: 'client/index.tpl.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new ExtractTextPlugin('[name]-[hash].min.css', {
      publicPath: '/'
    }),
    new StatsPlugin('webpack.stats.json', {
      source: false,
      modules: false
    }),
    new webpack.DefinePlugin({
      'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    })
  ],
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader:  'babel',
      query: {
        plugins: ['./server/utils/babelRelayPlugin'].map(require.resolve),
        cacheDirectory: 'node_modules/.cache/babel-loader'
      }
    }, {
      test: /\.json?$/,
      loader: 'json'
    },{
      test: /\.(png|woff|woff2|eot|ttf|svg)$/,
      loader: 'url-loader?limit=100000'
    },{
      test: /\.(scss|css)$/,
      loader: ExtractTextPlugin.extract('style', 'css?modules&constLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass!toolbox')
    },
    {
      test: /\.md$/,
      loader: "html!markdown"
    }]
  },
  resolve: {
    extensions: [
      '', '.jsx', '.scss', '.js', '.json', '.ttf', '.woff', '.svg', '.eot'
    ]
  },
  postcss: [ autoprefixer({ browsers: ['last 2 versions'] })],
  toolbox: {
    theme: path.join(__dirname, 'client/theme.scss')
  },
  node : { fs: 'empty', child_process: 'empty' }
};