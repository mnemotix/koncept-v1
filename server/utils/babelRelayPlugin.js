/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

var getbabelRelayPlugin = require('babel-relay-plugin');
var schema = require('../schema/schema.json');

module.exports = getbabelRelayPlugin(schema.data);