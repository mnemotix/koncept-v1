/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

require('babel-register');

var fs = require('fs');
var path = require('path');
var graphql = require('graphql').graphql;
var introspectionQuery = require('graphql/utilities').introspectionQuery;
var schema = require('../schema/schema').Schema;

graphql(schema, introspectionQuery).then(function(result) {
  if (result.errors) {
    return console.error(result.errors);
  }

  fs.writeFileSync(path.join(__dirname, '../schema/schema.json'), JSON.stringify(result, null, 2))
});
