/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import queryType from './types/queries/queries';
import mutationType from './types/mutations/mutations';

import {
  GraphQLSchema
} from 'graphql';

export var Schema = new GraphQLSchema({
  query: queryType,
  mutation: mutationType
});
