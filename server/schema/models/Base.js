/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 02/02/2016
 */
export default class {
  id;
  uri;

  constructor(id, uri, props = {}) {
    this.id = id;
    this.uri = uri;

    Object.keys(props).map(prop => {
      this[prop] = props[prop];
    });
  }
}