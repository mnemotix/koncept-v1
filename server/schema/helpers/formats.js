/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 01/07/2016
 */

import {remove as removeAccents} from 'diacritics';

/**
 * Get a label and return a matcher
 * @param {string} label
 * @private
 */
function transformLabelToMatcher(label = '') {
  label = label.toLowerCase();
  label = removeAccents(label);

  if (label.match(/ /)) {
    return `+"${label}"~3`;
  } else {
    return `+"${label}"`;
  }
}

export {
  transformLabelToMatcher
}