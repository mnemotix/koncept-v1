/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
  offsetToCursor
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge,
  collectionEdge,
  collectionType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'AddConceptToCollection',
  inputFields: {
    collectionId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({conceptRealId}, _, context) => database.getEndpointConnection(context).getConcept(conceptRealId)
    },
    collection: {
      type: collectionType,
      resolve: ({collectionRealId}, _, context) => database.getEndpointConnection(context).getCollection(collectionRealId)
    },
    collectionEdge: {
      type: collectionEdge,
      resolve: ({conceptRealId, collectionRealId}, _, context) => {
        return database.getEndpointConnection(context).getCollection(collectionRealId)
          .then(newCollection => {
            return database.getEndpointConnection(context).getCollectionsForConcept(conceptRealId)
              .then(collections => {
                return {
                  cursor: offsetToCursor(collections.findIndex(collection => collection.id == collectionRealId)),
                  node: newCollection
                }
              });
          });
      }
    },
    conceptEdge: {
      type: conceptEdge,
      resolve: ({conceptRealId, collectionRealId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(conceptRealId)
          .then(concept => {
            return database.getEndpointConnection(context).getConceptsOfCollection(collectionRealId)
              .then(concepts => {
                return {
                  cursor: offsetToCursor(concepts.findIndex(concept => concept.id == conceptRealId)),
                  node: concept
                }
              });
          });
      }
    }
  },
  mutateAndGetPayload: ({conceptId, collectionId}, _, context) => {
    const {id: conceptRealId} = fromGlobalId(conceptId),
      {id: collectionRealId} = fromGlobalId(collectionId);

    return database.getEndpointConnection(context).addConceptToCollection(collectionRealId, conceptRealId)
      .then(() => {
        return {conceptRealId, collectionRealId};
      });
  }
});







