/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';

export default mutationWithClientMutationId({
  name: 'SwitchFromRelatedToNarrowerRelation',
  inputFields: {
    sourceId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    targetId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    targetParentId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    newNarrowerConceptEdge: {
      type: conceptEdge,
      resolve: ({realTargetId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realTargetId)
          .then((relatedConcept) => {
            return {
              cursor: offsetToCursor(0),
              node: relatedConcept
            };
          });
      }
    },
    deletedRelatedConceptId: {
      type: GraphQLID,
      resolve: ({targetId}) => targetId
    },
    sourceConcept: {
      type: conceptType,
      resolve: ({realSourceId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realSourceId);
      }
    },
    targetParentConcept: {
      type: conceptType,
      resolve: ({realTargetParentId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realTargetParentId);
      }
    }
  },
  mutateAndGetPayload: ({sourceId, targetId, targetParentId}, _, context) => {
    const {id: realSourceId} = fromGlobalId(sourceId),
      {id: realTargetId} = fromGlobalId(targetId),
      {id: realTargetParentId} = fromGlobalId(targetParentId);

    return database.getEndpointConnection(context).removeNarrowerRelation(realTargetParentId, realTargetId)
      .then(() => {
        return database.getEndpointConnection(context).removeRelatedRelation(realSourceId, realTargetId)
          .then(() => {
            return database.getEndpointConnection(context).addNarrowerRelation(realSourceId, realTargetId)
              .then(() => {
                return {realSourceId, realTargetId, sourceId, targetId, realTargetParentId, targetParentId};
              });
          });
      });
  }
});



