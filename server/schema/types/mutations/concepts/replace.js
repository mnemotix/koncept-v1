/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesaurusType, skosElementInterface
} from '../../queries/types';



export default mutationWithClientMutationId({
  name: 'ReplaceConcept',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    targetConceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    thesoId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    conceptId:{
      type: GraphQLID,
      resolve: ({conceptId}) => conceptId
    },
    parent: {
      type: skosElementInterface,
      resolve: ({parent}, _, context) => parent
    },
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesoRealId}, _, context) => database.getEndpointConnection(context).getThesaurus(thesoRealId)
    }
  },
  mutateAndGetPayload: async ({conceptId, targetConceptId, thesoId}, _, context) => {
    const {id: realConceptId} = fromGlobalId(conceptId),
      {id: realTargetConceptId} = fromGlobalId(targetConceptId),
      {id: thesoRealId} = fromGlobalId(thesoId);

    let parent = await database.getEndpointConnection(context).getBroaderSkosElement(realConceptId);

    await database.getEndpointConnection(context).replaceConcept({
      conceptId: realConceptId,
      targetConceptId: realTargetConceptId
    });

    return {conceptId, parent, thesoRealId};
  }
});


