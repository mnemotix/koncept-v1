/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesaurusType,
  skosElementInterface
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveConcept',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    parentId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    thesoId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    isMoving:{
      description: 'In case of a moving action, inform the server not to destroy the concept, but just the skos:narrower relationship',
      type: GraphQLBoolean
    }
  },
  outputFields: {
    parent: {
      type: skosElementInterface,
      resolve: ({broader}) => broader
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({conceptId}) => conceptId
    },
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesoRealId}, _, context) => database.getEndpointConnection(context).getThesaurus(thesoRealId)
    }
  },
  mutateAndGetPayload: async ({conceptId, parentId, thesoId, isMoving}, _, context) => {
    const {id: conceptRealId} = fromGlobalId(conceptId),
      {id: broaderConceptId} = fromGlobalId(parentId),
      {id: thesoRealId} = fromGlobalId(thesoId);

    let broader = await database.getEndpointConnection(context).getBroaderSkosElement(conceptRealId);

    if (isMoving) {
      await database.getEndpointConnection(context).removeNarrowerRelation(broaderConceptId, conceptRealId);
    } else {
      await database.getEndpointConnection(context).removeConcept(conceptRealId);
    }

    return {broader, conceptId, thesoRealId};
  }
});


