/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge,
  skosElementInterface
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'MoveConcept',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    parentId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    targetId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    conceptId: {
      type: GraphQLID,
      resolve: ({conceptId}, _, context) => conceptId
    },
    parent: {
      type: skosElementInterface,
      resolve: ({parent}, _, context) => parent
    },
    target: {
      type: skosElementInterface,
      resolve: ({target}, _, context) => target
    },
    conceptEdge: {
      type: conceptEdge,
      resolve: ({concept}, _, context) => ({
        cursor: 0,
        node: concept
      })
    },
    concept: {
      type: conceptType,
      resolve: ({concept}, _, context) => concept
    }
  },
  mutateAndGetPayload: async ({targetId, parentId, conceptId}, _, context) => {
    const {id: conceptRealId} = fromGlobalId(conceptId),
      {id: parentRealId, type: parentType} = fromGlobalId(parentId),
      {id: targetRealId, type: targetType} = fromGlobalId(targetId);

    let {concept, parent, target} = await database.getEndpointConnection(context).moveConcept({
      conceptId: conceptRealId,
      parentId: parentRealId,
      targetId: targetRealId,
      parentType,
      targetType
    });

    return {concept, parent, target, conceptId};
  }
});


