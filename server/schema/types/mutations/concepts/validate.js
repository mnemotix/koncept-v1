/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType, thesaurusType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'ValidateConcept',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    thesoId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    conceptId:{
      type: GraphQLID,
      resolve: ({conceptId}) => conceptId
    },
    concept: {
      type: conceptType,
      resolve: ({concept}) => concept
    },
    thesaurus: {
      type: thesaurusType,
      resolve: ({realThesoId}, _, context) => database.getEndpointConnection(context).getThesaurus(realThesoId)
    }
  },
  mutateAndGetPayload: ({conceptId, thesoId}, _, context) => {
    const {id: realConceptId} = fromGlobalId(conceptId),
      {id: realThesoId} = fromGlobalId(thesoId);


    return database.getEndpointConnection(context).validateConcept({conceptId: realConceptId})
      .then((concept) => {
        return {concept, realThesoId, conceptId};
      });
  }
});


