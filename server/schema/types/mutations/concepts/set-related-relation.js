/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';

export default mutationWithClientMutationId({
  name: 'SetRelatedRelation',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    relatedConceptId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({realConceptId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realConceptId);
      }
    },
    relatedConcept: {
      type: conceptType,
      resolve: ({realRelatedConceptId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realRelatedConceptId);
      }
    }
  },
  mutateAndGetPayload: ({conceptId, relatedConceptId}, _, context) => {
    const {id: realConceptId} = fromGlobalId(conceptId),
      {id: realRelatedConceptId} = fromGlobalId(relatedConceptId);

    return database.getEndpointConnection(context).addRelatedRelation(realConceptId, realRelatedConceptId)
      .then(() => {
        return {realRelatedConceptId, realConceptId};
      });
  }
});



