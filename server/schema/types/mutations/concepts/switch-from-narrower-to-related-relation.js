/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';

export default mutationWithClientMutationId({
  name: 'SwitchFromNarrowerToRelatedRelation',
  inputFields: {
    sourceId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    targetId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    topConceptId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    newRelatedConceptEdge: {
      type: conceptEdge,
      resolve: ({realTargetId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realTargetId)
          .then((relatedConcept) => {
            return database.getEndpointConnection(context).getSiblings(realTargetId, 'related')
              .then((siblings) => {
                return {
                  cursor: offsetToCursor(siblings.findIndex(sibling => sibling.id == realTargetId)),
                  node: relatedConcept
                }
              });
          });
      }
    },
    newNarrowerConceptEdge: {
      type: conceptEdge,
      resolve: ({realTargetId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realTargetId)
          .then((relatedConcept) => {
            return database.getEndpointConnection(context).getSiblings(realTargetId, 'narrowers')
              .then((siblings) => {
                return {
                  cursor: offsetToCursor(siblings.findIndex(sibling => sibling.id == realTargetId)),
                  node: relatedConcept
                }
              });
          });
      }
    },
    deletedNarrowerConceptId: {
      type: GraphQLID,
      resolve: ({targetId}) => targetId
    },
    sourceConcept: {
      type: conceptType,
      resolve: ({realSourceId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realSourceId);
      }
    },
    topConcept: {
      type: conceptType,
      resolve: ({realTopConceptId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(realTopConceptId);
      }
    }
  },
  mutateAndGetPayload: ({sourceId, targetId, topConceptId}, _, context) => {
    const {id: realSourceId} = fromGlobalId(sourceId),
      {id: realTargetId} = fromGlobalId(targetId),
      {id: realTopConceptId} = fromGlobalId(topConceptId);

    return database.getEndpointConnection(context).addNarrowerRelation(realTopConceptId, realTargetId)
      .then(() => {
        return database.getEndpointConnection(context).removeNarrowerRelation(realSourceId, realTargetId)
          .then(() => {
            return database.getEndpointConnection(context).addRelatedRelation(realSourceId, realTargetId)
              .then(() => {
                return {realSourceId, realTargetId, sourceId, targetId, realTopConceptId, topConceptId};
              });
          });
      });
  }
});



