/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';

import {
  ruleInputType
} from '../../queries/types/rule/rule';


export default mutationWithClientMutationId({
  name: 'UpdateConceptRules',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    rules: {
      type: new GraphQLList(ruleInputType)
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({concept}) => concept
    }
  },
  mutateAndGetPayload: ({id, rules}, _, context) => {
    const {id: realConceptId} = fromGlobalId(id);

    return database.getEndpointConnection(context).updateConcept(realConceptId, {
        rules
      })
      .then((concept) => {
        return {concept};
      });
  }
});


