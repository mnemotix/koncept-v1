/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';

import {transformLabelToMatcher}  from '../../../helpers/formats';
import nanoId from 'nano-id';

export default mutationWithClientMutationId({
  name: 'CreateConceptAndSetRelatedRelation',
  inputFields: {
    thesoId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    relatedParentConceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    text: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({id}, _, context) => {
        return database.getEndpointConnection(context).getConcept(id);
      }
    },
    relatedParentConcept: {
      type: conceptType,
      resolve: ({parentId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(parentId);
      }
    },
    newRelatedConceptEdge: {
      type: conceptEdge,
      resolve: ({relatedConcept}, _, context) => {
        return database.getEndpointConnection(context).getSiblings(relatedConcept.id, 'narrowers')
          .then((siblings) => {
            return {
              cursor: offsetToCursor(siblings.findIndex(sibling => sibling.id == relatedConcept.id)),
              node: relatedConcept
            }
          });
      }
    }
  },
  mutateAndGetPayload: ({text, conceptId, thesoId, relatedParentConceptId}, _, context) => {
    const {id: id} = fromGlobalId(conceptId),
      {id: parentId} = fromGlobalId(relatedParentConceptId),
      {id: realThesoId} = fromGlobalId(thesoId);

    return database.getEndpointConnection(context).createConceptAndSetNarrowerRelation(realThesoId, parentId, [{
        id: nanoId(5),
        value : text,
        lang: 'fr',
        matchers: [
          {
            expression: transformLabelToMatcher(text),
            enabled: true
          }
        ]
      }])
      .then((relatedConcept) => {
        return database.getEndpointConnection(context).addRelatedRelation(id, relatedConcept.id)
          .then(() => {
            return {relatedConcept, parentId, id};
          });
      });
  }
});



