/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  collectionType,
  conceptType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveConceptFromCollection',
  inputFields: {
    collectionId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    collection: {
      type: collectionType,
      resolve: ({collectionRealId}, _, context) => {
        return database.getEndpointConnection(context).getCollection(collectionRealId);
      }
    },
    concept: {
      type: conceptType,
      resolve: ({conceptRealId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(conceptRealId);
      }
    },
    collectionId: {
      type: GraphQLID,
      resolve: ({collectionId}, _, context) => collectionId
    },
    conceptId: {
      type: GraphQLID,
      resolve: ({conceptId}, _, context) => conceptId
    }
  },
  mutateAndGetPayload: ({conceptId, collectionId}, _, context) => {
    const {id: conceptRealId} = fromGlobalId(conceptId),
      {id: collectionRealId} = fromGlobalId(collectionId);

    return database.getEndpointConnection(context).removeConceptFromCollection(collectionRealId, conceptRealId)
      .then(() => {
        return {collectionRealId, conceptRealId, conceptId, collectionId};
      });
  }
});







