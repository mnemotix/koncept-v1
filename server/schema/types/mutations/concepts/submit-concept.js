/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  labelInputType
} from '../../queries/types';

export default mutationWithClientMutationId({
  name: 'SubmitConcept',
  inputFields: {
    thesoId: {
      type: GraphQLID
    },
    schemeId: {
      type: GraphQLID
    },
    broaderId: {
      type: GraphQLID
    },
    relatedId: {
      type: GraphQLID
    },
    label: {
      type: labelInputType
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({concept}, _, context) => concept
    }
  },
  mutateAndGetPayload: async ({thesoId, schemeId, broaderId, relatedId, label}, _, context) => {
    let thesoRealId, schemeRealId, broaderRealId, relatedRealId;

    if (thesoId) {
      thesoRealId = fromGlobalId(thesoId).id;
    }

    if (schemeId) {
      schemeRealId = fromGlobalId(schemeId).id;
    }

    if(broaderId) {
      broaderRealId = fromGlobalId(broaderId).id;
    }

    if(relatedId) {
      relatedRealId = fromGlobalId(relatedId).id;
    }

    if (!thesoId && !schemeId && !broaderId) {
      throw "Either one of `thesoId`, `schemeId` or `broaderId` must be provided."
    }

    let concept = await database.getEndpointConnection(context).submitConcept({
      thesoId: thesoRealId,
      schemeId: schemeRealId,
      broaderId: broaderRealId,
      relatedId: relatedRealId,
      label
    });

    return {concept};
  }
});



