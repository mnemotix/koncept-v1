/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'SetNarrowerRelation',
  inputFields: {
    parentId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    narrowerId: {
      type: GraphQLID,
      description: "Id of the narrower concept. If NULL, mutation will create a new concept."
    }
  },
  outputFields: {
    parentConcept: {
      type: conceptType,
      resolve: ({parentConceptId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(parentConceptId);
      }
    },
    newConceptEdge: {
      type: conceptEdge,
      resolve: ({narrowerConceptId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(narrowerConceptId)
          .then((newConcept) => {
            return database.getEndpointConnection(context).getSiblings(narrowerConceptId, 'narrowers')
              .then((siblings) => {
                return {
                  cursor: offsetToCursor(siblings.findIndex(sibling => sibling.id == narrowerConceptId)),
                  node: newConcept
                }
              });
          });
      }
    }
  },
  mutateAndGetPayload: ({parentId, narrowerId}, _, context) => {
    const {id: parentConceptId} = fromGlobalId(parentId);
    const {id: narrowerConceptId} = fromGlobalId(narrowerId);

    return database.getEndpointConnection(context).addNarrowerRelation(parentConceptId, narrowerConceptId)
      .then(() => {
        return {narrowerConceptId, parentConceptId};
      });
  }
});



