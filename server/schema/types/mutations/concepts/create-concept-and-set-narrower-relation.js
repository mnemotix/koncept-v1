/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';

import {transformLabelToMatcher}  from '../../../helpers/formats';
import nanoId from 'nano-id';

export default mutationWithClientMutationId({
  name: 'CreateConceptAndSetNarrowerRelation',
  inputFields: {
    thesoId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    parentId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    text: {
      description: "Label of the narrower concept to create if not exists.",
      type: GraphQLString
    }
  },
  outputFields: {
    parentConcept: {
      type: conceptType,
      resolve: ({parentConceptId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(parentConceptId);
      }
    },
    newConceptEdge: {
      type: conceptEdge,
      resolve: ({narrowerConcept}, _, context) => {
        return database.getEndpointConnection(context).getSiblings(narrowerConcept.id, 'narrowers')
          .then((siblings) => {
            return {
              cursor: offsetToCursor(siblings.findIndex(sibling => sibling.id == narrowerConcept.id)),
              node: narrowerConcept
            }
          });
      }
    }
  },
  mutateAndGetPayload: ({text, parentId, thesoId}, _, context) => {
    const {id: parentConceptId} = fromGlobalId(parentId),
      {id: realThesoId} = fromGlobalId(thesoId);



    return database.getEndpointConnection(context).createConceptAndSetNarrowerRelation(realThesoId, parentConceptId, [{
      id: nanoId(5),
      value : text,
      lang: 'fr',
      matchers: [
        {
          expression: transformLabelToMatcher(text),
          enabled: true
        }
      ]
    }])
      .then((narrowerConcept) => {
        return {narrowerConcept, parentConceptId};
      });
  }
});



