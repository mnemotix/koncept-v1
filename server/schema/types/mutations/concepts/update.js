/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType,
  conceptEdge
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'UpdateConcept',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    field: {
      type: new GraphQLNonNull(GraphQLString)
    },
    value: {
      type: GraphQLString
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({concept}) => concept
    }
  },
  mutateAndGetPayload: ({id, field, value}, _, context) => {
    const {id: realConceptId} = fromGlobalId(id);


    return database.getEndpointConnection(context).updateConcept(realConceptId, {[field]: value})
      .then((concept) => {
        return {concept};
      });
  }
});


