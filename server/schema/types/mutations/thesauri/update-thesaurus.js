/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesaurusType,
  thesaurusEdge
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'UpdateThesaurus',
  inputFields: {
    thesaurusId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    title: {
      type: GraphQLString
    },
    description: {
      type: GraphQLString
    }
  },
  outputFields: {
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesaurus}) => thesaurus
    }
  },
  mutateAndGetPayload: ({thesaurusId, title, description}, _, context) => {
    const {id: thesaurusRealId} = fromGlobalId(thesaurusId);

    return database.getEndpointConnection(context).updateThesaurus(thesaurusRealId, {title, description, lang: context.lang})
      .then((thesaurus) => {
        return {thesaurus};
      });
  }
});


