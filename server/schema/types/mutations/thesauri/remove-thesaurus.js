/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesauthequeType,
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveThesaurus',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    thesautheque: {
      type: thesauthequeType,
      resolve: (__, _, context) => {
        return database.getEndpointConnection(context).getThesauri();
      }
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({id}) => id
    }
  },
  mutateAndGetPayload: ({id}, _, context) => {
    const {id: thesaurusId} = fromGlobalId(id);

    return database.getEndpointConnection(context).removeThesaurus(thesaurusId)
      .then(() => {
        return {id};
      });
  }
});







