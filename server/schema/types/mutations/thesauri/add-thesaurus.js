/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesauthequeType,
  thesaurusEdge,
} from '../../queries/types';


/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'AddThesaurus',
  inputFields: {
    title: {
      type: new GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  },
  outputFields: {
    thesautheque: {
      type: thesauthequeType,
      resolve: (_, __, context) => {
        return database.getEndpointConnection(context).getThesauri();
      }
    },
    newThesaurusEdge: {
      type: thesaurusEdge,
      resolve: ({newThesaurus}, _, context) => {
        return database.getEndpointConnection(context).getThesauri()
          .then((thesauri) => {
            return {
              cursor: offsetToCursor(thesauri.findIndex(thesaurus => thesaurus.id == newThesaurus.id)),
              node: newThesaurus
            }
          });
      }
    }
  },
  mutateAndGetPayload: ({title, description}, _, context) => {
    return database.getEndpointConnection(context).addThesaurus({title, description, lang: context.lang})
      .then((newThesaurus) => {
        return {newThesaurus };
      });
  }
});






