/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  collectionType,
  collectionEdge
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'UpdateCollection',
  inputFields: {
    collectionId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    title: {
      type: GraphQLString
    },
    description: {
      type: GraphQLString
    }
  },
  outputFields: {
    collection: {
      type: collectionType,
      resolve: ({collection}) => collection
    }
  },
  mutateAndGetPayload: ({collectionId, title, description}, _, context) => {
    const {id: collectionRealId} = fromGlobalId(collectionId);

    return database.getEndpointConnection(context).updateCollection(collectionRealId, {title, description, lang: context.lang})
      .then((collection) => {
        return {collection};
      });
  }
});


