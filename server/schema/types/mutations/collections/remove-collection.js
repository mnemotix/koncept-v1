/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesaurusType,
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveCollection',
  inputFields: {
    collectionId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    thesaurusId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getThesaurus(thesaurusRealId);
      }
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({collectionId}) => collectionId
    }
  },
  mutateAndGetPayload: ({thesaurusId, collectionId}, _, context) => {
    const {id: thesaurusRealId} = fromGlobalId(thesaurusId),
      {id: collectionRealId} = fromGlobalId(collectionId);

    return database.getEndpointConnection(context).removeCollectionFromThesaurus(thesaurusRealId, collectionRealId)
      .then(() => {
        return {thesaurusRealId, collectionId};
      });
  }
});







