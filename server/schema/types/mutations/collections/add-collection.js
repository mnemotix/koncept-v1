/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql';

import {
  offsetToCursor,
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import {
  thesaurusType,
  collectionEdge,
} from '../../queries/types';

/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'AddCollection',
  inputFields: {
    thesaurusId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    title: {
      type: new GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  },
  outputFields: {
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getThesaurus(thesaurusRealId);
      }
    },
    newCollectionEdge: {
      type: collectionEdge,
      resolve: ({newCollection, thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getCollectionsForThesaurus(thesaurusRealId)
          .then((collections) => {
            return {
              cursor: offsetToCursor(collections.findIndex(collection => collection.id === newCollection.id)),
              node: newCollection
            }
          });
      }
    }
  },
  mutateAndGetPayload: ({thesaurusId, title, description}, _, context) => {
    const {id: thesaurusRealId} = fromGlobalId(thesaurusId);

    return database.getEndpointConnection(context).addCollectionToThesaurus(thesaurusRealId, {title, description, lang: context.lang})
      .then((newCollection) => {
        return {newCollection, thesaurusRealId};
      });
  }
});






