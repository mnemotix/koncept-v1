/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import addThesaurusMutation from './thesauri/add-thesaurus';
import removeThesaurusMutation from './thesauri/remove-thesaurus';
import updateThesaurusMutation from './thesauri/update-thesaurus';

import createConceptAndSetNarrowerRelationMutation from './concepts/create-concept-and-set-narrower-relation';
import setNarrowerRelationMutation from './concepts/set-narrower-relation';
import createConceptAndSetRelatedRelationMutation from './concepts/create-concept-and-set-related-relation';
import setRelatedRelationMutation from './concepts/set-related-relation';
import addConceptToCollectionMutation from './concepts/add-concept-to-collection';
import removeConceptFromCollectionMutation from './concepts/remove-concept-from-collection';
import switchFromNarrowerToRelatedRelationMutation from './concepts/switch-from-narrower-to-related-relation';
import switchFromRelatedToNarrowerRelationMutation from './concepts/switch-from-related-to-narrower-relation';
import updateConceptRulesMutation from './concepts/update-rules';

import addLabelToConcept from './labels/add-label';
import removeLabelForConcept from './labels/remove-label';
import updateLabelForConcept from './labels/update-label';

import addMatcherToLabel from './matchers/add-matcher';
import removeMatcherForLabel from './matchers/remove-matcher';
import updateMatcherForLabel from './matchers/update-matcher';

import moveConceptMutation from './concepts/move';
import removeConceptMutation from './concepts/remove';
import updateConceptMutation from './concepts/update';
import addCollectionMutation from './collections/add-collection';
import removeCollectionMutation from './collections/remove-collection';
import updateCollectionMutation from './collections/update-collection';

import addSchemeMutation from './schemes/add-scheme';
import removeSchemeMutation from './schemes/remove-scheme';
import updateSchemeMutation from './schemes/update-scheme';
import createTopSchemeMutation from './schemes/create-top-scheme';
import addTopConceptMutation from './schemes/add-top-concept';
import removeTopSchemeMutation from './schemes/remove-top-scheme';
import removeTopConceptMutation from './schemes/remove-top-concept';

import submitConcept from './concepts/submit-concept';
import validateConcept from './concepts/validate';
import replaceConcept from './concepts/replace';

import {
  GraphQLObjectType,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    //Thesaurus type mutations
    addThesaurus: addThesaurusMutation,
    removeThesaurus: removeThesaurusMutation,
    updateThesaurus: updateThesaurusMutation,

    //Concept type mutations
    createConceptAndSetNarrowerRelation : createConceptAndSetNarrowerRelationMutation,
    setNarrowerRelation: setNarrowerRelationMutation,
    createConceptAndSetRelatedRelation : createConceptAndSetRelatedRelationMutation,
    setRelatedRelation: setRelatedRelationMutation,
    removeConcept: removeConceptMutation,
    moveConcept: moveConceptMutation,
    updateConcept: updateConceptMutation,
    updateConceptRules: updateConceptRulesMutation,
    removeConceptFromCollection: removeConceptFromCollectionMutation,
    addConceptToCollection: addConceptToCollectionMutation,
    switchFromNarrowerToRelatedRelation: switchFromNarrowerToRelatedRelationMutation,
    switchFromRelatedToNarrowerRelation: switchFromRelatedToNarrowerRelationMutation,
    submitConcept,
    validateConcept,
    replaceConcept,

    //Collection type mutations
    addCollection: addCollectionMutation,
    removeCollection: removeCollectionMutation,
    updateCollection: updateCollectionMutation,
    
    //Scheme type mutations
    addScheme: addSchemeMutation,
    removeScheme: removeSchemeMutation,
    removeTopScheme: removeTopSchemeMutation,
    updateScheme: updateSchemeMutation,
    createTopScheme: createTopSchemeMutation,
    addTopConcept: addTopConceptMutation,
    removeTopConcept: removeTopConceptMutation,

    //Label
    addLabelToConcept,
    updateLabelForConcept,
    removeLabelForConcept,

    //Matchers
    addMatcherToLabel,
    removeMatcherForLabel,
    updateMatcherForLabel
  }
});
