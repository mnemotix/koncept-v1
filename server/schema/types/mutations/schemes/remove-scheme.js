/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  thesaurusType,
  conceptType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveScheme',
  inputFields: {
    schemeId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    thesaurusId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    topConceptId: {
      type: GraphQLID
    }
  },
  outputFields: {
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getThesaurus(thesaurusRealId);
      }
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({schemeId}) => schemeId
    },
    concept: {
      type: conceptType,
      resolve: ({topConceptId}, _, context) => {
        if (topConceptId) {
          const {id: topConceptRealId} = fromGlobalId(topConceptId);

          return database.getEndpointConnection(context).getConcept(topConceptRealId);
        }
      }
    }
  },
  mutateAndGetPayload: ({thesaurusId, schemeId, topConceptId}, _, context) => {
    const {id: thesaurusRealId} = fromGlobalId(thesaurusId),
      {id: schemeRealId} = fromGlobalId(schemeId);

    return database.getEndpointConnection(context).removeSchemeFromThesaurus(thesaurusRealId, schemeRealId)
      .then(() => {
        return {thesaurusRealId, schemeId, topConceptId};
      });
  }
});







