/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql';

import {
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import {
  schemeType,
  conceptEdge
} from '../../queries/types';

/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'AddTopConcept',
  inputFields: {
    thesoId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    schemeId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    topConceptLabel: {
      type: GraphQLString
    }
  },
  outputFields: {
    scheme: {
      type: schemeType,
      resolve: ({schemeRealId}, _, context) => {
        return database.getEndpointConnection(context).getScheme(schemeRealId);
      }
    },
    newConceptEdge: {
      type: conceptEdge,
      resolve: ({newConcept}) => ({
        cursor: 0,
        node: newConcept
      })
    }
  },
  mutateAndGetPayload: ({schemeId, thesoId, topConceptLabel}, _, context) => {
    const {id: schemeRealId} = fromGlobalId(schemeId),
      {id: thesoRealId} = fromGlobalId(thesoId);

    return database.getEndpointConnection(context).addSchemeTopConcept(thesoRealId, schemeRealId, {
      topConceptLabel,
      lang: context.lang
    })
      .then((newConcept) => {
        return {newConcept, schemeRealId};
      });
  }
});






