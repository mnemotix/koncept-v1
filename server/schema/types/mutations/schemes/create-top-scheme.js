/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql';

import {
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import {
  thesaurusType,
  schemeEdge,
  conceptType
} from '../../queries/types';

/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'CreateTopScheme',
  inputFields: {
    thesaurusId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    schemeTitle: {
      type: new GraphQLNonNull(GraphQLString)
    },
    schemeDescription: {
      type: GraphQLString
    },
    schemeColor: {
      type: GraphQLString
    },
    topConceptLabel: {
      type: GraphQLString
    }
  },
  outputFields: {
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getThesaurus(thesaurusRealId);
      }
    },
    newSchemeEdge: {
      type: schemeEdge,
      resolve: ({newScheme}) => ({
        cursor: 0,
        node: newScheme
      })
    }
  },
  mutateAndGetPayload: ({thesaurusId, schemeTitle, schemeDescription, schemeColor}, _, context) => {
    const {id: thesaurusRealId} = fromGlobalId(thesaurusId);

    return database.getEndpointConnection(context).createTopSchemeToThesaurus(thesaurusRealId, {
      title: schemeTitle,
      description: schemeDescription,
      color: schemeColor,
      lang: context.lang
    })
      .then((newScheme) => {
        return {newScheme, thesaurusRealId};
      });
  }
});






