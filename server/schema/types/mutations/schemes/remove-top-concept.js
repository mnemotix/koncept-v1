/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  schemeType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveTopConcept',
  inputFields: {
    id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    schemeId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    scheme: {
      type: schemeType,
      resolve: ({schemeRealId}, _, context) => {
        return database.getEndpointConnection(context).getScheme(schemeRealId);
      }
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({id}) => id
    }
  },
  mutateAndGetPayload: ({id, schemeId}, _, context) => {
    const {id: conceptId} = fromGlobalId(id),
      {id: schemeRealId} = fromGlobalId(schemeId);

    return database.getEndpointConnection(context).removeConcept(conceptId)
      .then(() => {
        return {id, schemeRealId};
      });
  }
});


