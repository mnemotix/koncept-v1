/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql';

import {
  offsetToCursor,
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import {
  thesaurusType,
  schemeEdge,
  conceptType
} from '../../queries/types';


/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'AddScheme',
  inputFields: {
    thesaurusId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    topConceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    title: {
      type: new GraphQLNonNull(GraphQLString)
    },
    description: {
      type: GraphQLString
    }
  },
  outputFields: {
    thesaurus: {
      type: thesaurusType,
      resolve: ({thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getThesaurus(thesaurusRealId);
      }
    },
    newSchemeEdge: {
      type: schemeEdge,
      resolve: ({newScheme, thesaurusRealId}, _, context) => {
        return database.getEndpointConnection(context).getSchemesForThesaurus(thesaurusRealId)
          .then((schemes) => {
            return {
              cursor: offsetToCursor(schemes.findIndex(scheme => scheme.id == newScheme.id)),
              node: newScheme
            }
          });
      }
    },
    topConcept: {
      type: conceptType,
      resolve: ({newScheme}, _, context) => database.getEndpointConnection(context).getSchemeTopConcept(newScheme.id)
    }
  },
  mutateAndGetPayload: ({thesaurusId, topConceptId, title, description}, _, context) => {
    const {id: thesaurusRealId} = fromGlobalId(thesaurusId),
      {id: topConceptRealId} = fromGlobalId(topConceptId);

    return database.getEndpointConnection(context).addSchemeToThesaurus(thesaurusRealId, topConceptRealId, title, description)
      .then((newScheme) => {
        return {newScheme, thesaurusRealId};
      });
  }
});






