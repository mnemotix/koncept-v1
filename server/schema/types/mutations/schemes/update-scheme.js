/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  skosElementInterface
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'UpdateScheme',
  inputFields: {
    schemeId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    title: {
      type: GraphQLString
    },
    description: {
      type: GraphQLString
    },
    color: {
      type: GraphQLString
    }
  },
  outputFields: {
    scheme: {
      type: skosElementInterface,
      resolve: ({scheme}) => scheme
    }
  },
  mutateAndGetPayload: ({schemeId, title, description, color}, _, context) => {
    const {id: schemeRealId} = fromGlobalId(schemeId);

    return database.getEndpointConnection(context).updateScheme(schemeRealId, {title, description, color, lang: context.lang})
      .then((scheme) => {
        return {scheme};
      });
  }
});


