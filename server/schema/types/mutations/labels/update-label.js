/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  localizedLabelType,
  labelEdge,
  conceptType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'UpdateLabelForConcept',
  inputFields: {
    labelId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    labelType: {
      type: new GraphQLNonNull(GraphQLString)
    },
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    value: {
      type: GraphQLString
    },
    lang: {
      type: GraphQLString
    }
  },
  outputFields: {
    label: {
      type: localizedLabelType,
      resolve: ({label}) => label
    },
    concept: {
      type: conceptType,
      resolve: ({conceptRealId, _, context}) => database.getEndpointConnection(context).getConcept(conceptRealId)
    }
  },
  mutateAndGetPayload: ({labelId, conceptId, labelType, value, lang}, _, context) => {
    const {id: labelRealId} = fromGlobalId(labelId),
      {id: conceptRealId} = fromGlobalId(conceptId);

    return database.getEndpointConnection(context).updateLabelForConcept(conceptRealId, labelType, labelRealId, value, lang)
      .then((label) => {
        return {label, conceptRealId};
      });
  }
});


