/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql';

import {
  offsetToCursor,
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import {
  labelEdge,
  conceptType
} from '../../queries/types';


/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'AddLabelToConcept',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    labelType: {
      type: new GraphQLNonNull(GraphQLString)
    },
    lang: {
      type: new GraphQLNonNull(GraphQLString)
    },
    value: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({conceptRealId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(conceptRealId);
      }
    },
    newLabelEdge: {
      type: labelEdge,
      resolve: ({newLabel}, _, context) => ({
        cursor: offsetToCursor(0),
        node: newLabel
      })
    }
  },
  mutateAndGetPayload: ({conceptId, labelType, value, lang}, _, context) => {
    const {id: conceptRealId} = fromGlobalId(conceptId);

    return database.getEndpointConnection(context).addLabelToConcept(conceptRealId, labelType, value, lang)
      .then((newLabel) => {
        return {newLabel, conceptRealId};
      });
  }
});






