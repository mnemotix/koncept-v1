/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLString
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  conceptType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveLabelForConcept',
  inputFields: {
    conceptId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    labelId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    labelType: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    concept: {
      type: conceptType,
      resolve: ({conceptRealId}, _, context) => {
        return database.getEndpointConnection(context).getConcept(conceptRealId);
      }
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({labelId}) => labelId
    }
  },
  mutateAndGetPayload: ({conceptId, labelId, labelType}, _, context) => {
    const {id: conceptRealId} = fromGlobalId(conceptId),
      {id: labelRealId} = fromGlobalId(labelId);

    return database.getEndpointConnection(context).removeLabelForConcept(conceptRealId, labelType, labelRealId)
      .then(() => {
        return {conceptRealId, labelId};
      });
  }
});







