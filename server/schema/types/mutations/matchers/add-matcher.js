/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLNonNull,
  GraphQLString,
  GraphQLID
} from 'graphql';

import {
  offsetToCursor,
  mutationWithClientMutationId,
  fromGlobalId
} from 'graphql-relay';

import {
  matcherEdge,
  localizedLabelType
} from '../../queries/types';


/**
 * Add thesaurus mutation
 */
export default mutationWithClientMutationId({
  name: 'AddMatcherToLabel',
  inputFields: {
    labelId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    expression: {
      type: new GraphQLNonNull(GraphQLString)
    }
  },
  outputFields: {
    label: {
      type: localizedLabelType,
      resolve: ({labelRealId}, _, context) => {
        return database.getEndpointConnection(context).getLocalizedLabel(labelRealId);
      }
    },
    newMatcherEdge: {
      type: matcherEdge,
      resolve: ({newMatcher}, _, context) => ({
        cursor: offsetToCursor(0),
        node: newMatcher
      })
    }
  },
  mutateAndGetPayload: ({labelId, expression}, _, context) => {
    const {id: labelRealId} = fromGlobalId(labelId);

    return database.getEndpointConnection(context).addMatcherToLabel(labelRealId, expression)
      .then((newMatcher) => {
        return {newMatcher, labelRealId};
      });
  }
});






