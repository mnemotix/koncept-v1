/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLString
} from 'graphql';

import {
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  localizedLabelType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'RemoveMatcherForLabel',
  inputFields: {
    labelId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    matcherId: {
      type: new GraphQLNonNull(GraphQLID)
    }
  },
  outputFields: {
    label: {
      type: localizedLabelType,
      resolve: ({labelRealId}, _, context) => {
        return database.getEndpointConnection(context).getLocalizedLabel(labelRealId);
      }
    },
    deletedId: {
      type: GraphQLID,
      resolve: ({matcherId}) => matcherId
    }
  },
  mutateAndGetPayload: ({labelId, matcherId}, _, context) => {
    const {id: labelRealId} = fromGlobalId(labelId),
      {id: matcherRealId} = fromGlobalId(matcherId);

    return database.getEndpointConnection(context).removeMatcherForLabel(labelRealId, matcherRealId)
      .then(() => {
        return {labelRealId, matcherId};
      });
  }
});







