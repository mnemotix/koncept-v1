/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../../database';

import {
  GraphQLBoolean,
  GraphQLID,
  GraphQLNonNull,
  GraphQLString,
} from 'graphql';

import {
  offsetToCursor,
  fromGlobalId,
  mutationWithClientMutationId,
} from 'graphql-relay';

import {
  matcherType,
  localizedLabelType
} from '../../queries/types';


export default mutationWithClientMutationId({
  name: 'UpdateMatcherForLabel',
  inputFields: {
    matcherId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    labelId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    expression: {
      type: GraphQLString
    }
  },
  outputFields: {
    matcher: {
      type: matcherType,
      resolve: ({matcher}) => matcher
    },
    label: {
      type: localizedLabelType,
      resolve: ({labelRealId, _, context}) => database.getEndpointConnection(context).getLocalizedLabel(labelRealId)
    }
  },
  mutateAndGetPayload: ({matcherId, labelId, expression}, _, context) => {
    const {id: matcherRealId} = fromGlobalId(matcherId),
      {id: labelRealId} = fromGlobalId(labelId);

    return database.getEndpointConnection(context).updateMatcherForLabel(labelRealId, matcherRealId, expression)
      .then((matcher) => {
        return {matcher, labelRealId};
      });
  }
});


