/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLEnumType
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  cursorForObjectInConnection,
  offsetToCursor,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

import {ruleStatementType, ruleStatementInputType} from './ruleStatement';

var ruleType = new GraphQLObjectType({
  name: 'Rule',
  description: 'An elastic search matching expression',
  fields: () => ({
    statement: {
      type: ruleStatementType,
      description: 'Rule statement',
      resolve: (rule) => rule.statement
    },
    edgeType: {
      type: GraphQLString,
      description: 'Edge type to apply from a ruled concept to a matching object',
      resolve: (rule) => rule.edgeType
    }
  })
});

var ruleInputType = new GraphQLInputObjectType({
  name: 'RuleInput',
  fields: () => ({
    statement: {
      type: ruleStatementInputType,
      description: 'Rule statement'
    },
    edgeType: {
      type: GraphQLString,
      description: 'Edge type to apply from a ruled concept to a matching object'
    }
  })
});

export {
  ruleType,
  ruleInputType
};