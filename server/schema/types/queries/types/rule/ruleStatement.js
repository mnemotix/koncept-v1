/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLEnumType
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  cursorForObjectInConnection,
  offsetToCursor,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

var ruleStatementType = new GraphQLObjectType({
  name: 'RuleStatement',
  description: 'An elastic search matching expression',
  fields: () => ({
    nodeType: {
      type: GraphQLString,
      description: 'Node type',
      resolve: (ruleStatement) => ruleStatement.nodeType
    },
    field: {
      type: GraphQLString,
      description: 'Field on which apply the test',
      resolve: (ruleStatement) => ruleStatement.field
    },
    fieldType: {
      type: new GraphQLEnumType({
        name: 'RuleStatementFieldType',
        values: {
          STRING: { value: "string" },
          DATE: { value: "date" },
          NUMBER: { value: "number" },
          BOOLEAN: { value: "boolean" }
        }
      }),
      description: 'Field type off field on which apply the test',
      resolve: (ruleStatement) => ruleStatement.fieldType
    },
    criteria: {
      type: GraphQLString,
      description: 'Matching expression',
      resolve: (ruleStatement) => ruleStatement.criteria
    }
  })
});

var ruleStatementInputType = new GraphQLInputObjectType({
  name: 'RuleStatementInput',
  fields: () => ({
    nodeType: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'Node type'
    },
    field: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'Field on which apply the test'
    },
    fieldType: {
      type: new GraphQLEnumType({
        name: 'RuleStatementFieldInputType',
        values: {
          STRING: { value: "string" },
          DATE: { value: "date" },
          NUMBER: { value: "number" },
          BOOLEAN: { value: "boolean" }
        }
      }),
      description: 'Field type off field on which apply the test'
    },
    criteria: {
      type: GraphQLString,
      description: 'Matching expression'
    }
  })
});

export {
  ruleStatementType,
  ruleStatementInputType
};