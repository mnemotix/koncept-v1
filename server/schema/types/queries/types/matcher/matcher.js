/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  cursorForObjectInConnection,
  offsetToCursor,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

var matcherType = new GraphQLObjectType({
  name: 'Matcher',
  description: 'An elastic search matching expression',
  fields: () => ({
    id: globalIdField('Matcher'),
    expression: {
      type: GraphQLString,
      description: 'Matching expression',
      resolve: (matcher) => matcher.expression
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date',
      resolve: (matcher) => matcher.creationDate
    },
    enabled: {
      type: GraphQLBoolean,
      description: 'Is matcher enabled',
      resolve: (matcher) => matcher.enabled
    }
  }),
  interfaces: [nodeInterface]
});

var matcherInputType = new GraphQLInputObjectType({
  name: 'MatcherInput',
  fields: () => ({
    expression: {
      type: GraphQLString,
      description: 'Matching expression'
    }
  })
});

var {
  connectionType: matcherConnection,
  edgeType: matcherEdge
  } = connectionDefinitions({name: 'Matcher', nodeType: matcherType});


export {
  matcherType,
  matcherInputType,
  matcherConnection,
  matcherEdge
};