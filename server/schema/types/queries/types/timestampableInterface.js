/**
 * This file is part of the Weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import {
  GraphQLNonNull,
  GraphQLInterfaceType,
  GraphQLID,
  GraphQLFloat
} from 'graphql';

import {
  connectionDefinitions
} from 'graphql-relay';

const timestampableInterface = new GraphQLInterfaceType({
  name: 'TimestampableInterface',
  description: 'An interface for all timestampable objects',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID)
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date'
    },
    lastUpdate: {
      type: GraphQLFloat,
      description: 'Last Update'
    }
  })
});

const {
  connectionType: timestampableConnection,
  edgeType: timestampableEdge
  } = connectionDefinitions({name: 'TimestampableConnection', nodeType: timestampableInterface});

export {
  timestampableConnection,
  timestampableEdge,
  timestampableInterface
}