/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  cursorForObjectInConnection,
  offsetToCursor,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

var userType = new GraphQLObjectType({
  name: 'User',
  fields: () => ({
    id: globalIdField('User'),
    email: {
      type: GraphQLString,
      description: 'Email',
      resolve: (user) => user.email
    },
    firstName: {
      type: GraphQLString,
      description: 'First name',
      resolve: (user) => user.given_name
    },
    lastName: {
      type: GraphQLString,
      description: 'Last name',
      resolve: (user, args) => user.family_name
    }
  }),
  interfaces: [nodeInterface]
});

export { userType };