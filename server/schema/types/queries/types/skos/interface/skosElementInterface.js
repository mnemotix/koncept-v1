/**
 * This file is part of the Weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import {
  GraphQLNonNull,
  GraphQLInterfaceType,
  GraphQLID,
  GraphQLFloat
} from 'graphql';

import {
  connectionDefinitions
} from 'graphql-relay';

const skosElementInterface = new GraphQLInterfaceType({
  name: 'SkosElementInterface',
  description: 'A skos element',
  fields: () => ({
    id: {
      type: new GraphQLNonNull(GraphQLID)
    }
  })
});

const {
  connectionType: skosElementConnection,
  edgeType: skosElementEdge
  } = connectionDefinitions({name: 'SkosElementConnection', nodeType: skosElementInterface});

export {
  skosElementInterface,
  skosElementConnection,
  skosElementEdge
}