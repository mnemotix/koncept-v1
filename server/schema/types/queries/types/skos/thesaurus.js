/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLUnionType,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  globalIdField,
  fromGlobalId,
  toGlobalId
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

import {conceptType, conceptConnection} from './concept';
import {collectionConnection} from './collection';
import {schemeConnection, schemeType} from './scheme';
import {localizedLabelType} from './label';

import {Thesaurus} from '../../../../models';
import {skosElementInterface} from './interface/skosElementInterface';

let thesaurusType = new GraphQLObjectType({
  name: 'Thesaurus',
  description: 'A thesaurus',
  fields: () => ({
    id: globalIdField('Thesaurus'),
    title: {
      type: GraphQLString,
      description: 'Thesaurus title for current language',
      resolve: (th, _, context) => database.getEndpointConnection(context).getThesaurusTitleForLang(th, context.lang)
    },
    titleLabel: {
      type: localizedLabelType,
      description: 'Thesaurus title label for current language',
      resolve: (th, _, context) => database.getEndpointConnection(context).getThesaurusTitleForLang(th, context.lang, true)
    },
    description: {
      type: GraphQLString,
      description: 'Thesaurus description for current language',
      resolve: (th, _, context) => database.getEndpointConnection(context).getThesaurusDescriptionForLang(th, context.lang)
    },
    descriptionLabel: {
      type: localizedLabelType,
      description: 'Thesaurus description label for current language',
      resolve: (th, _, context) => database.getEndpointConnection(context).getThesaurusDescriptionForLang(th, context.lang, true)
    },
    color: {
      type: GraphQLString,
      description: 'Scheme color',
      resolve: (th) =>  th.color
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date',
      resolve: (th) => th.creationDate
    },
    lastUpdate: {
      type: GraphQLFloat,
      description: 'Last update',
      resolve: (th) => th.lastUpdate
    },
    topSchemes: {
      type: schemeConnection,
      description: 'Top level schemes',
      args: connectionArgs,
      resolve: (th, args, context) => database.getEndpointConnection(context).getThesaurusTopSchemes(th.id, args)
        .then(schemes => connectionFromArray(schemes, args))
    },
    schemes: {
      type: schemeConnection,
      description: 'Schemes',
      args: connectionArgs,
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getSchemesForThesaurus(th.id)
          .then(schemes => connectionFromArray(schemes, args));
      }
    },
    collections: {
      type: collectionConnection,
      description: 'skos:example value',
      args: connectionArgs,
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getCollectionsForThesaurus(th.id)
          .then(collections => connectionFromArray(collections, args));
      }
    },
    columns: {
      type: new GraphQLList(skosElementInterface),
      args: {
        ids: { type: new GraphQLList(GraphQLID) }
      },
      resolve: async (th, {ids}, context) => {
        let items = [];

        for (let id of ids) {
          let {id: realId, type} = fromGlobalId(id),
            item;

          if (type == 'Concept') {
            item = await database.getEndpointConnection(context).getConcept(realId);
          } else {
            item = await database.getEndpointConnection(context).getScheme(realId);
          }

          items.push(item);
        }

        return items;
      }
    },
    draftConceptsCount: {
      type: GraphQLInt,
      description: 'Count of concepts marked as drafts',
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getThesaurusDraftConceptsCount(th.id);
      }
    },
    draftConcepts: {
      type: conceptConnection,
      args: connectionArgs,
      description: 'Concepts marked as drafts',
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getThesaurusDraftConcepts(th.id, args)
          .then(concepts => connectionFromArray(concepts, args));
      }
    },
    inClipboardConcepts: {
      type: new GraphQLList(conceptType),
      args: {
        ids: { type: new GraphQLList(GraphQLID) }
      },
      resolve: (th, args, context) => {
        let promises = [],
          concepts = [];

        args.ids.map(conceptId => {
          let {id: realConceptId} = fromGlobalId(conceptId);

          promises.push(database.getEndpointConnection(context).getConcept(realConceptId)
            .then(concept => concepts.push(concept))
          );
        });

        return Promise.all(promises).then(() => concepts);
      }
    },
    searchConcepts: {
      type: new GraphQLList(conceptType),
      args: {
        query: { type: GraphQLString },
        first: { type: GraphQLInt}
      },
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).searchConcept(th.id, args);
      }
    },
    globalGraph: {
      type: new GraphQLObjectType({
        name: 'ThesaurusGraph',
        description: 'A graph representation',
        fields: () => ({
          nodes: {
            type: new GraphQLList(skosElementInterface),
            description: 'List of nodes',
            resolve: (graph) => graph.nodes
          },
          edges: {
            type:  new GraphQLList(new GraphQLObjectType({
              name: 'ThesaurusEdge',
              description: 'An egde representation',
              fields: () => ({
                id:{
                  type: GraphQLString,
                  resolve: (edge) => edge.id
                },
                label:{
                  type: GraphQLString,
                  resolve: (edge) => edge.pred
                },
                inV:{
                  type: GraphQLID,
                  resolve: (edge) => toGlobalId(edge.obj.replace(/:.*$/, ''), edge.obj)
                },
                outV:{
                  type: GraphQLID,
                  resolve: (edge) => toGlobalId(edge.subj.replace(/:.*$/, ''), edge.subj)
                }
              })
            })),
            description: 'Creation date',
            resolve: (graph) => graph.edges
          }
        })
      }),
      args: {
        query: { type: GraphQLString }
      },
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getThesaurusGlobalGraph(th.id);
      }
    }
  }),
  interfaces: [nodeInterface, skosElementInterface],
  isTypeOf: (value) => value instanceof Thesaurus
});

var {
  connectionType: thesaurusConnection,
  edgeType: thesaurusEdge
  } = connectionDefinitions({name: 'ThesaurusConnection', nodeType: thesaurusType});

export {
  thesaurusType,
  thesaurusEdge,
  thesaurusConnection
};