/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLObjectType,
} from 'graphql';

import {
  connectionArgs,
  connectionFromArray,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

import {thesaurusConnection} from './thesaurus';
import {conceptConnection} from './concept';

import {paginationArgs} from '../../definitions/helpers';

var thesauthequeType = new GraphQLObjectType({
  name: 'Thesautheque',
  fields: () => ({
    id: globalIdField('Thesautheque'),
    // Add your own root fields here
    thesauri: {
      type: thesaurusConnection,
      description: 'List of thesaurus',
      args: connectionArgs,
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getThesauri()
          .then((thesauri) => {
            return connectionFromArray(thesauri, args)
          });
      }
    },
    concepts: {
      type: conceptConnection,
      args: {
        ...connectionArgs,
        ...paginationArgs
      },
      resolve: (th, args, context) => {
        return database.getEndpointConnection(context).getConcepts(args)
          .then((concepts) => {
            return connectionFromArray(concepts, args)
          });
      }
    },
  }),
  interfaces: [nodeInterface]
});

export {
  thesauthequeType
};