/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLFloat,
  GraphQLInt,
  GraphQLList,
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

import {skosElementInterface} from './interface/skosElementInterface';

import {labelConnection, localizedLabelType} from './label';
import {collectionConnection} from './collection';
import {schemeType, schemeConnection} from './scheme';

import {ruleType} from '../rule/rule';

import {Concept} from '../../../../models';

let conceptType = new GraphQLObjectType({
  name: 'Concept',
  description: 'A skos:concept in a thesaurus',
  fields: () => ({
    id: globalIdField('Concept'),
    prefLabels:{
      type: labelConnection,
      args: connectionArgs,
      description: 'skos:prefLabel values',
      resolve: (concept, args, context) => database.getEndpointConnection(context)
          .getConceptPrefLabels(concept.id, args)
          .then(labels => connectionFromArray(labels, args))
    },
    prefLabelForLang: {
      type: localizedLabelType,
      args: {
        lang: {
          type: GraphQLString
        }
      },
      resolve: (concept, {lang}, context) => {
        return database.getEndpointConnection(context)
          .getConceptPrefLabels(concept.id)
          .then(labels => labels.find(label => label.lang == lang) || labels[0] || {
            value: "UNDEFINED",
            lang: "UNDEFINED"
          })
      }
    },
    altLabels: {
      type: labelConnection,
      args: connectionArgs,
      description: 'skos:altLabel values',
      resolve: (concept, args, context) =>
        database.getEndpointConnection(context).getConceptAltLabels(concept.id, args)
          .then(labels => connectionFromArray(labels, args))
    },
    hiddenLabels: {
      type: labelConnection,
      args: connectionArgs,
      description: 'skos:hiddenLabel values',
      resolve: (concept, args, context) =>
        database.getEndpointConnection(context).getConceptHiddenLabels(concept.id, args)
          .then(labels => connectionFromArray(labels, args))
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date',
      resolve: (concept) => concept.creationDate
    },
    lastUpdate: {
      type: GraphQLFloat,
      description: 'Last update',
      resolve: (concept) => concept.lastUpdate
    },
    childrenCount: {
      type: GraphQLInt,
      description: 'Is this concept have children',
      resolve: (ct, _, context) => {
        return database.getEndpointConnection(context).getNarrowersCount(ct.id)
      }
    },
    relatedCount : {
      type: GraphQLInt,
      description: 'Related concepts count',
      resolve: (ct, _, context) => {
        return database.getEndpointConnection(context).getRelatedCount(ct.id);
      }
    },
    topConceptOf:{
      type: schemeType,
      description: 'skos:topConceptOf semantic relationship.',
      resolve: (ct, _, context) => null //database.getEndpointConnection(context).getSchemeForTopConcept(ct.id)
    },
    scheme: {
      type: schemeType,
      description: 'skos:inScheme semantic relationship. This is a connection because SKOS allows a concept to belong to several schemes',
      resolve: (ct, _, context) => database.getEndpointConnection(context).getConceptScheme(ct.id)
    },
    broader: {
      type: conceptType,
      description: 'skos:broader semantic relationship',
      resolve: (ct, _, context) => database.getEndpointConnection(context).getBroader(ct.id)
    },
    narrowers: {
      type: conceptConnection,
      description: 'skos:narrower semantic relationship',
      args: connectionArgs,
      resolve: (ct, args, context) => {
        return database.getEndpointConnection(context).getNarrowers(ct.id)
          .then((narrowers) => connectionFromArray(narrowers, args))
      }
    },
    related: {
      type: conceptConnection,
      description: 'skos:related semantic relationship',
      args: connectionArgs,
      resolve: (ct, args, context) => {
        return database.getEndpointConnection(context).getRelated(ct.id)
          .then((related) => connectionFromArray(related, args))
      }
    },
    narrowerConceptsAndSchemes: {
      type: conceptConnection,
      description: 'skos:narrower semantic relationship',
      args: connectionArgs,
      resolve: (ct, args, context) => {
        return database.getEndpointConnection(context).getNarrowers(ct.id)
          .then((narrowers) => connectionFromArray(narrowers, args))
      }
    },
    scopeNote: {
      type: GraphQLString,
      description: 'skos:scopeNote value',
      resolve: (ct, _, context) => ct.scopeNote
    },
    historyNote: {
      type: GraphQLString,
      description: 'skos:historyNote value',
      resolve: (ct, _, context) => ct.historyNote
    },
    editorialNote: {
      type: GraphQLString,
      description: 'skos:editorialNote value',
      resolve: (ct, _, context) => ct.editorialNote
    },
    changeNote: {
      type: GraphQLString,
      description: 'skos:changeNote value',
      resolve: (ct, _, context) => ct.changeNote
    },
    definition: {
      type: GraphQLString,
      description: 'skos:definition value',
      resolve: (ct, _, context) => ct.definition
    },
    example: {
      type: GraphQLString,
      description: 'skos:example value',
      resolve: (ct, _, context) => ct.example
    },
    memberOf: {
      type: collectionConnection,
      description: 'Collection from which the concept is skos:member of',
      args: connectionArgs,
      resolve: (ct, args, context) => {
        return database.getEndpointConnection(context).getCollectionsForConcept(ct.id).
        then(collections => connectionFromArray(collections, args))
      }
    },
    rules: {
      type: new GraphQLList(ruleType),
      description: 'List of rules',
      resolve: (ct, args) => ct.rules
    },
    isDraft : {
      type: GraphQLBoolean,
      description: 'Is this concept draft',
      resolve: (ct, _, context) => ct.isDraft
    },
    ancestors : {
      type: new GraphQLList(skosElementInterface),
      description: "List of ancestors",
      resolve: (ct, _, context) => database.getEndpointConnection(context).getConceptAncestors(ct.id)
    },
    taggingCount: {
      type:GraphQLInt,
      description: "Related tagging count",
      resolve: (ct, _, context) => database.getEndpointConnection(context).getConceptTaggingsCount(ct.id)
    }
  }),
  interfaces: [nodeInterface, skosElementInterface],
  isTypeOf: (value) => value instanceof Concept
});

var {
  connectionType: conceptConnection,
  edgeType: conceptEdge
  } = connectionDefinitions({name: 'Concept', nodeType: conceptType});

export {
  conceptType,
  conceptConnection,
  conceptEdge
}