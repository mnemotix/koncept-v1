/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLInputObjectType,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface} from '../../definitions/nodeDefinitions';

import {matcherConnection} from '../matcher/matcher';

import {LocalizedLabel} from '../../../../models';
import {skosElementInterface} from './interface/skosElementInterface';

let localizedLabelType = new GraphQLObjectType({
  name: 'LocalizedLabel',
  description: 'A label in a skos:concept',
  fields: () => ({
    id: globalIdField('LocalizedLabel'),
    value: {
      type: GraphQLString,
      description: 'Value of the label',
      resolve: (label) => label.value
    },
    type: {
      type: GraphQLString,
      description: 'Type of the label',
      resolve: (label) => label.type
    },
    lang: {
      type: GraphQLString,
      description: 'Lang',
      resolve: (label) => label.lang
    },
    matchers: {
      type:  matcherConnection,
      args: connectionArgs,
      description: 'List of elastic search matchers related to this label',
      resolve: (label, args, context) =>  database.getEndpointConnection(context)
        .getLabelMatchers(label.id, args)
        .then(matchers => connectionFromArray(matchers, args))
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date',
      resolve: (label) => label.creationDate
    },
    lastUpdate: {
      type: GraphQLFloat,
      description: 'Last update',
      resolve: (label) => label.lastUpdate
    }
  }),
  interfaces: [nodeInterface, skosElementInterface],
  isTypeOf: (value) => value instanceof LocalizedLabel
});

var labelInputType = new GraphQLInputObjectType({
  name: 'LocalizedLabelInput',
  description: 'A label in a skos:concept',
  fields: () => ({
    value: {
      type: GraphQLString,
      description: 'Value of the label'
    },
    lang: {
      type: GraphQLString,
      description: 'Lang'
    }
  })
});

var {
  connectionType: labelConnection,
  edgeType: labelEdge
  } = connectionDefinitions({name: 'LocalizedLabel', nodeType: localizedLabelType});


export {
  localizedLabelType,
  labelInputType,
  labelConnection,
  labelEdge
};