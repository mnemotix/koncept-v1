/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLFloat,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  globalIdField,
} from 'graphql-relay';

import {nodeInterface, nodeField} from '../../definitions/nodeDefinitions';

import {conceptConnection} from './concept';
import {localizedLabelType} from './label';

import {Collection} from '../../../../models';

import {skosElementInterface} from './interface/skosElementInterface';

const collectionType = new GraphQLObjectType({
  name: 'Collection',
  description: 'A skos:collection in a thesaurus',
  fields: () => ({
    id: globalIdField('Collection'),
    title: {
      type: GraphQLString,
      description: 'Collection title for current language',
      resolve: (collection, _, context) => database.getEndpointConnection(context).getCollectionTitleForLang(collection, context.lang)
    },
    titleLabel: {
      type: localizedLabelType,
      description: 'Collection label title for current language',
      resolve: (collection, _, context) => database.getEndpointConnection(context).getCollectionTitleForLang(collection, context.lang, true)
    },
    description: {
      type: GraphQLString,
      description: 'Collection description for current language',
      resolve: (collection, _, context) => database.getEndpointConnection(context).getCollectionDescriptionForLang(collection, context.lang)
    },
    descriptionLabel: {
      type: localizedLabelType,
      description: 'Collection description label for current language',
      resolve: (collection, _, context) => database.getEndpointConnection(context).getCollectionDescriptionForLang(collection, context.lang, true)
    },
    color: {
      type: GraphQLString,
      description: 'Scheme color',
      resolve: (collection) =>  collection.color
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date',
      resolve: (collection) => collection.creationDate
    },
    lastUpdate: {
      type: GraphQLFloat,
      description: 'Last update',
      resolve: (collection) => collection.lastUpdate
    },
    members: {
      type: conceptConnection,
      description: 'Concept skos:member of the collection',
      args: connectionArgs,
      resolve: (collection, args, context) => {
        return database.getEndpointConnection(context).getConceptsOfCollection(collection.id)
          .then(concepts => {
            return connectionFromArray(concepts, args);
          })
      }
    }
  }),
  isTypeOf: (value) => value instanceof Collection,
  interfaces: [nodeInterface, skosElementInterface]
});

const {
  connectionType: collectionConnection,
  edgeType: collectionEdge
  } = connectionDefinitions({name: 'CollectionConnection', nodeType: collectionType});

export {
  collectionType,
  collectionConnection,
  collectionEdge
}