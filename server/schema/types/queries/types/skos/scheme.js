/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../../database';

import {
  GraphQLFloat,
  GraphQLInt,
  GraphQLObjectType,
  GraphQLString,
} from 'graphql';

import {
  connectionDefinitions,
  globalIdField,
  connectionArgs,
  connectionFromArray
} from 'graphql-relay';

import {nodeInterface} from '../../definitions/nodeDefinitions';

import {conceptConnection} from './concept';
import {localizedLabelType} from './label';

import {Scheme} from '../../../../models';

import {skosElementInterface} from './interface/skosElementInterface';

const schemeType  = new GraphQLObjectType({
  name: 'Scheme',
  description: 'A skos:scheme in a thesaurus',
  fields: () => ({
    id: globalIdField('Scheme'),
    uri: {
      type: GraphQLString,
      description: 'URI',
      resolve: (scheme, _, context) => scheme.uri
    },
    title: {
      type: GraphQLString,
      description: 'Scheme title for current language',
      resolve: (scheme, _, context) => database.getEndpointConnection(context).getSchemeTitleForLang(scheme, context.lang)
    },
    titleLabel: {
      type: localizedLabelType,
      description: 'Scheme title label for current language',
      resolve: (scheme, _, context) => database.getEndpointConnection(context).getSchemeTitleForLang(scheme, context.lang, true)
    },
    description: {
      type: GraphQLString,
      description: 'Scheme description for current language',
      resolve: (scheme, _, context) => database.getEndpointConnection(context).getSchemeDescriptionForLang(scheme, context.lang)
    },
    descriptionLabel: {
      type: localizedLabelType,
      description: 'Scheme description label for current language',
      resolve: (scheme, _, context) => database.getEndpointConnection(context).getSchemeDescriptionForLang(scheme, context.lang, true)
    },
    color: {
      type: GraphQLString,
      description: 'Scheme color',
      resolve: (scheme) =>  scheme.color
    },
    creationDate: {
      type: GraphQLFloat,
      description: 'Creation date',
      resolve: (scheme) => scheme.creationDate
    },
    lastUpdate: {
      type: GraphQLFloat,
      description: 'Last update',
      resolve: (scheme) => scheme.lastUpdate
    },
    topConceptsCount: {
      type: GraphQLInt,
      description: 'Top concepts count',
      resolve: (scheme, _, context) => database.getEndpointConnection(context).getSchemeTopConceptsCount(scheme.id)
    },
    topConcepts: {
      type: conceptConnection,
      args: connectionArgs,
      description: 'Top concepts',
      resolve: (scheme, args, context) => database.getEndpointConnection(context)
        .getSchemeTopConcepts(scheme.id)
        .then(concepts => connectionFromArray(concepts, args))
    }
  }),
  interfaces: [nodeInterface, skosElementInterface],
  isTypeOf: (value) => value instanceof Scheme
});

const {
  connectionType: schemeConnection,
  edgeType: schemeEdge
  } = connectionDefinitions({name: 'SchemeConnection', nodeType: schemeType});

export {
  schemeType,
  schemeConnection,
  schemeEdge
};