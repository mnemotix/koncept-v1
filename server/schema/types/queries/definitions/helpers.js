
import {
  GraphQLString,
  GraphQLList
} from 'graphql';

var paginationArgs = {
  qs:{
    type: GraphQLString
  },
  sortBy:{
    type: GraphQLString
  },
  sortDirection:{
    type: GraphQLString,
    defaultValue: 'desc'
  },
  filters:{
    type: new GraphQLList(GraphQLString)
  }
};

export {
  paginationArgs
};