/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 22/03/2016
 */

import database from '../../../database';

import {
  fromGlobalId,
  nodeDefinitions,
} from 'graphql-relay';

import * as Models from '../../../models';
import * as Types from '../types';

var {nodeInterface, nodeField} = nodeDefinitions(
  (globalId, context) => {
    var {type, id} = fromGlobalId(globalId);

    if(!!Object.keys(Models).find(model => model == type)){
      return database.getEndpointConnection(context)[`get${type}`](id);
    }
  },
  (obj) => {
    let model = Object.keys(Models).find(model => obj instanceof Models[model]);

    if(model){
      return Types[`${model.charAt(0).toLowerCase() + model.slice(1)}Type`];
    }
  }
);

export {
  nodeInterface, nodeField
};