/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import database from '../../database';

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  cursorForObjectInConnection,
  offsetToCursor,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
} from 'graphql-relay';

import {
  thesauthequeType,
  thesaurusType,
  conceptType,
  conceptConnection,
  schemeType,
  collectionType,
} from './types';

import {nodeInterface, nodeField} from './definitions/nodeDefinitions';

import {
  userType
} from './types/user/user';

/**
 * This is the type that will be the root of our query,
 * and the entry point into our schema.
 */

export default new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    thesautheque: {
      type: thesauthequeType,
      resolve: (root, _, context) => database.getEndpointConnection(context).getThesauri()
    },
    thesaurus: {
      type: thesaurusType,
      args: {
        thesoId: {
          type: GraphQLID
        }
      },
      resolve: (root, {thesoId}, context) => {
        const {id: id} = fromGlobalId(thesoId);

        return database.getEndpointConnection(context).getThesaurus(id);
      }
    },
    collection: {
      type: collectionType,
      args: {
        collectionId: {
          type: GraphQLID
        }
      },
      resolve: (root, {collectionId}, context) => {
        const {id: collectionRealId} = fromGlobalId(collectionId);

        return database.getEndpointConnection(context).getCollection(collectionRealId);
      }
    },
    concept: {
      type: conceptType,
      args: {
        conceptId: {
          type: GraphQLID
        }
      },
      resolve: (root, {conceptId}, context) => {
        const {id: conceptRealId} = fromGlobalId(conceptId);

        return database.getEndpointConnection(context).getConcept(conceptRealId);
      }
    },
    scheme: {
      type: schemeType,
      args: {
        schemeId: {
          type: GraphQLID
        }
      },
      resolve: (root, {schemeId}, context) => {
        const {id: schemeRealId} = fromGlobalId(schemeId);

        return database.getEndpointConnection(context).getScheme(schemeRealId);
      }
    },
    user: {
      type: userType,
      resolve: (parentValue, args,  { user }) => {
        return user;
      }
    },
  })
});