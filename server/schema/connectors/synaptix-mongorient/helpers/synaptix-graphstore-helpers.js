/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */

import {createModelFromNode} from './model-helpers';

import {uriGenPublisher, graphStoreMongorientPublisher as graphStorePublisher} from 'synaptix-nodejs/lib';

import {
  cursorToOffset
} from 'graphql-relay';

export default class {
  /**
   * Constructor
   * @param context connection context
   */
  constructor(context){
    this.context = context;
  }

  /**
   * Generic method to create a node
   *
   * @param {String} type      Node type (Use defined constant TYPES.*)
   * @param {Object} props     Node properties
   * @param {Object} metas     Node metadata (Used to index documents and search it)
   * @param {Function} transformNodeCallback    Callback called to post process the returned node.
   * @param {String} creatorId    Id of the person issuing creation
   * @param {Array} creatorInEdgeTypes   Input edges names between creator and node.
   * @param {Array} creatorOutEdgeTypes   Output edges names between creator and node.
   *
   * @returns {Promise<Object, Error>}
   */
  async createNode(type, props, metas = {}, transformNodeCallback = null, creatorId = null, creatorInEdgeTypes = [], creatorOutEdgeTypes = []) {
    var createdNode;

    let uri  = await uriGenPublisher.createUri(type);
    let node = await graphStorePublisher.createNode(type, uri, props, metas, this.context);

    if (creatorId) {
      for (let creatorInEdgeType of creatorInEdgeTypes) {
        await graphStorePublisher.createEdge(node.getId(), creatorInEdgeType, creatorId, {}, this.context);
      }

      for (let creatorOutEdgeType of creatorOutEdgeTypes) {
        await graphStorePublisher.createEdge(creatorId, creatorOutEdgeType, node.getId(), {}, this.context);
      }
    }

    return transformNodeCallback ? transformNodeCallback(node) : node;
  }

  /**
   * Generic method to update a node
   *
   * @param type
   * @param id
   * @param props
   * @param transformNodeCallback
   *
   * @returns {Promise<Object, Error>}
   * @private
   */
  async updateNode(type, id, props, transformNodeCallback = null) {
    let node = await graphStorePublisher.updateNode(type, id, props, this.context);

    return transformNodeCallback ? transformNodeCallback(node) : node;
  }


  /**
   * Generic method to remove a node
   *
   * @param id
   * @returns {*}
   * @private
   */
  removeNode(id) {
    return graphStorePublisher.deleteNode(id, this.context);
  }

  /**
   * Generic method to get a reified node
   *
   * @param reificationId
   * @param reificationOutEdgeLabel
   * @param modelClass
   *
   * @returns {Promise<modelClass, Error>}
   */
  async getReificationModel(reificationId, reificationOutEdgeLabel, modelClass){
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(reificationId, reificationOutEdgeLabel);

    if (nodes.length > 0) {
      if(typeof modelClass == "object") {
        return createModelFromNode(modelClass[nodes[0].nodeType], nodes[0]);
      } else {
        return createModelFromNode(modelClass, nodes[0]);
      }
    }
  }

  /**
   * Generic method to get nodes from graph query
   *
   * @param query
   * @param args
   * @param modelsCallbacks
   * @returns {*}
   */
  async queryGraphNodes(query, modelsCallbacks, args = {}){
    if (args.first) {
        if (args.after) {
          query += `.range(${cursorToOffset(args.after)}, ${args.first + 1})`;
        } else {
          query += `.limit(${args.first + 1})`;
        }
    }

    let nodes = await graphStorePublisher.queryGraphNodes(query, this.context);

    return nodes
      .map(node => {
        if (typeof modelsCallbacks == 'function') {
          return modelsCallbacks(node.id);
        } else if (typeof modelsCallbacks == 'object' && modelsCallbacks[node.nodeType]) {
          return modelsCallbacks[node.nodeType](node.id);
        }
      });
  }
}