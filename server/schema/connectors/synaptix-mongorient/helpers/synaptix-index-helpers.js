/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */

import {createModelFromNode} from './model-helpers';
import {createNode} from 'synaptix-nodejs/lib/graphstore-mongorient/models';

import {indexPublisher} from 'synaptix-nodejs';

export default class {
  /**
   * Constructor
   *
   * @param context connection context
   */
  constructor(context){
    this.context = context;
  }

  /**
   * Make a fast search on index type
   *
   * @param types
   * @param modelClasses
   * @param args
   * @param extraFilters
   * @param fields
   * @returns {*}
   * @private
   */
  fastSearch(types, modelClasses, args = {}, extraFilters = [], fields = null) {
    let {qs, size, offset, sortBy, sortDirection, filters} = args;
    let filtersBooleans = '', sortings = {}, extra = {};

    if (!qs) {
      qs = '';
    }

    if (filters) {
      extraFilters = [...filters, ...extraFilters];
    }

    extraFilters.map(filter => filtersBooleans += `AND (${filter}) `);

    if (sortBy) {
      sortings = {[sortBy]: {order : sortDirection ? sortDirection : 'asc'}};
    }

    if (fields) {
      extra.fields = fields;
    }

    let query = {
      "query": {
        "filtered": {
          "query": {
            "query_string": {
              "query": `${qs || ''}* ${filtersBooleans}`
            }
          }
        }
      },
      "sort": sortings,
      ...extra
    };

    return indexPublisher.search(types, query, size, offset)
      .then(result => {
        return result.hits.map(hit => {
          let modelClass = typeof modelClasses == "object" ? modelClasses[hit._type] : modelClasses;

          return createModelFromNode(modelClass, createNode(types, hit._id, hit._source))
        })
      });
  }

  /**
   * Make a fast search on index type
   *
   * @param type
   * @param term
   * @param args
   * @param filters
   * @returns {*}
   * @private
   */
  _facetsOnTypeByTerm(type, term, args, filters = []) {
    const {qs} = args;
    let filtersBooleans = '', sortings = {}, extra = {};

    filters.map(filter => filtersBooleans += `AND ${filter} `);

    let query = {
      "query": {
        "filtered": {
          "query": {
            "query_string": {
              "query": `${qs || ''}* ${filtersBooleans}`
            }
          }
        }
      },
      "aggs": {
        "results" : {
          "terms" : { "field" : term }
        }
      }
    };

    return indexPublisher.search(type, query, 0, 1, true)
      .then(result => {
        return result.aggregations.results.buckets;
      });
  }
}