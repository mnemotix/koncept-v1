/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 10/11/2016
 */

/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import Driver from './driver';

export default class {
  /**
   * Init the listening of graphstore AMQP synchronisation messages.
   */
  init() { }

  /**
   * Returns a connector with context
   * @param {object} context
   *
   * @returns SynaptixConnector;
   */
  getDriver(context) {
    return new Driver(context ? context.user : null);
  }
}
