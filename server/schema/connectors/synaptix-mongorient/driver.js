/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import AbstractDriver from '../abstract-driver';

import nanoId from 'nano-id';

import {graphStoreMongorientPublisher as graphStorePublisher, indexPublisher, uriGenPublisher} from 'synaptix-nodejs';

import {createNode, createEdge, Node, Edge} from 'synaptix-nodejs/lib/graphstore-mongorient/models';

import {SynaptixGraphStoreHelpers, SynaptixIndexHelpers} from './helpers';

import {EDGES, TYPES} from './datamodel';

import {
  Concept, Collection, Thesaurus, Scheme, LocalizedLabel, Matcher, Rule
} from '../../models';

import {transformLabelToMatcher}  from '../../helpers/formats';

import naturalCompare from 'string-natural-compare';

export default class extends AbstractDriver {
  /** var {synaptixGraphStoreHelpers} Synaptix GraphStore helpers */
  synaptixGraphStoreHelpers;

  /** var {synaptixIndexHelpers} Synaptix Index helpers */
  synaptixIndexHelpers;

  constructor(context) {
    super(context);

    this.synaptixGraphStoreHelpers = new SynaptixGraphStoreHelpers(context);
    this.synaptixIndexHelpers = new SynaptixIndexHelpers(context);
  }

  /**
   * Init connector
   */
  init() {
  }

  /**
   * Parse a synaptix graphStore node to a concept model.
   *
   * @param {Node} node
   * @returns {Concept}
   */
  createConceptFromNode(node) {
    return new Concept(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a thesaurus model.
   *
   * @param {Node} node
   * @returns {Thesaurus}
   */
  createThesaurusFromNode(node) {
    return new Thesaurus(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a collection model.
   *
   * @param {Node} node
   * @returns {Collection}
   */
  createCollectionFromNode(node) {
    return new Collection(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a scheme model.
   *
   * @param {Node} node
   * @returns {Scheme}
   */
  createSchemeFromNode(node) {
    return new Scheme(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a matcher model.
   * @param node
   * @returns {Matcher}
   */
  createMatcher(matcher) {
    return new Matcher(
      matcher.id,
      '',
      {
        expression: matcher.expression
      }
    );
  }

  /**
   * Parse a synaptix graphStore node to a matcher model.
   * @param node
   * @returns {Matcher}
   */
  createLocalizedLabel(label) {
    return new LocalizedLabel(
      label.id,
      '',
      {
        value: label.value || label.label,
        lang: label.lang,
        matchers: label.matchers
      }
    );
  }

  /**
   * Parse a synaptix graphStore node to a matcher model.
   * @param node
   * @returns {Matcher}
   */
  createRuleFromNode(node) {
    return new Rule(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Add thesaurus
   *
   * @param props
   * @param props.title
   * @param props.description
   * @returns {Promise.<string,Error>} Returns a thesaurus ID or an error.
   */
  async addThesaurus(props) {
    const {title, description} = props;

    return this.synaptixGraphStoreHelpers.createNode(
      TYPES.THESAURUS,
      {title, description, creationDate: (new Date()).getTime()},
      {},
      this.createThesaurusFromNode.bind(this)
    );
  }

  /**
   * Remove a thesaurus
   *
   * @param id
   * @returns {Promise.<boolean,Error>} Returns true if thesaurus is deleted, false otherwise or an error.
   */
  async removeThesaurus(id) {
    let collections = await this.getCollectionsForThesaurus(id);
    let schemes = await this.getSchemesForThesaurus(id);
    let concepts = await this.getConceptsForThesaurus(id);

    await Promise.all(collections.map(collection => this.removeCollectionFromThesaurus(id, collection.id)));
    await Promise.all(schemes.map(scheme => this.removeSchemeFromThesaurus(id, scheme.id)));
    await Promise.all(concepts.map(concept => this.removeConcept(concept.id)));

    return graphStorePublisher.deleteNode(id);
  }

  /**
   * Update a thesaurus fields
   *
   * @param thesoId
   * @param props
   * @param props.title
   * @param props.description
   *
   * @returns {Promise.<Thesaurus,Error>} Returns a thesaurus object or an error.
   */
  async updateThesaurus(thesoId, props) {
    const {title, description} = props;

    let node = await graphStorePublisher.updateNode(TYPES.THESAURUS, thesoId, {
      title,
      description,
      lastUpdate: (new Date()).getTime()
    });

    return this.createThesaurusFromNode(node);
  }

  /**
   * Get list of thesaurus
   */
  async getThesauri() {
    let nodes = await graphStorePublisher.getNodesWithLabel(TYPES.THESAURUS);

    return nodes.map(node => this.createThesaurusFromNode(node));
  }

  /**
   * Get thesaurus
   * @param id
   */
  async getThesaurus(id) {
    let node = await graphStorePublisher.getNode(id);

    return this.createThesaurusFromNode(node);
  }

  /**
   * Get thesaurus title for lang.
   * @param thesaurus
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getThesaurusTitleForLang(thesaurus, lang, returnAsLocalizedLabel){
    return thesaurus.title;
  }

  /**
   * Get thesaurus description for lang.
   * @param thesaurus
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getThesaurusDescriptionForLang(thesaurus, lang, returnAsLocalizedLabel){
    return thesaurus.description
  }
  
  /**
   * Get thesaurus concepts
   * @param thesaurusId
   */
  async getThesaurusTopSchemes(thesaurusId, args) {
    let ps = await this.synaptixGraphStoreHelpers.queryGraphNodes(
      `g.V('${thesaurusId}')
          .in('${EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF}')
          .where(out('${EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT}').not(__.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')))
          .dedup()`,
      this.getScheme.bind(this),
      args
    );

    let schemes = await Promise.all(ps);

    return schemes.sort((a, b) => naturalCompare(a.title, b.title));
  }

  /**
   * Add concept as skos:narrower by creating a new concept
   *
   *
   * @param thesoId
   * @param parentId
   * @param prefLabels
   *
   * @returns {Promise.<Concept,Error>} A promise that returns a concept object or an Error.
   */
  async createConceptAndSetNarrowerRelation(thesoId, parentId, prefLabels) {
    let conceptUri = await uriGenPublisher.createUri(TYPES.CONCEPT);

    let scheme = await this.getConceptScheme(parentId);

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.CONCEPT, conceptUri, {
        prefLabels
      }/*, {
        thesaurus: thesoId,
        scheme: scheme.id
      }*/)
    }, [
      Edge.serializePartial(parentId, EDGES.CONCEPT.OUT.CONCEPT.NARROWER, '_:0'),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, scheme.id),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF, thesoId)
    ]);

    return this.createConceptFromNode(graph.nodes["_:0"]);
  }

  /**
   * Add concept as skos:narrower by creating a new concept
   *
   *
   * @param thesoId
   * @param schemeId
   * @param prefLabels
   *
   * @returns {Promise.<Concept,Error>} A promise that returns a concept object or an Error.
   */
  async createConcept(thesoId, schemeId, prefLabels) {
    let conceptUri = await uriGenPublisher.createUri(TYPES.CONCEPT);

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.CONCEPT, conceptUri, {
        prefLabels
      }/*, {
        thesaurus: thesoId,
        scheme: schemeId
      }*/)
    }, [
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, schemeId),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF, thesoId)
    ]);

    return this.createConceptFromNode(graph.nodes["_:0"]);
  }


  /**
   * Create top scheme to thesaurus
   *
   * @param thesoId
   * @param props
   * @param props.title
   * @param props.description
   * @param props.color
   * @param props.topConceptLabel
   * @returns {*}
   */
  async createTopSchemeToThesaurus(thesoId, props) {
    const {title: schemeTitle, description: schemeDescription, color: schemeColor, topConceptLabel, lang} = props;

    let conceptUri = await uriGenPublisher.createUri(TYPES.CONCEPT);
    let schemeUri = await uriGenPublisher.createUri(TYPES.SCHEME);

    let topConceptLabels = [{
      id: nanoId(5),
      value : topConceptLabel,
      lang,
      matchers: [
        {
          expression: transformLabelToMatcher(topConceptLabel),
          enabled: true
        }
      ]
    }];

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.SCHEME, schemeUri, {
        title: schemeTitle,
        description: schemeDescription,
        color: schemeColor
      }/*, {
        thesaurus: thesoId
      }*/)
    }, [
      Edge.serializePartial('_:0', EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF, thesoId)
    ]);


    let scheme = this.createSchemeFromNode(graph.nodes["_:0"]);

    await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.CONCEPT, conceptUri, {
        prefLabels: topConceptLabels
      }/*, {
        thesaurus: thesoId,
        scheme: scheme.id
      }*/)
    }, [
      Edge.serializePartial(scheme.id, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, '_:0'),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, scheme.id),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF, thesoId)
    ]);

    return scheme;
  }

  /**
   * Get all concepts of a thesaurus
   * @param thesoId
   */
  async getConceptsForThesaurus(thesoId) {
    let nodes = await graphStorePublisher.getNodesWithInRelationTo(thesoId, EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF);

    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Add a concept as skos:narrower of an othen concept
   *
   * @param parentId
   * @param narrowerId
   * @returns {Promise.<Edge,Error>} Return an egde or an Error
   */
  async addNarrowerRelation(parentId, narrowerId) {
    return graphStorePublisher.createEdge(parentId, EDGES.CONCEPT.OUT.CONCEPT.NARROWER, narrowerId);
  }

  /**
   * Remove concept as skos:narrower
   *
   * @param parentId
   * @param narrowerId
   * @returns {Promise.<boolean,Error>} Return True if deleted or an Error
   */
  async removeNarrowerRelation(parentId, narrowerId) {
    return graphStorePublisher.deleteEdges(parentId, EDGES.CONCEPT.OUT.CONCEPT.NARROWER, narrowerId);
  }

  /**
   * Add concept as skos:related
   *
   * @param fromId
   * @param toId
   *
   * @returns {Promise.<Edge,Error>} Return an egde or an Error
   */
  async addRelatedRelation(fromId, toId) {
    return graphStorePublisher.createEdge(fromId, EDGES.CONCEPT.OUT.CONCEPT.RELATED, toId);
  }

  /**
   * Remove concept from skos:related relation
   *
   * @param fromId
   * @param toId
   *
   * @returns {Promise.<boolean,Error>} Return True if deleted or an Error
   */
  async removeRelatedRelation(fromId, toId) {
    return graphStorePublisher.deleteEdges(fromId, EDGES.CONCEPT.OUT.CONCEPT.RELATED, toId);
  }

  /**
   * Get concepts
   */
  async getConcepts(args){
    const {qs} = args;

    if (qs === "") {
      return [];
    }

    return this.synaptixIndexHelpers.fastSearch(TYPES.CONCEPT, Concept, args);
  }

  /**
   * Get concept
   *
   * @param id
   *
   * @returns {Promise.<Concept,Error>} Returns a concept object or an Error.
   */
  async getConcept(id) {
    let node = await graphStorePublisher.getNode(id);

    return this.createConceptFromNode(node);
  }

  /**
   * Get concept scheme
   *
   * @param id
   *
   * @returns {Promise.<Scheme,Error>} Returns a scheme object or an Error.
   */
  async getConceptScheme(id) {
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(id, EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME);

    if (nodes.length > 0) {
      return this.createSchemeFromNode(nodes[0]);
    }
  }

  /**
   * Get concept pref labels
   *
   * @param conceptId
   *
   * @returns {Promise.<Label[],Error>} Returns a scheme object or an Error.
   */
  async getConceptPrefLabels(conceptId) {
    let concept = await this.getConcept(conceptId);

    return (concept.prefLabels || []).map(label => this.createLocalizedLabel(label));

    return [];
  }

  /**
   * Get concept alt labels
   *
   * @param conceptId
   *
   * @returns {Promise.<Label[],Error>} Returns a scheme object or an Error.
   */
  async getConceptAltLabels(conceptId) {
    let concept = await this.getConcept(conceptId);

    return (concept.altLabels || []).map(label => this.createLocalizedLabel(label));
  }

  /**
   * Get concept hidden labels
   *
   * @param conceptId
   *
   * @returns {Promise.<Label[],Error>} Returns a scheme object or an Error.
   */
  async getConceptHiddenLabels(conceptId) {
    let concept = await this.getConcept(conceptId);

    return (concept.hiddenLabels || []).map(label => this.createLocalizedLabel(label));
  }

  /**
   * Search for concepts
   *
   * @param thesoId
   * @param query
   * @param size
   * @param offset
   * @returns {*}
   */
  async searchConcept(thesoId, query, size, offset) {
    if (query == "") {
      return [];
    }

    let result = await this.indexPublisher.search(TYPES.CONCEPT, {
      "query": {
        "bool": {
          "must": [
            {
              "query_string": {
                "default_field": "_all",
                "query": `${query}*`
              }
            }
          ],
          "filter": [
            {
              "nested": {
                "path": "_meta",
                "filter": {
                  "term": {"_meta.thesaurus": thesoId}
                }
              }
            }
          ]
        }
      }
    }, size, offset);

    return result.hits.map(hit =>
      this.createConceptFromNode(createNode(TYPES.CONCEPT, hit._id, hit._source))
    )
  }

  /**
   * Get narrowers concepts
   *
   * @param parentId
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getNarrowers(parentId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${parentId}')
       .coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}')
       )
    `);

    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Get narrowers concepts count
   *
   * @param parentId
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getNarrowersCount(parentId){
    let count = await graphStorePublisher.queryGraph(`
      g.V('${parentId}')
       .coalesce(
         __.in('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}'),
         __.out('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}')
       )
       .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get related concepts
   *
   * @param relatedId
   *
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getRelated(relatedId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${relatedId}')
        .coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}')
        )
        .dedup()`
    );


    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Get related concepts count
   *
   * @param relatedId
   *
   * @returns {Promise.<Number,Error>} Returns a list of concept objects or an Error.
   */
  async getRelatedCount(relatedId) {
    let count = await graphStorePublisher.queryGraph(`
      g.V('${relatedId}')
        .coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}')
        )
        .dedup()
        .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get siblings concepts
   *
   * @param conceptId
   * @param relationType
   *
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getSiblings(conceptId, relationType, args) {
    let relationLabel;

    switch (relationType) {
      case 'narrowers':
        relationLabel = `.coalesce(
            __.out('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
            __.in('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')
          )`;
        break;
      case 'related':
        relationLabel = `.out('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}')`;
        break;
    }

    return await this.synaptixGraphStoreHelpers.queryGraphNodes(
      `g.V('${conceptId}')
          .coalesce(
            __.in('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
            __.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')
          )
          ${relationLabel}
          .where(
             __.not(hasId('${conceptId}'))
          )
          .dedup()`,
      this.getConcept.bind(this),
      args
    );
  }


  /**
   * Get broader of a concept
   *
   * @param conceptId
   *
   * @returns {Promise.<Concept,Error>} Returns a concept object or an Error.
   */
  async getBroader(conceptId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${conceptId}').coalesce(
        __.in('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
        __.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')
      )
    `);

    if (nodes.length > 0) {
      return this.createConceptFromNode(nodes[0]);
    }
  }

  /**
   * Get collections for concept
   *
   * @param conceptId
   *
   * @returns {Promise.<Collection[],Error>} Returns a list of collection objects or an Error.
   */
  getCollectionsForConcept(conceptId) {
    return graphStorePublisher.getNodesWithOutRelationTo(conceptId, EDGES.CONCEPT.OUT.COLLECTION.MEMBER)
      .then(nodes => nodes.map(node => this.createCollectionFromNode(node)));
  }

  /**
   * Remove a concept
   *
   * @param id
   *
   * @returns {Promise.<boolean,Error>} Return True if deleted or an Error
   */
  async removeConcept(id) {
    let taggings = await graphStorePublisher.getNodesWithInRelationTo(id, EDGES.TAGGING.OUT.CONCEPT.TAGGING_SUBJECT);

    await Promise.all(taggings.map(tagging => graphStorePublisher.deleteNode(tagging.getId())));

    return graphStorePublisher.deleteNode(id);
  }

  /**
   * Updates a concept
   *
   * @param id
   * @param {object} properties
   *
   * @returns {Promise.<Concept,Error>} Returns a concept object or an Error.
   */
  updateConcept(id, properties) {
    return graphStorePublisher.updateNode(TYPES.CONCEPT, id, properties)
      .then(node => this.createConceptFromNode(node));
  }

  /**
   * Get thesaurus skos:collections
   *
   * @param thesoId Thesaurus Id
   */
  getCollectionsForThesaurus(thesoId) {
    return graphStorePublisher.getNodesWithInRelationTo(thesoId, EDGES.COLLECTION.OUT.THESAURUS.COLLECTION_OF)
      .then(nodes => nodes.map(node => this.createCollectionFromNode(node)));
  }

  /**
   * Get thesaurus skos:collection
   *
   * @param collectionId
   */
  getCollection(collectionId) {
    return graphStorePublisher.getNode(collectionId)
      .then(node => this.createCollectionFromNode(node));
  }

  /**
   * Get collection title for lang.
   * @param collection
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getCollectionTitleForLang(collection, lang, returnAsLocalizedLabel){
    return collection.title;
  }

  /**
   * Get collection description for lang.
   * @param collection
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getCollectionDescriptionForLang(collection, lang, returnAsLocalizedLabel){
    return collection.description;
  }
  
  /**
   * Add a new skos:collection to a thesaurus
   *
   * @param thesoId
   * @param props
   * @param props.title
   * @param props.description
   */
  addCollectionToThesaurus(thesoId, props) {
    const {title, description} = props;

    return uriGenPublisher.createUri(TYPES.COLLECTION)
      .then(uri => graphStorePublisher.createGraph({
        "_:0": Node.serializePartial(TYPES.COLLECTION, uri, {
          title,
          description
        }/*, {
          thesaurus: thesoId
        }*/)
      }, [
        Edge.serializePartial('_:0', EDGES.COLLECTION.OUT.THESAURUS.COLLECTION_OF, thesoId)
      ]))
      .then(graph => this.createCollectionFromNode(graph.nodes["_:0"]));
  }

  /**
   * Update thesaurus skos:collection
   *
   * @param collectionId
   * @param props
   * @param props.title
   * @param props.description
   */
  updateCollection(collectionId, props) {
    consr
    var properties = {};

    if (title) {
      properties.title = title;
    }

    if (description) {
      properties.description = description;
    }

    return graphStorePublisher.updateNode(TYPES.COLLECTION, collectionId, properties)
      .then(node => this.createCollectionFromNode(node));
  }

  /**
   * Remove a skos:collection from a thesaurus
   *
   * @param thesoId
   * @param collectionId
   */
  removeCollectionFromThesaurus(thesoId, collectionId) {
    return graphStorePublisher.deleteNode(collectionId);
  }

  /**
   * Get thesaurus skos:collection concepts
   *
   * @param collectionId
   */
  getConceptsOfCollection(collectionId) {
    return graphStorePublisher.getNodesWithInRelationTo(collectionId, EDGES.CONCEPT.OUT.COLLECTION.MEMBER)
      .then(nodes => {
        return nodes.map(node => this.createConceptFromNode(node))
      });
  }

  /**
   * Add a concept to a thesaurus skos:collection
   *
   * @param collectionId
   * @param conceptId
   */
  addConceptToCollection(collectionId, conceptId) {
    return graphStorePublisher.createEdge(conceptId, EDGES.CONCEPT.OUT.COLLECTION.MEMBER, collectionId);
  }

  /**
   * Remove a concept from a thesaurus skos:collection
   *
   * @param collectionId
   * @param conceptId
   */
  removeConceptFromCollection(collectionId, conceptId) {
    return graphStorePublisher.deleteEdges(conceptId, EDGES.CONCEPT.OUT.COLLECTION.MEMBER, collectionId);
  }

  /**
   * Get schemes of a thesaurus
   * @param thesoId
   */
  getSchemesForThesaurus(thesoId) {
    return graphStorePublisher.getNodesWithInRelationTo(thesoId, EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF)
      .then(nodes => nodes.map(node => this.createSchemeFromNode(node)));
  }

  /**
   * Get scheme
   * @param schemeId
   */
  getScheme(schemeId) {
    return graphStorePublisher.getNode(schemeId)
      .then(node => this.createSchemeFromNode(node));
  }

  /**
   * Get scheme title for lang.
   * @param scheme
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getSchemeTitleForLang(scheme, lang, returnAsLocalizedLabel){
    return scheme.title;
  }

  /**
   * Get scheme description for lang.
   * @param scheme
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getSchemeDescriptionForLang(scheme, lang, returnAsLocalizedLabel){
    return scheme.description;
  }

  /**
   * Update scheme properties
   *
   * @param schemeId
   * @param props
   * @param props.title
   * @param props.description
   */
  async updateScheme(schemeId, props) {
    const {title, description, color} = props;
    let properties = {};

    if (title) {
      properties.title = title;
    }

    if (description) {
      properties.description = description;
    }

    if (color) {
      properties.color = color;
    }

    let node = await graphStorePublisher.updateNode(TYPES.SCHEME, schemeId, properties);

    return this.createSchemeFromNode(node);
  }

  /**
   * Add scheme to thesaurus
   *
   * @param thesoId
   * @param title
   * @param description
   */
  addSchemeToThesaurus(thesoId, topConceptId, title, description) {
    return uriGenPublisher.createUri(TYPES.SCHEME)
      .then(uri => graphStorePublisher.createGraph({
        "_:0": Node.serializePartial(TYPES.SCHEME, uri, {
          title,
          description
        }/*, {
          thesaurus: thesoId
        }*/)
      }, [
        Edge.serializePartial('_:0', EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF, thesoId),
        Edge.serializePartial('_:0', EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, topConceptId)
      ]))
      .then(graph => this.createSchemeFromNode(graph.nodes["_:0"]));
  }

  /**
   * Remove scheme
   *
   * @param schemeId
   */
  removeSchemeFromThesaurus(thesoId, schemeId) {
    return graphStorePublisher.deleteNode(schemeId);
  }

  /**
   * Get scheme top concepts
   *
   * @param schemeId
   */
  async getSchemeTopConcepts(schemeId) {
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT);
    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Get scheme top concepts count
   *
   * @param schemeId
   */
  async getSchemeTopConceptsCount(schemeId) {
    let count = await graphStorePublisher.queryGraph(`
      g.V('${schemeId}').outE('${EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT}').count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get scheme given a top concept id
   * @param conceptId
   */
  getSchemeForTopConcept(conceptId) {
    return graphStorePublisher.getNodesWithInRelationTo(conceptId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT)
      .then(nodes => nodes.length > 0 ? this.createSchemeFromNode(nodes[0]) : null);
  }

  /**
   * Change top concept of a scheme
   *
   * @param schemeId
   * @param conceptId
   */
  updateSchemeTopConcept(schemeId, conceptId) {
    return graphStorePublisher.deleteEdges(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, conceptId)
      .then(() => graphStorePublisher.createEdge(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, conceptId));
  }

  ///
  // LABELS
  ///

  /**
   * Get a localized label
   * @param labelId
   *
   * @returns {Promise.<LocalizedLabel,Error>} Returns a LocalizedLabel object or an Error.
   */
  async getLocalizedLabel(labelId) {
    let concept = await this.getConceptForLabelId(labelId);

    let labels = [].concat(
      concept.getPropertyValue('prefLabels') || [],
      concept.getPropertyValue('altLabels') || [],
      concept.getPropertyValue('hiddenLabels') || []
    );

    let label = labels.find(label => label.id == labelId);

    return this.createLocalizedLabel(label);
  }

  /**
   * Get a concept for a labelId
   *
   * @param labelId
   * @returns {Promise.<T>|*}
   */
  getConceptForLabelId(labelId) {
    return graphStorePublisher.getNodesBy({
        "selector": {
          "nodeType": TYPES.CONCEPT,
          "$or": [{
            "_source.prefLabels": {
              "$elemMatch": {
                id: labelId
              }
            }
          }, {
            "_source.altLabels": {
              "$elemMatch": {
                id: labelId
              }
            }
          }, {
            "_source.hiddenLabels": {
              "$elemMatch": {
                id: labelId
              }
            }
          }]
        }
      })
      .then(nodes => {
        if (nodes.length > 0) {
          return nodes[0];
        }
      });
  }

  /**
   * Add a localized label to a concept
   * @param conceptId
   * @param labelType
   * @param value
   * @param lang
   * @returns {Promise.<LocalizedLabel,Error>} Returns a LocalizedLabel object or an Error.
   */
  async addLabelToConcept(conceptId, labelType, value, lang) {
    let concept = await this.getConcept(conceptId);

    let storedLabels = concept[labelType],
      newLabel = {
        id: nanoId(5),
        value,
        lang,
        matchers: [
          {
            id: nanoId(5),
            expression: transformLabelToMatcher(value),
            enabled: true
          }
        ]
      };

    if (!storedLabels) {
      storedLabels = [];
    }

    storedLabels.push(newLabel);

    await this.updateConcept(conceptId, {
      [labelType]: storedLabels
    });

    return newLabel
  }

  /**
   * Update a localized label
   *
   * @param conceptId
   * @param labelId
   * @param labelType
   * @param value
   * @param lang
   *
   * @returns {Promise.<LocalizedLabel,Error>} Returns a LocalizedLabel object or an Error.
   */
  async updateLabelForConcept(conceptId, labelType, labelId, value, lang) {
    let concept = await this.getConcept(conceptId);

    let storedLabels = concept[labelType],
      labelIndex = storedLabels.findIndex(storedLabel => storedLabel.id == labelId),
      updatedLabel = {
        ...storedLabels[labelIndex],
        value,
        lang,
        matchers: [
          {
            id: nanoId(5),
            expression: transformLabelToMatcher(value),
            enabled: true
          }
        ]
      };

    storedLabels[labelIndex] = updatedLabel;

    await this.updateConcept(conceptId, {
      [labelType]: storedLabels
    });

    return updatedLabel;
  }

  /**
   * Remove a localized label
   *
   * @param conceptId
   * @param labelType
   * @param labelId
   *
   * @returns {Promise.<Boolean,Error>} Returns a boolean or an Error.
   */
  async removeLabelForConcept(conceptId, labelType, labelId) {
    let concept = await this.getConcept(conceptId);

    let storedLabels = concept[labelType],
      labelIndex = storedLabels.findIndex(storedLabel => storedLabel.id == labelId);

    storedLabels.splice(labelIndex, 1);

    return this.updateConcept(conceptId, {
      [labelType]: storedLabels
    });
  }

  /**
   * Get matcher
   *
   * @param matcherId
   */
  async getMatcher(matcherId) {
    return graphStorePublisher.getNodesBy({
        "selector": {
          "nodeType": TYPES.CONCEPT,
          "$or": [{
            "_source.prefLabels": {
              "$elemMatch": {
                matchers: {
                  "$elemMatch": {
                    id: matcherId
                  }
                }
              }
            }
          }, {
            "_source.altLabels": {
              "$elemMatch": {
                matchers: {
                  "$elemMatch": {
                    id: matcherId
                  }
                }
              }
            }
          }, {
            "_source.hiddenLabels": {
              "$elemMatch": {
                matchers: {
                  "$elemMatch": {
                    id: matcherId
                  }
                }
              }
            }
          }]
        }
      })
      .then(nodes => {
        if (nodes.length > 0) {
          let matchers = [];

          for (let labelType of ['prefLabels', 'altLabels', 'hiddenLabels']) {
            if (nodes[0].getPropertyValue(labelType)) {
              nodes[0].getPropertyValue(labelType).map(label => {
                matchers = matchers.concat(label.matchers || [])
              });
            }
          }

          let matcher = matchers.find(matcher => matcher.id == matcherId);

          return this.createMatcher(matcher);
        }
      });
  }

  /**
   * Get matchers for label
   *
   * @param labelId
   * @param args
   */
  async getLabelMatchers(labelId, args) {
    let label = await this.getLocalizedLabel(labelId);

    return (label.matchers || []).map(matcher => this.createMatcher(matcher));
  }

  /**
   * Add a matcher to a label
   *
   * @param labelId
   * @param expression
   */
  async addMatcherToLabel(labelId, expression) {
    let concept = await this.getConceptForLabelId(labelId);

    let newMatcher = {
      id: nanoId(5),
      expression,
      enabled: true
    };

    for (let labelType of ['prefLabels', 'altLabels', 'hiddenLabels']) {
      if (concept.getPropertyValue(labelType)) {
        let labels = concept.getPropertyValue(labelType),
          labelIndex = (labels || []).findIndex(label => label.id == labelId);

        if (labelIndex > -1) {
          labels[labelIndex].matchers = (labels[labelIndex].matchers || []).concat([newMatcher]);

          await this.updateConcept(concept.id, {[labelType]: labels});
          return this.createMatcher(newMatcher);
        }
      }
    }
  }

  /**
   * Add a matcher to a label
   *
   * @param labelId
   * @param matcherId
   * @param expression
   */
  async updateMatcherForLabel(labelId, matcherId, expression) {
    let concept = await this.getConceptForLabelId(labelId),
      matcher = await this.getMatcher(matcherId);

    let updatedMatcher = {
      ...matcher,
      expression
    };

    for (let labelType of ['prefLabels', 'altLabels', 'hiddenLabels']) {
      if (concept.getPropertyValue(labelType)) {
        let labels = concept.getPropertyValue(labelType),
          labelIndex = (labels || []).findIndex(label => label.id == labelId);

        if (labelIndex > -1) {
          let matcherIndex = labels[labelIndex].matchers.findIndex(matcher => matcher.id == matcherId);

          labels[labelIndex].matchers[matcherIndex] = updatedMatcher;

          await this.updateConcept(concept.id, {[labelType]: labels});
          return this.createMatcher(updatedMatcher);
        }
      }
    }
  }

  /**
   * Remove a matcher for a label
   *
   * @param labelId
   * @param matcherId
   * @param expression
   */
  async removeMatcherForLabel(labelId, matcherId) {
    let concept = await this.getConceptForLabelId(labelId);

    for (let labelType of ['prefLabels', 'altLabels', 'hiddenLabels']) {
      if (concept.getPropertyValue(labelType)) {
        let labels = concept.getPropertyValue(labelType),
          labelIndex = (labels || []).findIndex(label => label.id == labelId);

        if (labelIndex > -1) {
          let matcherIndex = labels[labelIndex].matchers.findIndex(matcher => matcher.id == matcherId);

          labels[labelIndex].matchers.splice(matcherIndex, 1);

          await this.updateConcept(concept.id, {[labelType]: labels});
          return;
        }
      }
    }
  }
}


