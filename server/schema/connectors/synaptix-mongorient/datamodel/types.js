/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */

export default {
  THESAURUS: 'Thesaurus',
  CONCEPT: 'Concept',
  SCHEME: 'Scheme',
  COLLECTION: 'Collection',
  LABEL: 'LocalizedLabel',
  MATCHER: 'Matcher',
  RULE: 'Rule'
};