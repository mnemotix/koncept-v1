import SynaptixConnector from '../synaptix-connector';
import jsondiffpatch from 'jsondiffpatch';

import {Thesaurus, Collection, Concept, Scheme, Matcher, Label} from '../../models';

import {Edge, Node} from 'synaptix-nodejs/lib/graphstore/models';


describe('SynaptixConnector', () => {
  var connector = new SynaptixConnector();

  var createdThesoId, rootConcept, concepts = [];

  it('creates a thesaurus', (done) => {
    connector.addThesaurus('foo', 'lorem ipsum')
      .then(thesaurus => {
        createdThesoId = thesaurus.id;
        expect(thesaurus instanceof Thesaurus).toBeTruthy();
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      })
  });

  it('gets a thesaurus', (done) => {
    connector.getThesaurus(createdThesoId)
      /**
       * @param {Thesaurus} thesaurus
       */
      .then(thesaurus => {
        expect(thesaurus instanceof Thesaurus).toBeTruthy();
        expect(thesaurus.id).toEqual(createdThesoId);
        expect(thesaurus.name).toEqual('foo');
        expect(thesaurus.description).toEqual('lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      })
  });

  it('gets a thesaurus top concept', (done) => {
    connector.getThesaurusRootConcept(createdThesoId)
      .then(concept => {
        rootConcept = concept;
        expect(concept instanceof Concept).toBeTruthy();
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('updates a thesaurus', (done) => {
    connector.updateThesaurus(createdThesoId, 'foo2', 'lorem ipsum2')
      .then(thesaurus => {
        expect(thesaurus instanceof Thesaurus).toBeTruthy();
        expect(thesaurus.id).toEqual(createdThesoId);
        expect(thesaurus.name).toEqual('foo2');
        expect(thesaurus.description).toEqual('lorem ipsum2');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('creates concepts and relations', (done) => {
    var ps = ['concept 1', 'concept 2', 'concept 3']
      .map(label => connector.createConceptAndSetNarrowerRelation(rootConcept.id, label, 'fr')
        .then(concept => {
          expect(concept instanceof Concept).toBeTruthy();
          concepts.push(concept);
        })
        .catch(err => {
          fail(' A ' + JSON.stringify(err));
          done();
        })
      );

    Promise.all(ps)
      .then(() => {
        return connector.addRelatedRelation(concepts[0].id, concepts[1].id)
      })
      .then((edge) => {
        expect(edge instanceof Edge).toBeTruthy();
        expect(edge.getSubj()).toEqual(concepts[0].id);
        expect(edge.getObj()).toEqual(concepts[1].id);
        return connector.addRelatedRelation(concepts[0].id, concepts[2].id);
      })
      .then(done)
      .catch(err => {
        fail('B ' + JSON.stringify(err));
        done();
      });
  });

  it('updates a concept', (done) => {
    connector.updateConcept(concepts[0].id, {historyNote: 'test'})
      .then(concept => {
        expect(concept instanceof Concept).toBeTruthy();
        expect(concept.historyNote).toEqual('test');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('gets a concept', (done) => {
    connector.getConcept(concepts[0].id)
      .then(concept => {
        expect(concept instanceof Concept).toBeTruthy();
        expect(concept.id).toEqual(concepts[0].id);
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('gets narrowers relations', (done) => {
    connector.getNarrowers(rootConcept.id)
      .then(concepts => {
        expect(concepts.length).toEqual(3);
        concepts.map(concept => expect(concept instanceof Concept).toBeTruthy());
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('gets related relations', (done) => {
    connector.getRelated(concepts[0].id)
      .then(concepts => {
        expect(concepts.length).toEqual(2);
        concepts.map(concept => expect(concept instanceof Concept).toBeTruthy());
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  var collections = [];

  it('add a collection to thesaurus', (done) => {
    connector.addCollectionToThesaurus(createdThesoId, 'collection Foo', 'Lorem ipsum')
      .then((collection) => {
        collections.push(collection);

        expect(collection instanceof Collection).toBeTruthy();
        expect(collection.label).toEqual('collection Foo');
        expect(collection.description).toEqual('Lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get collections of a thesaurus', (done) => {
    connector.getCollectionsForThesaurus(createdThesoId)
      .then(collections => {
        expect(collections.length).toEqual(1);
        collections.map(collection => expect(collection instanceof Collection).toBeTruthy());

        expect(collections[0].label).toEqual('collection Foo');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get a collection', (done) => {
    connector.getCollection(collections[0].id)
      .then((collection) => {
        expect(collection instanceof Collection).toBeTruthy();
        expect(collection.label).toEqual('collection Foo');
        expect(collection.description).toEqual('Lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('updates a collection', (done) => {
    connector.updateCollection(collections[0].id, 'collection Foo renamed')
      .then((collection) => {
        expect(collection instanceof Collection).toBeTruthy();
        expect(collection.label).toEqual('collection Foo renamed');
        expect(collection.description).toEqual('Lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('adds concept to a collection', (done) => {
    connector.addConceptToCollection(collections[0].id, concepts[0].id)
      .then((edge) => {
        expect(edge instanceof Edge).toBeTruthy();
        expect(edge.getObj()).toEqual(collections[0].id);
        expect(edge.getSubj()).toEqual(concepts[0].id);
        expect(edge.getPred()).toEqual('MEMBER');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get concepts of a collection', (done) => {
    connector.getConceptsOfCollection(collections[0].id)
      .then((collConcepts) => {
        expect(collConcepts.length).toEqual(1);
        expect(collConcepts[0].id).toEqual(concepts[0].id);
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get collections of a concept', (done) => {
    connector.getCollectionsForConcept(concepts[0].id)
      .then((conCollection) => {
        expect(conCollection.length).toEqual(1);
        expect(conCollection[0].id).toEqual(collections[0].id);
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('removes concept from a collection', (done) => {
    connector.removeConceptFromCollection(collections[0].id, concepts[0].id)
      .then((success) => {
        expect(success).toBeTruthy();
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('removes a collection', (done) => {
    connector.removeCollectionFromThesaurus(null, collections[0].id)
      .then((success) => {
        expect(success).toBeTruthy();
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  var schemes = [];

  it('add a scheme to thesaurus', (done) => {
    connector.addSchemeToThesaurus(createdThesoId, concepts[0].id, 'scheme Foo', 'Lorem ipsum')
      .then((scheme) => {
        schemes.push(scheme);

        expect(scheme instanceof Scheme).toBeTruthy();
        expect(scheme.label).toEqual('scheme Foo');
        expect(scheme.description).toEqual('Lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get schemes of a thesaurus', (done) => {
    connector.getSchemesForThesaurus(createdThesoId)
      .then(schemes => {
        expect(schemes.length).toEqual(1);
        schemes.map(scheme => expect(scheme instanceof Scheme).toBeTruthy());

        expect(schemes[0].label).toEqual('scheme Foo');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get a scheme', (done) => {
    connector.getScheme(schemes[0].id)
      .then((scheme) => {
        expect(scheme instanceof Scheme).toBeTruthy();
        expect(scheme.label).toEqual('scheme Foo');
        expect(scheme.description).toEqual('Lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('updates a scheme', (done) => {
    connector.updateScheme(schemes[0].id, 'scheme Foo renamed')
      .then((scheme) => {
        expect(scheme instanceof Scheme).toBeTruthy();
        expect(scheme.label).toEqual('scheme Foo renamed');
        expect(scheme.description).toEqual('Lorem ipsum');
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get top concept of a scheme', (done) => {
    connector.getSchemeTopConcept(schemes[0].id)
      .then((concept) => {
        expect(concept instanceof Concept).toBeTruthy();
        expect(concept.id).toEqual(concepts[0].id);
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('get scheme for top concept', (done) => {
    connector.getSchemeForTopConcept(concepts[0].id)
      .then((scheme) => {
        expect(scheme instanceof Scheme).toBeTruthy();
        expect(scheme.id).toEqual(schemes[0].id);
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('updates schemes top concept', (done) => {
    connector.updateSchemeTopConcept(schemes[0].id, concepts[1].id)
      .then((edge) => {
        expect(edge instanceof Edge).toBeTruthy();
        expect(edge.getSubj()).toEqual(concepts[1].id);
        expect(edge.getPred()).toEqual('TOP_CONCEPT_OF');
        expect(edge.getObj()).toEqual(schemes[0].id);
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });

  it('removes a scheme', (done) => {
    connector.removeSchemeFromThesaurus(null, schemes[0].id)
      .then((success) => {
        expect(success).toBeTruthy();
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      });
  });
  


  it('deletes nodes', (done) => {
    Promise.all(
      concepts.map(concept => connector.removeConcept(concept.id)
        .then((success) => {
          expect(success).toBeTruthy();
        })
        .catch(err => {
          fail(JSON.stringify(err));
          done();
        })
      )
    ).then(done);
  });

  it('deletes a thesaurus', (done) => {
    connector.removeThesaurus(createdThesoId)
      .then(success => {
        expect(success).toBeTruthy();
        done();
      })
      .catch(err => {
        fail(JSON.stringify(err));
        done();
      })
  });

  it('fails to retrieve deleted thesaurus', (done) => {
    connector.getThesaurus(createdThesoId)
      .then(() => {
        fail('Not expected to exists...');
        done();
      })
      .catch(err => {
        expect(err).toBeTruthy();
        done();
      });
  });
});




