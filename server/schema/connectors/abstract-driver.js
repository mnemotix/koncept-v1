/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

export default class {
  /**
   * GraphQL context object
   */
  context;

  /**
   * Constructor
   *
   * @param context GraphQL context object
   */
  constructor(context){
    this.context = context;

    let abstractMethods = [
      //Thesaurus
      'getThesauri',
      'getThesaurus',
      'addThesaurus',
      'updateThesaurus',
      'removeThesaurus',
      'getThesaurusTopSchemes',
      //Concept
      'getConcepts',
      'getConcept',
      'getConceptPrefLabels',
      'getConceptAltLabels',
      'getConceptHiddenLabels',
      'getNarrowers',
      'getNarrowersCount',
      'getRelated',
      'getRelatedCount',
      'getSiblings',
      'getBroader',
      'getCollectionsForConcept',
      'removeConcept',
      'updateConcept',
      'createConceptAndSetNarrowerRelation',
      'addNarrowerRelation',
      'removeNarrowerRelation',
      'addRelatedRelation',
      'removeRelatedRelation',
      'searchConcept',
      //Label
      'getLocalizedLabel',
      'addLabelToConcept',
      'updateLabelForConcept',
      'removeLabelForConcept',
      //Collections
      'getCollectionsForThesaurus',
      'getCollection',
      'updateCollection',
      'addCollectionToThesaurus',
      'removeCollectionFromThesaurus',
      'getConceptsOfCollection',
      'addConceptToCollection',
      'removeConceptFromCollection',
      //Schemes
      'getSchemesForThesaurus',
      'getScheme',
      'updateScheme',
      'addSchemeToThesaurus',
      'removeSchemeFromThesaurus',
      'getSchemeTopConcepts',
      'getSchemeTopConceptsCount',
      'getSchemeForTopConcept',
      'updateSchemeTopConcept'
    ];

    let missingMethods = abstractMethods.filter((abstractMethod) => this[abstractMethod] === undefined);

    if (missingMethods.length > 0) {
      throw new TypeError(`Following methods must be instantiated in ${this.constructor.name} : ${missingMethods}`);
    }
  }
}