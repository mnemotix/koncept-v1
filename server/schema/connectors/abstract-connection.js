/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

export default class {
  /**
   * Initialisation method called once on application start.
   */
  init() {
    throw "You must implement a `init` method";
  }

  /**
   * Returns a connection driver with context
   * @param {object} GraphQL context object
   *
   * @returns SynaptixConnector;
   */
  getDriver(context) {
    throw "You must implement a `getDriver` method";
  }
}