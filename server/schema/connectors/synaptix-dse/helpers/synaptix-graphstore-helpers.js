/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */

import {createModelFromNode} from './model-helpers';

import {TYPES, EDGES} from '../datamodel';

import {uriGenPublisher, graphStoreDsePublisher as graphStorePublisher,
  createDseNode as createNode, DseNode as Node, DseEdge as Edge} from 'synaptix-nodejs';

import {LocalizedLabel} from '../../../models';

import {transformLabelToMatcher}  from '../../../helpers/formats';

import winston from 'winston-color';
import uuid from 'uuid';

import {
  cursorToOffset
} from 'graphql-relay';

export default class {
  /**
   * Constructor
   * @param context connection context
   */
  constructor(context){
    this.context = context;
  }

  /**
   * Generic method to create a node
   *
   * @param {String} type      Node type (Use defined constant TYPES.*)
   * @param {Object} props     Node properties
   * @param {Object} metas     Node metadata (Used to index documents and search it)
   * @param {Function} transformNodeCallback    Callback called to post process the returned node.
   * @param {String} creatorId    Id of the person issuing creation
   * @param {Array} creatorInEdgeTypes   Input edges names between creator and node.
   * @param {Array} creatorOutEdgeTypes   Output edges names between creator and node.
   *
   * @returns {Promise<Object, Error>}
   */
  async createNode(type, props, metas = {}, transformNodeCallback = null, creatorId = null, creatorInEdgeTypes = [], creatorOutEdgeTypes = []) {
    var createdNode;

    let uri  = await uriGenPublisher.createUri(type);
    let node = await graphStorePublisher.createNode(type, uri, props, metas, this.context);

    if (creatorId) {
      for (let creatorInEdgeType of creatorInEdgeTypes) {
        await graphStorePublisher.createEdge(node.getId(), creatorInEdgeType, creatorId, {}, this.context);
      }

      for (let creatorOutEdgeType of creatorOutEdgeTypes) {
        await graphStorePublisher.createEdge(creatorId, creatorOutEdgeType, node.getId(), {}, this.context);
      }
    }

    return transformNodeCallback ? transformNodeCallback(node) : node;
  }

  /**
   * Generic method to update a node
   *
   * @param type
   * @param id
   * @param props
   * @param transformNodeCallback
   *
   * @returns {Promise<Object, Error>}
   * @private
   */
  async updateNode(type, id, props, transformNodeCallback = null) {
    let node = await graphStorePublisher.updateNode(type, id, props, this.context);

    return transformNodeCallback ? transformNodeCallback(node) : node;
  }


  /**
   * Generic method to remove a node
   * @param nodeType
   * @param id
   */
  async removeNode(nodeType, id, removeCascadingLabels = true) {
    if (removeCascadingLabels){
      let localizedLabelsNodes = await this.queryGraphNodes(`
        g.V('${id}')
        .both()
        .hasLabel('${TYPES.LOCALIZED_LABEL}')
        .${graphStorePublisher.getEnabledNodeFilter()}
      `);

      for(let localizedLabelNode of localizedLabelsNodes) {
        winston.debug(`Removing localized label ${localizedLabelNode.id} (${localizedLabelNode.getPropertyValue('value')})`);
        await graphStorePublisher.disableNode(TYPES.LOCALIZED_LABEL, localizedLabelNode.id, this.context);
      }
    }


    return graphStorePublisher.disableNode(nodeType, id, this.context);
  }

  /**
   * Generic method to get a reified node
   *
   * @param reificationId
   * @param reificationOutEdgeLabel
   * @param modelClass
   *
   * @returns {Promise<modelClass, Error>}
   */
  async getReificationModel(reificationId, reificationOutEdgeLabel, modelClass){
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(reificationId, reificationOutEdgeLabel);

    if (nodes.length > 0) {
      if(typeof modelClass === "object") {
        return createModelFromNode(modelClass[nodes[0].nodeType], nodes[0]);
      } else {
        return createModelFromNode(modelClass, nodes[0]);
      }
    }
  }

  /**
   * Generic method to get nodes from graph query
   *
   * @param query
   * @param args
   * @returns {*}
   */
  async queryGraphNodes(query, args = {}){
    if (args.first) {
        if (args.after) {
          query += `.range(${cursorToOffset(args.after)}, ${args.first + 1})`;
        } else {
          query += `.limit(${args.first + 1})`;
        }
    }

    return await graphStorePublisher.queryGraphNodes(query, this.context);
  }

  /**
   * Generic method to update localized labels of a node
   *
   * @param props.lang
   * @param props.sourceNodeId
   * @param props.localizedLabelsProps
   * @param props.returnAsGraphObject
   */
  async updateNodeLocalizedLabels(props) {
    const {lang, sourceNodeId, localizedLabelsProps, returnAsGraphObject} = props;

    let graphNodes = {}, graphEdges = [];

    for(let {value, relationName} of localizedLabelsProps) {
      let localizedLabel = await this.getLocalizedLabelForNode({
        sourceNodeId,
        lang,
        relationName,
        returnAsLocalizedLabel: true
      });

      if (localizedLabel) {
        if(returnAsGraphObject) {
          graphNodes[localizedLabel.id] = Node.serializeNode(TYPES.LOCALIZED_LABEL, localizedLabel.id, localizedLabel.uri, {
            value
          });
        } else {
          await graphStorePublisher.updateNode(TYPES.LOCALIZED_LABEL, localizedLabel.id, {
            value
          }, this.context);
        }
      } else {
        let updateGraph = await this.addLocalizedLabel({
          sourceNodeId,
          lang,
          value,
          relationName,
          returnAsGraphObject
        });

        if (returnAsGraphObject) {
          Object.assign(graphNodes, updateGraph.graphNodes);
          graphEdges = graphEdges.concat(updateGraph.graphEdges);
        }
      }
    }

    if(returnAsGraphObject) {
      return {graphNodes, graphEdges};
    }
  }

  /**
   * Add a localized label
   * @param props.sourceNodeId Source node id
   * @param props.lang Lang
   * @param props.relationName Relation name
   * @param props.returnAsGraphObject
   */
  async addLocalizedLabel(props){
    let {sourceNodeId, lang, value, relationName,
      returnAsGraphObject, returnAsModelClass,
      setRelatedMatcher
    } = props;

    if (!lang) {
      lang = 'fr';
    }

    let graphNodes = {}, graphEdges = [], labeNodeId = uuid.v4();

    let labelUri = await uriGenPublisher.createUri(TYPES.LOCALIZED_LABEL);

    graphNodes[labeNodeId] = Node.serializePartial(TYPES.LOCALIZED_LABEL, labelUri, {
      creationDate: (new Date()).getTime(),
      lang,
      value
    });

    if (Array.isArray(relationName)) {
      relationName = relationName[0];
    }

    graphEdges.push(Edge.serializePartial(sourceNodeId, relationName, labeNodeId));

    if (setRelatedMatcher) {
      let matcherUri = await uriGenPublisher.createUri(TYPES.MATCHER);
      let matcherNodeId = uuid.v4();

      graphNodes[matcherNodeId] = Node.serializePartial(TYPES.MATCHER, matcherUri, {
        creationDate: (new Date()).getTime(),
        expression: transformLabelToMatcher(value),
        _enabled: true
      });

      graphEdges.push(Edge.serializePartial(labeNodeId, EDGES.LABEL.OUT.MATCHER.HAS_MATCHER, matcherNodeId));
    }

    if (returnAsGraphObject) {
      return {graphNodes, graphEdges};
    } else {
      let {nodes} = await graphStorePublisher.createGraph(graphNodes, graphEdges);

      if (returnAsModelClass) {
        return createModelFromNode(returnAsModelClass, nodes[labeNodeId]);
      }
    }
  }

  /**
   * Get a localized label
   * @param props.sourceNode Source node
   * @param props.lang Lang
   * @param props.relationName Relation name
   * @param props.relationDirection Relation direction
   * @param props.returnAsLocalizedLabel Return
   *
   */
  async getLocalizedLabelForNode(props){
    let {sourceNodeId, lang, relationName, relationDirection, returnAsLocalizedLabel, dontReturnFallbackLabel} = props;

    if (!lang) {
      lang = 'fr';
    }

    if (!relationDirection) {
      relationDirection = 'out';
    }

    if (!Array.isArray(relationName)) {
      relationName = [relationName];
    }

    let nodes = await this.queryGraphNodes(
      `g.V('${sourceNodeId}')
        .${relationDirection}(${relationName.map(name => `'${name}'`).join(',')})
        ${dontReturnFallbackLabel === true ? `.has('lang', '${lang}')` : "" }
      `
    );

    if (nodes.length > 0) {
      let targetNode = nodes.find(node => node.getPropertyValue('lang') === lang);

      if (!targetNode) {
        targetNode = nodes[0];
      }

      return returnAsLocalizedLabel ? createModelFromNode(LocalizedLabel, targetNode) : targetNode.getPropertyValue('value');
    }
  }
}