/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */

import {createModelFromNode} from './model-helpers';
import {createNode} from 'synaptix-nodejs/lib/graphstore-mongorient/models';

import {indexPublisher} from 'synaptix-nodejs';

export default class {
  /**
   * Constructor
   *
   * @param context connection context
   */
  constructor(context){
    this.context = context;
  }

  /**
   * Make a fast search on index type
   *
   * @param types
   * @param modelClasses
   * @param args
   * @param extraFilters
   * @param fields
   * @returns {*}
   * @private
   */
  fastSearch(types, modelClasses, args = {}, extraFilters = [], fields = null) {
    let {qs, size, offset, sortBy, sortDirection, filters} = args;
    let filtersBooleans = '', sortings = {}, extra = {};

    if (!qs) {
      qs = '';
    }

    if (filters) {
      filters.map(filter => filtersBooleans += `AND (${filter}) `);
    }

    if (sortBy) {
      sortings = {[sortBy]: {order : sortDirection ? sortDirection : 'asc'}};
    }

    if (fields) {
      extra.fields = fields;
    }

    let query = {
      "query": {
        "filtered": {
          "query": {
            "query_string": {
              "query": `${qs || ''}* ${filtersBooleans}`
            }
          },
          "filter": [
            {
              "term" : { "_enabled" : true }
            },
            ...extraFilters
          ]
        }
      },
      "sort": sortings,
      ...extra
    };

    return indexPublisher.search(types, query, size, offset)
      .then(result => {
        return result.hits.map(hit => {
          let modelClass = typeof modelClasses == "object" ? modelClasses[hit._type] : modelClasses;

          return createModelFromNode(modelClass, createNode(types, hit._id, hit._source))
        })
      });
  }

  /**
   * Make a fast search on index type
   *
   * @param type
   * @param term
   * @param args
   * @param filters
   * @returns {*}
   * @private
   */
  _facetsOnTypeByTerm(type, term, args, filters = []) {
    const {qs} = args;
    let filtersBooleans = '', sortings = {}, extra = {};

    filters.map(filter => filtersBooleans += `AND ${filter} `);

    let query = {
      "query": {
        "filtered": {
          "query": {
            "query_string": {
              "query": `${qs || ''}* ${filtersBooleans}`
            }
          },
          "filter": [
            {
              "term" : { "_enabled" : true }
            }
          ]
        }
      },
      "aggs": {
        "results" : {
          "terms" : { "field" : term }
        }
      }
    };

    return indexPublisher.search(type, query, 0, 1, true)
      .then(result => {
        return result.aggregations.results.buckets;
      });
  }

  /**
   * Make a fulltext search on index type
   *
   * @param types
   * @param modelClasses
   * @param args
   * @param query
   * @param extraFilters
   */
  async fulltextSearch({types, modelClasses, args, query, extraFilters, fields}) {
    let {first: size, after} = args || {};
    let from = after ? cursorToOffset(after) : 0;

    if (!size) {
      size = 1;
    } else {
      // This is for Relay weird cursor connection https://facebook.github.io/relay/docs/graphql-connections.html
      size += from + 2;
    }

    let {sortBy, sortDirection} = args;
    let sortings = {},
      extra = {
        from: 0,
        size
      };

    if (sortBy) {
      if (sortBy.match(/^_count:/)) {
        let key = sortBy.slice(7);

        sortings = {
          _script : {
            type : "number",
            script: {
              inline: `doc['${key}'].values.size()`
            },
            order: sortDirection ? sortDirection : 'asc'
          }
        }
      } else {
        sortings = {[sortBy]: {order : sortDirection ? sortDirection : 'asc'}};
      }
    }

    if (fields) {
      extra._source = fields;
    }

    let result = await indexPublisher.search(
      types,
      {
        "query": {
          "filtered": {
            "query": query,
            "filter": {
              "and" : [
                {
                  "term" : { "_enabled" : true }
                },
                ...(extraFilters || {})
              ]
            }
          }
        },
        "sort": sortings,
        ...extra
      }
    );

    return result.hits.map(hit => {
      let modelClass = typeof modelClasses === "object" ? modelClasses[hit._type] : modelClasses;

      return createModelFromNode(modelClass, createNode(types, hit._id, hit._source))
    })
  }
}