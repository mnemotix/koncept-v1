/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */


/**
 * Parse a synaptix graphStore node to a person model.
 *
 * @param modelClass
 * @param {Node} node
 *
 * @returns {*}
 */
export function createModelFromNode(modelClass, node) {
  return new modelClass(node.getId(), node.getUri(), node.getProperties());
}