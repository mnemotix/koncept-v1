/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 25/01/2016
 */

import AbstractDriver from '../abstract-driver';

import {graphStoreDsePublisher as graphStorePublisher, uriGenPublisher} from 'synaptix-nodejs';

import {createNode, Node, Edge} from 'synaptix-nodejs/lib/graphstore-dse/models';

import {SynaptixGraphStoreHelpers, SynaptixIndexHelpers} from './helpers';

import {EDGES, TYPES} from './datamodel';

import {
  Concept, Collection, Thesaurus, Scheme, LocalizedLabel, Matcher, Rule
} from '../../models';

import {transformLabelToMatcher}  from '../../helpers/formats';

import naturalCompare from 'string-natural-compare';

import winston from 'winston-color';
import {createModelFromNode} from "./helpers/model-helpers";

export default class extends AbstractDriver  {

  /** var {synaptixGraphStoreHelpers} Synaptix GraphStore helpers */
  synaptixGraphStoreHelpers;

  /** var {synaptixIndexHelpers} Synaptix Index helpers */
  synaptixIndexHelpers;

  constructor(context) {
    super(context);

    this.synaptixGraphStoreHelpers = new SynaptixGraphStoreHelpers(context);
    this.synaptixIndexHelpers = new SynaptixIndexHelpers(context);
  }

  /**
   * Parse a synaptix graphStore node to a concept model.
   *
   * @param {Node} node
   * @returns {Concept}
   */
  createConceptFromNode(node) {
    return new Concept(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a thesaurus model.
   *
   * @param {Node} node
   * @returns {Thesaurus}
   */
  createThesaurusFromNode(node) {
    return new Thesaurus(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a collection model.
   *
   * @param {Node} node
   * @returns {Collection}
   */
  createCollectionFromNode(node) {
    return new Collection(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a scheme model.
   *
   * @param {Node} node
   * @returns {Scheme}
   */
  createSchemeFromNode(node) {
    return new Scheme(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a concept model.
   *
   * @param {Node} node
   * @returns {LocalizedLabel}
   */
  createLabelFromNode(node) {
    return new LocalizedLabel(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }


  /**
   * Parse a synaptix graphStore node to a concept model.
   *
   * @param {Node} node
   * @returns {Matcher}
   */
  createMatcherFromNode(node) {
    return new Matcher(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Parse a synaptix graphStore node to a matcher model.
   * @param node
   * @returns {Rule}
   */
  createRuleFromNode(node) {
    return new Rule(
      node.getId(),
      node.getUri(),
      node.getProperties()
    );
  }

  /**
   * Update a localized Label
   * @param props.lang
   * @param props.sourceNodeId
   * @param props.localizedLabelsProps
   * @param props.returnAsGraphObject
   */
  async updateLocalizedLabelsForNode(props){
    return this.synaptixGraphStoreHelpers.updateNodeLocalizedLabels(props)
  }

  /**
   * Get a localized label
   * @param props.sourceNodeId Source node
   * @param props.lang Lang
   * @param props.relationName Relation name
   * @param props.relationDirection Relation direction
   * @param props.returnAsLocalizedLabel Return
   *
   */
  async getLocalizedLabelForNode(props){
    return this.synaptixGraphStoreHelpers.getLocalizedLabelForNode(props);
  }

  /**
   * Add a localized label
   * @param props.sourceNodeId Source node id
   * @param props.lang Lang
   * @param props.relationName Relation name
   * @param props.returnAsGraphObject
   */
  async addLocalizedLabel(props){
    return this.synaptixGraphStoreHelpers.addLocalizedLabel(props);
  }

  /**
   * Remove a localized label
   * @param labelId
   */
  async removeLocalizedLabel(labelId){
    return this.synaptixGraphStoreHelpers.removeNode(TYPES.LOCALIZED_LABEL, labelId);
  }

  /**
   * Add thesaurus
   *
   * @param props.title
   * @param props.description
   * @param props.lang
   *
   * @returns {Thesaurus} Returns a thesaurus.
   */
  async addThesaurus(props) {
    let {title, description, lang} = props;

    let thesaurusUri = await uriGenPublisher.createUri(TYPES.THESAURUS);
    let extraNodes = {}, extraEdges = [];

    if (title) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value : title,
        relationName: EDGES.THESAURUS.OUT.LOCALIZED_LABEL.TITLE,
        returnAsGraphObject: true
      });

      Object.assign(extraNodes, labelNodes);
      extraEdges = extraEdges.concat(labelEdges);
    }

    if (description) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value : description,
        relationName: EDGES.THESAURUS.OUT.LOCALIZED_LABEL.DESCRIPTION,
        returnAsGraphObject: true
      });

      Object.assign(extraNodes, labelNodes);
      extraEdges = extraEdges.concat(labelEdges);
    }


    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.THESAURUS, thesaurusUri, {
        creationDate: (new Date()).getTime()
      }),
      ...extraNodes
    }, extraEdges);

    return this.createThesaurusFromNode(graph.nodes["_:0"]);
  }

  /**
   * Remove a thesaurus
   *
   * @param id
   * @returns {Promise.<boolean,Error>} Returns true if thesaurus is deleted, false otherwise or an error.
   */
  async removeThesaurus(id) {
    let collections = await this.getCollectionsForThesaurus(id);
    let schemes = await this.getSchemesForThesaurus(id);
    let concepts = await this.getConceptsForThesaurus(id);

    await Promise.all(collections.map(collection => this.removeCollectionFromThesaurus(id, collection.id)));
    await Promise.all(schemes.map(scheme => this.removeSchemeFromThesaurus(id, scheme.id)));
    await Promise.all(concepts.map(concept => this.removeConcept(concept.id)));

    return this.synaptixGraphStoreHelpers.removeNode(TYPES.THESAURUS, id);
  }

  /**
   * Update a thesaurus fields
   *
   * @param thesoId
   * @param props
   * @param props.title
   * @param props.description
   *
   * @returns {Thesaurus} Returns a thesaurus object or an error.
   */
  async updateThesaurus(thesoId, props) {
    let {title, description, lang} = props;

    let thesaurus = await this.getThesaurus(thesoId);

    let localizedLabelsProps = [];

    if (title) {
      localizedLabelsProps.push({
        value: title,
        relationName: EDGES.THESAURUS.OUT.LOCALIZED_LABEL.TITLE
      });
    }

    if (description) {
      localizedLabelsProps.push({
        value: description,
        relationName: EDGES.THESAURUS.OUT.LOCALIZED_LABEL.DESCRIPTION
      });
    }

    let {graphNodes, graphEdges} = await this.updateLocalizedLabelsForNode({
      localizedLabelsProps,
      lang,
      sourceNodeId: thesoId,
      returnAsGraphObject: true
    });

    let graph = await graphStorePublisher.createGraph({
      [thesoId]: Node.serializeNode(TYPES.THESAURUS, thesoId, thesaurus.uri, {
      }),
      ...graphNodes
    }, graphEdges);

    return this.createThesaurusFromNode(graph.nodes[thesoId]);
  }

  /**
   * Get list of thesaurus
   */
  async getThesauri() {
    let nodes = await graphStorePublisher.getNodesWithLabel(TYPES.THESAURUS);

    return nodes.map(node => this.createThesaurusFromNode(node));
  }

  /**
   * Get thesaurus
   * @param id
   */
  async getThesaurus(id) {
    let node = await graphStorePublisher.getNode(id);

    return this.createThesaurusFromNode(node);
  }

  /**
   * Get thesaurus title for lang.
   * @param thesaurus
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getThesaurusTitleForLang(thesaurus, lang, returnAsLocalizedLabel){
    return this.getLocalizedLabelForNode({
      sourceNodeId : thesaurus.id,
      relationName : EDGES.THESAURUS.OUT.LOCALIZED_LABEL.TITLE,
      lang, returnAsLocalizedLabel
    });
  }

  /**
   * Get thesaurus description for lang.
   * @param thesaurus
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getThesaurusDescriptionForLang(thesaurus, lang, returnAsLocalizedLabel){
    return this.getLocalizedLabelForNode({
      sourceNodeId : thesaurus.id,
      relationName : EDGES.THESAURUS.OUT.LOCALIZED_LABEL.DESCRIPTION,
      lang, returnAsLocalizedLabel
    });
  }

  /**
   * Create top scheme to thesaurus
   *
   * @param thesoId
   * @param props
   * @param props.title
   * @param props.description
   * @param props.color
   * @param props.topConceptLabel
   * @returns {Scheme}
   */
  async createTopSchemeToThesaurus(thesoId, props){
    const {title, description, color, isSandbox, lang} = props;

    let schemeUri = await uriGenPublisher.createUri(TYPES.SCHEME);
    let extraNodes = {}, extraEdges = [];

    if (title) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value : title,
        relationName: EDGES.SCHEME.OUT.LOCALIZED_LABEL.TITLE,
        returnAsGraphObject: true
      });

      Object.assign(extraNodes, labelNodes);
      extraEdges = extraEdges.concat(labelEdges);
    }

    if (description) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value : description,
        relationName: EDGES.SCHEME.OUT.LOCALIZED_LABEL.DESCRIPTION,
        returnAsGraphObject: true
      });

      Object.assign(extraNodes, labelNodes);

      extraEdges = extraEdges.concat(labelEdges);
    }

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.SCHEME, schemeUri, {
        color,
        isSandbox,
        creationDate: (new Date()).getTime()
      }),
      ...extraNodes
    },[
      Edge.serializePartial('_:0', EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF, thesoId),
      ...extraEdges
    ]);

    return this.createSchemeFromNode(graph.nodes["_:0"]);
  }

  /**
   * Add a sandbox scheme to thesaurus
   * @return {Thesaurus}
   */
  async createSandboxSchemeToThesaurus(thesaurusId, props){
    let sandBoxScheme = await this.getThesaurusSandboxTopScheme(thesaurusId);

    if (!sandBoxScheme){
      return this.createTopSchemeToThesaurus(thesaurusId, {
        title: "Sandbox",
        description: "This is a branch where submitted concepts are gathered and waited for validation",
        lang: "en",
        color: "#DDDDDD",
        isSandbox: true,
        ...props
      })
    } else {
      return sandBoxScheme;
    }

  }

  /**
   * Get thesaurus sandbox scheme.
   *
   * @param thesaurusId
   * @return {Scheme}
   */
  async getThesaurusSandboxTopScheme(thesaurusId){
    let nodes = await graphStorePublisher.queryGraphNodes(
      `g.V('${thesaurusId}')
        .in('${EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF}')
        .has('isSandbox', true)  
        .${graphStorePublisher.getEnabledNodeFilter()}
      `
    );

    if(nodes.length > 0){
      return this.createSchemeFromNode(nodes[0]);
    }
  }

  /**
   * Add concept as skos:narrower by creating a new concept
   *
   *
   * @param thesoId
   * @param parentId
   * @param prefLabels
   *
   * @returns {Promise.<Concept,Error>} A promise that returns a concept object or an Error.
   */
  async createConceptAndSetNarrowerRelation(thesoId, parentId, prefLabels, conceptExtraProps) {
    let conceptUri = await uriGenPublisher.createUri(TYPES.CONCEPT);

    let scheme = await this.getConceptScheme(parentId);

    let extraNodes = {},
      extraEdges = [];

    for (let {value, lang} of prefLabels) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value,
        relationName: EDGES.CONCEPT.OUT.LABEL.PREF_LABEL,
        returnAsGraphObject: true,
        setRelatedMatcher: true
      });

      Object.assign(extraNodes, labelNodes);
      extraEdges = extraEdges.concat(labelEdges);
    }

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.CONCEPT, conceptUri, {
        creationDate: (new Date()).getTime(),
        ...conceptExtraProps
      }),
      ...extraNodes
    },[
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.CONCEPT.BROADER, parentId),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, scheme.id),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF, thesoId),
      ...extraEdges
    ]);

    return this.createConceptFromNode(graph.nodes["_:0"]);
  }


  /**
   * Get concept pref labels
   *
   * @param id
   *
   * @returns {Promise.<LocalizedLabel,Error>} Returns a scheme object or an Error.
   */
  async getConceptPrefLabels(id){
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(id, EDGES.CONCEPT.OUT.LABEL.PREF_LABEL);

    return nodes.map(node => this.createLabelFromNode(node));
  }

  /**
   * Get concept alt labels
   *
   * @param id
   *
   * @returns {Promise.<LocalizedLabel,Error>} Returns a scheme object or an Error.
   */
  async getConceptAltLabels(id){
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(id, EDGES.CONCEPT.OUT.LABEL.ALT_LABEL);

    return nodes.map(node => this.createLabelFromNode(node));
  }

  /**
   * Get concept hidden labels
   *
   * @param id
   *
   * @returns {Promise.<LocalizedLabel,Error>} Returns a scheme object or an Error.
   */
  async getConceptHiddenLabels(id){
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(id, EDGES.CONCEPT.OUT.LABEL.HIDDEN_LABEL);

    return nodes.map(node => this.createLabelFromNode(node));
  }

  /**
   * Get a localized label
   * @param labelId
   *
   * @returns {Promise.<LocalizedLabel,Error>} Returns a LocalizedLabel object or an Error.
   */
  async getLocalizedLabel(labelId) {
    let node = await graphStorePublisher.getNode(labelId);

    return this.createLabelFromNode(node);
  }

  /**
   * Add a localized label to a concept
   * @param conceptId
   * @param labelType
   * @param value
   * @param lang
   * @returns {Promise.<LocalizedLabel,Error>} Returns a LocalizedLabel object or an Error.
   */
  async addLabelToConcept(conceptId, labelType, value, lang){
    let labelEdge;

    switch(labelType) {
      case 'altLabels':
        labelEdge = EDGES.CONCEPT.OUT.LABEL.ALT_LABEL;
        break;
      case 'hiddenLabels':
        labelEdge = EDGES.CONCEPT.OUT.LABEL.HIDDEN_LABEL;
        break;
      default:
        labelEdge = EDGES.CONCEPT.OUT.LABEL.PREF_LABEL;
    }

    let labelUri = await uriGenPublisher.createUri(TYPES.LABEL);

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.LABEL, labelUri, {
        creationDate: (new Date()).getTime(),
        value,
        lang
      })
    },[
      Edge.serializePartial(conceptId, labelEdge, '_:0')
    ]);

    let label = this.createLabelFromNode(graph.nodes["_:0"]);

    await this.addMatcherToLabel(label.id, transformLabelToMatcher(value));

    return label;
  }

  /**
   * Update a localized label
   *
   * @param conceptId
   * @param labelType
   * @param labelId
   * @param value
   * @returns {Promise.<LocalizedLabel,Error>} Returns a LocalizedLabel object or an Error.
   */
  async updateLabelForConcept(conceptId, labelType, labelId, value, lang) {
    let node = await graphStorePublisher.updateNode(TYPES.LABEL, labelId, {value, lang});
    let label = this.createLabelFromNode(node);

    let matchers = await this.getLabelMatchers(labelId);

    for(let matcher of matchers) {
      await this.removeMatcherForLabel(labelId, matcher.id);
    }

    await this.addMatcherToLabel(label.id, transformLabelToMatcher(value));

    return label;
  }

  /**
   * Remove a localized label
   *
   * @param labelId
   *
   * @returns {Promise.<Boolean,Error>} Returns a boolean or an Error.
   */
  async removeLabelForConcept(conceptId, labelType, labelId) {
    let matchers = await this.getLabelMatchers(labelId);

    for(let matcher of matchers) {
      await this.removeMatcherForLabel(labelId, matcher.id);
    }

    return this.synaptixGraphStoreHelpers.removeNode(TYPES.LOCALIZED_LABEL, labelId);
  }

  /**
   * Get matcher
   *
   * @param matcherId
   */
  async getMatcher(matcherId) {
    let node = await graphStorePublisher.getNode(matcherId);

    return this.createMatcherFromNode(node);
  }

  /**
   * Get matchers for label
   *
   * @param labelId
   * @param args
   */
  async getLabelMatchers(labelId, args) {
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(labelId, EDGES.LABEL.OUT.MATCHER.HAS_MATCHER);

    return nodes.map(node => this.createMatcherFromNode(node));
  }

  /**
   * Add a matcher to a label
   *
   * @param labelId
   * @param expression
   */
  async addMatcherToLabel(labelId, expression) {
    let matcherUri = await uriGenPublisher.createUri(TYPES.MATCHER);

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.MATCHER, matcherUri, {
        creationDate: (new Date()).getTime(),
        expression,
        _enabled: true
      })
    },[
      Edge.serializePartial(labelId, EDGES.LABEL.OUT.MATCHER.HAS_MATCHER, '_:0')
    ]);

    return this.createMatcherFromNode(graph.nodes["_:0"]);
  }

  /**
   * Add a matcher to a label
   *
   * @param labelId
   * @param matcherId
   * @param expression
   */
  async updateMatcherForLabel(labelId, matcherId, expression) {
    let node = await graphStorePublisher.updateNode(TYPES.MATCHER, matcherId, {expression});

    return this.createMatcherFromNode(node);
  }

  /**
   * Remove a matcher for a label
   *
   * @param labelId
   * @param matcherId
   * @param expression
   */
  async removeMatcherForLabel(labelId, matcherId) {
    return this.synaptixGraphStoreHelpers.removeNode(TYPES.MATCHER, matcherId, false);
  }

  /**
   * Get thesaurus concepts
   * @param thesaurusId
   */
  async getThesaurusTopSchemes(thesaurusId, args = {}) {
    let nodes = await graphStorePublisher.queryGraphNodes(
      `g.V('${thesaurusId}')
          .in('${EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF}')
          .${graphStorePublisher.getEnabledNodeFilter()}
          `
    );

    let schemes = nodes.map(node => this.createSchemeFromNode(node));

    return schemes.sort((a, b) => naturalCompare(a.title, b.title));
  }

  /**
   * Get thesaurus global graph
   *
   * @param thesaurusId
   * @param args
   */
  async getThesaurusGlobalGraph(thesaurusId, args = {}) {

    let nodes = [], {edges, nodes: nodesRaw} = await graphStorePublisher.selectSubGraph(`
g.V('${thesaurusId}')
 .inE('${EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF}').subgraph('subGraph').outV()
 .cap('subGraph')
 .next()
`,
      args
    );

    nodesRaw.map(nodeRaw => {
      if (this[`create${nodeRaw.nodeType}FromNode`]){
        nodes.push(this[`create${nodeRaw.nodeType}FromNode`](nodeRaw));
      }
    });

    return {nodes, edges};
  }

  /**
   * Get all concepts of a thesaurus
   * @param thesoId
   */
  async getConceptsForThesaurus(thesoId) {
    let nodes = await graphStorePublisher.getNodesWithInRelationTo(thesoId, EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF);

    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Add a concept as skos:narrower of an othen concept
   *
   * @param parentId
   * @param narrowerId
   * @returns {Promise.<Edge,Error>} Return an egde or an Error
   */
  async addNarrowerRelation(parentId, narrowerId) {
    return graphStorePublisher.createEdge(narrowerId, EDGES.CONCEPT.OUT.CONCEPT.BROADER, parentId);
  }

  /**
   * Remove concept as skos:narrower
   *
   * @param parentId
   * @param narrowerId
   * @returns {Promise.<boolean,Error>} Return True if deleted or an Error
   */
  async removeNarrowerRelation(parentId, narrowerId) {
    return graphStorePublisher.deleteEdges(narrowerId, EDGES.CONCEPT.OUT.CONCEPT.BROADER, parentId);
  }

  /**
   * Add concept as skos:related
   *
   * @param fromId
   * @param toId
   *
   * @returns {Promise.<Edge,Error>} Return an egde or an Error
   */
  async addRelatedRelation(fromId, toId) {
    await graphStorePublisher.createEdge(fromId, EDGES.CONCEPT.OUT.CONCEPT.RELATED, toId);
    await this.updateConcept(fromId, {});
    await this.updateConcept(toId, {});
  }

  /**
   * Remove concept from skos:related relation
   *
   * @param fromId
   * @param toId
   *
   * @returns {Promise.<boolean,Error>} Return True if deleted or an Error
   */
  async removeRelatedRelation(fromId, toId) {
    return graphStorePublisher.deleteEdges(fromId, EDGES.CONCEPT.OUT.CONCEPT.RELATED, toId);
  }


  /**
   * Get concepts
   */
  async getConcepts(args){
    const {qs, first, offset} = args;

    if (qs === "") {
      return [];
    }

    return this.synaptixIndexHelpers.fulltextSearch({
      types: TYPES.CONCEPT,
      modelClasses: Concept,
      args: {
        qs, first, offset
      },
      query : {
        "function_score": {
          "query": {
            "bool": {
              "should": [
                {
                  "nested": {
                    "path": "prefLabels",
                    "query": {
                      "multi_match": {
                        "query": qs,
                        "type": "phrase_prefix",
                        "fields": ["prefLabels.value"],
                        "boost": 3
                      }
                    }
                  }
                },
                {
                  "nested": {
                    "path": "altLabels",
                    "query": {
                      "multi_match": {
                        "query": qs,
                        "type": "phrase_prefix",
                        "fields": ["altLabels.value"],
                        "boost": 1.5
                      }
                    }
                  }
                },
                {
                  "query_string" : {
                    "query" : `${qs}*`,
                  }
                }
              ]
            },
          },
          "min_score": 0.6
        }

      }
    });
  }

  /**
   * Get concept
   *
   * @param id
   *
   * @returns {Promise.<Concept,Error>} Returns a concept object or an Error.
   */
  async getConcept(id) {
    let node = await graphStorePublisher.getNode(id);

    return this.createConceptFromNode(node);
  }

  /**
   * Get concept scheme
   *
   * @param id
   *
   * @returns {Promise.<Scheme,Error>} Returns a scheme object or an Error.
   */
  async getConceptScheme(id) {
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(id, EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME);

    if (nodes.length > 0) {
      return this.createSchemeFromNode(nodes[0]);
    }
  }

  /**
   * Search for concepts
   *
   * @param thesoId
   * @param qs
   * @param first
   * @param offset
   * @returns {*}
   */
  async searchConcept(thesoId, {query: qs, first, offset}) {
    if (qs === "") {
      return [];
    }

    return this.synaptixIndexHelpers.fulltextSearch({
      types: TYPES.CONCEPT,
      modelClasses: Concept,
      args: {
        qs, first, offset
      },
      query : {
        "function_score": {
          "query": {
            "bool": {
              "must" : [
                {
                  "has_parent": {
                    "type": TYPES.THESAURUS.toLowerCase(),
                    "query": {
                      "ids": {
                        "values": [thesoId]
                      }
                    }
                  }
                }
              ],
              "should": [
                {
                  "nested": {
                    "path": "prefLabels",
                    "query": {
                      "multi_match": {
                        "query": qs,
                        "type": "phrase_prefix",
                        "fields": ["prefLabels.value"],
                        "boost": 3
                      }
                    }
                  }
                },
                {
                  "nested": {
                    "path": "altLabels",
                    "query": {
                      "multi_match": {
                        "query": qs,
                        "type": "phrase_prefix",
                        "fields": ["altLabels.value"],
                        "boost": 1.5
                      }
                    }
                  }
                },
                {
                  "query_string" : {
                    "query" : `${qs}*`,
                  }
                }
              ]
            },
          },
          "min_score": 0.6
        }

      }
    });
  }

  /**
   * Get narrowers concepts
   *
   * @param parentId
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getNarrowers(parentId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${parentId}')
       .coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}')
       )
       .${graphStorePublisher.getEnabledNodeFilter()}
    `);

    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Get narrowers concepts count
   *
   * @param parentId
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getNarrowersCount(parentId){
    let count = await graphStorePublisher.queryGraph(`
      g.V('${parentId}')
       .coalesce(
         __.in('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}'),
         __.out('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}')
       )
       .${graphStorePublisher.getEnabledNodeFilter()}
       .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get related concepts
   *
   * @param relatedId
   *
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getRelated(relatedId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${relatedId}')
        .coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}')
        )
        .${graphStorePublisher.getEnabledNodeFilter()}
        .dedup()`
    );


    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Get related concepts count
   *
   * @param relatedId
   *
   * @returns {Promise.<Number,Error>} Returns a list of concept objects or an Error.
   */
  async getRelatedCount(relatedId) {
    let count = await graphStorePublisher.queryGraph(`
      g.V('${relatedId}')
        .coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}')
        )
        .${graphStorePublisher.getEnabledNodeFilter()}
        .dedup()
        .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get siblings concepts
   *
   * @param conceptId
   * @param relationType
   *
   * @returns {Promise.<Concept[],Error>} Returns a list of concept objects or an Error.
   */
  async getSiblings(conceptId, relationType, args) {
    let relationLabel;

    switch (relationType) {
      case 'narrowers':
        relationLabel = `.coalesce(
            __.out('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
            __.in('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')
          )`;
        break;
      case 'related':
        relationLabel = `.out('${EDGES.CONCEPT.OUT.CONCEPT.RELATED}')`;
        break;
    }

    return await this.synaptixGraphStoreHelpers.queryGraphNodes(
      `g.V('${conceptId}')
          .coalesce(
            __.in('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
            __.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')
          )
          .${graphStorePublisher.getEnabledNodeFilter()}
          ${relationLabel}
          .where(
             __.not(hasId('${conceptId}'))
          )
          .dedup()`,
      args
    );
  }


  /**
   * Get broader of a concept
   *
   * @param conceptId
   *
   * @returns {Promise.<Concept,Error>} Returns a concept object or an Error.
   */
  async getBroader(conceptId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${conceptId}').coalesce(
        __.in('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
        __.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}')
      ).${graphStorePublisher.getEnabledNodeFilter()}
    `);

    if (nodes.length > 0) {
      return this.createConceptFromNode(nodes[0]);
    }
  }

  /**
   * Get broader concept or related scheme
   *
   * @param conceptId
   * @return {Concept|Scheme}
   */
  async getBroaderSkosElement(conceptId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${conceptId}').coalesce(
        __.in('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
        __.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}'),
        __.out('${EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME}')
      ).${graphStorePublisher.getEnabledNodeFilter()}
    `);

    if (nodes.length > 0) {
      let node = nodes[0];
      return node.getNodeType() === 'Concept' ? this.createConceptFromNode(node) : this.createSchemeFromNode(node)
    }
  }

  /**
   * Get collections for concept
   *
   * @param conceptId
   *
   * @returns {Promise.<Collection[],Error>} Returns a list of collection objects or an Error.
   */
  getCollectionsForConcept(conceptId) {
    return graphStorePublisher.getNodesWithOutRelationTo(conceptId, EDGES.CONCEPT.OUT.COLLECTION.MEMBER)
      .then(nodes => nodes.map(node => this.createCollectionFromNode(node)));
  }

  /**
   * Remove a concept
   *
   * @param id
   *
   * @returns {Promise.<boolean,Error>} Return True if deleted or an Error
   */
  async removeConcept(id) {
    let taggings = await graphStorePublisher.getNodesWithInRelationTo(id, EDGES.TAGGING.OUT.CONCEPT.TAGGING_SUBJECT);

    await Promise.all(taggings.map(tagging => {
      this.synaptixGraphStoreHelpers.removeNode(TYPES.TAGGING, tagging.getId());
    }));

    return this.synaptixGraphStoreHelpers.removeNode(TYPES.CONCEPT, id);
  }

  /**
   * Updates a concept
   *
   * @param id
   * @param {object} properties
   *
   * @returns {Promise.<Concept,Error>} Returns a concept object or an Error.
   */
  updateConcept(id, properties) {
    return graphStorePublisher.updateNode(TYPES.CONCEPT, id, properties)
      .then(node => this.createConceptFromNode(node));
  }

  /**
   * Get thesaurus skos:collections
   *
   * @param thesoId Thesaurus Id
   */
  getCollectionsForThesaurus(thesoId) {
    return graphStorePublisher.getNodesWithInRelationTo(thesoId, EDGES.COLLECTION.OUT.THESAURUS.COLLECTION_OF)
      .then(nodes => nodes.map(node => this.createCollectionFromNode(node)));
  }

  /**
   * Get thesaurus skos:collection
   *
   * @param collectionId
   */
  getCollection(collectionId) {
    return graphStorePublisher.getNode(collectionId)
      .then(node => this.createCollectionFromNode(node));
  }

  /**
   * Get collection title for lang.
   * @param collection
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getCollectionTitleForLang(collection, lang, returnAsLocalizedLabel){
    return this.getLocalizedLabelForNode({
      sourceNodeId : collection.id,
      relationName : EDGES.COLLECTION.OUT.LOCALIZED_LABEL.TITLE,
      lang, returnAsLocalizedLabel
    });
  }

  /**
   * Get collection description for lang.
   * @param collection
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getCollectionDescriptionForLang(collection, lang, returnAsLocalizedLabel){
    return this.getLocalizedLabelForNode({
      sourceNodeId : collection.id,
      relationName : EDGES.COLLECTION.OUT.LOCALIZED_LABEL.DESCRIPTION,
      lang, returnAsLocalizedLabel
    });
  }

  /**
   * Add a new skos:collection to a thesaurus
   *
   * @param thesoId
   * @param props
   * @param props.title
   * @param props.description
   * @param props.lang
   *
   */
  async addCollectionToThesaurus(thesoId, props) {
    let {title, description, lang} = props;

    let collectionUri = await uriGenPublisher.createUri(TYPES.COLLECTION);
    let extraNodes = {}, extraEdges = [];

    if (title) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value : title,
        relationName: EDGES.COLLECTION.OUT.LOCALIZED_LABEL.TITLE,
        returnAsGraphObject: true
      });

      Object.assign(extraNodes, labelNodes);
      extraEdges = extraEdges.concat(labelEdges);
    }

    if (description) {
      let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
        sourceNodeId: "_:0",
        lang,
        value : description,
        relationName: EDGES.COLLECTION.OUT.LOCALIZED_LABEL.DESCRIPTION,
        returnAsGraphObject: true
      });

      Object.assign(extraNodes, labelNodes);
      extraEdges = extraEdges.concat(labelEdges);
    }


    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.COLLECTION, collectionUri, {
        creationDate: (new Date()).getTime()
      }),
      ...extraNodes
    }, [
      Edge.serializePartial('_:0', EDGES.COLLECTION.OUT.THESAURUS.COLLECTION_OF, thesoId),
      ...extraEdges
    ]);

    return this.createCollectionFromNode(graph.nodes["_:0"]);
  }

  /**
   * Update thesaurus skos:collection
   *
   * @param collectionId
   * @param props
   * @param props.title
   * @param props.description
   * @param props.lang
   */
  async updateCollection(collectionId, props) {
    let {title, description, lang} = props;

    let collection = await this.getCollection(collectionId);

    let localizedLabelsProps = [];

    if (title) {
      localizedLabelsProps.push({
        value: title,
        relationName: EDGES.COLLECTION.OUT.LOCALIZED_LABEL.TITLE
      });
    }

    if (description) {
      localizedLabelsProps.push({
        value: description,
        relationName: EDGES.COLLECTION.OUT.LOCALIZED_LABEL.DESCRIPTION
      });
    }

    let {graphNodes, graphEdges} = await this.updateLocalizedLabelsForNode({
      localizedLabelsProps,
      lang,
      sourceNodeId: collectionId,
      returnAsGraphObject: true
    });

    let graph = await graphStorePublisher.createGraph({
      [collectionId]: Node.serializeNode(TYPES.COLLECTION, collectionId, collection.uri, {
      }),
      ...graphNodes
    }, graphEdges);

    return this.createCollectionFromNode(graph.nodes[collectionId]);
  }

  /**
   * Remove a skos:collection from a thesaurus
   *
   * @param thesoId
   * @param collectionId
   */
  removeCollectionFromThesaurus(thesoId, collectionId) {
    return this.synaptixGraphStoreHelpers.removeNode(TYPES.COLLECTION, collectionId);
  }

  /**
   * Get thesaurus skos:collection concepts
   *
   * @param collectionId
   */
  getConceptsOfCollection(collectionId) {
    return graphStorePublisher.getNodesWithOutRelationTo(collectionId, EDGES.COLLECTION.OUT.CONCEPT.MEMBER)
      .then(nodes => {
        return nodes.map(node => this.createConceptFromNode(node))
      });
  }

  /**
   * Add a concept to a thesaurus skos:collection
   *
   * @param collectionId
   * @param conceptId
   */
  addConceptToCollection(collectionId, conceptId) {
    return graphStorePublisher.createEdge(collectionId, EDGES.COLLECTION.OUT.CONCEPT.MEMBER, conceptId);
  }

  /**
   * Remove a concept from a thesaurus skos:collection
   *
   * @param collectionId
   * @param conceptId
   */
  removeConceptFromCollection(collectionId, conceptId) {
    return graphStorePublisher.deleteEdges(collectionId, EDGES.COLLECTION.OUT.CONCEPT.MEMBER, conceptId);
  }

  /**
   * Get schemes of a thesaurus
   * @param thesoId
   */
  getSchemesForThesaurus(thesoId) {
    return graphStorePublisher.getNodesWithInRelationTo(thesoId, EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF)
      .then(nodes => nodes.map(node => this.createSchemeFromNode(node)));
  }

  /**
   * Get scheme
   * @param schemeId
   */
  getScheme(schemeId) {
    return graphStorePublisher.getNode(schemeId)
      .then(node => this.createSchemeFromNode(node));
  }

  /**
   * Get scheme title for lang.
   * @param scheme
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getSchemeTitleForLang(scheme, lang, returnAsLocalizedLabel){
    return this.getLocalizedLabelForNode({
      sourceNodeId : scheme.id,
      relationName : EDGES.SCHEME.OUT.LOCALIZED_LABEL.TITLE,
      lang, returnAsLocalizedLabel
    });
  }

  /**
   * Get scheme description for lang.
   * @param scheme
   * @param lang
   * @param returnAsLocalizedLabel
   */
  async getSchemeDescriptionForLang(scheme, lang, returnAsLocalizedLabel){
    return this.getLocalizedLabelForNode({
      sourceNodeId : scheme.id,
      relationName : EDGES.SCHEME.OUT.LOCALIZED_LABEL.DESCRIPTION,
      lang, returnAsLocalizedLabel
    });
  }

  /**
   * Update scheme properties
   *
   * @param schemeId
   * @param props
   * @param props.title
   * @param props.description
   * @param props.color
   * @param props.lang
   */
  async updateScheme(schemeId, props) {
    let {title, description, color, lang} = props;

    let scheme = await this.getScheme(schemeId);

    let localizedLabelsProps = [];
    let straightProps = {};

    if (title) {
      localizedLabelsProps.push({
        value: title,
        relationName: EDGES.SCHEME.OUT.LOCALIZED_LABEL.TITLE
      });
    }

    if (description) {
      localizedLabelsProps.push({
        value: description,
        relationName: EDGES.SCHEME.OUT.LOCALIZED_LABEL.DESCRIPTION
      });
    }

    if(color) {
      straightProps.color = color;
    }

    let {graphNodes, graphEdges} = await this.updateLocalizedLabelsForNode({
      localizedLabelsProps,
      lang,
      sourceNodeId: schemeId,
      returnAsGraphObject: true
    });

    let graph = await graphStorePublisher.createGraph({
      [schemeId]: Node.serializeNode(TYPES.SCHEME, schemeId, scheme.uri, {
        ...straightProps
      }),
      ...graphNodes
    }, graphEdges);

    return this.createSchemeFromNode(graph.nodes[schemeId]);
  }

  /**
   * Add scheme to thesaurus
   *
   * @param thesoId
   * @param topConceptId
   * @param title
   * @param description
   */
  addSchemeToThesaurus(thesoId, topConceptId, title, description) {
    return uriGenPublisher.createUri(TYPES.SCHEME)
      .then(uri => graphStorePublisher.createGraph({
        "_:0": Node.serializePartial(TYPES.SCHEME, uri, {
          title,
          description
        }/*, {
         thesaurus: thesoId
         }*/)
      }, [
        Edge.serializePartial('_:0', EDGES.SCHEME.OUT.THESAURUS.SCHEME_OF, thesoId),
        Edge.serializePartial('_:0', EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, topConceptId)
      ]))
      .then(graph => this.createSchemeFromNode(graph.nodes["_:0"]));
  }

  /**
   * Remove scheme
   *
   * @param schemeId
   */
  async removeSchemeFromThesaurus(thesoId, schemeId) {
    let concepts = await this.getSchemeConcepts(schemeId);

    await Promise.all(concepts.map(concept => this.removeConcept(concept.id)));

    return this.synaptixGraphStoreHelpers.removeNode(TYPES.SCHEME, schemeId);
  }

  /**
   * Get scheme top concepts
   *
   * @param schemeId
   */
  async getSchemeTopConcepts(schemeId) {
    let nodes = await graphStorePublisher.getNodesWithOutRelationTo(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT);
    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Get scheme concepts
   *
   * @param schemeId
   */
  async getSchemeConcepts(schemeId){
    let nodes = await graphStorePublisher.getNodesWithInRelationTo(schemeId, EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME);
    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Add top concept to scheme
   * @param thesoId
   * @param schemeId
   * @param props
   * @param props.lang
   * @param props.topConceptLabel
   */
  async addSchemeTopConcept(thesoId, schemeId, props, conceptExtraProps) {
    const {topConceptLabel, lang} = props;

    let extraNodes = {}, extraEdges = [];
    let conceptUri = await uriGenPublisher.createUri(TYPES.CONCEPT);

    let {graphNodes : labelNodes, graphEdges: labelEdges} = await this.addLocalizedLabel({
      sourceNodeId: "_:0",
      lang,
      value : topConceptLabel,
      relationName: EDGES.CONCEPT.OUT.LABEL.PREF_LABEL,
      returnAsGraphObject: true,
      setRelatedMatcher: true
    });

    Object.assign(extraNodes, labelNodes);
    extraEdges = extraEdges.concat(labelEdges);

    let graph = await graphStorePublisher.createGraph({
      "_:0": Node.serializePartial(TYPES.CONCEPT, conceptUri, {
        creationDate: (new Date()).getTime(),
        ...conceptExtraProps
      }),
      ...extraNodes
    }, [
      Edge.serializePartial(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, '_:0'),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, schemeId),
      Edge.serializePartial('_:0', EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF, thesoId),
      ...extraEdges
    ]);

    return this.createConceptFromNode(graph.nodes["_:0"]);
  }

  /**
   * Get scheme top concepts count
   *
   * @param schemeId
   */
  async getSchemeTopConceptsCount(schemeId) {
    let count = await graphStorePublisher.queryGraph(`
      g.V('${schemeId}')
      .out('${EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT}')
      .${graphStorePublisher.getEnabledNodeFilter()}
      .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get scheme given a top concept id
   * @param conceptId
   */
  getSchemeForTopConcept(conceptId) {
    return graphStorePublisher.getNodesWithInRelationTo(conceptId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT)
      .then(nodes => nodes.length > 0 ? this.createSchemeFromNode(nodes[0]) : null);
  }

  /**
   * Change top concept of a scheme
   *
   * @param schemeId
   * @param conceptId
   */
  updateSchemeTopConcept(schemeId, conceptId) {
    return graphStorePublisher.deleteEdges(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, conceptId)
      .then(() => graphStorePublisher.createEdge(schemeId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, conceptId));
  }

  ///
  // LABELS
  ///

  /**
   * Get a concept for a labelId
   *
   * @param labelId
   * @returns {Promise.<T>|*}
   */
  getConceptForLabelId(labelId) {
    return graphStorePublisher.getNodesBy({
      "selector": {
        "nodeType": TYPES.CONCEPT,
        "$or": [{
          "_source.prefLabels": {
            "$elemMatch": {
              id: labelId
            }
          }
        }, {
          "_source.altLabels": {
            "$elemMatch": {
              id: labelId
            }
          }
        }, {
          "_source.hiddenLabels": {
            "$elemMatch": {
              id: labelId
            }
          }
        }]
      }
    })
      .then(nodes => {
        if (nodes.length > 0) {
          return nodes[0];
        }
      });
  }

  /**
   * Submit a concept
   *
   * @return {Concept}
   */
  async submitConcept({label, thesoId, schemeId, broaderId, relatedId}) {
    let {value, lang} = label;
    let concept;
    let extraConceptProps = {
      isDraft: true
    };

    if (broaderId) {
      concept = await this.createConceptAndSetNarrowerRelation(thesoId, broaderId, [label], extraConceptProps);
    } else {
      if (!schemeId) {
        let sandboxScheme = await this.createSandboxSchemeToThesaurus(thesoId);
        schemeId = sandboxScheme.id;
      }

      concept = await this.addSchemeTopConcept(thesoId, schemeId, {topConceptLabel: label.value, lang: label.lang}, extraConceptProps)
    }

    if (relatedId){
      await this.addRelatedRelation(concept.id, relatedId);
    }

    return concept;
  }

  /**
   * Validate concept
   * @param conceptId
   */
  async validateConcept({conceptId}) {
    return this.updateConcept(conceptId, {isDraft: false});
  }

  /**
   * Get draft concepts
   *
   * @return {Concept[]}
   */
  async getThesaurusDraftConcepts(thesoId, args = {}){
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${thesoId}')
       .in('${EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF}')
       .${graphStorePublisher.getEnabledNodeFilter()}
       .has('isDraft', true)
    `, {args});

    return nodes.map(node => this.createConceptFromNode(node));
  }

  /**
   * Count draft concepts
   * @param thesoId
   * @return {Int}
   */
  async getThesaurusDraftConceptsCount(thesoId){
    let count = await graphStorePublisher.queryGraph(`
      g.V('${thesoId}')
       .in('${EDGES.CONCEPT.OUT.THESAURUS.CONCEPT_OF}')
       .${graphStorePublisher.getEnabledNodeFilter()}
       .has('isDraft', true)
       .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Get concept ancestors
   *
   * @param conceptId
   */
  async getConceptAncestors(conceptId) {
    let nodes = await graphStorePublisher.queryGraphNodes(`
      g.V('${conceptId}')
       .repeat(
         coalesce(
          __.in('${EDGES.CONCEPT.OUT.CONCEPT.NARROWER}'),
          __.out('${EDGES.CONCEPT.OUT.CONCEPT.BROADER}'),
          __.in('${EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT}')
        )
        .${graphStorePublisher.getEnabledNodeFilter()}
       )
       .times(10)
       .emit().as('x').select('x')
    `);

    nodes.reverse();

    return nodes.map(node => node.getNodeType() === 'Concept' ? this.createConceptFromNode(node) : this.createSchemeFromNode(node));
  }

  /**
   * Get concept taggings count
   * @param conceptId
   * @return {int}
   */
  async getConceptTaggingsCount(conceptId){
    let count = await graphStorePublisher.queryGraph(`
      g.V('${conceptId}')
       .in('${EDGES.TAGGING.OUT.CONCEPT.TAGGING_SUBJECT}')
       .${graphStorePublisher.getEnabledNodeFilter()}
       .count()
    `);

    return Array.isArray(count) ? count[0] : count;
  }

  /**
   * Replace concept with another one.
   *
   * @param conceptId
   * @param targetConceptId
   * @return {Concept}
   */
  async replaceConcept({conceptId, targetConceptId}){
    let targetConcept = await this.getConcept(targetConceptId);

    let taggings = await graphStorePublisher.queryGraphNodes(`
      g.V('${conceptId}')
       .in('${EDGES.TAGGING.OUT.CONCEPT.TAGGING_SUBJECT}')
       .${graphStorePublisher.getEnabledNodeFilter()}
    `);

    let extraEdges = [], extraNodes = {};

    for(let tagging of taggings) {
      await graphStorePublisher.deleteEdges(tagging.id, EDGES.TAGGING.OUT.CONCEPT.TAGGING_SUBJECT, conceptId);
      extraEdges.push(Edge.serializePartial(tagging.id, EDGES.TAGGING.OUT.CONCEPT.TAGGING_SUBJECT, targetConceptId));
    }

    let graph = await graphStorePublisher.createGraph({
      [targetConcept.id]: Node.serializeNode(TYPES.CONCEPT, targetConcept.id, targetConcept.uri, {
        lastUpdate: Date.now()
      })
    }, extraEdges);

    await this.removeConcept(conceptId);
  }

  /**
   * Move a concept
   *
   * @param conceptId
   * @param parentId
   * @param targetId
   * @param parentType
   * @param targetType
   * @return {Promise.<void>}
   */
  async moveConcept({ conceptId, parentId, targetId, parentType, targetType }){
    let extraEdges = [];

    let parentScheme = parentType === "Concept" ? await this.getConceptScheme(parentId) : await this.getScheme(parentId);
    let targetScheme = targetType === "Concept" ? await this.getConceptScheme(targetId) : await this.getScheme(targetId);

    if (parentType === "Concept"){
      await graphStorePublisher.deleteEdges(conceptId, EDGES.CONCEPT.OUT.CONCEPT.BROADER, parentId);
      await graphStorePublisher.deleteEdges(parentId, EDGES.CONCEPT.OUT.CONCEPT.NARROWER, conceptId);

      if (parentScheme && parentScheme.id !== targetScheme.id) {
        await graphStorePublisher.deleteEdges(conceptId, EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, parentScheme.id);
      }
    } else {
      await graphStorePublisher.deleteEdges(conceptId, EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, parentId);
      await graphStorePublisher.deleteEdges(parentId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, conceptId);
    }

    if (targetType === "Concept") {
      extraEdges.push(
        Edge.serializePartial(conceptId, EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, targetScheme.id)
      );
    } else {
      extraEdges.push(
        Edge.serializePartial(targetId, EDGES.SCHEME.OUT.CONCEPT.TOP_CONCEPT, conceptId)
      );
    }

    let {nodes} = await graphStorePublisher.createGraph({
      [conceptId]: Node.serializeNode(TYPES.CONCEPT, conceptId, "", {
        lastUpdate: Date.now()
      }),
      [parentId]: Node.serializeNode(parentType === "Concept" ? TYPES.CONCEPT : TYPES.SCHEME, parentId, "", {
        lastUpdate: Date.now()
      }),
      [targetId]: Node.serializeNode(parentType === "Concept" ?  TYPES.CONCEPT : TYPES.SCHEME, targetId, "", {
        lastUpdate: Date.now()
      })
    }, [
      Edge.serializePartial(conceptId, targetType === "Concept" ? EDGES.CONCEPT.OUT.CONCEPT.BROADER : EDGES.CONCEPT.OUT.SCHEME.IN_SCHEME, targetId)
    ].concat(extraEdges));

    return {
      concept: this.createConceptFromNode(nodes[conceptId]),
      parent: parentType === "Concept" ? this.createConceptFromNode(nodes[parentId]) : this.createSchemeFromNode(nodes[parentId]),
      target: targetType === "Concept" ? this.createConceptFromNode(nodes[targetId]) : this.createSchemeFromNode(nodes[targetId]),
    }
  }
}
