/**
 * This file is part of the weever package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/07/2016
 */

export default {
  THESAURUS: {
    OUT: {
      LOCALIZED_LABEL: {
        TITLE: 'TITLE',
        DESCRIPTION: 'DESCRIPTION'
      }
    }
  },
  CONCEPT: {
    OUT: {
      SCHEME: {
        IN_SCHEME: 'IN_SCHEME'
      },
      THESAURUS: {
        CONCEPT_OF: 'CONCEPT_OF'
      },
      CONCEPT: {
        NARROWER: 'NARROWER',
        BROADER:  'BROADER',
        RELATED:  'RELATED'
      },
      COLLECTION: {
        MEMBER: 'MEMBER_OF'
      },
      LABEL: {
        PREF_LABEL: 'PREF_LABEL',
        ALT_LABEL: 'ALT_LABEL',
        HIDDEN_LABEL: 'HIDDEN_LABEL'
      },
      RULE: {
        HAS_RULE: 'HAS_RULE'
      }
    }
  },
  LABEL: {
    OUT: {
      MATCHER: {
        HAS_MATCHER: 'HAS_MATCHER'
      }
    }
  },
  SCHEME: {
    OUT: {
      THESAURUS: {
        SCHEME_OF : 'SCHEME_OF'
      },
      CONCEPT : {
        TOP_CONCEPT: 'HAS_TOP_CONCEPT'
      },
      LOCALIZED_LABEL: {
        TITLE: ['TITLE', 'PREF_LABEL'],
        DESCRIPTION: 'DESCRIPTION'
      }
    }
  },
  COLLECTION: {
    OUT: {
      THESAURUS: {
        COLLECTION_OF : 'COLLECTION_OF'
      },
      LOCALIZED_LABEL: {
        TITLE: ['TITLE', 'PREF_LABEL'],
        DESCRIPTION: 'DESCRIPTION'
      },
      COLLECTION: {
        MEMBER : 'MEMBER'
      },
      CONCEPT: {
        MEMBER : 'MEMBER'
      }
    }
  },
  TAGGING: {
    OUT: {
      PERSON: {
        CREATOR: 'CREATOR'
      },
      OBJECT: {
        TAGGING_OBJECT: 'TAGGING_OBJECT'
      },
      CONCEPT: {
        TAGGING_SUBJECT: 'TAGGING_SUBJECT'
      }
    }
  },
  NOTIFICATION: {
    OUT: {
      PERSON: {
        CREATOR: 'CREATOR',
        NOTIFICATION_TARGET: 'NOTIFICATION_TARGET'
      },
      OBJECT: {
        NOTIFICATION_OBJECT: 'NOTIFICATION_OBJECT'
      }
    }
  }
};
