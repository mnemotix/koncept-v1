import SynaptixConnector from '../synaptix-connector';
import jsondiffpatch from 'jsondiffpatch';

describe('SynaptixConnector', () => {
  var connector = new SynaptixConnector();

  beforeEach(function() {
    connector = new SynaptixConnector();

    spyOn(connector, 'publish').and.callFake((message) => {
      message = JSON.parse(message.toJSON());
      delete message._date;
      return message;
    });
  });
});




