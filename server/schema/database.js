/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/01/2016
 */

import AbstractDriver from './connectors/abstract-driver';
import AbstractConnection from './connectors/abstract-connection';

import SynaptixMongOrientConnector from './connectors/synaptix-mongorient/connection';
import SynaptixDSEConnector from './connectors/synaptix-dse/connection';

import SynaptixDriver from './connectors/synaptix-dse/driver';

/** {AbstractConnection} */
let connection;

switch (process.env.DB_CONNECTOR) {
  case 'mongorient':
    connection = new SynaptixMongOrientConnector();
    break;
  default:
    connection = new SynaptixDSEConnector();
}

class Database{
  /**
   * Get a endpoint connector
   *
   * @param context GraphQL context object
   * @returns {SynaptixDriver}
   */
  getEndpointConnection(context){
    let driver = connection.getDriver(context);

    if (!(driver instanceof AbstractDriver)){
      throw `Object ${driver} must implement class AbstractDriver.`;
    }

    return driver;
  }

  /**
   * Method called once on application start.
   */
  init() {
    connection.init();
  }
}

export default new Database();