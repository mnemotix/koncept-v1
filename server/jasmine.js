/**
 * This file is part of the Koncept package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 08/02/2016
 */

process.env.UUID = "KonceptGraphQLEndPoint-IntegrationTests-f86d12da-12ef-4ac9-a22b-0def5ce5ed6e";

import Jasmine from 'jasmine';
import SpecReporter from 'jasmine-spec-reporter';

var noop = function() {};

var jrunner = new Jasmine();
jrunner.configureDefaultReporter({print: noop});    // remove default reporter logs
jasmine.getEnv().addReporter(new SpecReporter());   // add jasmine-spec-reporter
jrunner.loadConfigFile(process.env.NODE_ENV == 'integration' ? './server/jasmine-integration.json' : './server/jasmine.json'); // load jasmine.json configuration
jrunner.execute();